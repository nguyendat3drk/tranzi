package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="customer")
@Proxy(lazy = false)

public class Customer 	implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id;
	
	@Column(name="full_name")
	String  fullName;
	
	@Column(name="password")
	String password;
	
	@Column(name="email")
	String email;
	
	@Column(name="secret_key")
	String secretKey;
	
	@Column(name="created_at")
	Integer createdAt;
	
	@Column(name="updated_at")
	Integer updatedAt;
	
	@Column(name="is_email_verified")
	Integer isEmailVerified;
	
	@Column(name="status")
	Integer status;
	

	public Customer(long id, String fullName, String password, String email, String secretKey, Integer createdAt,
			Integer updatedAt, Integer isEmailVerified, Integer status) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.password = password;
		this.email = email;
		this.secretKey = secretKey;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.isEmailVerified = isEmailVerified;
		this.status = status;
	}


	public Customer() {
		super();
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSecretKey() {
		return secretKey;
	}


	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}


	public Integer getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Integer createdAt) {
		this.createdAt = createdAt;
	}


	public Integer getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Integer updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Integer getIsEmailVerified() {
		return isEmailVerified;
	}


	public void setIsEmailVerified(Integer isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
