package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="customer_order_product")
@Proxy(lazy = false)
public class CustomerOrderProduct implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id;
	
	@Column(name="product_id")
	Integer  productId;
	
	@Column(name="customer_order_id")
	Integer  customerOrderId;
	
	@Column(name="quantity")
	Integer  quantity;
	
	@Column(name="unit_price")
	Integer  unitPrice;
	
	@Column(name="sub_total")
	Integer  subTotal;
	
	@Column(name="discount_total")
	Integer  discountTotal;
	
	@Column(name="discount_id")
	Integer  discountId;
	
	@Column(name="discount_code")
	Integer  discountCode;
	
	@Column(name="total")
	Integer  total;
	
	
	@Column(name="created_at")
	Integer  createdAt;

	@Column(name="updated_at")
	Integer  updatedAt;

	@Transient
	String orginSku;
	
	@Transient
	String sku;
	
	@Transient
	String productName;
	
	
	@Transient
	String productDescription;
	
	@Transient
	String  manufactureName;
	
	@Transient
	long manufactureId;
	
	public CustomerOrderProduct() {
		super();
	}


	public CustomerOrderProduct(long id, Integer productId, Integer customerOrderId, Integer quantity,
			Integer unitPrice, Integer subTotal, Integer discountTotal, Integer discountId, Integer discountCode,
			Integer total, Integer createdAt, Integer updatedAt, String orginSku, String sku, String productName,
			String productDescription, String manufactureName, long manufactureId) {
		super();
		this.id = id;
		this.productId = productId;
		this.customerOrderId = customerOrderId;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.subTotal = subTotal;
		this.discountTotal = discountTotal;
		this.discountId = discountId;
		this.discountCode = discountCode;
		this.total = total;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.orginSku = orginSku;
		this.sku = sku;
		this.productName = productName;
		this.productDescription = productDescription;
		this.manufactureName = manufactureName;
		this.manufactureId = manufactureId;
	}


	public String getManufactureName() {
		return manufactureName;
	}


	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}


	public long getManufactureId() {
		return manufactureId;
	}


	public void setManufactureId(long manufactureId) {
		this.manufactureId = manufactureId;
	}


	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getOrginSku() {
		return orginSku;
	}


	public void setOrginSku(String orginSku) {
		this.orginSku = orginSku;
	}


	public String getSku() {
		return sku;
	}


	public void setSku(String sku) {
		this.sku = sku;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Integer getCustomerOrderId() {
		return customerOrderId;
	}


	public void setCustomerOrderId(Integer customerOrderId) {
		this.customerOrderId = customerOrderId;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(Integer discountTotal) {
		this.discountTotal = discountTotal;
	}

	public Integer getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}

	public Integer getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(Integer discountCode) {
		this.discountCode = discountCode;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Integer createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Integer updatedAt) {
		this.updatedAt = updatedAt;
	}

}
