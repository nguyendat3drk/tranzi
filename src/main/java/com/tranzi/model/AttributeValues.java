package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "attribute_values")
@Proxy(lazy = false)
public class AttributeValues {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@Column(name = "attribute_id")
	long attributeId;
	
	@Column(name = "value")
	String value;

	@Column(name = "category_id")
	Integer categoryId;
//	
//	@Column(name = "product_id")
//	Integer productId; 
	
	@Transient
	boolean blSelected;
	
	@Transient
	String  attributeName;
	
	

	public AttributeValues(long id, long attributeId, String value, Integer categoryId,
			boolean blSelected, String attributeName) {
		super();
		this.id = id;
		this.attributeId = attributeId;
		this.value = value;
		this.categoryId = categoryId;
		this.blSelected = blSelected;
		this.attributeName = attributeName;
	}


	

	public String getAttributeName() {
		return attributeName;
	}


	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public boolean isBlSelected() {
		return blSelected;
	}

	public void setBlSelected(boolean blSelected) {
		this.blSelected = blSelected;
	}

	public AttributeValues() {
		super();
	}


	public long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}


	
}