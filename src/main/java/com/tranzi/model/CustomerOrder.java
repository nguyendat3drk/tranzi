package com.tranzi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="customer_order")
@Proxy(lazy = false)
public class CustomerOrder implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	long id;
	
	@Column(name="order_code")
	String  orderCode;
	
	@Column(name="shipping_address")
	String  shippingAddress;
	
	@Column(name="shipping_name")
	String  shippingName;
	
	@Column(name="shipping_mobile")
	String  shippingMobile;
	
	@Column(name="shipping_district")
	String  shippingDistrict;
	
	@Column(name="shipping_city")
	String  shippingCity;
	
	@Column(name="shipping_email")
	String  shippingEmail;
	
	@Column(name="customer_id")
	long  customerId;
	
	@Column(name="sub_total")
	Integer  subTotal;
	
	@Column(name="discount_total")
	Integer  discountTotal;
	
	@Column(name="discount_id")
	Integer  discountId;
	
	@Column(name="discount_code")
	String  discountCode;
	
	@Column(name="shipping_fee")
	Integer  shippingFee;
	
	@Column(name="total")
	Integer  total;
	
	
	@Column(name="status")
	Integer  status;
	
	@Column(name="payment_method")
	Integer  paymentMethod;
	
	@Column(name="payment_status")
	String  paymentStatus;
	
	@Column(name="shipment_status")
	Integer  shipmentStatus;

	@Column(name="note")
	String  note;
	
	@Column(name="created_at")
	Integer  createdAt;

	@Column(name="updated_at")
	Integer  updatedAt;
	
	@Transient
	List<CustomerOrderProduct> listProductOrder; 
	
	@Transient
	List<CustomerOrderProduct> listProductOrderDelete; 

	public CustomerOrder() {
		super();
	}
	


	public CustomerOrder(long id, String orderCode, String shippingAddress, String shippingName, String shippingMobile,
			String shippingDistrict, String shippingCity, String shippingEmail, long customerId, Integer subTotal,
			Integer discountTotal, Integer discountId, String discountCode, Integer shippingFee, Integer total,
			Integer status, Integer paymentMethod, String paymentStatus, Integer shipmentStatus, String note,
			Integer createdAt, Integer updatedAt, List<CustomerOrderProduct> listProductOrder,
			List<CustomerOrderProduct> listProductOrderDelete) {
		super();
		this.id = id;
		this.orderCode = orderCode;
		this.shippingAddress = shippingAddress;
		this.shippingName = shippingName;
		this.shippingMobile = shippingMobile;
		this.shippingDistrict = shippingDistrict;
		this.shippingCity = shippingCity;
		this.shippingEmail = shippingEmail;
		this.customerId = customerId;
		this.subTotal = subTotal;
		this.discountTotal = discountTotal;
		this.discountId = discountId;
		this.discountCode = discountCode;
		this.shippingFee = shippingFee;
		this.total = total;
		this.status = status;
		this.paymentMethod = paymentMethod;
		this.paymentStatus = paymentStatus;
		this.shipmentStatus = shipmentStatus;
		this.note = note;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.listProductOrder = listProductOrder;
		this.listProductOrderDelete = listProductOrderDelete;
	}



	public List<CustomerOrderProduct> getListProductOrderDelete() {
		return listProductOrderDelete;
	}


	public void setListProductOrderDelete(List<CustomerOrderProduct> listProductOrderDelete) {
		this.listProductOrderDelete = listProductOrderDelete;
	}




	public List<CustomerOrderProduct> getListProductOrder() {
		return listProductOrder;
	}


	public void setListProductOrder(List<CustomerOrderProduct> listProductOrder) {
		this.listProductOrder = listProductOrder;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	

	public String getOrderCode() {
		return orderCode;
	}


	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}


	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getShippingName() {
		return shippingName;
	}

	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}

	public String getShippingMobile() {
		return shippingMobile;
	}

	public void setShippingMobile(String shippingMobile) {
		this.shippingMobile = shippingMobile;
	}

	public String getShippingDistrict() {
		return shippingDistrict;
	}

	public void setShippingDistrict(String shippingDistrict) {
		this.shippingDistrict = shippingDistrict;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingEmail() {
		return shippingEmail;
	}

	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public Integer getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getDiscountTotal() {
		return discountTotal;
	}

	public void setDiscountTotal(Integer discountTotal) {
		this.discountTotal = discountTotal;
	}

	public Integer getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public Integer getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(Integer shippingFee) {
		this.shippingFee = shippingFee;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(Integer paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Integer getShipmentStatus() {
		return shipmentStatus;
	}

	public void setShipmentStatus(Integer shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Integer createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Integer updatedAt) {
		this.updatedAt = updatedAt;
	}

}
