package com.tranzi.model;

import java.util.List;

public class ListAttributeAndValues {
	private Attributes attribute;
	private List<AttributeValues> listAttributeValues;

	public Attributes getAttribute() {
		return attribute;
	}

	public void setAttribute(Attributes attribute) {
		this.attribute = attribute;
	}

	public List<AttributeValues> getListAttributeValues() {
		return listAttributeValues;
	}

	public void setListAttributeValues(List<AttributeValues> listAttributeValues) {
		this.listAttributeValues = listAttributeValues;
	}

	public ListAttributeAndValues(Attributes attribute, List<AttributeValues> listAttributeValues) {
		super();
		this.attribute = attribute;
		this.listAttributeValues = listAttributeValues;
	}

	public ListAttributeAndValues() {
		super();
	}

}
