package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "product_tag")
@Proxy(lazy = false)
public class Tags {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "name")
	private String displayValue;
	
	@Column(name = "slug")
	private String slug;
	
	@Column(name = "product_id")
	private Integer productId;
	
	@Column(name = "article_id")
	private Integer articleId;

	public Tags() {
		super();
	}
	

	public Tags(long id, String displayValue, String slug, Integer productId, Integer articleId) {
		super();
		this.id = id;
		this.displayValue = displayValue;
		this.slug = slug;
		this.productId = productId;
		this.articleId = articleId;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	

	public String getDisplayValue() {
		return displayValue;
	}


	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}


	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}
	
	
}