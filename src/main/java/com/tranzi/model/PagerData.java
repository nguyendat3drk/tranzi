package com.tranzi.model;

public class PagerData {
	private int totalRow;
	private int pager;
	private int pageSize;
	private Object content;
	private Object content1;
	private Object content2;
	private String code;
	private String mesi;
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getMesi() {
		return mesi;
	}
	public void setMesi(String mesi) {
		this.mesi = mesi;
	}
	public int getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(int totalRow) {
		this.totalRow = totalRow;
	}
	public int getPager() {
		return pager;
	}
	public void setPager(int pager) {
		this.pager = pager;
	}
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	public Object getContent1() {
		return content1;
	}
	public void setContent1(Object content1) {
		this.content1 = content1;
	}
	public Object getContent2() {
		return content2;
	}
	public void setContent2(Object content2) {
		this.content2 = content2;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
