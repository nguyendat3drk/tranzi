package com.tranzi.model;

public class ProductsAtributeValues {
	long attributeValueId;
	long productId;

	public long getAttributeValueId() {
		return attributeValueId;
	}

	public void setAttributeValueId(long attributeValueId) {
		this.attributeValueId = attributeValueId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public ProductsAtributeValues(long attributeValueId, long productId) {
		super();
		this.attributeValueId = attributeValueId;
		this.productId = productId;
	}

	public ProductsAtributeValues() {
		super();

	}

}
