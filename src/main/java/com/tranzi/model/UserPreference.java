package com.tranzi.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="user_preference")
@Proxy(lazy = false)
public class UserPreference {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="user_id")
	int userId;
	
	@Column(name="setting_type")
	String settingType;
	
	@Column(name="setting_item")
	String settingItem;
	
	@Column(name="status")
	int status;
	
	@Column(name="widget_position")
	String widgetPosition;
	
	@Transient
	private List<UserPreference> userPreferences;
	
	public UserPreference() {
	}
	
	public UserPreference(int id, int userId, String settingType, String settingItem, int status, String widgetPosition) {
		super();
		this.id = id;
		this.userId = userId;
		this.settingType = settingType;
		this.settingItem = settingItem;
		this.status = status;
		this.widgetPosition = widgetPosition;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getSettingItem() {
		return settingItem;
	}

	public void setSettingItem(String settingItem) {
		this.settingItem = settingItem;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<UserPreference> getUserPreferences() {
		return userPreferences;
	}

	public void setUserPreferences(List<UserPreference> userPreferences) {
		this.userPreferences = userPreferences;
	}

	public String getWidgetPosition() {
		return widgetPosition;
	}

	public void setWidgetPosition(String widgetPosition) {
		this.widgetPosition = widgetPosition;
	}
}
