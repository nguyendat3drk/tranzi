package com.tranzi.model;

public class ContactReport {
	private String dateTime ;
	private String timeCall ;
	private Integer	totalCall ;
	private Integer	abandonCall ;
	private Integer	answeredCall ;
	private String	averageTime ;
	private Integer	avgTime ;
	public ContactReport() {
		super();
	}
	
	public ContactReport(String dateTime, String timeCall, Integer totalCall, Integer abandonCall, Integer answeredCall,
			String averageTime, Integer avgTime) {
		super();
		this.dateTime = dateTime;
		this.timeCall = timeCall;
		this.totalCall = totalCall;
		this.abandonCall = abandonCall;
		this.answeredCall = answeredCall;
		this.averageTime = averageTime;
		this.avgTime = avgTime;
	}

	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getTimeCall() {
		return timeCall;
	}
	public void setTimeCall(String timeCall) {
		this.timeCall = timeCall;
	}
	public Integer getTotalCall() {
		return totalCall;
	}
	public void setTotalCall(Integer totalCall) {
		this.totalCall = totalCall;
	}
	public Integer getAbandonCall() {
		return abandonCall;
	}
	public void setAbandonCall(Integer abandonCall) {
		this.abandonCall = abandonCall;
	}
	public Integer getAnsweredCall() {
		return answeredCall;
	}
	public void setAnsweredCall(Integer answeredCall) {
		this.answeredCall = answeredCall;
	}
	public String getAverageTime() {
		return averageTime;
	}
	public void setAverageTime(String averageTime) {
		this.averageTime = averageTime;
	}
	public Integer getAvgTime() {
		return avgTime;
	}
	public void setAvgTime(Integer avgTime) {
		this.avgTime = avgTime;
	}
	
}
