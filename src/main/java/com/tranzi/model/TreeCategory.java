package com.tranzi.model;

public class TreeCategory {
		private long id;
		
		private String name;
		
		private Integer size;
		
		private String nameEn;
		
		

		public TreeCategory(long id, String name, Integer size, String nameEn) {
			super();
			this.id = id;
			this.name = name;
			this.size = size;
			this.nameEn = nameEn;
		}

		public String getNameEn() {
			return nameEn;
		}

		public void setNameEn(String nameEn) {
			this.nameEn = nameEn;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getSize() {
			return size;
		}

		public void setSize(Integer size) {
			this.size = size;
		}

		public TreeCategory(long id, String name, Integer size) {
			super();
			this.id = id;
			this.name = name;
			this.size = size;
		}
		public TreeCategory() {
			super();
		}
		
}
