package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="menu_permission")
@Proxy(lazy = false)
public class MenuPermission {
	@Id
	@Column(name="role_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int roleId;
	
	@Column(name="menu_id")
	int menuId;
	
	@Column(name="status")
	int status;
	
	@Column(name="insert_pri")
	int insertPri;
	
	@Column(name="delete_pri")
	int deletePri;
	@Column(name="update_pri")
	int updatePri;
	@Column(name="query_pri")
	int queryPri;
	@Transient
	String menuName;
	public MenuPermission() {
		super();
	}

	public MenuPermission(int roleId, int menuId, int status, int insertPri, int updatePri, int deletePri,
			int queryPri) {
		super();
		this.roleId = roleId;
		this.menuId = menuId;
		this.status = status;
		this.insertPri = insertPri;
		this.updatePri = updatePri;
		this.deletePri = deletePri;
		this.queryPri = queryPri;
	}
//	public MenuPermission(MenuPermission dto) {
//		super();
//		this.roleId = dto.getRoleId();
//		this.menuId = dto.getMenuId();
//		this.status = dto.getStatus();
//		this.insertPri = dto.getInsertPri();
//		this.updatePri = dto.getUpdatePri();
//		this.deletePri = dto.getDeletePri();
//		this.queryPri = dto.getQueryPri();
//	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getUpdatePri() {
		return updatePri;
	}

	public void setUpdatePri(int updatePri) {
		this.updatePri = updatePri;
	}

	public int getInsertPri() {
		return insertPri;
	}

	public void setInsertPri(int insertPri) {
		this.insertPri = insertPri;
	}

	public int getDeletePri() {
		return deletePri;
	}

	public void setDeletePri(int deletePri) {
		this.deletePri = deletePri;
	}

	public int getQueryPri() {
		return queryPri;
	}

	public void setQueryPri(int queryPri) {
		this.queryPri = queryPri;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	
}
