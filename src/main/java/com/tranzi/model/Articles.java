package com.tranzi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "articles")
@Proxy(lazy = false)
public class Articles implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "author_id")
	private long authorId;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "created_time")
	private long createdTime;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "slug")
	private String slug;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "seo_title")
	private String seoTitle;
	
	@Column(name = "seo_keywords")
	private String seoKeywords;
	
	@Column(name = "seo_description")
	private String seoDescription;
	
	@Column(name = "seo_index")
	private int seoIndex;
	
	@Column(name = "type")
	private Integer type;
	

	@Transient
	private long termsid;
	
	@Transient
	private long productId;
	
	@Transient
	private String productName;
	
	@Transient
	private List<Integer> listTerms;
	
	@Transient
	private List<ArticleLinkUpload> listArticleLinkUpload;
	
	
	public List<ArticleLinkUpload> getListArticleLinkUpload() {
		return listArticleLinkUpload;
	}

	public void setListArticleLinkUpload(List<ArticleLinkUpload> listArticleLinkUpload) {
		this.listArticleLinkUpload = listArticleLinkUpload;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	
	public Articles(long id, long authorId, String content, long createdTime, String name, String slug,
			String description, String url, String seoTitle, String seoKeywords, String seoDescription, int seoIndex,
			Integer type, long termsid, long productId, String productName, List<Integer> listTerms,
			List<ArticleLinkUpload> listArticleLinkUpload) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.content = content;
		this.createdTime = createdTime;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.url = url;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.type = type;
		this.termsid = termsid;
		this.productId = productId;
		this.productName = productName;
		this.listTerms = listTerms;
		this.listArticleLinkUpload = listArticleLinkUpload;
	}

	public Articles(long id, long authorId, String content, long createdTime, String name, String slug,
			String description, String url, String seoTitle, String seoKeywords, String seoDescription, int seoIndex,
			Integer type, long termsid, long productId, String productName, List<Integer> listTerms) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.content = content;
		this.createdTime = createdTime;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.url = url;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.type = type;
		this.termsid = termsid;
		this.productId = productId;
		this.productName = productName;
		this.listTerms = listTerms;
	}

	public List<Integer> getListTerms() {
		return listTerms;
	}

	public void setListTerms(List<Integer> listTerms) {
		this.listTerms = listTerms;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public int getSeoIndex() {
		return seoIndex;
	}

	public void setSeoIndex(int seoIndex) {
		this.seoIndex = seoIndex;
	}

	public long getTermsid() {
		return termsid;
	}

	public void setTermsid(long termsid) {
		this.termsid = termsid;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Articles(long id, long authorId, String content, long createdTime, String name, String slug,
			String description, String url, String seoTitle, String seoKeywords, String seoDescription, int seoIndex,
			long termsid, long productId, String productName) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.content = content;
		this.createdTime = createdTime;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.url = url;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.termsid = termsid;
		this.productId = productId;
		this.productName = productName;
	}
	
    public Articles(){
    	super();
    }
	
		
}
