package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "media")
@Proxy(lazy = false)
public class Media {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "created_time")
	long createdTime;

	@Column(name = "server_path")
	String serverPath;
	
	@Column(name = "url_path")
	String urlPath;
	
	@Column(name = "mime_type")
	String mimeType;
	
	@Column(name = "type")
	String type;

	public Media() {
		super();
	}

	public Media(int id, long createdTime, String serverPath, String urlPath, String mimeType, String type) {
		super();
		this.id = id;
		this.createdTime = createdTime;
		this.serverPath = serverPath;
		this.urlPath = urlPath;
		this.mimeType = mimeType;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getServerPath() {
		return serverPath;
	}

	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}

	public String getUrlPath() {
		return urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
