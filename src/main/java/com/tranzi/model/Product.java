package com.tranzi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;
//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "products")
@Proxy(lazy = false)
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Product implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "author_id")
	private long authorId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "slug")
	private String slug;

	@Column(name = "short_description")
	private String shortDescription;

	@Column(name = "description")
	private String description;

	@Column(name = "created_time")
	private long createdTime;

	@Column(name = "normal_price")
	private Integer normalPrice;

	@Column(name = "sale_price")
	private Integer salePrice;

	@Column(name = "sale_start")
	private Integer saleStart;

	@Column(name = "sale_end")
	private Integer saleEnd;

	@Column(name = "sku")
	private String sku;

	@Column(name = "orgin_sku")
	private String orginSku;

	@Column(name = "manufacture_id")
	private Integer manufactureId;

	@Column(name = "in_stock")
	private Integer inStock;

	@Column(name = "featured")
	private int featured;

	@Column(name = "orgin_from")
	private String orginFrom;

	@Column(name = "package_type_id")
	private long packageTypeId;

	@Column(name = "min_order")
	private long minOrder;

	@Column(name = "shipping_time")
	private String shippingTime;

	@Column(name = "thumbnail_image")
	private String thumbnailImage;

	@Column(name = "actived")
	private int actived;

	@Column(name = "seo_title")
	private String seoTitle;

	@Column(name = "seo_keywords")
	private String seoKeywords;

	@Column(name = "seo_description")
	private String seoDescription;

	@Column(name = "seo_index")
	private int seoIndex;
	
	@Transient
	private String categoriesName;
	
	@Column(name = "category_id")
	private Integer categoryId;
	
	@Column(name = "product_status")
	private String productStatus;
	
	@Transient
	private List<Media> listMedia;
	
	@Transient
	private List<Media> listMediaThumbnail;
	
	@Transient
	private List<Long> listCategory;
	
	
	
	@Transient
	private String manufactureName;
	
	@Transient
	private List<Tags> listTags;
	
	@Transient
	private List<ListAttributeAndValues> listAttributeAndValues;
	
	@Transient
	private List<ArticleLinkUpload> listArticleLinkUpload;
	


	public List<ArticleLinkUpload> getListArticleLinkUpload() {
		return listArticleLinkUpload;
	}


	public void setListArticleLinkUpload(List<ArticleLinkUpload> listArticleLinkUpload) {
		this.listArticleLinkUpload = listArticleLinkUpload;
	}


	

	public Product(long id, long authorId, String name, String slug, String shortDescription, String description,
			long createdTime, Integer normalPrice, Integer salePrice, Integer saleStart, Integer saleEnd, String sku,
			String orginSku, Integer manufactureId, Integer inStock, int featured, String orginFrom, long packageTypeId,
			long minOrder, String shippingTime, String thumbnailImage, int actived, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex, String categoriesName, Integer categoryId, String productStatus,
			List<Media> listMedia, List<Media> listMediaThumbnail, List<Long> listCategory, String manufactureName,
			List<Tags> listTags, List<ListAttributeAndValues> listAttributeAndValues,
			List<ArticleLinkUpload> listArticleLinkUpload) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.name = name;
		this.slug = slug;
		this.shortDescription = shortDescription;
		this.description = description;
		this.createdTime = createdTime;
		this.normalPrice = normalPrice;
		this.salePrice = salePrice;
		this.saleStart = saleStart;
		this.saleEnd = saleEnd;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureId = manufactureId;
		this.inStock = inStock;
		this.featured = featured;
		this.orginFrom = orginFrom;
		this.packageTypeId = packageTypeId;
		this.minOrder = minOrder;
		this.shippingTime = shippingTime;
		this.thumbnailImage = thumbnailImage;
		this.actived = actived;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.categoriesName = categoriesName;
		this.categoryId = categoryId;
		this.productStatus = productStatus;
		this.listMedia = listMedia;
		this.listMediaThumbnail = listMediaThumbnail;
		this.listCategory = listCategory;
		this.manufactureName = manufactureName;
		this.listTags = listTags;
		this.listAttributeAndValues = listAttributeAndValues;
		this.listArticleLinkUpload = listArticleLinkUpload;
	}


	public Product(long id, long authorId, String name, String slug, String shortDescription, String description,
			long createdTime, Integer normalPrice, Integer salePrice, Integer saleStart, Integer saleEnd, String sku,
			String orginSku, Integer manufactureId, Integer inStock, int featured, String orginFrom, long packageTypeId,
			long minOrder, String shippingTime, String thumbnailImage, int actived, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex, String categoriesName, Integer categoryId, List<Media> listMedia,
			List<Media> listMediaThumbnail, List<Long> listCategory, List<Tags> listTags,
			List<ListAttributeAndValues> listAttributeAndValues) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.name = name;
		this.slug = slug;
		this.shortDescription = shortDescription;
		this.description = description;
		this.createdTime = createdTime;
		this.normalPrice = normalPrice;
		this.salePrice = salePrice;
		this.saleStart = saleStart;
		this.saleEnd = saleEnd;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureId = manufactureId;
		this.inStock = inStock;
		this.featured = featured;
		this.orginFrom = orginFrom;
		this.packageTypeId = packageTypeId;
		this.minOrder = minOrder;
		this.shippingTime = shippingTime;
		this.thumbnailImage = thumbnailImage;
		this.actived = actived;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.categoriesName = categoriesName;
		this.categoryId = categoryId;
		this.listMedia = listMedia;
		this.listMediaThumbnail = listMediaThumbnail;
		this.listCategory = listCategory;
		this.listTags = listTags;
		this.listAttributeAndValues = listAttributeAndValues;
	}


	public Product(long id, long authorId, String name, String slug, String shortDescription, String description,
			long createdTime, Integer normalPrice, Integer salePrice, Integer saleStart, Integer saleEnd, String sku,
			String orginSku, Integer manufactureId, Integer inStock, int featured, String orginFrom, long packageTypeId,
			long minOrder, String shippingTime, String thumbnailImage, int actived, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex, String categoriesName, Integer categoryId, List<Media> listMedia,
			List<Media> listMediaThumbnail, List<Tags> listTags, List<ListAttributeAndValues> listAttributeAndValues) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.name = name;
		this.slug = slug;
		this.shortDescription = shortDescription;
		this.description = description;
		this.createdTime = createdTime;
		this.normalPrice = normalPrice;
		this.salePrice = salePrice;
		this.saleStart = saleStart;
		this.saleEnd = saleEnd;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureId = manufactureId;
		this.inStock = inStock;
		this.featured = featured;
		this.orginFrom = orginFrom;
		this.packageTypeId = packageTypeId;
		this.minOrder = minOrder;
		this.shippingTime = shippingTime;
		this.thumbnailImage = thumbnailImage;
		this.actived = actived;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.categoriesName = categoriesName;
		this.categoryId = categoryId;
		this.listMedia = listMedia;
		this.listMediaThumbnail = listMediaThumbnail;
		this.listTags = listTags;
		this.listAttributeAndValues = listAttributeAndValues;
	}

	
	public Product(long id, long authorId, String name, String slug, String shortDescription, String description,
			long createdTime, Integer normalPrice, Integer salePrice, Integer saleStart, Integer saleEnd, String sku,
			String orginSku, Integer manufactureId, Integer inStock, int featured, String orginFrom, long packageTypeId,
			long minOrder, String shippingTime, String thumbnailImage, int actived, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex, String categoriesName, Integer categoryId, String productStatus,
			List<Media> listMedia, List<Media> listMediaThumbnail, List<Long> listCategory, String manufactureName,
			List<Tags> listTags, List<ListAttributeAndValues> listAttributeAndValues) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.name = name;
		this.slug = slug;
		this.shortDescription = shortDescription;
		this.description = description;
		this.createdTime = createdTime;
		this.normalPrice = normalPrice;
		this.salePrice = salePrice;
		this.saleStart = saleStart;
		this.saleEnd = saleEnd;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureId = manufactureId;
		this.inStock = inStock;
		this.featured = featured;
		this.orginFrom = orginFrom;
		this.packageTypeId = packageTypeId;
		this.minOrder = minOrder;
		this.shippingTime = shippingTime;
		this.thumbnailImage = thumbnailImage;
		this.actived = actived;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.categoriesName = categoriesName;
		this.categoryId = categoryId;
		this.productStatus = productStatus;
		this.listMedia = listMedia;
		this.listMediaThumbnail = listMediaThumbnail;
		this.listCategory = listCategory;
		this.manufactureName = manufactureName;
		this.listTags = listTags;
		this.listAttributeAndValues = listAttributeAndValues;
	}


	public String getProductStatus() {
		return productStatus;
	}


	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}


	public String getManufactureName() {
		return manufactureName;
	}


	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}


	public List<Long> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<Long> listCategory) {
		this.listCategory = listCategory;
	}

	public List<Media> getListMediaThumbnail() {
		return listMediaThumbnail;
	}

	public void setListMediaThumbnail(List<Media> listMediaThumbnail) {
		this.listMediaThumbnail = listMediaThumbnail;
	}

	public List<ListAttributeAndValues> getListAttributeAndValues() {
		return listAttributeAndValues;
	}

	public void setListAttributeAndValues(List<ListAttributeAndValues> listAttributeAndValues) {
		this.listAttributeAndValues = listAttributeAndValues;
	}

	public List<Tags> getListTags() {
		return listTags;
	}

	public void setListTags(List<Tags> listTags) {
		this.listTags = listTags;
	}

	public List<Media> getListMedia() {
		return listMedia;
	}


	public void setListMedia(List<Media> listMedia) {
		this.listMedia = listMedia;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(Integer normalPrice) {
		this.normalPrice = normalPrice;
	}

	public Integer getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}

	public Product() {
		super();
	}

	public String getCategoriesName() {
		return categoriesName;
	}

	public void setCategoriesName(String categoriesName) {
		this.categoriesName = categoriesName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	
	public Integer getSaleStart() {
		return saleStart;
	}

	public void setSaleStart(Integer saleStart) {
		this.saleStart = saleStart;
	}

	public Integer getSaleEnd() {
		return saleEnd;
	}

	public void setSaleEnd(Integer saleEnd) {
		this.saleEnd = saleEnd;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getOrginSku() {
		return orginSku;
	}

	public void setOrginSku(String orginSku) {
		this.orginSku = orginSku;
	}

	public Integer getManufactureId() {
		return manufactureId;
	}

	public void setManufactureId(Integer manufactureId) {
		this.manufactureId = manufactureId;
	}

	public Integer getInStock() {
		return inStock;
	}

	public void setInStock(Integer inStock) {
		this.inStock = inStock;
	}

	public int getFeatured() {
		return featured;
	}

	public void setFeatured(int featured) {
		this.featured = featured;
	}

	public String getOrginFrom() {
		return orginFrom;
	}

	public void setOrginFrom(String orginFrom) {
		this.orginFrom = orginFrom;
	}

	public long getPackageTypeId() {
		return packageTypeId;
	}

	public void setPackageTypeId(long packageTypeId) {
		this.packageTypeId = packageTypeId;
	}

	public long getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(long minOrder) {
		this.minOrder = minOrder;
	}

	public String getShippingTime() {
		return shippingTime;
	}

	public void setShippingTime(String shippingTime) {
		this.shippingTime = shippingTime;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(String thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public int getActived() {
		return actived;
	}

	public void setActived(int actived) {
		this.actived = actived;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public int getSeoIndex() {
		return seoIndex;
	}

	public void setSeoIndex(int seoIndex) {
		this.seoIndex = seoIndex;
	}

}
