package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "setting")
@Proxy(lazy = false)
public class SettingItem {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@Column(name = "key")
	String key;

	@Column(name = "type")
	String type;

	@Column(name = "value")
	String value;

	@Column(name = "url")
	String url;

	@Column(name = "short_description")
	String shortDescription;

	@Column(name = "description")
	String description;

	@Column(name = "created_at")
	long createdAt;

	@Column(name = "updated_at")
	long updatedAt;

	@Column(name = "title")
	String title;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public SettingItem() {
		super();
	}
	public SettingItem(long id, String key, String type, String value, String url, String shortDescription,
			String description, long createdAt, long updatedAt, String title) {
		super();
		this.id = id;
		this.key = key;
		this.type = type;
		this.value = value;
		this.url = url;
		this.shortDescription = shortDescription;
		this.description = description;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.title = title;
	}
	
}
