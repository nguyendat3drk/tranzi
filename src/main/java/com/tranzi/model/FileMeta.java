package com.tranzi.model;

public class FileMeta {
	private String comments ;
	private String	contentSubType ;
	private String	contentType ;
	private Integer	id ;
	private String	name ;
	private String	type ;
	private String size;
	private byte[] bytes;
	
	public FileMeta() {
		super();
		
	}
	
	public FileMeta(String comments, String contentSubType, String contentType, Integer id, String name, String type,
			String size, byte[] bytes) {
		super();
		this.comments = comments;
		this.contentSubType = contentSubType;
		this.contentType = contentType;
		this.id = id;
		this.name = name;
		this.type = type;
		this.size = size;
		this.bytes = bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getContentSubType() {
		return contentSubType;
	}
	public void setContentSubType(String contentSubType) {
		this.contentSubType = contentSubType;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	

}
