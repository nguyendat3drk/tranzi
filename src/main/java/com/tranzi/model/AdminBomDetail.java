package com.tranzi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "admin_bom")
@Proxy(lazy = false)
public class AdminBomDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "admin_bom_id")
	private long adminBomId;

	@Column(name = "product_id")
	private long productId;

	@Column(name = "quantity")
	private int quantity;
	public long getId() {
		return id;
	}
	public AdminBomDetail() {
		super();
	}

	public AdminBomDetail(long id, long adminBomId, long productId, int quantity) {
		super();
		this.id = id;
		this.adminBomId = adminBomId;
		this.productId = productId;
		this.quantity = quantity;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAdminBomId() {
		return adminBomId;
	}

	public void setAdminBomId(long adminBomId) {
		this.adminBomId = adminBomId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	
}
