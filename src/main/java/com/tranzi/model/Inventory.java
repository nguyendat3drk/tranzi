package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "inventory")
@Proxy(lazy = false)
public class Inventory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "product_id")
	private long productId;
	
	@Column(name = "stock")
	private Integer stock;
	
	@Column(name = "current_stock")
	private Integer currentStock;
	
	@Column(name = "created_at")
	private long createdAt;
	
	@Column(name = "updated_at")
	private long updatedAt;

	@Column(name = "inventory_area")
	private String inventoryArea;
	
	@Column (name = "inventory_area_id")
	private int inventoryAreaId; 
	
	@Transient
	private String productName;
	
	@Transient
	private String sku;
	
	
	
	public Inventory(long id, long productId, Integer stock, Integer currentStock, long createdAt, long updatedAt,
			String inventoryArea, int inventoryAreaId, String productName, String sku) {
		super();
		this.id = id;
		this.productId = productId;
		this.stock = stock;
		this.currentStock = currentStock;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.inventoryArea = inventoryArea;
		this.inventoryAreaId = inventoryAreaId;
		this.productName = productName;
		this.sku = sku;
	}

	public int getInventoryAreaId() {
		return inventoryAreaId;
	}

	public void setInventoryAreaId(int inventoryAreaId) {
		this.inventoryAreaId = inventoryAreaId;
	}

	public String getInventoryArea() {
		return inventoryArea;
	}

	public void setInventoryArea(String inventoryArea) {
		this.inventoryArea = inventoryArea;
	}

	public Inventory() {
		super();
	}

	public Inventory(long id, long productId, Integer stock, Integer currentStock, long createdAt, long updatedAt,
			String productName, String sku) {
		super();
		this.id = id;
		this.productId = productId;
		this.stock = stock;
		this.currentStock = currentStock;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.productName = productName;
		this.sku = sku;
	}

	public Inventory(long id, long productId, Integer stock, Integer currentStock, long createdAt, long updatedAt,
			String productName) {
		super();
		this.id = id;
		this.productId = productId;
		this.stock = stock;
		this.currentStock = currentStock;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.productName = productName;
	}

	public Inventory(long id, long productId, Integer stock, Integer currentStock, long createdAt, long updatedAt) {
		super();
		this.id = id;
		this.productId = productId;
		this.stock = stock;
		this.currentStock = currentStock;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getId() {
		return id;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getCurrentStock() {
		return currentStock;
	}

	public void setCurrentStock(Integer currentStock) {
		this.currentStock = currentStock;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Inventory(long id, long productId, Integer stock, Integer currentStock, long createdAt, long updatedAt,
			String inventoryArea, String productName, String sku) {
		super();
		this.id = id;
		this.productId = productId;
		this.stock = stock;
		this.currentStock = currentStock;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.inventoryArea = inventoryArea;
		this.productName = productName;
		this.sku = sku;
	}
	
	
	
}
