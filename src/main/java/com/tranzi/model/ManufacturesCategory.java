package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "manufactures_category")
@Proxy(lazy = false)
public class ManufacturesCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "manufactures_id")
	private Integer manufacturesId;

	@Column(name = "category_id")
	private Integer nacategoryId;

	public ManufacturesCategory(long id, Integer manufacturesId, Integer nacategoryId) {
		super();
		this.id = id;
		this.manufacturesId = manufacturesId;
		this.nacategoryId = nacategoryId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getManufacturesId() {
		return manufacturesId;
	}

	public void setManufacturesId(Integer manufacturesId) {
		this.manufacturesId = manufacturesId;
	}

	public Integer getNacategoryId() {
		return nacategoryId;
	}

	public void setNacategoryId(Integer nacategoryId) {
		this.nacategoryId = nacategoryId;
	}

	public ManufacturesCategory() {
		super();

	}

}
