package com.tranzi.model;

import java.util.List;

public class TreeNode {
	String label;
	Object data;
	Object icon;
	Object expandedIcon;
	Object collapsedIcon;
	List<TreeNode> children;
	Boolean leaf;
	Boolean expanded;
	String type;
	TreeNode parent;
	Boolean partialSelected;
	String styleClass;
	Boolean draggable;
	Boolean droppable;
	Boolean selectable;
	Integer level;

	public TreeNode() {
		super();
		
	}
	
	public TreeNode(String label, Object data, Object icon, Object expandedIcon, Object collapsedIcon,
			List<TreeNode> children, Boolean leaf, Boolean expanded, String type, TreeNode parent,
			Boolean partialSelected, String styleClass, Boolean draggable, Boolean droppable, Boolean selectable,
			Integer level) {
		super();
		this.label = label;
		this.data = data;
		this.icon = icon;
		this.expandedIcon = expandedIcon;
		this.collapsedIcon = collapsedIcon;
		this.children = children;
		this.leaf = leaf;
		this.expanded = expanded;
		this.type = type;
		this.parent = parent;
		this.partialSelected = partialSelected;
		this.styleClass = styleClass;
		this.draggable = draggable;
		this.droppable = droppable;
		this.selectable = selectable;
		this.level = level;
	}

	public TreeNode(String label, Object data, Object icon, Object expandedIcon, Object collapsedIcon,
			List<TreeNode> children, Boolean leaf, Boolean expanded, String type, TreeNode parent,
			Boolean partialSelected, String styleClass, Boolean draggable, Boolean droppable, Boolean selectable) {
		super();
		this.label = label;
		this.data = data;
		this.icon = icon;
		this.expandedIcon = expandedIcon;
		this.collapsedIcon = collapsedIcon;
		this.children = children;
		this.leaf = leaf;
		this.expanded = expanded;
		this.type = type;
		this.parent = parent;
		this.partialSelected = partialSelected;
		this.styleClass = styleClass;
		this.draggable = draggable;
		this.droppable = droppable;
		this.selectable = selectable;
	}
	
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getIcon() {
		return icon;
	}
	public void setIcon(Object icon) {
		this.icon = icon;
	}
	public Object getExpandedIcon() {
		return expandedIcon;
	}
	public void setExpandedIcon(Object expandedIcon) {
		this.expandedIcon = expandedIcon;
	}
	public Object getCollapsedIcon() {
		return collapsedIcon;
	}
	public void setCollapsedIcon(Object collapsedIcon) {
		this.collapsedIcon = collapsedIcon;
	}
	public List<TreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	public Boolean getLeaf() {
		return leaf;
	}
	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}
	public Boolean getExpanded() {
		return expanded;
	}
	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public TreeNode getParent() {
		return parent;
	}
	public void setParent(TreeNode parent) {
		this.parent = parent;
	}
	public Boolean getPartialSelected() {
		return partialSelected;
	}
	public void setPartialSelected(Boolean partialSelected) {
		this.partialSelected = partialSelected;
	}
	public String getStyleClass() {
		return styleClass;
	}
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
	public Boolean getDraggable() {
		return draggable;
	}
	public void setDraggable(Boolean draggable) {
		this.draggable = draggable;
	}
	public Boolean getDroppable() {
		return droppable;
	}
	public void setDroppable(Boolean droppable) {
		this.droppable = droppable;
	}
	public Boolean getSelectable() {
		return selectable;
	}
	public void setSelectable(Boolean selectable) {
		this.selectable = selectable;
	}
	
	
	
	
}
