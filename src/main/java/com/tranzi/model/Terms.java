package com.tranzi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "terms")
@Proxy(lazy = false)
public class Terms implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "slug")
	private String slug;
	
	@Column(name = "description")
	private String description;
	
	
	@Column(name = "banner_image")
	private String bannerImage;
	
	@Column(name = "banner_url")
	private String bannerLink;
	
	
	@Column(name = "seo_title")
	private String seoTitle;
	
	@Column(name = "seo_keywords")
	private String seoKeywords;
	
	@Column(name = "seo_description")
	private String seoDescription;
	
	@Column(name = "seo_index")
	private int seoIndex;
	
	@Column(name = "parent_id")
	private Integer parentId;
	
	
	@Column(name = "count_article")
	private Integer countArticle;
	

	@Transient
	private List<Long> listCategory;
	
	

	public Terms(long id, String name, String slug, String description, String bannerImage, String bannerLink,
			String seoTitle, String seoKeywords, String seoDescription, int seoIndex, Integer parentId,
			Integer countArticle, List<Long> listCategory) {
		super();
		this.id = id;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.parentId = parentId;
		this.countArticle = countArticle;
		this.listCategory = listCategory;
	}

	public Integer getCountArticle() {
		return countArticle;
	}

	public void setCountArticle(Integer countArticle) {
		this.countArticle = countArticle;
	}


	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<Long> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<Long> listCategory) {
		this.listCategory = listCategory;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getBannerLink() {
		return bannerLink;
	}

	public void setBannerLink(String bannerLink) {
		this.bannerLink = bannerLink;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public int getSeoIndex() {
		return seoIndex;
	}

	public void setSeoIndex(int seoIndex) {
		this.seoIndex = seoIndex;
	}
	public Terms(){
		super();
	}

	public Terms(long id, String name, String slug, String description, String bannerImage, String bannerLink,
			String seoTitle, String seoKeywords, String seoDescription, int seoIndex) {
		super();
		this.id = id;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
	}
	
		
}
