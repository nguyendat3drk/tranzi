package com.tranzi.model;

import java.util.List;

public class ProductSearch {
	int page;
	int pageSize;
	String columnName;
	String typeOrder;
	String search;
	boolean blSale;
	int priceStart;
	int priceEnd;
	boolean blInsufficient;
	List<Integer> listCategoriID;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getTypeOrder() {
		return typeOrder;
	}
	public void setTypeOrder(String typeOrder) {
		this.typeOrder = typeOrder;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public boolean isBlSale() {
		return blSale;
	}
	public void setBlSale(boolean blSale) {
		this.blSale = blSale;
	}
	public int getPriceStart() {
		return priceStart;
	}
	public void setPriceStart(int priceStart) {
		this.priceStart = priceStart;
	}
	public int getPriceEnd() {
		return priceEnd;
	}
	public void setPriceEnd(int priceEnd) {
		this.priceEnd = priceEnd;
	}
	public List<Integer> getListCategoriID() {
		return listCategoriID;
	}
	public void setListCategoriID(List<Integer> listCategoriID) {
		this.listCategoriID = listCategoriID;
	}
	public ProductSearch(int page, int pageSize, String columnName, String typeOrder, String search, boolean blSale,
			int priceStart, int priceEnd, List<Integer> listCategoriID) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.columnName = columnName;
		this.typeOrder = typeOrder;
		this.search = search;
		this.blSale = blSale;
		this.priceStart = priceStart;
		this.priceEnd = priceEnd;
		this.listCategoriID = listCategoriID;
	}
	public ProductSearch() {
		super();
	}
	public boolean isBlInsufficient() {
		return blInsufficient;
	}
	public void setBlInsufficient(boolean blInsufficient) {
		this.blInsufficient = blInsufficient;
	}
	public ProductSearch(int page, int pageSize, String columnName, String typeOrder, String search, boolean blSale,
			int priceStart, int priceEnd, boolean blInsufficient, List<Integer> listCategoriID) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.columnName = columnName;
		this.typeOrder = typeOrder;
		this.search = search;
		this.blSale = blSale;
		this.priceStart = priceStart;
		this.priceEnd = priceEnd;
		this.blInsufficient = blInsufficient;
		this.listCategoriID = listCategoriID;
	}
	

}
