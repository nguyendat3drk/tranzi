package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "categories")
@Proxy(lazy = false)
public class Categories implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "name")
	private String name;
	
	
	@Column(name = "name_en")
	private String nameEn;
	
	@Column(name = "slug")
	private String slug;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "created_time")
	private long createdTime;
	
	@Column(name = "icon_url")
	private String iconUrl;
	
	@Column(name = "banner_image")
	private String bannerImage;
	
	@Column(name = "banner_link")
	private String bannerLink;
	
	@Column(name = "orders")
	private Integer order;
	
	@Column(name = "parent_id")
	private Integer parentId;
	
	@Column(name = "seo_title")
	private String seoTitle;
	
	@Column(name = "seo_keywords")
	private String seoKeywords;
	
	@Column(name = "seo_description")
	private String seoDescription;
	
	@Column(name = "seo_index")
	private int seoIndex;
	
	@Column(name = "count_product")
	private int countProduct;
	
	@Column(name = "type")
	private Integer type;

	public Categories(long id, String name, String slug, String description, long createdTime, String iconUrl,
			String bannerImage, String bannerLink, Integer order, Integer parentId, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex, int countProduct) {
		super();
		this.id = id;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.createdTime = createdTime;
		this.iconUrl = iconUrl;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.order = order;
		this.parentId = parentId;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.countProduct = countProduct;
	}

	public Categories(long id, String name, String nameEn, String slug, String description, long createdTime,
			String iconUrl, String bannerImage, String bannerLink, Integer order, Integer parentId, String seoTitle,
			String seoKeywords, String seoDescription, int seoIndex, int countProduct, Integer type) {
		super();
		this.id = id;
		this.name = name;
		this.nameEn = nameEn;
		this.slug = slug;
		this.description = description;
		this.createdTime = createdTime;
		this.iconUrl = iconUrl;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.order = order;
		this.parentId = parentId;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.countProduct = countProduct;
		this.type = type;
	}

	public int getCountProduct() {
		return countProduct;
	}

	public void setCountProduct(int countProduct) {
		this.countProduct = countProduct;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Categories() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getBannerLink() {
		return bannerLink;
	}

	public void setBannerLink(String bannerLink) {
		this.bannerLink = bannerLink;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}



	public int getSeoIndex() {
		return seoIndex;
	}

	public void setSeoIndex(int seoIndex) {
		this.seoIndex = seoIndex;
	}

	public Categories(long id, String name, String slug, String description, long createdTime, String iconUrl,
			String bannerImage, String bannerLink, Integer order, Integer parentId, String seoTitle, String seoKeywords,
			String seoDescription, int seoIndex) {
		super();
		this.id = id;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.createdTime = createdTime;
		this.iconUrl = iconUrl;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.order = order;
		this.parentId = parentId;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
	}

	public Categories(long id, String name, String nameEn, String slug, String description, long createdTime,
			String iconUrl, String bannerImage, String bannerLink, Integer order, Integer parentId, String seoTitle,
			String seoKeywords, String seoDescription, int seoIndex, int countProduct) {
		super();
		this.id = id;
		this.name = name;
		this.nameEn = nameEn;
		this.slug = slug;
		this.description = description;
		this.createdTime = createdTime;
		this.iconUrl = iconUrl;
		this.bannerImage = bannerImage;
		this.bannerLink = bannerLink;
		this.order = order;
		this.parentId = parentId;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
		this.countProduct = countProduct;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	
}
