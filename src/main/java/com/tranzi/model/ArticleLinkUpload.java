package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "article_link_upload")
@Proxy(lazy = false)
public class ArticleLinkUpload {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "article_id")
	private long articleId;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "type")
	private Integer type;
	
	@Transient
	private Integer status;

	public Integer getStatus() {
		return status;
	}

	public ArticleLinkUpload(long id, long articleId, String value, Integer type, Integer status) {
		super();
		this.id = id;
		this.articleId = articleId;
		this.value = value;
		this.type = type;
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public long getArticleId() {
		return articleId;
	}

	public ArticleLinkUpload(long id, long articleId, String value, Integer status) {
		super();
		this.id = id;
		this.articleId = articleId;
		this.value = value;
		this.status = status;
	}

	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}

	public ArticleLinkUpload() {
		super();
	}

}
