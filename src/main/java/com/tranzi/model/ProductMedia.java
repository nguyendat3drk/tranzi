
package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "products_media")
@Proxy(lazy = false)
public class ProductMedia {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@Column(name = "product_id")
	long productId;
	

	@Column(name = "media_id")
	Integer mediaId;


	public ProductMedia(long id, long productId, Integer mediaId) {
		super();
		this.id = id;
		this.productId = productId;
		this.mediaId = mediaId;
	}

	public ProductMedia() {
		super();
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getProductId() {
		return productId;
	}


	public void setProductId(long productId) {
		this.productId = productId;
	}


	public Integer getMediaId() {
		return mediaId;
	}


	public void setMediaId(Integer mediaId) {
		this.mediaId = mediaId;
	}
	
}