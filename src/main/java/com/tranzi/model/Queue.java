package com.tranzi.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="QUEUE")
@Proxy(lazy = false)
public class Queue {
	@Id
	@Column(name="id")
	int id;
	
	@Column(name="name")
	String name;	
	
	@Column(name="description")
	String description;
	
	@Column(name="status")
	int status;
	
	@Column(name="call_routing_strategy")
	String callRoutingStrategy;
	
	@Column(name="line")
	int line;
	
	@Column(name="welcome_audio")
	String welcomeAudio;
	
	@Column(name="queue_audio")
	String queueAudio;
	
	@Column(name="overflow_audio")
	String overflowAudio;
	
	@Column(name="audio_interval")
	int audioInterval;
	
	@Column(name="max_call_of_queue")
	int maxCallOfQueue;
	
	@Column(name="max_waiting_time")
	int maxWaitingTime;
	
	@Column(name="breaking_time")
	int breakingTime;
	
	@Column(name="created_time")
	Date createdTime;
	
	@Column(name="created_by")
	String createdBy;
	
	@Column(name="last_modify")
	Date lastModify;
	
	@Column(name="modified_by")
	String modifiedBy;

	@Column(name="public_number")
	String publicNumber;
	
	@Column(name="delete_state")
	int deleteState;
	
	@Transient
	List<User> listUser;
	
	public Queue() {
		super();
	}

	public String getPublicNumber() {
		return publicNumber;
	}

	public void setPublicNumber(String publicNumber) {
		this.publicNumber = publicNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCallRoutingStrategy() {
		return callRoutingStrategy;
	}

	public void setCallRoutingStrategy(String callRoutingStrategy) {
		this.callRoutingStrategy = callRoutingStrategy;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public String getWelcomeAudio() {
		return welcomeAudio;
	}

	public void setWelcomeAudio(String welcomeAudio) {
		this.welcomeAudio = welcomeAudio;
	}

	public String getQueueAudio() {
		return queueAudio;
	}

	public void setQueueAudio(String queueAudio) {
		this.queueAudio = queueAudio;
	}

	public String getOverflowAudio() {
		return overflowAudio;
	}

	public void setOverflowAudio(String overflowAudio) {
		this.overflowAudio = overflowAudio;
	}

	public int getAudioInterval() {
		return audioInterval;
	}

	public void setAudioInterval(int audioInterval) {
		this.audioInterval = audioInterval;
	}

	public int getMaxCallOfQueue() {
		return maxCallOfQueue;
	}

	public void setMaxCallOfQueue(int maxCallOfQueue) {
		this.maxCallOfQueue = maxCallOfQueue;
	}

	public int getMaxWaitingTime() {
		return maxWaitingTime;
	}

	public void setMaxWaitingTime(int maxWaitingTime) {
		this.maxWaitingTime = maxWaitingTime;
	}

	public int getBreakingTime() {
		return breakingTime;
	}

	public void setBreakingTime(int breakingTime) {
		this.breakingTime = breakingTime;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModify() {
		return lastModify;
	}

	public void setLastModify(Date lastModify) {
		this.lastModify = lastModify;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

	public int getDeleteState() {
		return deleteState;
	}

	public void setDeleteState(int deleteState) {
		this.deleteState = deleteState;
	}

	public Queue(int id, String name, String description, int status, String callRoutingStrategy, int line,
			String welcomeAudio, String queueAudio, String overflowAudio, int audioInterval, int maxCallOfQueue,
			int maxWaitingTime, int breakingTime, Date createdTime, String createdBy, Date lastModify,
			String modifiedBy, String publicNumber, int deleteState, List<User> listUser) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.status = status;
		this.callRoutingStrategy = callRoutingStrategy;
		this.line = line;
		this.welcomeAudio = welcomeAudio;
		this.queueAudio = queueAudio;
		this.overflowAudio = overflowAudio;
		this.audioInterval = audioInterval;
		this.maxCallOfQueue = maxCallOfQueue;
		this.maxWaitingTime = maxWaitingTime;
		this.breakingTime = breakingTime;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.publicNumber = publicNumber;
		this.listUser = listUser;
		this.deleteState = deleteState;
	}
	
}
