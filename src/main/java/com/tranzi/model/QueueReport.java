package com.tranzi.model;

public class QueueReport {
	private String dateTime ;
	private String queueName ;
	private String maxTime	 ;
	private Integer maxTimeNumber	 ;
	private String avgTime	 ;
	private Integer avgTimeNumber;
	private Integer	abandonCall ;
	private String	avgBeforeTime ;
	
	public QueueReport() {
		super();
	}
	public QueueReport(String dateTime, String queueName, String maxTime, Integer maxTimeNumber, String avgTime,
			Integer avgTimeNumber, Integer abandonCall, String avgBeforeTime) {
		super();
		this.dateTime = dateTime;
		this.queueName = queueName;
		this.maxTime = maxTime;
		this.maxTimeNumber = maxTimeNumber;
		this.avgTime = avgTime;
		this.avgTimeNumber = avgTimeNumber;
		this.abandonCall = abandonCall;
		this.avgBeforeTime = avgBeforeTime;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getMaxTime() {
		return maxTime;
	}
	public void setMaxTime(String maxTime) {
		this.maxTime = maxTime;
	}
	public Integer getMaxTimeNumber() {
		return maxTimeNumber;
	}
	public void setMaxTimeNumber(Integer maxTimeNumber) {
		this.maxTimeNumber = maxTimeNumber;
	}
	public String getAvgTime() {
		return avgTime;
	}
	public void setAvgTime(String avgTime) {
		this.avgTime = avgTime;
	}
	public Integer getAvgTimeNumber() {
		return avgTimeNumber;
	}
	public void setAvgTimeNumber(Integer avgTimeNumber) {
		this.avgTimeNumber = avgTimeNumber;
	}
	public Integer getAbandonCall() {
		return abandonCall;
	}
	public void setAbandonCall(Integer abandonCall) {
		this.abandonCall = abandonCall;
	}
	public String getAvgBeforeTime() {
		return avgBeforeTime;
	}
	public void setAvgBeforeTime(String avgBeforeTime) {
		this.avgBeforeTime = avgBeforeTime;
	}
	
	
}
