package com.tranzi.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "attributes")
@Proxy(lazy = false)   
public class Attributes {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@Column(name = "name")
	String name;
	
	@Column(name = "name_en")
	String nameEn;
	
	@Column(name = "description")
	String description;
	
	
	@Column(name = "type")
	Integer type;
	
	@Column(name = "orderby")
	Integer orderby;
	
	@Transient
	List<Integer> selectListCategory;
	
	@Transient
	String listCategoryName;
	
	
	public Attributes(long id, String name, String nameEn, String description, Integer type,
			List<Integer> selectListCategory, String listCategoryName) {
		super();
		this.id = id;
		this.name = name;
		this.nameEn = nameEn;
		this.description = description;
		this.type = type;
		this.selectListCategory = selectListCategory;
		this.listCategoryName = listCategoryName;
	}

	public Attributes(long id, String name, String nameEn, String description, List<Integer> selectListCategory,
			String listCategoryName) {
		super();
		this.id = id;
		this.name = name;
		this.nameEn = nameEn;
		this.description = description;
		this.selectListCategory = selectListCategory;
		this.listCategoryName = listCategoryName;
	}

	public Attributes(long id, String name, String nameEn, String description, Integer type, Integer orderby,
			List<Integer> selectListCategory, String listCategoryName) {
		super();
		this.id = id;
		this.name = name;
		this.nameEn = nameEn;
		this.description = description;
		this.type = type;
		this.orderby = orderby;
		this.selectListCategory = selectListCategory;
		this.listCategoryName = listCategoryName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public Attributes() {
		super();
	}

	public Attributes(long id, String name, String description, List<Integer> selectListCategory,
			String listCategoryName) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.selectListCategory = selectListCategory;
		this.listCategoryName = listCategoryName;
	}

	public String getListCategoryName() {
		return listCategoryName;
	}

	public void setListCategoryName(String listCategoryName) {
		this.listCategoryName = listCategoryName;
	}

	public List<Integer> getSelectListCategory() {
		return selectListCategory;
	}

	public void setSelectListCategory(List<Integer> selectListCategory) {
		this.selectListCategory = selectListCategory;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrderby() {
		return orderby;
	}

	public void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}
	
}