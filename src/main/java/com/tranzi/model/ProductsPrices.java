package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "products_prices")
@Proxy(lazy = false)
public class ProductsPrices {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "product_id")
	Integer productId;

	@Column(name = "min_quantity")
	Integer minQuantity;
	
	@Column(name = "price")
	Integer price;
	
	@Column(name = "sale_price")
	Integer salePrice;

	public int getId() {
		return id;
	}

	public ProductsPrices(int id, Integer productId, Integer minQuantity, Integer price, Integer salePrice) {
		super();
		this.id = id;
		this.productId = productId;
		this.minQuantity = minQuantity;
		this.price = price;
		this.salePrice = salePrice;
	}

	public Integer getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Integer salePrice) {
		this.salePrice = salePrice;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public ProductsPrices(int id, Integer productId, Integer minQuantity, Integer price) {
		super();
		this.id = id;
		this.productId = productId;
		this.minQuantity = minQuantity;
		this.price = price;
	}
	public ProductsPrices() {
		super();
		
	}



}
