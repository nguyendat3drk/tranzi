package com.tranzi.model;

import java.util.List;

public class ProductImportData {
	String datasheets; //1
	String imageUrl;//2
	String productName;//3
	String sku;//4
	String orginSku;//5
	String manufactureName;//6
	String shortDescription;//7
	String inStock;//8
	String normalPrice;//9
	String minOrder;//10
	String seoTitlte;
	String seoKeyword;
	String seoDescription;
	List<ImportAttribute> listAttribute;
	
	public ProductImportData() {
		super();
		
	}
	
	
	public ProductImportData(String datasheets, String imageUrl, String productName, String sku, String orginSku,
			String manufactureName, String shortDescription, String inStock, String normalPrice, String minOrder,
			String seoTitlte, String seoKeyword, String seoDescription, List<ImportAttribute> listAttribute) {
		super();
		this.datasheets = datasheets;
		this.imageUrl = imageUrl;
		this.productName = productName;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureName = manufactureName;
		this.shortDescription = shortDescription;
		this.inStock = inStock;
		this.normalPrice = normalPrice;
		this.minOrder = minOrder;
		this.seoTitlte = seoTitlte;
		this.seoKeyword = seoKeyword;
		this.seoDescription = seoDescription;
		this.listAttribute = listAttribute;
	}


	public ProductImportData(String datasheets, String imageUrl, String productName, String sku, String orginSku,
			String manufactureName, String shortDescription, String inStock, String normalPrice, String minOrder,
			List<ImportAttribute> listAttribute) {
		super();
		this.datasheets = datasheets;
		this.imageUrl = imageUrl;
		this.productName = productName;
		this.sku = sku;
		this.orginSku = orginSku;
		this.manufactureName = manufactureName;
		this.shortDescription = shortDescription;
		this.inStock = inStock;
		this.normalPrice = normalPrice;
		this.minOrder = minOrder;
		this.listAttribute = listAttribute;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDatasheets() {
		return datasheets;
	}
	public void setDatasheets(String datasheets) {
		this.datasheets = datasheets;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getOrginSku() {
		return orginSku;
	}
	public void setOrginSku(String orginSku) {
		this.orginSku = orginSku;
	}
	public String getManufactureName() {
		return manufactureName;
	}
	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getInStock() {
		return inStock;
	}
	public void setInStock(String inStock) {
		this.inStock = inStock;
	}
	public String getNormalPrice() {
		return normalPrice;
	}
	public void setNormalPrice(String normalPrice) {
		this.normalPrice = normalPrice;
	}
	public String getMinOrder() {
		return minOrder;
	}
	public void setMinOrder(String minOrder) {
		this.minOrder = minOrder;
	}
	public List<ImportAttribute> getListAttribute() {
		return listAttribute;
	}
	public void setListAttribute(List<ImportAttribute> listAttribute) {
		this.listAttribute = listAttribute;
	}

	public String getSeoTitlte() {
		return seoTitlte;
	}

	public void setSeoTitlte(String seoTitlte) {
		this.seoTitlte = seoTitlte;
	}

	public String getSeoKeyword() {
		return seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}
	
	
}