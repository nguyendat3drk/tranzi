package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "users")
@Proxy(lazy = false)
public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;

	@Column(name = "ip_address")
	String ipAddress;

	@Column(name = "password")
	String password;

	@Column(name = "username")
	String username;

	@Column(name = "salt")
	String salt;

	@Column(name = "email")
	String email;

	@Column(name = "activation_code")
	String activationCode;

	@Column(name = "forgotten_password_code")
	String forgottenPasswordCode;

	@Column(name = "forgotten_password_time")
	String forgottenPasswordTime;
	
	@Column(name = "remember_code")
	String rememberCode;
	
	@Column(name = "created_on")
	long createdOn;
	
	@Column(name = "last_login")
	long last_login;
	
	@Column(name = "active")
	int active;
	
	@Column(name = "first_name")
	String firstName;
	
	@Column(name = "last_name")
	String lastName;
	
	@Column(name = "company")
	String company;
	
	@Column(name = "phone")
	String phone;
	
	@Column (name = "currentSession")
	String currentSession;
	
	@Column(name = "roles_admin_id")
	int rolesAdminId;
	

	public Account() {
		super();
	}
	
	

	

	public Account(int id, String ipAddress, String password, String username, String salt, String email,
			String activationCode, String forgottenPasswordCode, String forgottenPasswordTime, String rememberCode,
			long createdOn, long last_login, int active, String firstName, String lastName, String company,
			String phone, String currentSession, int rolesAdminId) {
		super();
		this.id = id;
		this.ipAddress = ipAddress;
		this.password = password;
		this.username = username;
		this.salt = salt;
		this.email = email;
		this.activationCode = activationCode;
		this.forgottenPasswordCode = forgottenPasswordCode;
		this.forgottenPasswordTime = forgottenPasswordTime;
		this.rememberCode = rememberCode;
		this.createdOn = createdOn;
		this.last_login = last_login;
		this.active = active;
		this.firstName = firstName;
		this.lastName = lastName;
		this.company = company;
		this.phone = phone;
		this.currentSession = currentSession;
		this.rolesAdminId = rolesAdminId;
	}


	public String getCurrentSession() {
		return currentSession;
	}



	public void setCurrentSession(String currentSession) {
		this.currentSession = currentSession;
	}



	public int getRolesAdminId() {
		return rolesAdminId;
	}



	public void setRolesAdminId(int rolesAdminId) {
		this.rolesAdminId = rolesAdminId;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getForgottenPasswordCode() {
		return forgottenPasswordCode;
	}

	public void setForgottenPasswordCode(String forgottenPasswordCode) {
		this.forgottenPasswordCode = forgottenPasswordCode;
	}

	public String getForgottenPasswordTime() {
		return forgottenPasswordTime;
	}

	public void setForgottenPasswordTime(String forgottenPasswordTime) {
		this.forgottenPasswordTime = forgottenPasswordTime;
	}

	public String getRememberCode() {
		return rememberCode;
	}

	public void setRememberCode(String rememberCode) {
		this.rememberCode = rememberCode;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public long getLast_login() {
		return last_login;
	}

	public void setLast_login(long last_login) {
		this.last_login = last_login;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
