package com.tranzi.model;

public class AgentReport {
	private String dateTime ;
	private String agentId ;
	private String	agentName ;
	private String	maxTalkTime ;
	private String	avgTalkTime ;
	private Integer	totalCalls ;
	private Integer answeredcall;
	private Integer	abandonCalls ;
	private String loginTime;
	private String lastLoginTime;
	private String logoutTime;
	private String workingTime;
	private String  workingTimeNumber;
	
	

	public AgentReport() {
		super();
	}
	
	
	

	public AgentReport(String dateTime, String agentId, String agentName, String maxTalkTime, String avgTalkTime,
			Integer totalCalls, Integer answeredcall, Integer abandonCalls, String loginTime, String lastLoginTime,
			String logoutTime, String workingTime, String workingTimeNumber) {
		super();
		this.dateTime = dateTime;
		this.agentId = agentId;
		this.agentName = agentName;
		this.maxTalkTime = maxTalkTime;
		this.avgTalkTime = avgTalkTime;
		this.totalCalls = totalCalls;
		this.answeredcall = answeredcall;
		this.abandonCalls = abandonCalls;
		this.loginTime = loginTime;
		this.lastLoginTime = lastLoginTime;
		this.logoutTime = logoutTime;
		this.workingTime = workingTime;
		this.workingTimeNumber = workingTimeNumber;
	}




	public String getLastLoginTime() {
		return lastLoginTime;
	}




	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}




	public Integer getAnsweredcall() {
		return answeredcall;
	}


	public void setAnsweredcall(Integer answeredcall) {
		this.answeredcall = answeredcall;
	}


	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getMaxTalkTime() {
		return maxTalkTime;
	}
	public void setMaxTalkTime(String maxTalkTime) {
		this.maxTalkTime = maxTalkTime;
	}
	public String getAvgTalkTime() {
		return avgTalkTime;
	}
	public void setAvgTalkTime(String avgTalkTime) {
		this.avgTalkTime = avgTalkTime;
	}
	public Integer getTotalCalls() {
		return totalCalls;
	}
	public void setTotalCalls(Integer totalCalls) {
		this.totalCalls = totalCalls;
	}
	public Integer getAbandonCalls() {
		return abandonCalls;
	}
	public void setAbandonCalls(Integer abandonCalls) {
		this.abandonCalls = abandonCalls;
	}
	public String getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}
	public String getLogoutTime() {
		return logoutTime;
	}
	public void setLogoutTime(String logoutTime) {
		this.logoutTime = logoutTime;
	}
	public String getWorkingTime() {
		return workingTime;
	}
	public void setWorkingTime(String workingTime) {
		this.workingTime = workingTime;
	}


	public String getWorkingTimeNumber() {
		return workingTimeNumber;
	}


	public void setWorkingTimeNumber(String workingTimeNumber) {
		this.workingTimeNumber = workingTimeNumber;
	}
	

}
