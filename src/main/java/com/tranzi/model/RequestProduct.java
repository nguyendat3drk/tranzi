package com.tranzi.model;
import java.util.List;

public class RequestProduct {
	String nameCategory;
	List<ProductImportData> data;
	public RequestProduct() {
		super();
	}
	public RequestProduct(String nameCategory, List<ProductImportData> data) {
		super();
		this.nameCategory = nameCategory;
		this.data = data;
	}
	public String getNameCategory() {
		return nameCategory;
	}
	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}
	public List<ProductImportData> getData() {
		return data;
	}
	public void setData(List<ProductImportData> data) {
		this.data = data;
	}

}
