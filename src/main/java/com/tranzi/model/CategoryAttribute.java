package com.tranzi.model;

public class CategoryAttribute {
	private Integer categoryId;
	private Integer attributeId;
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Integer attributeId) {
		this.attributeId = attributeId;
	}
	public CategoryAttribute(Integer categoryId, Integer attributeId) {
		super();
		this.categoryId = categoryId;
		this.attributeId = attributeId;
	}
	public CategoryAttribute() {
		super();
	}
	
	

}
