package com.tranzi.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;


@Entity
@Table(name="MENU")
@Proxy(lazy = false)
public class Menu {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="menu_code")
	String menuCode;
	
	@Column(name="menu_name")
	String menuName;
	
	@Column(name="status")
	int status;
	
	@Column(name="description")
	String description;
	
	@Column(name="path")
	String path;
	
	@Column(name="title")
	String title;
	
	@Column(name="icon")
	String icon;
	
	@Column(name="selected")
	int selected;
	
	@Column(name="expanded")
	int expanded;
	
	@Column(name="order")
	int order;
	
	@Column(name="parent_code")
	String parentCode;

	public Menu() {
		super();
	}
	
	public Menu(int id, String menuCode, String menuName, int status, String description, String path, String title,
			String icon, int selected, int expanded, int order, String parentCode) {
		super();
		this.id = id;
		this.menuCode = menuCode;
		this.menuName = menuName;
		this.status = status;
		this.description = description;
		this.path = path;
		this.title = title;
		this.icon = icon;
		this.selected = selected;
		this.expanded = expanded;
		this.order = order;
		this.parentCode = parentCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
	}

	public int getExpanded() {
		return expanded;
	}

	public void setExpanded(int expanded) {
		this.expanded = expanded;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

}