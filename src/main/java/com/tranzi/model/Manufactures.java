package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "manufactures")
@Proxy(lazy = false)
public class Manufactures implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "created_time")
	private long createdTime;

	@Column(name = "name")
	private String name;

	@Column(name = "slug")
	private String slug;

	@Column(name = "description")
	private String description;

	@Column(name = "logo_image")
	private String logoImage;

	@Column(name = "address")
	private String address;

	@Column(name = "website")
	private String website;

	@Column(name = "seo_title")
	private String seoTitle;

	@Column(name = "seo_keywords")
	private String seoKeywords;

	@Column(name = "seo_description")
	private String seoDescription;

	@Column(name = "seo_index")
	private int seoIndex;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public int getSeoIndex() {
		return seoIndex;
	}

	public void setSeoIndex(int seoIndex) {
		this.seoIndex = seoIndex;
	}

	public Manufactures(long id, long createdTime, String name, String slug, String description, String logoImage,
			String address, String website, String seoTitle, String seoKeywords, String seoDescription, int seoIndex) {
		super();
		this.id = id;
		this.createdTime = createdTime;
		this.name = name;
		this.slug = slug;
		this.description = description;
		this.logoImage = logoImage;
		this.address = address;
		this.website = website;
		this.seoTitle = seoTitle;
		this.seoKeywords = seoKeywords;
		this.seoDescription = seoDescription;
		this.seoIndex = seoIndex;
	}

	public Manufactures() {
		super();

	}

}
