package com.tranzi.model;

//@Entity
//@Table(name = "products_categories")
//@Proxy(lazy = false)
public class ProductsCategories {
//	@Id
//	@Column(name = "id")
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	int id;

	
	Integer productId;

	Integer categoryId;

	public ProductsCategories() {
		super();
		
	}
	public ProductsCategories(Integer productId, Integer categoryId) {
		super();
		this.productId = productId;
		this.categoryId = categoryId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
