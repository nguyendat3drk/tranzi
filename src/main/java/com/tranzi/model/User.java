package com.tranzi.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="USER")
@Proxy(lazy = false)
public class User {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="user_name")
	String accountName;
	
	@Column(name="password")
	String password;
	
	@Column(name="extension")
	int extension;
	
	@Column(name="full_name")
	String fullName;
	
	@Column(name="address")
	String address;
	
	@Column(name="email")
	String email;
	
	@Column(name="mobile")
	String mobile;
	
	@Column(name="role")
	int role;
	
	@Column(name="status")
	int status;
	
	@Column(name="description")
	String description;
	
	@Column(name="skill")
	String skill;

	@Column(name="created_time")
	Date createdTime;
	
	@Column(name="created_by")
	String createdBy;
	
	@Column(name="last_modify")
	Date lastModify;
	
	@Column(name="modified_by")
	String modifiedBy;
	
	@Column(name="first_login")
	int firstLogin;
	
	@Column(name="multiple_group")
	int multipleGroup;
	
	@Column(name="state")
	String state;
	
	@Column(name = "last_queue")
	int lastQueue;
	
	@Column(name="delete_state")
	int deleteState;
	
	@Column(name="current_session")
	String currentSession;
	
	@Column(name="used_password")
	String usedPassword;
	
	@Column(name="type_of_disconnect")
	int typeOfDisconnect;
	
	@Column(name="registered")
	int registered;
	
	@Transient
	String roleName;
	@Transient
	String groupName;
	
	@Transient
	String oldPass;
	
	public User(){
		super();
	}

	public User(int id, String accountName, String password, int extension, String fullName, String address,
			String email, String mobile, int role, int status, String description, Date createdTime, String createdBy,
			Date lastModify, String modifiedBy, String state) {
		super();
		this.id = id;
		this.accountName = accountName;
		this.password = password;
		this.extension = extension;
		this.fullName = fullName;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.role = role;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.state = state;
	}

	public User(String accountName, String password, int extension, String fullName, String address, String email,
			String mobile, int role, int status, String description, Date createdTime, String createdBy,
			Date lastModify, String modifiedBy, String state) {
		super();
		this.accountName = accountName;
		this.password = password;
		this.extension = extension;
		this.fullName = fullName;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.role = role;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.state = state;
	}

	public User(String accountName, String password, int extension, String fullName, String address, String email,
			String mobile, int role, int status, String description, String state) {
		super();
		this.accountName = accountName;
		this.password = password;
		this.extension = extension;
		this.fullName = fullName;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.role = role;
		this.status = status;
		this.description = description;
		this.state = state;
	}


	public User(String accountName, String password, int extension, String fullName, String address, String email,
			String mobile, int role, int status, String description, String skill, Date createdTime, String createdBy,
			Date lastModify, String modifiedBy, int firstLogin, int multipleGroup, String roleName,
			String groupName, String oldPass, String state, String usedPassword) {
		super();
		this.accountName = accountName;
		this.password = password;
		this.extension = extension;
		this.fullName = fullName;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.role = role;
		this.status = status;
		this.description = description;
		this.skill = skill;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.firstLogin = firstLogin;
		this.multipleGroup = multipleGroup;
		this.roleName = roleName;
		this.groupName = groupName;
		this.oldPass = oldPass;
		this.state = state;
	}
	

	public User(int id, String accountName, String password, int extension, String fullName, String address,
			String email, String mobile, int role, int status, String description, String skill, Date createdTime,
			String createdBy, Date lastModify, String modifiedBy, int firstLogin, int multipleGroup, String state,
			int deleteState, String roleName, String groupName, String oldPass, int lastQueue, String currentSession, String usedPassword) {
		super();
		this.id = id;
		this.accountName = accountName;
		this.password = password;
		this.extension = extension;
		this.fullName = fullName;
		this.address = address;
		this.email = email;
		this.mobile = mobile;
		this.role = role;
		this.status = status;
		this.description = description;
		this.skill = skill;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.firstLogin = firstLogin;
		this.multipleGroup = multipleGroup;
		this.state = state;
		this.deleteState = deleteState;
		this.roleName = roleName;
		this.groupName = groupName;
		this.oldPass = oldPass;
		this.lastQueue = lastQueue;
		this.currentSession = currentSession;
		this.usedPassword = usedPassword;
	}

	public int getTypeOfDisconnect() {
		return typeOfDisconnect;
	}

	public void setTypeOfDisconnect(int typeOfDisconnect) {
		this.typeOfDisconnect = typeOfDisconnect;
	}

	public int getRegistered() {
		return registered;
	}

	public void setRegistered(int registered) {
		this.registered = registered;
	}

	public String getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(String currentSession) {
		this.currentSession = currentSession;
	}

	public int getLastQueue() {
		return lastQueue;
	}

	public void setLastQueue(int lastQueue) {
		this.lastQueue = lastQueue;
	}

	public String getOldPass() {
		return oldPass;
	}

	public void setOldPass(String oldPass) {
		this.oldPass = oldPass;
	}

	public int getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(int firstLogin) {
		this.firstLogin = firstLogin;
	}

	public int getMultipleGroup() {
		return multipleGroup;
	}

	public void setMultipleGroup(int multipleGroup) {
		this.multipleGroup = multipleGroup;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getExtension() {
		return extension;
	}

	public void setExtension(int extension) {
		this.extension = extension;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModify() {
		return lastModify;
	}

	public void setLastModify(Date lastModify) {
		this.lastModify = lastModify;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getDeleteState() {
		return deleteState;
	}

	public void setDeleteState(int deleteState) {
		this.deleteState = deleteState;
	}

	public String getUsedPassword() {
		return usedPassword;
	}

	public void setUsedPassword(String usedPassword) {
		this.usedPassword = usedPassword;
	}
	
	
}
