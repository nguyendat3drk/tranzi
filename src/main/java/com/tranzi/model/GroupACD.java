package com.tranzi.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name="GROUPACD")
@Proxy(lazy = false)
public class GroupACD{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="group_name")
	String groupName;	
	
	@Column(name="status")
	int status;
	
	@Column(name="description")
	String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_time")
	Date createdTime;
	
	@Column(name="created_by")
	String createdBy;
	
	@Column(name="last_modify")
	Date lastModify;
	
	@Column(name="modified_by")
	String modifiedBy;
	
	@Column(name="delete_state")
	int deleteState;
	
	@Transient
	String userIds;
	
	@Transient
	boolean isChecked;
	/*@Transient
	private Set<User> users = new HashSet<User>(0);*/
	
	public GroupACD() {
		super();
	}
	
	public GroupACD(String groupName, int status) {
		super();
		this.groupName = groupName;
		this.status=status;
	}

	public GroupACD(String groupName, int status, String description, Date createdTime, String createdBy) {
		super();
		this.groupName = groupName;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
	}

	public GroupACD(String groupName, int status, String description, Date createdTime, String createdBy, Date lastModify,
			String modifiedBy) {
		super();
		this.groupName = groupName;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
	}

	public GroupACD(int id, String groupName, int status, String description, Date createdTime, String createdBy,
			Date lastModify, String modifiedBy, int deleteState) {
		super();
		this.id = id;
		this.groupName = groupName;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.deleteState = deleteState;
	}
	
	
	
	/*@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "user_group_granted", catalog = "acd_f9db", joinColumns = {
			@JoinColumn(name = "group_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "user_id",
					nullable = false, updatable = false) })
	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}*/

	public String getUserIds() {
		return userIds;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
//		this.createdTime = new Date(createdTime.getTime());
		this.createdTime = createdTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModify() {
		return lastModify;
	}

	public void setLastModify(Date lastModify) {
		this.lastModify = lastModify;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getDeleteState() {
		return deleteState;
	}

	public void setDeleteState(int deleteState) {
		this.deleteState = deleteState;
	}
}