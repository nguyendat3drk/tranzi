package com.tranzi.model;

import java.util.List;

public class PriceProductImportData {
	
	String sku;//4
	String startDate;//5
	String endDate;//6
	String numberPrice;
	
	public PriceProductImportData() {
		super();
		
	}

	public PriceProductImportData(String sku, String startDate, String endDate, String numberPrice) {
		super();
		this.sku = sku;
		this.startDate = startDate;
		this.endDate = endDate;
		this.numberPrice = numberPrice;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNumberPrice() {
		return numberPrice;
	}

	public void setNumberPrice(String numberPrice) {
		this.numberPrice = numberPrice;
	}
	
	
}