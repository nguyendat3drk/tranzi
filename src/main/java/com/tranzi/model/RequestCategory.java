package com.tranzi.model;

import java.util.List;

public class RequestCategory {
	String categoryName;
	List<String> listchildren;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public List<String> getListchildren() {
		return listchildren;
	}
	public void setListchildren(List<String> listchildren) {
		this.listchildren = listchildren;
	}
	public RequestCategory(String categoryName, List<String> listchildren) {
		super();
		this.categoryName = categoryName;
		this.listchildren = listchildren;
	}
	public RequestCategory() {
		super();
		
	}
}
