package com.tranzi.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name="roles_admin")
@Proxy(lazy = false)
public class Role {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="role_name")
	String name;
	
	@Column(name="status")
	int status;
	
	@Column(name="description")
	String description;
	
	@Column(name="created_date")
	Date createdate;
	
	@Column(name="created_by")
	String createby;
	
	@Column(name="last_modify")
	Date lastModify;
	
	@Column(name="modified_by")
	String modifiedBy;
	
	@Column(name="type")
	int type;
	
	@Column(name="delete_state")
	int deleteState;

	@Transient
	private List<MenuPermission> listPermission;
	
	public Role() {
		super();
	}
	
	public Role(int id, String name, int status, String description,Date createdate, String createby, Date lastModify,String modifiedBy,int type, int deleteState) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.description = description;
		this.createdate = createdate;
		this.createby = createby;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.type = type;
		this.deleteState = deleteState;
	}
	public Role(int id, String name, int status, String description,Date createdate, String createby, Date lastModify,String modifiedBy,int type, int deleteState, List<MenuPermission> listPermission) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.description = description;
		this.createdate = createdate;
		this.createby = createby;
		this.lastModify = lastModify;
		this.modifiedBy = modifiedBy;
		this.type = type;
		this.deleteState = deleteState;
		this.listPermission = listPermission;
	}

	public List<MenuPermission> getListPermission() {
		return listPermission;
	}

	public void setListPermission(List<MenuPermission> listPermission) {
		this.listPermission = listPermission;
		if(listPermission!=null && listPermission.size()>0){
			int idRole=0;
			idRole = getId();
			//delete idRole
			
			//add new
			for(MenuPermission dto:listPermission){
//				MenuPermission dtoItem = new MenuPermission(dto); 
			}
		}
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getCreateby() {
		return createby;
	}
	public void setCreateby(String createby) {
		this.createby = createby;
	}
	
	public Date getLastModify() {
		return lastModify;
	}
	public void setLastModify(Date lastModify) {
		this.lastModify = lastModify;
	}

	public int getDeleteState() {
		return deleteState;
	}

	public void setDeleteState(int deleteState) {
		this.deleteState = deleteState;
	}
	
}
