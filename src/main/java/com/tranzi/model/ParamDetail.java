package com.tranzi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
@Entity
@Table(name="app_param_detail")
@Proxy(lazy = false)
public class ParamDetail {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="par_code")
	int parCode;
	
	@Column(name="app_param_id")
	String appParamId;
	
	@Column(name="type")
	String  type;
	@Column(name="name")
	String  name;
	@Column(name="value")
	String  value;
	@Column(name="order")
	int  order;
	@Column(name="description")
	String  description;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParCode() {
		return parCode;
	}
	public void setParCode(int parCode) {
		this.parCode = parCode;
	}
	public String getAppParamId() {
		return appParamId;
	}
	public void setAppParamId(String appParamId) {
		this.appParamId = appParamId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}