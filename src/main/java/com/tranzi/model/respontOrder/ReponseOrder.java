package com.tranzi.model.respontOrder;

import java.util.List;

public class ReponseOrder {
	List<RespontGenBarCodeOrder> list;
	String pathOrder;
	public List<RespontGenBarCodeOrder> getList() {
		return list;
	}
	public void setList(List<RespontGenBarCodeOrder> list) {
		this.list = list;
	}
	public String getPathOrder() {
		return pathOrder;
	}
	public void setPathOrder(String pathOrder) {
		this.pathOrder = pathOrder;
	}
	public ReponseOrder(List<RespontGenBarCodeOrder> list, String pathOrder) {
		super();
		this.list = list;
		this.pathOrder = pathOrder;
	}
	public ReponseOrder() {
		super();
		
	}
	


}
