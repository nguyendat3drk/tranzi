package com.tranzi.model.respontOrder;

public class RespontGenBarCodeOrder {

	Integer customerProductId;
	String pathBarCode;
	public Integer getCustomerProductId() {
		return customerProductId;
	}
	public void setCustomerProductId(Integer customerProductId) {
		this.customerProductId = customerProductId;
	}
	public String getPathBarCode() {
		return pathBarCode;
	}
	public void setPathBarCode(String pathBarCode) {
		this.pathBarCode = pathBarCode;
	}
	public RespontGenBarCodeOrder() {
		super();
	}	
	
	public RespontGenBarCodeOrder(Integer customerProductId, String pathBarCode) {
		super();
		this.customerProductId = customerProductId;
		this.pathBarCode = pathBarCode;
	}	
	
}
