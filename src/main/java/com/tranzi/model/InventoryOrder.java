package com.tranzi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
@Entity
@Table(name = "inventoryorder")
@Proxy(lazy = false)
public class InventoryOrder implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
	private long id;
	
	@Column(name = "sku")
	private String sku;
	
	@Column(name = "product_id")
	private long productId;
	
	@Column(name = "inventory_id")
	private long inventoryId;
	
	@Column(name = "reason_id")
	private Integer reasonId;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "unit_price")
	private Integer unitPrice;
	
	@Column(name = "total_price")
	private long totalPrice;

	//1 = nhập, 2= xuất	
	@Column(name = "type")
	private Integer type;
	
	@Column(name = "serial_numbers")
	private String serialNumbers;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "created_at")
	private long createdTime;
	
	@Column(name = "updated_at")
	private long updatedAt;

	public InventoryOrder(long id, String sku, long productId, long inventoryId, Integer reasonId, Integer quantity,
			Integer unitPrice, Integer totalPrice, String serialNumbers, String comment, long createdTime,
			long updatedAt) {
		super();
		this.id = id;
		this.sku = sku;
		this.productId = productId;
		this.inventoryId = inventoryId;
		this.reasonId = reasonId;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.totalPrice = totalPrice;
		this.serialNumbers = serialNumbers;
		this.comment = comment;
		this.createdTime = createdTime;
		this.updatedAt = updatedAt;
	}

	public InventoryOrder(long id, String sku, long productId, long inventoryId, Integer reasonId, Integer quantity,
			Integer unitPrice, Integer totalPrice, Integer type, String serialNumbers, String comment, long createdTime,
			long updatedAt) {
		super();
		this.id = id;
		this.sku = sku;
		this.productId = productId;
		this.inventoryId = inventoryId;
		this.reasonId = reasonId;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.totalPrice = totalPrice;
		this.type = type;
		this.serialNumbers = serialNumbers;
		this.comment = comment;
		this.createdTime = createdTime;
		this.updatedAt = updatedAt;
	}

	public InventoryOrder() {
		super();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(long inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Integer getReasonId() {
		return reasonId;
	}

	public void setReasonId(Integer reasonId) {
		this.reasonId = reasonId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSerialNumbers() {
		return serialNumbers;
	}

	public void setSerialNumbers(String serialNumbers) {
		this.serialNumbers = serialNumbers;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	
}
