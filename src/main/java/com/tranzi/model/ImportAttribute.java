package com.tranzi.model;

public class ImportAttribute {
	String parentName;
	String attributeName;
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public ImportAttribute(String parentName, String attributeName) {
		super();
		this.parentName = parentName;
		this.attributeName = attributeName;
	}
	public ImportAttribute() {
		super();
	}
	

}
