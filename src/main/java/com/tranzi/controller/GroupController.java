package com.tranzi.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.GroupACD;
import com.tranzi.model.User;
import com.tranzi.service.GroupService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/group")
public class GroupController {
    @Autowired 
    GroupService groupService;
    
    @RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<GroupACD> getAllsGroup(@RequestParam(value = "page", required = true) int page, 
    		@RequestParam(value = "pageSize", required = true) int pageSize,
    		@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestHeader(value = "authorization", required = true) String authorization) {
        List<GroupACD> listOfGroups = groupService.getAlls(page, pageSize, columnName, typeOrder);
        return listOfGroups;
    }
    
	@RequestMapping(value = "/getRowCount", method = RequestMethod.GET, headers = "Accept=application/json")
	@CrossOrigin(origins = "*")
	public int getRowCount(@RequestHeader(value = "authorization", required = true) String authorization, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String mAttr = (String) session.getAttribute("attributeName");
		System.out.println("attributeName is: " + mAttr);
		System.out.println("authorization: " + authorization);
		int rowCount = groupService.getRowCount();
		return rowCount;
	}
    
    @RequestMapping(value = "/getUsersByGroupIdPaging", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public ResponseObject getUsersByGroupIdPaging(@RequestParam(value = "page", required = false) int page,
			@RequestParam(value = "pageSize", required = false) int pageSize,
			@RequestParam(value = "id", required = false) int id) {
    	return groupService.getUsersByGroupIdPaging(page, pageSize, id);
    }
    
    @RequestMapping(value = "/getRowCountUserByGroup", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseObject getRowCountUserByGroup(@RequestParam(value = "id", required = false) int groupId) {
    	System.out.println(groupService.getRowCountUserByGroup(groupId).getObject().toString());
		return groupService.getRowCountUserByGroup(groupId);
	}
    
    @RequestMapping(value = "/getUsersNotInGroup/{groupId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<User> getUsersNotInGroup(@PathVariable int groupId) {
        List<User> listUsersOfGroup = groupService.getUsersNotInGroup(groupId);
        return listUsersOfGroup;
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.GET, headers = "Accept=application/json")
    public List<GroupACD> searchGroups(@RequestParam(value="name", required=false) String name) {
        List<GroupACD> listOfCountries = groupService.searchGroup(name);
        return listOfCountries;
    }

    @RequestMapping(value = "/getGroup/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public GroupACD getGroupById(@PathVariable int id) {
        return groupService.getGroup(id);
    }
    
    @RequestMapping(value = "/getUserIdsOfGroup/{groupId}", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public ResponseObject getUserIdsByGroupId(@PathVariable int groupId){
    	return groupService.getUserIdsByGroupId(groupId);
    }

    @RequestMapping(value = "/addGroup", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseObject addGroup(@RequestBody GroupACD group) {
//    	System.out.println("users of group: " + group.getUserIds());
        return groupService.addGroup(group);
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.PUT, headers = "Accept=application/json")
    @ResponseBody
    public ResponseObject updateGroup(@RequestBody GroupACD group) {
        return groupService.updateGroup(group);
    }
    
    @RequestMapping(value = "/changeGroupStatus", method = RequestMethod.PUT, headers = "Accept=application/json")
    public GroupACD changeGroupStatus(@RequestBody GroupACD group) {
        groupService.changeGroupStatus(group);
        return group;
    }

    @RequestMapping(value = "/deleteGroup/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseObject deleteGroup(@PathVariable("id") int id) {
        return groupService.deleteGroup(id);
    }
    
    @RequestMapping(value = "/isGroupExists", method = RequestMethod.POST, headers = "Accept=application/json")
    public boolean isGroupExists(@RequestBody GroupACD group) {
//    	System.out.println("users of group: " + group.getUserIds());
    	return groupService.isGroupExists(group);
    }
    
    @RequestMapping(value = "/deleteListGroup", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ResponseObject deleteListGroup(@RequestParam(value = "groupIds", required = false) String groupIds) {
    	System.out.println("groupIds: " + groupIds);
    	return groupService.deleteListGroup(groupIds);
    }
}
