package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.dao.PageMenu;
import com.tranzi.entity.MenuPerItem;
import com.tranzi.model.Menu;
import com.tranzi.service.MenuService;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/menu")
public class MenuController {
	@Autowired
	MenuService menuService;

	@RequestMapping(value = "/getDashboard", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Menu> getAllMenuDashboard(){
		return menuService.getAllMenuDashboard();
	}


	@RequestMapping(value = "/getAll", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Menu> getAllMenu() {
		return menuService.getAllMenu();
	}
	
	
	@RequestMapping(value = "/getMenuCheck/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Menu> getMenuCheck(@PathVariable int id) {
		return menuService.getMenuCheck(id);
	}
	
	@RequestMapping(value = "/getMenuByRole/{roleId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<PageMenu> getPageMenu(@PathVariable int roleId){
		return menuService.getPageMenuByRole(roleId);
	}
	
	@RequestMapping(value = "/getMenuPermissionByRole/{roleId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<MenuPerItem> getMenuPermissionByRole(@PathVariable int roleId){
		return menuService.getMenuItemsByRole(roleId);
	}
	@RequestMapping(value = "/getMenuParent", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Menu> getMenuParent() {
		return menuService.getMenuParent();
	}
}
