package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PackageTypes;
import com.tranzi.model.PagerData;
import com.tranzi.model.PackageTypes;
import com.tranzi.service.PackageTypeService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/packageType")
@SuppressWarnings("unchecked")  
public class PackageTypesController {
	
	@Autowired
	PackageTypeService packageTypeService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject  re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(packageTypeService.getAlls());
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			re.setErrorMessage("có lỗi xảy ra");
		}
		return re;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody PackageTypes dto) {
		ResponseObject res = new ResponseObject();
		try {
			return packageTypeService.create(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}
		
		return res;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updatePackageTypes(@RequestBody PackageTypes dto) {
		ResponseObject res = new ResponseObject();
		try {
			return packageTypeService.update(dto);	
//			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}
		
		return res;
	}
	@RequestMapping(value = "/getPackageTypesId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(packageTypeService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getDeletePackageTypesID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(packageTypeService.getDelete(id));
			res.setObject("Thành công");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
}
