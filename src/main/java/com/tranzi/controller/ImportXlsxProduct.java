package com.tranzi.controller;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tranzi.common.AddImageWatermark;
import com.tranzi.common.CommonConfig;
import com.tranzi.common.ConvetSlug;

//@CrossOrigin(origins = "*")
//@RestController
//@RequestMapping(value = "/importData")
public class ImportXlsxProduct {

	public List<String> uploadImageImport(String urlImage, String nameProduct) {
		List<String> respon = new ArrayList<String>();
		String imageList = urlImage;
		if (urlImage != null && !"".equals(urlImage)) {
			String image[] = urlImage.split(">");
			if (image != null && image.length > 0) {
				for (String url : image) {
					String a = addFileProduct(nameProduct, url);
					if (a != null && !"".equals(a)) {

						respon.add(a);
					}
				}
			}
		}
		return respon;
	}

	public String addFileProduct(String name, String url) {
		try {
			String strUrl = url;
			int index = strUrl.indexOf("http");
			int indexHttps = strUrl.indexOf("https");
			if (index >= 0 || indexHttps >= 0) {
				strUrl = strUrl;
			} else {
				int index2 = strUrl.indexOf("//");
				if (index2 == 0) {
					strUrl = "http:" + strUrl;
				} else {
					strUrl = "http://" + strUrl;
				}

			}
			InputStream in = new URL(strUrl).openStream();
			CommonConfig cf = new CommonConfig();
			ConvetSlug cs = new ConvetSlug();
			Date dt = new Date();
			String nameFile = cs.toUrlFriendly(name, 1) + dt.getMinutes() + dt.getSeconds();
			String folderImage = createPath(cf.uploadMedia);
			Files.copy(in, Paths.get(cf.uploadMedia +folderImage+ nameFile + ".png"));
			String path = cf.uploadMedia+ folderImage + nameFile + ".png";
			// System.out.println("path:"+path);
			// ImportXlsxProduct dtAddImage = new ImportXlsxProduct();
			// dtAddImage.start();
			// dtAddImage.addLogo(path);
			try {
				AddImageWatermark aw = new AddImageWatermark();
				aw.addLogo(path);
				// System.out.println("done:"+path);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return cf.domainWeb +folderImage + nameFile + ".png";

		} catch (Exception e) {
			// TODO: handle exception
			// e.printStackTrace();
			System.out.println("download file loi:" + name);
		}
		return null;
	}

	public void addLogo(String path) {
		try {
			System.out.println("path1:" + path);
			Thread.sleep(500);
			AddImageWatermark aw = new AddImageWatermark();
			aw.addLogo(path);
			System.out.println("done:" + path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public String createPath(String path) {
		String resPath = path;
		Date dt = new Date();
		LocalDateTime now = LocalDateTime.now();
		String nameFolder = now.getYear() + "" + now.getMonthValue() + now.getDayOfMonth() + "";
		resPath = resPath + nameFolder + "/";
		File theDir = new File(resPath);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + theDir.getName());
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
		return nameFolder;
	}

	public static void main(String[] args) {
		// String aa= "2.0";
		// if(aa!=null && aa.length()>2){
		// System.out.println(aa.substring(aa.length()-2,aa.length()));
		// aa = aa.substring(0,aa.length()-2);
		// System.out.println(aa);
		// }
		String nameFolder = "";
		Date dt = new Date();
		LocalDateTime now = LocalDateTime.now();
		nameFolder = now.getYear() + "" + now.getMonthValue() + now.getDayOfMonth() + "";
		File theDir = new File("C:\\Users\\User\\Desktop\\Image\\upload\\" + nameFolder + "/");

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + theDir.getName());
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
	}

}