package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PagerData;
import com.tranzi.model.Product;
import com.tranzi.model.ProductsPrices;
import com.tranzi.service.ProductPriceService;
import com.tranzi.service.ProductService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/productprice")
@SuppressWarnings("unchecked")  
public class ProductPriceController {
	
	@Autowired
	ProductPriceService productPriceService;

	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "/getAllProduct/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAllByProductId(@PathVariable int id) {
		List<ProductsPrices> list = productPriceService.getListByProductID(id);
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody ProductsPrices dto) {
		ResponseObject res = new ResponseObject();
		try {
			return productPriceService.create(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}
		
		return res;
	}
	
	@RequestMapping(value = "/createList", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject createList(@RequestBody List<ProductsPrices> list) {
		ResponseObject res = new ResponseObject();
		try {
			
			res = productPriceService.createList(list);
			if(list!=null && list.size()>0){
				Integer price = list.get(0).getPrice();
				Integer salePrice = list.get(0).getSalePrice();
				Integer min = list.get(0).getMinQuantity();
				Integer idProduct = list.get(0).getProductId();
				for(ProductsPrices dt: list){
					if(dt.getSalePrice()<salePrice){
						salePrice = dt.getSalePrice();
					}
					if(dt.getPrice()<price){
						price = dt.getPrice();
					}
					if(dt.getMinQuantity()<min){
						min = dt.getMinQuantity();
					}
				}
				Product pro = productService.getId(idProduct);
				if(pro!=null){
					pro.setNormalPrice(price);
					pro.setSalePrice(salePrice);
					pro.setMinOrder(min);
					res.setObject(productService.updatePrice(pro));
				}
			}
			return res;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}
		
		return res;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateProductPrice(@RequestBody ProductsPrices dto) {
		ResponseObject res = new ResponseObject();
		try {
			return productPriceService.update(dto);	
//			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}
		
		return res;
	}
	
	@RequestMapping(value = "/deleteByProductID/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteByProductID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return productPriceService.deleteByProductID(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		
		return res;
	}
	
}
