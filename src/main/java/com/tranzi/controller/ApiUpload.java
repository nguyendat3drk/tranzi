package com.tranzi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.common.CommonConfig;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/apiUpload")
public class ApiUpload {
	public static final String user_ACDServer = "five9";
	public static final String pass_ACDServer = "FMC1sd3@df0r3v3r";
	public static final String url_ACDServer_Address = "http://113.161.99.141:8084/rest/acd/announcements";
	// public static final String url_ACDServerPUT =
	// "https://113.161.99.141:8444/rest/acd/announcements/welcome";
	public static final String url_ACDServerPUT = "http://113.161.99.141:8044/rest/acd/announcements/";
	public static final int BUFFER_SIZE = 4096;

	private void skipSSL() {
		try {
			TrustManager[] trustAllCerts = { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			SSLContext sc = SSLContext.getInstance("SSL");

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String arg0, SSLSession arg1) {
					return true;
				}
			};
			sc.init(null, trustAllCerts, new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (Exception localException) {
		}
	}

	@RequestMapping(value = "/getUpload", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String> getAddress() throws Exception {
		new CommonConfig().loadConfig();
//		String path = CommonConfig.path;
		String result = "";
		try {
			// skipSSL();
			String strUrl = url_ACDServer_Address;
			HttpURLConnection connection = (HttpURLConnection) new URL(strUrl).openConnection();
			String auth = user_ACDServer + ":" + pass_ACDServer;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
			String authHeader = "Basic " + new String(encodedAuth);
			connection.setRequestProperty("Authorization", authHeader);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);

			try {

				System.out.println("***** Content of the URL *******");
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					System.out.println(input);
					result += input;
				}
				br.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	public boolean convertHZFile(String pathInput,String pathOutput,String name){
		String command = "ffmpeg -i "+pathInput+name+" -acodec pcm_s16le -ac 1 -ar 8000 "+pathOutput+name;
		try {
			Process curlProc;
			curlProc = Runtime.getRuntime().exec(command);
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
		return true;
	}

}
