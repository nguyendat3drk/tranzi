package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Tags;
import com.tranzi.service.TagsService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/tags")
public class TagsController {
	@Autowired
	TagsService tagsService;

	@RequestMapping(value = "/getTagsByProductId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getTagsByProductId(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(tagsService.getTagsByProductId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}
		return res;
	}

	@RequestMapping(value = "/getTagsByArticleId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Tags> getTagsByArticleId(@PathVariable int id) {
		return tagsService.getTagsByArticleId(id);
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public Tags getById(@PathVariable int id) {
		return tagsService.getById(id);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public int delete(@PathVariable int id) {
		return tagsService.delete(id);
	}

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Tags> getAlls() {
		return tagsService.getAlls();
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject create(@RequestBody Tags dto) {
		ResponseObject res = new ResponseObject();
		try {
			return tagsService.create(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}

		return res;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject update(@RequestBody Tags dto) {
		ResponseObject res = new ResponseObject();
		try {
			return tagsService.update(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}

		return res;
	}

	public int deleteTagsByIdProduct(long id) {
		return tagsService.deleteTagsByIdProduct(id);
	}
}
