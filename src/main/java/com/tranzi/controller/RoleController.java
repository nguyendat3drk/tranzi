package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.GroupACD;
import com.tranzi.model.MenuPermission;
import com.tranzi.model.Role;
import com.tranzi.model.Role;
import com.tranzi.service.RoleService;
import com.tranzi.service.RoleService;

@RestController
@RequestMapping(value = "/role")
public class RoleController {
	@Autowired
	RoleService roleService;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getRoles() {
		List<Role> listOfRoles = roleService.getAllAgents(1);
		return new ResponseEntity(listOfRoles, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getRolesPaging", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getRolesPaging(
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder) {
			List<Role> listOfRoles = roleService.getRolePaging(page, pageSize, columnName, typeOrder);
			return new ResponseEntity(listOfRoles, HttpStatus.OK);
	}
	
	/*@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAllRoleStatus", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAllRoleStatus() {
		if (roleService.getAllAgents(0) != null)
			return new ResponseEntity(roleService.getAllAgents(0), HttpStatus.OK);
		else
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}*/

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getRowCount", method = RequestMethod.GET, headers = "Accept=application/json")
	public int getRowCount() {
		int rowCount = roleService.getRowCount();
		return rowCount;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/search", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Role> searchRoles(@RequestParam(value = "role", required = false) Integer role,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "RoleName", required = false) String RoleName) {
		List<Role> listOfRoles = roleService.searchRoles(role, id, RoleName);
		return listOfRoles;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getRoleID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public Role getRoleById(@PathVariable int id) {
		if (id == 0) {
			Role dt = new Role();
			dt.setId(0);
			dt.setStatus(1);
		}
		return roleService.getRole(id);
	}

	/*@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAllRoles", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAllAgents() {
		if (roleService.getAllAgents(1) != null)
			return new ResponseEntity(roleService.getAllAgents(1), HttpStatus.OK);
		else
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}*/

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAllSupervisors", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAllSupervisors() {
		if (roleService.getAllSupervisors() != null)
			return new ResponseEntity(roleService.getAllSupervisors(), HttpStatus.OK);
		else
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
	public int addRole(@RequestBody Role Role) {
			return roleService.addRole(Role);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/updateStatusRole", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<?> updateStatusRole(@RequestBody Role Role) {
			return roleService.updateStatusRole(Role);
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/saveRole", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<?> saveRole(@RequestBody Role Role) {
			return roleService.saveRole(Role);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseObject deleteRole(@RequestParam(value = "roles", required = true) String roles) {
		return roleService.deleteRoleList(roles);
		
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/checkDelete/{roles}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseObject checkDelete(@PathVariable String roles) {
		return roleService.checkDelete(roles);
	}

//	@CrossOrigin(origins = "*")
//	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE, headers = "Accept=application/json")
//	public ResponseEntity<?> deleteRole(@PathVariable("ids") Integer[] ids,
//			@RequestHeader(value = "token", required = true) String authoToken) {
//		if (checkAuthor(authoToken) == 0) {
//			return new ResponseEntity(null, HttpStatus.FORBIDDEN);
//		} else {
//			return roleService.deleteRole(ids);
//		}
//	}
	 @CrossOrigin(origins = "*")
	    @RequestMapping(value = "/checkName", method = RequestMethod.POST, headers = "Accept=application/json")
	    public int checkUnique(@RequestBody Role role) {
		 	return roleService.checkUnique(role);
	    }

     @CrossOrigin(origins = "*")
     @RequestMapping(value = "/checkUser", method = RequestMethod.POST, headers = "Accept=application/json")
     public int checkUser(@RequestBody Role role) {
        return roleService.checkUser(role.getId());
     }
     


	public int checkAuthor(String myHeader) {
		// if (myHeader == null || myHeader.equals("")) {
		// return 0;
		// } else {
		// Token myToken = new JWTokenUtility().validate(myHeader);
		// // String myToken = new MD5EnCrypt().getMD5()
		// if (myToken == null) {
		// return 0;
		// } else {
		return 1;
		// }
		// }
	}

}
