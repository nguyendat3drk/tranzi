package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PagerData;
import com.tranzi.model.Product;
import com.tranzi.model.ProductSearch;
import com.tranzi.service.ProductService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/product")
@SuppressWarnings("unchecked")  
public class ProductController {
	
	@Autowired
	ProductService productService;

	
	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAlls() {
		List<Product> list = productService.getAlls();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	@RequestMapping(value = "/getAllsNotDetail", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAllsNotDetail() {
		List<Product> list = productService.getAllsNotDetail();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	@RequestMapping(value = "/getAllProduct", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAllProduct() {
		List<Product> list = productService.getAllProduct();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	@RequestMapping(value = "/pagerAllProduct", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			List<Product> list = new ArrayList<Product>();
			List<Product> listRS = new ArrayList<Product>();
			list = productService.getPageProduct(page, pageSize, columnName, typeOrder, search);
			if(list!=null && list.size()>0){
				for(Product d: list){
					if(d!=null){
						if(d.getManufactureId()==null){
							d.setManufactureId(0);
						}
						listRS.add(d);
					}
				}
			}
			pa.setContent(listRS);
			pa.setTotalRow(productService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(201);
		}
		return re;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody Product dto) {
		ResponseObject res = new ResponseObject();
		try {
			return productService.create(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
		}
		
		return res;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateProduct(@RequestBody Product dto) {
		ResponseObject res = new ResponseObject();
		try {
			return productService.updateProduct(dto);	
//			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}
		
		return res;
	}
	@RequestMapping(value = "/getProductId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(productService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getDeleteProductID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return productService.deleteProductID(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/deleteAllProduct", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteAllProduct() {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(productService.deleteAllProduct());
			res.setObject("Thành công");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/getListIDAttributeValue/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getListIDAttributeValue(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(productService.getListIDAttributeValue(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/countProduct", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public int countProduct() {
		return  productService.getCountTotalPage(1, 1000000000, "id", null,null);
	}
	@RequestMapping(value = "/searchMoveAllProduct", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject searchMoveAllProduct(@RequestBody ProductSearch dto) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(dto.getPage());
		pa.setPageSize(dto.getPageSize());
		re.setResultCode(200);
		try {
			List<Product> list = new ArrayList<Product>();
			List<Product> listRS = new ArrayList<Product>();
			list = productService.getPageProductMore(dto);
			if(list!=null && list.size()>0){
				for(Product d: list){
					if(d!=null){
						if(d.getManufactureId()==null){
							d.setManufactureId(0);
						}
						listRS.add(d);
					}
				}
			}
			pa.setContent(listRS);
			pa.setTotalRow(productService.getCountTotalPageMore(dto));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(201);
		}
		return re;
	}
//	list product by Inventory area
	@RequestMapping(value = "/pagerAllProductArea", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPagerByInventoryArea(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "area", required = false) int area,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			List<Product> list = new ArrayList<Product>();
			list = productService.getPageProductByInventoryArea(page, pageSize, columnName, typeOrder, search, area );
			pa.setContent(list);
			pa.setTotalRow(productService.getCountTotalPageByInventoryArea(page, pageSize, columnName, typeOrder, search, area));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(201);
		}
		return re;
	}
	
	@RequestMapping(value = "/deleteListID", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteListID(@RequestBody List<Integer> list){
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			return productService.deleteListID(list);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(201);
		}
		return re;
	}
	@RequestMapping(value = "/checkArticleProeduct/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject checkArticleProeduct(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(productService.checkArticleProeduct(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
}
