package com.tranzi.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.tranzi.entity.ResponseObject;
import com.tranzi.entity.Token;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.model.User;
import com.tranzi.model.UserPreference;
import com.tranzi.service.AccountService;
import com.tranzi.service.AuthenticationService;
import com.tranzi.service.DashboardService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/dashboard")
public class DashboardController {

	@Autowired
	DashboardService dashboardService;
	
	@Autowired
	AuthenticationService authenService;
	
	@Autowired
	AccountService accountService;

	/*@RequestMapping(value = "/getTotalCall", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<ResponseObject> getTotalCall(@RequestBody List<String> dates) throws ParseException {
		return dashboardService.getTotalCall(dates);
	}*/

	@RequestMapping(value = "/getDashboardState/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseObject getDashboardState(@PathVariable int id, @RequestHeader(value = "Authorization", required = true) String authenToken) throws ParseException {
		if(checkAuthentication(authenToken)){
			return dashboardService.getDashboardState(id);
		}else{
			ResponseObject responseObj = new ResponseObject();
			responseObj.setResultCode(405);
			responseObj.setObject(null);
			responseObj.setErrorMessage(null);
			return responseObj;
		}
	}

	@RequestMapping(value = "/saveDashboardState", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseObject saveDashboardState(@RequestBody UserPreference userPreference, 
			@RequestHeader(value = "Authorization", required = true) String authenToken) throws ParseException {
		if(checkAuthentication(authenToken)){
			return dashboardService.saveDashboardState(userPreference);
		}else{
			ResponseObject responseObj = new ResponseObject();
			responseObj.setResultCode(405);
			responseObj.setObject(null);
			responseObj.setErrorMessage(null);
			return responseObj;
		}
	}

	@RequestMapping(value = "/getDataDashBoard/{agentId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseObject getDataDashBoard(@PathVariable int agentId,
			@RequestParam(value = "fromDate", required = true) long fromDate,
			@RequestParam(value = "toDate", required = true) long toDate,
			@RequestHeader(value = "Authorization", required = true) String authenToken) throws ParseException {
//		System.out.println("authenToken: " + authenToken);
		if(checkAuthentication(authenToken)){
			return dashboardService.getDataDashBoard(agentId, fromDate, toDate);
		}else{
			ResponseObject responseObject = accountService.getAccountById(agentId);
			ResponseObject responseObj = new ResponseObject();
			responseObj.setResultCode(405);
			responseObj.setObject(null);
			responseObj.setErrorMessage(null);
			return responseObj;
		}
	}
	
	@RequestMapping(value = "/savePosition", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseObject savePosition(@RequestBody UserPreference userPreference) throws ParseException {
			return dashboardService.savePosition(userPreference);
	}
	
	public Token checkAuthor(String myHeader) {
		if (myHeader == null || myHeader.equals("")) {
			return null;
		} else {
			Token myToken = new JWTokenUtility().validate(myHeader);
			if (myToken == null) {
				return null;
			} else {
				return myToken;
			}
		}
	}
	
	private boolean checkAuthentication(String mToken){
		Token token = checkAuthor(mToken);
		if (token != null) {
			String currentSession = token.getId() + token.getIssueAt();
			System.out.println("currentSession: " + currentSession);
			boolean isUserSession = authenService.checkUserSession(token.getIssuer(), currentSession);
			if(isUserSession){
				return true;
			}else{
//				return new ResponseEntity(roleService.getAllAgents(0), HttpStatus.OK);
				return false;
			}
		}else{
			return false;
		}
	}
	
	
	
}