package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.common.CommonConfig;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.CustomerOrder;
import com.tranzi.model.CustomerOrderProduct;
import com.tranzi.model.Manufactures;
import com.tranzi.model.PagerData;
import com.tranzi.service.CustomerOrderProductService;
import com.tranzi.service.CustomerOrderService;
import com.tranzi.service.ManufacturesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/customerorder")
public class CustomerOrderController {

	@Autowired
	CustomerOrderService customerOrderService;

	@Autowired
	CustomerOrderProductService customerOrderProductService;
	
	@Autowired
	ManufacturesService manufacturesService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(customerOrderService.getAlls());
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			re.setErrorMessage("có lỗi xảy ra");
		}
		return re;
	}

	@RequestMapping(value = "/pagerAllCustomer", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(customerOrderService.getPageCustomer(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(customerOrderService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject create(@RequestBody CustomerOrder dto) {
		ResponseObject re = new ResponseObject();
		try {
			return customerOrderService.create(dto);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage("Not connect");
		}

		return re;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject update(@RequestBody CustomerOrder dto) {
		ResponseObject res = new ResponseObject();
		try {
			return customerOrderService.update(dto);
			// res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}

		return res;
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(customerOrderService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteCustomerID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return customerOrderService.getDelete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("not connect server");
		}

		return res;
	}

	@RequestMapping(value = "/countOrder", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public int countCustomer() {
		try {
			return customerOrderService.getCountTotalPage(0, 100000000, "id", null, null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return 0;
	}

	@RequestMapping(value = "/exportOrder/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject exportOrder(@PathVariable int id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			CustomerOrder customerOrder = new CustomerOrder();
			List<CustomerOrderProduct> listDetail = new ArrayList<>();
			List<Manufactures>  manfactoty  = new ArrayList<Manufactures>();
			customerOrder = customerOrderService.getId(id);
			listDetail =customerOrderProductService.getByCustomerOrderIDExport(id);
			
			manfactoty = manufacturesService.getAlls();
			if(listDetail!=null && manfactoty!=null){
				for(int i =0;i< listDetail.size();i++){
					if(listDetail.get(i).getManufactureId()>0){
						for(int j =0;j< manfactoty.size();j++){
							if(listDetail.get(i).getManufactureId() == manfactoty.get(j).getId()){
								listDetail.get(i).setManufactureName(manfactoty.get(j).getName());
								break;
							}
						}
					}
				}
			}
			ExportBomController exp = new ExportBomController();
			re.setData(exp.exprortOrderCustomer(customerOrder, listDetail));
			
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			e.printStackTrace();
			re.setErrorMessage("có lỗi xảy ra");
		}
		try {
			resetChmod(CommonConfig.pathExportOrderCustomer);
			resetChmod(CommonConfig.pathExportBarCodeProduct);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return re;
	}
	public void resetChmod(String path) {
		try {
			Process p = Runtime.getRuntime().exec("chmod -R 777 " + path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}


}
