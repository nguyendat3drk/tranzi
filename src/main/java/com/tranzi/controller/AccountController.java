package com.tranzi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.common.GsonHelper;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Account;
import com.tranzi.model.PagerData;
import com.tranzi.service.AccountService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/account")
public class AccountController {

	@Autowired
	AccountService accountService;
	@RequestMapping(value = "/pagerAllAccount", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(accountService.getPageAccount(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(accountService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}
	@RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseObject addAccount(@RequestBody Account account) {
		// System.out.println("My Header: " + authoToken);
		ResponseObject re = new ResponseObject();
		re.setResultCode(201);
		re.setObject(account);
		int intCheck = 0;
		try {
			return accountService.addAccount(account);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return re;

	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseObject updateAccount(@RequestBody Account account) {

		ResponseObject re = new ResponseObject();
		re.setResultCode(201);
		re.setObject(account);
		try {
			return accountService.updateAccount(account);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		re.setErrorMessage("Cannot connect to Aarenet server now. Please update extension later!");
		return re;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteId(@PathVariable int id) {
		return accountService.deleteId(id);
	}
	
	@RequestMapping(value = "/getUserById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAccountById(@PathVariable int id) {
		return accountService.getAccountById(id);
	}

	

	@RequestMapping(value = "/getAllUserRole/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAllUserRole(@PathVariable int id) {
			return new ResponseEntity(accountService.getAllUserRole(id), HttpStatus.OK);
		
	}


	

	@RequestMapping(value = "/resetMail", method = RequestMethod.GET, headers = "Accept=application/json")
	public int resetMail(@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "title", required = false) String title) {
		int rowCount = accountService.resetMail(email, title);
		return rowCount;
	}

	public GsonHelper gsonHelper = new GsonHelper();

}
