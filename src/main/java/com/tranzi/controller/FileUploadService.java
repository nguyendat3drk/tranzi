package com.tranzi.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.tranzi.common.CommonConfig;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.FileMeta;
import com.tranzi.model.User;
import com.tranzi.service.RoleService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/upload")
public class FileUploadService {
	private static final String UPLOAD_FOLDER = "c:/uploadedFiles/";
//	private static final String path = "/home/mediaserver/";
//	private static final String path = "C:/UP/";
//	final static String path = CommonConfig.path;
	RoleService roleService;
	ApiUpload apiUp = new ApiUpload();
	public FileUploadService() {

	}

	@Context

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public @ResponseBody ResponseObject addMultiDisCar(MultipartHttpServletRequest request,
			HttpServletResponse response, HttpServletRequest httpRequest) {
		new CommonConfig().loadConfig();
		String path = CommonConfig.path;
		File fileNew = new File(path);
        if (!fileNew.exists()) {
        	fileNew = new File("/home/mediaserver");
        	fileNew.mkdir();
        	fileNew =  new File("/home/mediaserver/mediafile");
        }else{
        	fileNew = new File(path+"mediafile");
        	if (!fileNew.exists()) {
        		fileNew.mkdir();
        	}
        }
		HttpSession session = request.getSession();
	    String username = (String)request.getAttribute("un");
	    session.setAttribute("upload1", request);
		
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile mpf = null;
//		String path = "C:/UP/";
		for (Iterator<String> it = request.getFileNames(); it.hasNext();) {
			mpf = request.getFile(it.next());
			

			FileMeta fileContainer = new FileMeta();
			fileContainer.setName(mpf.getOriginalFilename());
			fileContainer.setSize(mpf.getSize() / 1024 + "Kb");
			fileContainer.setType(mpf.getContentType());

			try {
				fileContainer.setBytes(mpf.getBytes());
				// createFolderIfNotExists();
				final String fileName =mpf.getOriginalFilename().trim().replace(" ", "-"); 
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(path + fileName));
				if(apiUp.convertHZFile(path,path+"mediafile/",mpf.getOriginalFilename().trim().replace(" ", "-"))){
//					apiUp.postMedia(mpf.getBytes(),mpf.getOriginalFilename());
//					final MultipartFile mpf1 = mpf;
					System.out.println("mpf1 name: " + fileName);
					 new Thread(new Runnable() {
		                @Override
		                public void run() {
		                	try {
								Thread.sleep(1500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		                	try {
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            }
			        }).start();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		ResponseObject dto = new ResponseObject();
		dto.setObject("successful");
		dto.setResultCode(200);
		return dto;
	}
	@RequestMapping(value = "/add21", method = RequestMethod.POST)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public @ResponseBody ResponseObject addMultiDisCar22(MultipartHttpServletRequest request,
			HttpServletResponse response, HttpServletRequest httpRequest) {
			ResponseObject dto = new ResponseObject();
			dto.setObject("successful");
			dto.setResultCode(200);
			return dto;
	}
	@RequestMapping(value = "/add2", method = RequestMethod.POST)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public @ResponseBody ResponseObject addMultiDisCar2(MultipartHttpServletRequest request,
			HttpServletResponse response, HttpServletRequest httpRequest) {
		new CommonConfig().loadConfig();
		String path = CommonConfig.path;
//		String path = "C:/UP/";
		 File fileNew = new File(path);
	        if (!fileNew.exists()) {
	        	fileNew = new File("/home/mediaserver");
	        	fileNew.mkdir();
	        	fileNew =  new File("/home/mediaserver/mediafile");
	        }else{
	        	fileNew = new File(path+"mediafile");
	        	if (!fileNew.exists()) {
	        		fileNew.mkdir();
	        	}
	        }
//		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile mpf = null;
//		String path = "C:/UP/";
		for (Iterator<String> it = request.getFileNames(); it.hasNext();) {
			mpf = request.getFile(it.next());

			FileMeta fileContainer = new FileMeta();
			fileContainer.setName(mpf.getOriginalFilename());
			fileContainer.setSize(mpf.getSize() / 1024 + "Kb");
			fileContainer.setType(mpf.getContentType());

			try {
				fileContainer.setBytes(mpf.getBytes());
				// createFolderIfNotExists();
				final String fileName =mpf.getOriginalFilename().trim().replace(" ", "-"); 
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(path + mpf.getOriginalFilename().trim().replace(" ", "-")));
				if(apiUp.convertHZFile(path,path+"mediafile/",mpf.getOriginalFilename().trim().replace(" ", "-"))){
//					apiUp.postMedia(mpf.getBytes(),mpf.getOriginalFilename());
//					final MultipartFile mpf2 = mpf;
					 new Thread(new Runnable() {
		                @Override
		                public void run() {
		                	try {
								Thread.sleep(1500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		                	try {
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            }
			        }).start();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		ResponseObject dto = new ResponseObject();
		dto.setObject("successful");
		dto.setResultCode(200);
		return dto;
	}
	@RequestMapping(value = "/add3", method = RequestMethod.POST)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("text/plain")
	public @ResponseBody ResponseObject addMultiDisCar3(MultipartHttpServletRequest request,
			HttpServletResponse response, HttpServletRequest httpRequest) {
		new CommonConfig().loadConfig();
		String path = CommonConfig.path;
//		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile mpf = null;
//		String path = "C:/UP/";
		File fileNew = new File(path);
        if (!fileNew.exists()) {
        	fileNew = new File("/home/mediaserver");
        	fileNew.mkdir();
        	fileNew =  new File("/home/mediaserver/mediafile");
        }else{
        	fileNew = new File(path+"mediafile");
        	if (!fileNew.exists()) {
        		fileNew.mkdir();
        	}
        }
		for (Iterator<String> it = request.getFileNames(); it.hasNext();) {
			mpf = request.getFile(it.next());

			FileMeta fileContainer = new FileMeta();
			fileContainer.setName(mpf.getOriginalFilename());
			fileContainer.setSize(mpf.getSize() / 1024 + "Kb");
			fileContainer.setType(mpf.getContentType());

			try {
				fileContainer.setBytes(mpf.getBytes());
				// createFolderIfNotExists();
				final String fileName =mpf.getOriginalFilename().trim().replace(" ", "-"); 
				FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(path + mpf.getOriginalFilename().trim().replace(" ", "-")));
				if(apiUp.convertHZFile(path,path+"mediafile/",mpf.getOriginalFilename().trim().replace(" ", "-"))){
//					final MultipartFile mpf3 = mpf;
					 new Thread(new Runnable() {
		                @Override
		                public void run() {
		                	try {
								Thread.sleep(1500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		                	try {
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            }
			        }).start();
					
					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		ResponseObject dto = new ResponseObject();
		dto.setObject("successful");
		dto.setResultCode(200);
		return dto;
	}

	/*@RequestMapping(value = "/file", method = RequestMethod.POST, headers = "Accept=application/json")
	public String addAccount(@RequestBody byte[] data) {
		System.out.println("My Header: " + data);
		// Path path = Paths.get("C:/Users/DatNH/Desktop/TESTDATA/abc.txt");
		Path path = Paths.get("C:/Users/DatNH6/Desktop/Demotes/abc2.jpg");
		try {
			// Files.write(path, data);
			Files.write(path, data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			File dstFile = new File("C:/Users/DatNH6/Desktop/Demotes/abc2.wav");
			FileOutputStream out = new FileOutputStream(dstFile);
			// byte[] buf = new byte[1024];
			int len;
			// while ((len = in.read(data)) > 0) {
			// out.write(data, 0, len);
			// }

			// in.close();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}*/
	/*@RequestMapping(value = "/getUpload", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public void uploadFile(){
//		HttpSession session = request.getSession();
//	    String username = (String)request.getAttribute("un");
//	    session.setAttribute("upload1", request);
	}*/
	/*public static void main(String[] args) {

	    Properties prop = new Properties();
	    InputStream input = null;

	    try {

	        input = new FileInputStream("resources/config.properties");

	        // load a properties file
	        prop.load(input);

	        // get the property value and print it out
	        System.out.println(prop.getProperty("database"));
	        System.out.println(prop.getProperty("dbuser"));
	        System.out.println(prop.getProperty("dbpassword"));

	    } catch (IOException ex) {
	        ex.printStackTrace();
	    } finally {
	        if (input != null) {
	            try {
	                input.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }

	  }*/

	
}