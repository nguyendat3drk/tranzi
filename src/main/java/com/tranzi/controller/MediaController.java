package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Media;
import com.tranzi.service.MediaService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/medias")
public class MediaController {
	@Autowired
	MediaService mediaService;
	// Lay danh sach File san pham theo id product
	@RequestMapping(value = "/getMediaByProductID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getMediaByProductID(@PathVariable int id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			List<Media> list = mediaService.getMediaByProductID(id);
			re.setObject(list);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
		}
		
		return re;
	}
	// Lay danh sach anh san pham theo id product
	@RequestMapping(value = "/getMediaByProductMediaID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getMediaByProductMediaID(@PathVariable int id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			List<Media> list = mediaService.getMediaByProductIDMedia(id);
			re.setObject(list);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
		}
		
		return re;
	}
	@RequestMapping(value = "/getMediaByProductIDMedia/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getMediaByProductIDMedia(@PathVariable int id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			List<Media> list = mediaService.getMediaByProductIDMedia(id);
			re.setObject(list);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
		}
		
		return re;
	}
}
