package com.tranzi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.LoginView;
import com.tranzi.entity.ResponseObject;
import com.tranzi.service.AuthenticationService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/authentication")
public class AuthenticationController {
	
	@Autowired
	AuthenticationService authenticationService;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseObject checkLogin(@RequestBody LoginView accountLogin) {
		return authenticationService.checkLogin(accountLogin);
	}
}
