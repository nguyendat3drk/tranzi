package com.tranzi.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;

import com.businessrefinery.barcode.Barcode;
import com.tranzi.common.CommonConfig;
import com.tranzi.common.GenerateQRCode;
import com.tranzi.model.CustomerOrder;
import com.tranzi.model.CustomerOrderProduct;
import com.tranzi.model.respontOrder.ReponseOrder;
import com.tranzi.model.respontOrder.RespontGenBarCodeOrder;

public class ExportBomController {
	// Create File BarCode product
	public List<RespontGenBarCodeOrder> genBarCodeOrder(List<CustomerOrderProduct> listDetail,
			CustomerOrder customerOrder) {
		List<RespontGenBarCodeOrder> ListRes = new ArrayList<RespontGenBarCodeOrder>();
		if (listDetail != null) {
			RespontGenBarCodeOrder re;
			int i = 0;
			for (CustomerOrderProduct dto : listDetail) {
				i++;
				try {
					String pathQRCodeOrer = "";
					String pathDomain = "";
					String pathTemplate = CommonConfig.pathExportTempate + "DetailOrderProduct.xlsx";
					FileInputStream inputStream = new FileInputStream(new File(pathTemplate));
					Workbook workbook = WorkbookFactory.create(inputStream);
					Sheet sheet = workbook.getSheetAt(0);
					if (customerOrder != null) {
						re = new RespontGenBarCodeOrder();
						re.setCustomerProductId(dto.getProductId());
						// Start tạo QR Code sản phẩm
						int size = 130;
						String fileType = "png";

						String nameQR = "date:" + customerOrder.getCreatedAt() + ";sku:" + dto.getSku() + ";orginSku:"
								+ dto.getOrginSku() + ";shippingName:" + customerOrder.getShippingName() + ";Quantity:"
								+ dto.getQuantity();
						String nameFileOrder = "order_id-" + customerOrder.getId() + "-Order_Code-"
								+ customerOrder.getOrderCode().trim() + "-productId-" + dto.getId();
						pathQRCodeOrer = CommonConfig.pathExportQRCode + nameFileOrder + ".png";
						File qrFile = new File(pathQRCodeOrer);
						GenerateQRCode genQR = new GenerateQRCode();
						genQR.createQRImage(qrFile, nameQR, size, fileType);
						// End QR Code sản phẩm
						// Ghep QR Coe vào báo cáo
						if (pathQRCodeOrer != null) {
							InputStream my_banner_image = new FileInputStream(pathQRCodeOrer);
							/* Convert picture to be added into a byte array */
							byte[] bytes = IOUtils.toByteArray(my_banner_image);
							/*
							 * Add Picture to Workbook, Specify picture type as
							 * PNG and Get an Index
							 */
							int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
							/*
							 * Close the InputStream. We are ready to attach the
							 * image to workbook now
							 */
							my_banner_image.close();
							/* Create the drawing container */
							XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
							/* Create an anchor point */
							XSSFClientAnchor my_anchor = new XSSFClientAnchor();
							/*
							 * Define top left corner, and we can resize picture
							 * suitable from there
							 */
							my_anchor.setCol1(0);
							my_anchor.setRow1(7);
							/*
							 * Invoke createPicture and pass the anchor point
							 * and ID
							 */
							XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
							/* Call resize method, which resizes the image */
							my_picture.resize();
						}

						// Start add thông tin product detail
						// Mã TranZi
						Row row = sheet.getRow(1);
						if (row == null) {
							row = sheet.createRow(1);
						}
						Cell cell = row.getCell(1);
						if (cell == null) {
							cell = row.createCell(1);
						}
						cell.setCellType(CellType.STRING);
						cell.setCellValue(dto.getSku());
						// Tên Nhà Sản xuất
						Row rowNameManfactory = sheet.getRow(2);
						if (rowNameManfactory == null) {
							rowNameManfactory = sheet.createRow(2);
						}
						Cell cellNameManfactory = rowNameManfactory.getCell(1);
						if (cellNameManfactory == null) {
							cellNameManfactory = rowNameManfactory.createCell(1);
						}
						cellNameManfactory.setCellType(CellType.STRING);
						cellNameManfactory.setCellValue(dto.getManufactureName());
						// Mã Nhà Sản xuất
						Row rowCodeManfactory = sheet.getRow(3);
						if (rowCodeManfactory == null) {
							rowCodeManfactory = sheet.createRow(3);
						}
						Cell cellCodeManfactory = rowCodeManfactory.getCell(1);
						if (cellCodeManfactory == null) {
							cellCodeManfactory = rowCodeManfactory.createCell(1);
						}
						cellCodeManfactory.setCellType(CellType.STRING);
						cellCodeManfactory.setCellValue(dto.getOrginSku());
						// Miêu tả mặt hàng
						Row rowDescription = sheet.getRow(4);
						if (rowDescription == null) {
							rowDescription = sheet.createRow(4);
						}
						Cell cellDescription = rowDescription.getCell(1);
						if (cellDescription == null) {
							cellDescription = rowDescription.createCell(1);
						}
						cellDescription.setCellType(CellType.STRING);
						cellDescription.setCellValue(dto.getProductDescription());
						// Name customer
						Row rowName = sheet.getRow(5);
						if (rowName == null) {
							rowName = sheet.createRow(5);
						}
						Cell cellname = rowName.getCell(1);
						if (cellname == null) {
							cellname = rowName.createCell(1);
						}
						cellname.setCellType(CellType.STRING);
						cellname.setCellValue(customerOrder.getShippingName());
						// Code customer
						Row rowCode = sheet.getRow(6);
						if (rowCode == null) {
							rowCode = sheet.createRow(6);
						}
						Cell cellCode = rowCode.getCell(1);
						if (cellCode == null) {
							cellCode = rowCode.createCell(1);
						}
						cellCode.setCellType(CellType.STRING);
						cellCode.setCellValue(customerOrder.getCustomerId());
						// quantity
						Row rowQuantity = sheet.getRow(7);
						if (rowQuantity == null) {
							rowQuantity = sheet.createRow(7);
						}
						Cell cellQuantity = rowQuantity.getCell(2);
						if (cellQuantity == null) {
							cellQuantity = rowQuantity.createCell(2);
						}
						cellQuantity.setCellType(CellType.NUMERIC);
						cellQuantity.setCellValue(dto.getQuantity());
						// DM
						Row rowDM = sheet.getRow(9);
						if (rowDM == null) {
							rowDM = sheet.createRow(9);
						}
						Cell cellDM = rowDM.getCell(2);
						if (cellDM == null) {
							cellDM = rowDM.createCell(2);
						}
						cellDM.setCellType(CellType.NUMERIC);
						cellDM.setCellValue(i);
						// DM
						Row rowOrderCode = sheet.getRow(10);
						if (rowOrderCode == null) {
							rowOrderCode = sheet.createRow(10);
						}
						Cell cellOrderCode = rowOrderCode.getCell(2);
						if (cellOrderCode == null) {
							cellOrderCode = rowOrderCode.createCell(2);
						}
						cellOrderCode.setCellType(CellType.STRING);
						cellOrderCode.setCellValue(customerOrder.getOrderCode());

						// End add thông tin product detail
						
						// TODO
						// Start add barCode
						String nameBarCode = dto.getSku() + "-"+ dto.getQuantity();
						String nameFileBarCode = "idProduct-" + dto.getId() + "tranziSku-" + customerOrder.getId()
								+ "-Quantity-" + customerOrder.getOrderCode().trim() + "-productId-" + dto.getId();
						String pathBarCodeOrer = CommonConfig.pathExportBarCode + nameFileBarCode + ".png";
						// Gen Image
						try {
							GenBarCode(nameBarCode, pathBarCodeOrer);	
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						
						// End Ghen image
						// Gep bar code vao file
						if (pathBarCodeOrer != null) {
							InputStream my_banner_image = new FileInputStream(pathBarCodeOrer);
							byte[] bytes = IOUtils.toByteArray(my_banner_image);
							int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
							my_banner_image.close();
							XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
							XSSFClientAnchor my_anchor = new XSSFClientAnchor();
							my_anchor.setCol1(5);
							my_anchor.setRow1(2);
							XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
							my_picture.resize();
						}
						// End Ghep bar code vao file
						// End add barCode
						// Save File
						inputStream.close();
						Date dtNewExport = new Date();
						String nameFile = "Export-Order-Detail-" + dtNewExport.getTime() + ".xlsx";
						pathDomain = CommonConfig.domainWebBarCodeProduct + nameFile;
						String pathOutput = CommonConfig.pathExportBarCodeProduct + nameFile;
						FileOutputStream outputStream = new FileOutputStream(pathOutput);
						workbook.write(outputStream);
						workbook.close();
						outputStream.close();

						// Set path luu nhan detail product
						re.setPathBarCode(pathDomain);
						// add list rs
						ListRes.add(re);
					}

					// End Ghép QR code

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
		}

		return ListRes;

	}

	public String GenBarCode(String nameGen, String path) throws ServletException {
		try {
			int size = 130;
			String fileType = "png";
			// 1. Create linear barcode object.
//			Barcode barcode = new Barcode();
//			// 2. Set barcode properties.
//			barcode.setSymbology(Barcode.CODE39EX);
//			barcode.setCode(nameGen);
//			barcode.setDisplayText(false);
////			barcode.setBarHeight(294);
//			barcode.setBarWidth(1);
//			barcode.setBarcodeHeight(152);
//			barcode.setRotate(barcode.ROTATE_90);
//			// 3. Draw barcode, and output to gif format
//			barcode.drawImage2File(path);
			GenerateQRCode genQR = new GenerateQRCode();
			File qrFile = new File(path);
			genQR.createQRImage(qrFile, nameGen, size, fileType);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			  throw new ServletException(e);
		}
		return "thanh cong";
	}

	public ReponseOrder exprortOrderCustomer(CustomerOrder customerOrder, List<CustomerOrderProduct> listDetail) {
		String pathOutput = "";
		String pathDomain = "";
		System.out.println("vao exprortOrderCustomer");
		ReponseOrder rep = new ReponseOrder();
		try {
			
			String pathQRCodeOrer = "";
			String pathTemplate = CommonConfig.pathExportTempate + "invoicegui_product.xlsx";
			FileInputStream inputStream = new FileInputStream(new File(pathTemplate));
			Workbook workbook = WorkbookFactory.create(inputStream);
			Sheet sheet = workbook.getSheetAt(0);
			if (customerOrder != null) {
				int size = 100;
				String fileType = "png";

				String nameQR = "order_id-" + customerOrder.getId() + "-Order_Code-"
						+ customerOrder.getOrderCode().trim() + "-CustomerId-" + customerOrder.getCustomerId()
						+ "shippingName" + customerOrder.getShippingName();
				String nameFileOrder = "order_id-" + customerOrder.getId() + "-Order_Code-"
						+ customerOrder.getOrderCode().trim();
				pathQRCodeOrer = CommonConfig.pathExportQRCode + nameFileOrder + ".png";
				File qrFile = new File(pathQRCodeOrer);
				GenerateQRCode genQR = new GenerateQRCode();
				genQR.createQRImage(qrFile, nameQR, size, fileType);
				// ghép QRCode vào đơn hàng
				if (pathQRCodeOrer != null) {
					InputStream my_banner_image = new FileInputStream(pathQRCodeOrer);
					/* Convert picture to be added into a byte array */
					byte[] bytes = IOUtils.toByteArray(my_banner_image);
					/*
					 * Add Picture to Workbook, Specify picture type as PNG and
					 * Get an Index
					 */
					int my_picture_id = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
					/*
					 * Close the InputStream. We are ready to attach the image
					 * to workbook now
					 */
					my_banner_image.close();
					/* Create the drawing container */
					XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
					/* Create an anchor point */
					XSSFClientAnchor my_anchor = new XSSFClientAnchor();
					/*
					 * Define top left corner, and we can resize picture
					 * suitable from there
					 */
					my_anchor.setCol1(15);
					my_anchor.setRow1(4);
					/* Invoke createPicture and pass the anchor point and ID */
					XSSFPicture my_picture = drawing.createPicture(my_anchor, my_picture_id);
					/* Call resize method, which resizes the image */
					my_picture.resize();
				}
				// Ghép thông tin order lên đơn hàng

				// Tên khách hàng
				Row row = sheet.getRow(4);
				if (row == null) {
					row = sheet.createRow(4);
				}
				Cell cell = row.getCell(8);
				if (cell == null) {
					cell = row.createCell(8);
				}
				cell.setCellType(CellType.STRING);
				cell.setCellValue(customerOrder.getShippingName());
				// địa chỉ
				Row rowAddress = sheet.getRow(6);
				if (rowAddress == null) {
					rowAddress = sheet.createRow(6);
				}
				Cell cellAddress = rowAddress.getCell(8);
				if (cellAddress == null) {
					cellAddress = rowAddress.createCell(8);
				}
				cellAddress.setCellType(CellType.STRING);
				cellAddress.setCellValue(customerOrder.getShippingAddress());
				// Phone
				Row rowPhone = sheet.getRow(8);
				if (rowPhone == null) {
					rowPhone = sheet.createRow(8);
				}
				Cell cellPhone = rowPhone.getCell(8);
				if (cellPhone == null) {
					cellPhone = rowPhone.createCell(8);
				}
				cellPhone.setCellType(CellType.STRING);
				cellPhone.setCellValue(customerOrder.getShippingMobile());
				// Email
				Row rowEmail = sheet.getRow(9);
				if (rowEmail == null) {
					rowEmail = sheet.createRow(9);
				}
				Cell cellEmail = rowEmail.getCell(8);
				if (cellEmail == null) {
					cellEmail = rowEmail.createCell(8);
				}
				cellEmail.setCellType(CellType.STRING);
				cellEmail.setCellValue(customerOrder.getShippingEmail());
				// Ngay tao don
				Row rowCreateDate = sheet.getRow(13);
				if (rowCreateDate == null) {
					rowCreateDate = sheet.createRow(13);
				}
				Cell cellCreateDate = rowCreateDate.getCell(13);
				if (cellCreateDate == null) {
					cellCreateDate = rowCreateDate.createCell(13);
				}
				cellCreateDate.setCellType(CellType.STRING);
				try {
					if (customerOrder.getCreatedAt() > 0) {
						Date dt = new Date((customerOrder.getCreatedAt() * 1000));
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						String dateString = format.format(dt);
						cellCreateDate.setCellValue(dateString);
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				// code invoice
				Row rowInvoice = sheet.getRow(15);
				if (rowInvoice == null) {
					rowInvoice = sheet.createRow(15);
				}
				Cell cellInvoice = rowInvoice.getCell(13);
				if (cellInvoice == null) {
					cellInvoice = rowInvoice.createCell(13);
				}
				cellInvoice.setCellType(CellType.STRING);
				cellInvoice.setCellValue(customerOrder.getOrderCode());
				// code customer
				// Row rowCodeCustomer = sheet.getRow(16);
				// if (rowCodeCustomer == null) {
				// rowCodeCustomer = sheet.createRow(16);
				// }
				// Cell cellCodeCustomer = rowCodeCustomer.getCell(13);
				// if (cellCodeCustomer == null) {
				// cellCodeCustomer = rowCodeCustomer.createCell(13);
				// }
				// cellCodeCustomer.setCellType(CellType.STRING);
				// cellCodeCustomer.setCellValue(customerOrder.getOrderCode());

				// Set list sản phẩn detail
				int num = 1;
				if (listDetail != null) {
					num = listDetail.size();
				}
				sheet.shiftRows(21, 37, num);
				if (listDetail != null) {
					for (int i = 0; i < listDetail.size(); i++) {
						Row rowDetail = sheet.getRow(21 + i);
						if (rowDetail == null) {
							rowDetail = sheet.createRow(21 + i);
						}
						Cell cellDetailSTT = rowDetail.getCell(0);
						if (cellDetailSTT == null) {
							cellDetailSTT = rowDetail.createCell(0);
						}
						cellDetailSTT.setCellType(CellType.STRING);
						cellDetailSTT.setCellValue(i + 1);
						// Mã LK
						Cell cellDetailLK = rowDetail.getCell(1);
						if (cellDetailLK == null) {
							cellDetailLK = rowDetail.createCell(1);
						}
						cellDetailLK.setCellType(CellType.STRING);
						cellDetailLK.setCellValue(listDetail.get(i).getSku());
						// Mã NSX
						Cell cellDetailNSX = rowDetail.getCell(2);
						if (cellDetailNSX == null) {
							cellDetailNSX = rowDetail.createCell(2);
						}
						cellDetailNSX.setCellType(CellType.STRING);
						cellDetailNSX.setCellValue(listDetail.get(i).getOrginSku());
						// Mã Name NSX
						Cell cellDetailNameNSX = rowDetail.getCell(3);
						if (cellDetailNameNSX == null) {
							cellDetailNameNSX = rowDetail.createCell(3);
						}
						cellDetailNameNSX.setCellType(CellType.STRING);
						cellDetailNameNSX.setCellValue(listDetail.get(i).getManufactureName());
						// Descroption
						Cell cellDetailDescroption = rowDetail.getCell(4);
						if (cellDetailDescroption == null) {
							cellDetailDescroption = rowDetail.createCell(4);
						}
						cellDetailDescroption.setCellType(CellType.STRING);
						cellDetailDescroption.setCellValue(listDetail.get(i).getProductDescription());
						// Mã unit
						Cell cellDetailUnit = rowDetail.getCell(11);
						if (cellDetailUnit == null) {
							cellDetailUnit = rowDetail.createCell(11);
						}
						cellDetailUnit.setCellType(CellType.STRING);
						cellDetailUnit.setCellValue(listDetail.get(i).getId());
						// Mã number
						Cell cellDetailNumber = rowDetail.getCell(12);
						if (cellDetailNumber == null) {
							cellDetailNumber = rowDetail.createCell(12);
						}
						cellDetailNumber.setCellType(CellType.NUMERIC);
						cellDetailNumber.setCellValue(listDetail.get(i).getQuantity());
						// price
						Cell cellDetailprice = rowDetail.getCell(13);
						if (cellDetailprice == null) {
							cellDetailprice = rowDetail.createCell(13);
						}
						cellDetailprice.setCellType(CellType.NUMERIC);
						cellDetailprice.setCellValue(listDetail.get(i).getUnitPrice());
						// Mã VAT
						// Cell cellDetailVAT = rowDetail.getCell(14);
						// if (cellDetailVAT == null) {
						// cellDetailVAT = rowDetail.createCell(14);
						// }
						// cellDetailVAT.setCellType(CellType.STRING);
						// cellDetailVAT.setCellValue("v");
						// total price
						Cell cellDetailTotalprice = rowDetail.getCell(15);
						if (cellDetailTotalprice == null) {
							cellDetailTotalprice = rowDetail.createCell(15);
						}
						cellDetailTotalprice.setCellType(CellType.NUMERIC);
						cellDetailTotalprice.setCellValue(listDetail.get(i).getTotal());

					}
				}
				inputStream.close();
				Date dtNewExport = new Date();
				String nameFile = "Export-Order-" + dtNewExport.getTime() + ".xlsx";
				pathDomain = CommonConfig.domainWebCustomerOrder + nameFile;
				pathOutput = CommonConfig.pathExportOrderCustomer + nameFile;
				FileOutputStream outputStream = new FileOutputStream(pathOutput);
				workbook.write(outputStream);
				workbook.close();
				outputStream.close();
				rep.setList(genBarCodeOrder(listDetail, customerOrder));
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		rep.setPathOrder(pathDomain);
		System.out.println("End exprortOrderCustomer");
		return rep;
	}

	public static void main(String[] args) throws java.io.IOException {
		ExportBomController exp = new ExportBomController();
		try {
			exp.GenBarCode("dfds","C:/Users/User/Desktop/log/file.png");
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// exp.exprortOrderCustomer(null, null);
		// If using Professional version, put your serial key below.
		// SpreadsheetInfo.setLicense("FREE-LIMITED-KEY");
		// String path =
		// "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/Template.xlsx";
		// ExcelFile workbook = ExcelFile.load(path);
		//
		// int workingDays = 8;
		//
		// LocalDateTime startDate = LocalDateTime.now().plusDays(-workingDays);
		// LocalDateTime endDate = LocalDateTime.now();
		//
		// ExcelWorksheet worksheet = workbook.getWorksheet(0);
		//
		// // Find cells with placeholder text and set their values.
		// RowColumn rowColumnPosition;
		// if ((rowColumnPosition = worksheet.getCells().findText("[Company
		// Name]", true, true)) != null)
		// worksheet.getCell(rowColumnPosition.getRow(),
		// rowColumnPosition.getColumn()).setValue("ACME Corp");
		// if ((rowColumnPosition = worksheet.getCells().findText("[Company
		// Address]", true, true)) != null)
		// worksheet.getCell(rowColumnPosition.getRow(),
		// rowColumnPosition.getColumn())
		// .setValue("240 Old Country Road, Springfield, IL");
		// if ((rowColumnPosition = worksheet.getCells().findText("[Start
		// Date]", true, true)) != null)
		// worksheet.getCell(rowColumnPosition.getRow(),
		// rowColumnPosition.getColumn()).setValue(startDate);
		// if ((rowColumnPosition = worksheet.getCells().findText("[End Date]",
		// true, true)) != null)
		// worksheet.getCell(rowColumnPosition.getRow(),
		// rowColumnPosition.getColumn()).setValue(endDate);
		//
		// // Copy template row.
		// int row = 17;
		// worksheet.getRows().insertCopy(row + 1, workingDays - 1,
		// worksheet.getRow(row));
		//
		// // Fill inserted rows with sample data.
		// Random random = new Random();
		// for (int i = 0; i < workingDays; i++) {
		// ExcelRow currentRow = worksheet.getRow(row + i);
		// currentRow.getCell(1).setValue(startDate.plusDays(i));
		// currentRow.getCell(2).setValue(random.nextInt(11) + 1);
		// }
		//
		// // Calculate formulas in worksheet.
		// worksheet.calculate();
		//
		// workbook.save("E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/Template
		// Use.xlsx");
	}

}
