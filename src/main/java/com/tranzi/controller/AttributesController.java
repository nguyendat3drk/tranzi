package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.PagerData;
import com.tranzi.service.AttributesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/attributes")
@SuppressWarnings("unchecked")  
public class AttributesController {
	
	@Autowired
	AttributesService attributesService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject  re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(attributesService.getAlls());
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			re.setErrorMessage("có lỗi xảy ra");
		}
		return re;
	}
	
	@RequestMapping(value = "/pagerAllAttributes", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(attributesService.getPageAttributes(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(attributesService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody Attributes dto) {
		ResponseObject  re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(attributesService.create(dto));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage("Not connect");
		}
		
		return re;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject update(@RequestBody Attributes dto) {
		ResponseObject res = new ResponseObject();
		try {
			return attributesService.update(dto);	
//			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}
		
		return res;
	}
	@RequestMapping(value = "/getPackageTypesId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(attributesService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject delete(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return attributesService.delete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("not connect server");
		}
		
		return res;
	}
	@RequestMapping(value = "/getListCategoryByAttribute/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<Long> getListCategoryByAttribute(@PathVariable Integer id) {
		ResponseObject res = new ResponseObject();
		try {
			return attributesService.getListCategoryByattribute(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("not connect server");
		}
		return null;
	}
	
	@RequestMapping(value = "/getAttributeByCategory/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAttributeByCategory(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(attributesService.getAttributeByCategory(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/updateOrder", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateOrder(@RequestBody List<Attributes> list) {
		ResponseObject res = new ResponseObject();
		try {
			
			res.setObject(attributesService.updateOrder(list));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}
	@RequestMapping(value = "/fixAttribute", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject fixAttribute() {
		ResponseObject res = new ResponseObject();
		try {
			
			res.setObject(attributesService.fixAttribute());
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}

}
