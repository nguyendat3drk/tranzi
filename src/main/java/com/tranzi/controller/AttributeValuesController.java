package com.tranzi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.PagerData;
import com.tranzi.service.AttributeValuesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/attributevalues")
@SuppressWarnings("unchecked")  
public class AttributeValuesController {
	
	@Autowired
	AttributeValuesService attributeValuesService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject  re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(attributeValuesService.getAlls());
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			re.setErrorMessage("có lỗi xảy ra");
		}
		return re;
	}
	
	@RequestMapping(value = "/pagerAllAttributes", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(attributeValuesService.getPageAttributes(page, pageSize, columnName, typeOrder, search, 0));
			pa.setTotalRow(attributeValuesService.getCountTotalPage(page, pageSize, columnName, typeOrder, search, 0));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}
	@RequestMapping(value = "/pagerAllAttributesByID", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject pagerAllAttributesByID(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "id", required = false) long id) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(attributeValuesService.getPageAttributes(page, pageSize, columnName, typeOrder, search, id));
			pa.setTotalRow(attributeValuesService.getCountTotalPage(page, pageSize, columnName, typeOrder, search, id));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody AttributeValues dto) {
		ResponseObject  re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(attributeValuesService.create(dto));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage("Not connect");
		}
		
		return re;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updatePackageTypes(@RequestBody AttributeValues dto) {
		ResponseObject res = new ResponseObject();
		try {
			return attributeValuesService.update(dto);	
//			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			res.setErrorMessage("Not connect");
			e.printStackTrace();
		}
		
		return res;
	}
	@RequestMapping(value = "/getPackageTypesId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(attributeValuesService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		
		return res;
	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getDeletePackageTypesID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return attributeValuesService.getDelete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		
		return res;
	}
}
