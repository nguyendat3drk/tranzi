package com.tranzi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.InventoryArea;
import com.tranzi.model.PagerData;
import com.tranzi.service.InventoryAreaService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/inventoryArea")
@SuppressWarnings("unchecked")
public class InventoryAreaController {

	@Autowired
	InventoryAreaService InventoryAreaService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(InventoryAreaService.getAlls());
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(204);
			re.setErrorMessage("có lỗi xảy ra");
		}
		return re;
	}

	@RequestMapping(value = "/getAllPage", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(InventoryAreaService.getPageInventoryArea(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(InventoryAreaService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	@RequestMapping(value = "/addupdate", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody InventoryArea dto) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			re.setObject(InventoryAreaService.addupdate(dto));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage("Not connect");
		}

		return re;
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(InventoryAreaService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject delete(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return InventoryAreaService.delete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage("not connect server");
		}

		return res;
	}
}
