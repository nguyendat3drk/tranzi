package com.tranzi.controller;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tranzi.common.CommonConfig;
import com.tranzi.dao.AuthenticationDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.entity.Token;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.model.PriceProductImportData;
import com.tranzi.model.Product;
import com.tranzi.model.ProductsPrices;
import com.tranzi.service.AccountService;
import com.tranzi.service.AuthenticationService;
import com.tranzi.service.CategoriesService;
import com.tranzi.service.ProductPriceService;
import com.tranzi.service.ProductService;
import com.tranzi.service.SettingItemService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/listPrice")
public class ImportListPriceController {

	@Autowired
	AuthenticationDAO authenticationDao;

	@Autowired
	AccountService accountService;

	@Autowired
	SettingItemService contactCenterService;

	@Autowired
	AuthenticationService authenService;

	@Autowired
	ProductService productService;

	@Autowired
	ProductPriceService productPriceService;

	@Autowired
	CategoriesService categoriesService;

	public Token checkAuthor(String myHeader) {
		if (myHeader == null || myHeader.equals("")) {
			return null;
		} else {
			Token myToken = new JWTokenUtility().validate(myHeader);
			if (myToken == null) {
				return null;
			} else {
				return myToken;
			}
		}
	}

	private boolean checkAuthentication(String mToken) {
		Token token = checkAuthor(mToken);
		if (token != null) {
			String currentSession = token.getId() + token.getIssueAt();
			System.out.println("currentSession: " + currentSession);
			boolean isUserSession = authenService.checkUserSession(token.getIssuer(), currentSession);
			if (isUserSession) {
				return true;
			} else {
				// return new ResponseEntity(roleService.getAllAgents(0),
				// HttpStatus.OK);
				return false;
			}
		} else {
			return false;
		}
	}

	// upload list product
	@RequestMapping(value = "/uploadxls", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject importProduct(@RequestParam("files") MultipartFile[] uploadfiles) {
		// ImportXlsxProduct up = new ImportXlsxProduct();
		return importData(uploadfiles);
	}

	public ResponseObject importData(MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<PriceProductImportData> listPrice = new ArrayList<PriceProductImportData>();
		System.out.println("=========Start Doc File Import list price====" + (new Date()).toString());
		for (MultipartFile file : uploadfiles) {
			try {
				Workbook workbook = new XSSFWorkbook(file.getInputStream());
				List<PriceProductImportData> listrequestDTO = new ArrayList<PriceProductImportData>();
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					PriceProductImportData requestDTO = new PriceProductImportData();
					Sheet datatypeSheet = workbook.getSheetAt(i);
					DataFormatter fmt = new DataFormatter();
					Iterator<Row> iterator = datatypeSheet.iterator();
					Row firstRow = iterator.next();
					String sku = "";
					sku = firstRow.getCell(0) + "";
					// add name category theo sheetName
					// requestDTO.setsku(workbook.getSheetName(i));
					// add category name theo A1
					int maxFor = 1;
					System.out
							.println("Check data sku :" + sku + ";so luong data xlsx:" + workbook.getNumberOfSheets());
					if ("LK".toUpperCase().equals(sku != null ? sku.toUpperCase().trim() : null)) {
						// continue;
						for (int j = 1; j < 50; j++) {
							if (!"".equals(firstRow.getCell(j) + "") && "StartDate".toUpperCase()
									.equals((firstRow.getCell(j) + "").toUpperCase().trim() + "")) {
								maxFor = j;
							}
						}
					}
					requestDTO.setSku(sku);

					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						PriceProductImportData dto = new PriceProductImportData();
						System.out.println("check data LK:" + currentRow.getCell(0));

						System.out.println("Vào data LK:" + currentRow.getCell(0));
						dto.setSku(currentRow.getCell(0) != null ? (currentRow.getCell(0) + "").trim() : "");
						String listPriceString = "";
						for (int j = 1; j < maxFor; j++) {
							if (currentRow.getCell(j) != null && (currentRow.getCell(j) + "").trim() != null) {
								listPriceString = listPriceString + ";" + currentRow.getCell(j);
							}
						}
						dto.setNumberPrice(listPriceString);
						dto.setStartDate(
								currentRow.getCell(maxFor) != null ? (currentRow.getCell(maxFor) + "").trim() : "");
						dto.setEndDate(currentRow.getCell(maxFor + 1) != null
								? (currentRow.getCell(maxFor + 1) + "").trim() : "");

						if (dto.getSku() != null && !"".equals(dto.getSku())) {
							listPrice.add(dto);
						}

					}

				}

				System.out.println(listPrice);
				workbook.close();
				if (listPrice != null && listPrice.size() > 0) {
					re = addProduct(listPrice);
					// try {
					// resetChmod(CommonConfig.uploadMedia);
					// } catch (Exception e) {
					// // TODO: handle exception
					// e.printStackTrace();
					// }
					return re;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		re.setResultCode(200);
		re.setObject(0);
		return re;
	}

	public void resetChmod(String path) {
		try {
			Process p = Runtime.getRuntime().exec("chmod -R 777 " + path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// Luu Object to database;
	public ResponseObject addProduct(List<PriceProductImportData> list) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		int countProduc = 0;
		if (list != null && list.size() > 0) {
			for (PriceProductImportData dto : list) {

				if (dto.getSku() != null && dto.getNumberPrice() != null) {
					List<String> responPrice = new ArrayList<String>();
					String imageList = dto.getNumberPrice();
					if (imageList != null && !"".equals(imageList)) {
						String price[] = imageList.split(";");
						if (price != null && price.length > 0) {
							productPriceService.deleteByProductSku(dto.getSku());
							int i = 0;
							for (String dtoPrice : price) {
								if (dtoPrice != null && !"".equals(dtoPrice)) {

									String dprice[] = dtoPrice.split(">");
									ProductsPrices pr = new ProductsPrices();
									pr.setId(0);
									Product pro = new Product();
									// pro =
									// productService.getByLK(dto.getSku(),
									// dto.getStartDate(), dto.getEndDate());
									if(dprice[1]!=null && Integer.parseInt(dprice[1])>0){
										pro = productService.getByLKPrice(dto.getSku(), Integer.parseInt(dprice[1]), i);
									}else{
										pro = productService.getByLKPrice(dto.getSku(),0, 2);	
									}
									

									if (pro != null && pro.getId() > 0) {
										pr.setProductId(Integer.parseInt(pro.getId() + ""));
										pr.setMinQuantity(0);
										pr.setPrice(0);
										try {
											pr.setMinQuantity(dprice[0] != null ? Integer.parseInt(dprice[0]) : 0);
											pr.setPrice(dprice[1] != null ? Integer.parseInt(dprice[1]) : 0);
										} catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
										}
									}
									productPriceService.create(pr);

								}
								i++;
							}
						}
					}

				} else {

					re.setResultCode(201);
					re.setErrorMessage("Cấu trúc import không đúng");
				}
			}
		}
		re.setObject(countProduc);
		return re;
	}

}
