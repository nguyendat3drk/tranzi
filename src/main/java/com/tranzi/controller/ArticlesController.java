package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Articles;
import com.tranzi.model.PagerData;
import com.tranzi.model.respontOrder.SelectLinkFile;
import com.tranzi.service.ArticlesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/articles")
public class ArticlesController {
	@Autowired
	ArticlesService articlesService;

	private List<Integer> listId = new ArrayList<>();

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAlls() {
		List<Articles> list = articlesService.getAlls();
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllRespont", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAllRespont() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		re.setObject(articlesService.getAlls());
		return re;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatVideos(@RequestBody Articles dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(articlesService.create(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateVideos(@RequestBody Articles dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(articlesService.update(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
			// TODO: handle exception
		}

		return res;
	}


	@RequestMapping(value = "/pagerAllArticles", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(articlesService.getPageVideos(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(articlesService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			re.setResultCode(201);
		}
		return re;
	}

	
	@RequestMapping(value = "/getArticlesId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(articlesService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
	

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject delete(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return articlesService.deleteVideos(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		return res;
	}
	@RequestMapping(value = "/checkArticleProeduct/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject checkArticleProeduct(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(articlesService.checkArticleProeduct(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
	@RequestMapping(value = "/getListFileArticleUpload/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getListFileArticleUpload(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(articlesService.getListFileArticleUpload(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
	@RequestMapping(value = "/getByIdProductLinkUpload/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getByIdProductLinkUpload(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(articlesService.getByIdProductLinkUpload(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
	
	@RequestMapping(value = "/getSearchArticleLinkUpload", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getSearchArticleLinkUpload(@RequestBody SelectLinkFile selectLinkFile){
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(articlesService.getSearchArticleLinkUpload(selectLinkFile));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
}
