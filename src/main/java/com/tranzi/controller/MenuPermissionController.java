package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.model.GroupACD;
import com.tranzi.model.MenuPermission;
import com.tranzi.service.MenuPermissionService;

@RestController
@RequestMapping(value = "/menupermission")
public class MenuPermissionController {
	@Autowired
	MenuPermissionService menupermissionservice;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getRoles() {
		List<MenuPermission> listOfRoles = menupermissionservice.getAllAgents();
		return new ResponseEntity(listOfRoles, HttpStatus.OK);
	}


	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getAllPermission", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAllAgents() {
		if (menupermissionservice.getAllAgents() != null)
			return new ResponseEntity(menupermissionservice.getAllAgents(), HttpStatus.OK);
		else
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> addMenuPermission(@RequestBody MenuPermission menupermission,
			@RequestHeader(value = "token", required = true) String authoToken) {
		// System.out.println("My Header: " + authoToken);
		if (checkAuthor(authoToken) == 0) {
			return new ResponseEntity(null, HttpStatus.FORBIDDEN);
		} else {
			System.out.println(menupermission);
			return menupermissionservice.addMenupermission(menupermission);
		}
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/delete/{ids}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteRole(@PathVariable("ids") int ids,
			@RequestHeader(value = "token", required = true) String authoToken) {
		if (checkAuthor(authoToken) == 0) {
			return new ResponseEntity(null, HttpStatus.FORBIDDEN);
		} else {
			return menupermissionservice.deletePermission(ids);
		}
	}
//	@CrossOrigin(origins = "*/")
//	@RequestMapping(value = "/persionRole/{ids}",  method = RequestMethod.GET, headers = "Accept=application/json")
//	public List<MenuPermission> getPermissionRole(@PathVariable("ids") int ids,
//			@RequestHeader(value = "token", required = true) String authoToken) {
//		List<MenuPermission> list = new ArrayList<MenuPermission>();
//		if (checkAuthor(authoToken) == 0) {
//			return list;
//		} else {
//			return menupermissionservice.getPermissionRole(ids);
//		}
//	}
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/persionRole/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<MenuPermission> getPermissionRole(@PathVariable int id) {
		return menupermissionservice.getPermissionRole(id);
	}
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getPerStatistic/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<MenuPermission> getPerStatistic(@PathVariable int id) {
		return menupermissionservice.getPerStatistic(id);
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	public MenuPermission updatePermissionRole(@RequestBody List<MenuPermission> menupermission) {
		return menupermissionservice.updatePermission(menupermission);
	}

	public int checkAuthor(String myHeader) {
//		if (myHeader == null || myHeader.equals("")) {
//			return 0;
//		} else {
//			Token myToken = new JWTokenUtility().validate(myHeader);
//			// String myToken = new MD5EnCrypt().getMD5()
//			if (myToken == null) {
//				return 0;
//			} else {
				return 1;
//			}
//		}
	}
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/persionRealTime/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<MenuPermission> getPermissionRealTime(@PathVariable int id) {
		return menupermissionservice.getPermissionRealTime(id);
	}
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/getPerDashBoard/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<MenuPermission> getPerDashBoard(@PathVariable int id) {
		return menupermissionservice.getPerDashBoard(id);
	}


}
