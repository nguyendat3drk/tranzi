package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PagerData;
import com.tranzi.model.Terms;
import com.tranzi.model.TreeCategory;
import com.tranzi.model.TreeNode;
import com.tranzi.service.TermsService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/terms")
public class TermsController {
	@Autowired
	TermsService termsService;

	private List<Integer> listId = new ArrayList<>();

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAlls() {
		List<Terms> list = termsService.getAlls();
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllRespont", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAllRespont() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		re.setObject(termsService.getAlls());
		return re;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatTerms(@RequestBody Terms dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(termsService.create(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}
	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateTerms(@RequestBody Terms dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(termsService.update(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}


	@RequestMapping(value = "/pagerAllTerms", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "level", required = false) int level) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(termsService.getPageTerms(page, pageSize, columnName, typeOrder, search, level));
			pa.setTotalRow(termsService.getCountTotalPage(page, pageSize, columnName, typeOrder, search, level));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	
	@RequestMapping(value = "/getTermsId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(termsService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject delete(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return termsService.deleteTerms(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		return res;
	}
//	GET DANH SACH TERM CHA
	
}
