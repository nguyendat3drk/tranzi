package com.tranzi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Categories;
import com.tranzi.model.PagerData;
import com.tranzi.model.TreeCategory;
import com.tranzi.model.TreeNode;
import com.tranzi.service.CategoriesService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/categories")
public class CategoriesController {
	@Autowired
	CategoriesService categoriesService;

	private List<Integer> listId = new ArrayList<>();

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAlls() {
		List<Categories> list = categoriesService.getAlls();
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllRespont", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAllRespont() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		re.setObject(categoriesService.getAlls());
		return re;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatCategories(@RequestBody Categories dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(categoriesService.create(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}

	@RequestMapping(value = "/pagerAllCategories", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(categoriesService.getPageCategories(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(categoriesService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateCategories(@RequestBody Categories dto) {
		ResponseObject res = new ResponseObject();
		try {
			if (dto.getSlug() != null) {
				dto.setSlug(dto.getSlug().trim());
			}
			res.setObject(categoriesService.update(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}
	@RequestMapping(value = "/updateOrder", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateOrder(@RequestBody List<Categories> list) {
		ResponseObject res = new ResponseObject();
		try {
			
			res.setObject(categoriesService.updateOrder(list));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}
	

	@RequestMapping(value = "/getCategoriesId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(categoriesService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/deleteALL", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getDeleteAllCategori() {
		ResponseObject res = new ResponseObject();
		try {
			return categoriesService.deleteAllCategori();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		return res;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteCategori(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return categoriesService.deleteCategori(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		return res;
	}

	@RequestMapping(value = "/getListParentCategories", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getListParentCategories() {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(categoriesService.getListParentCategories());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/getListChildrenCategories", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getListChildrenCategories() {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(categoriesService.getListChildrenCategories());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/getTreeNode/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getTree(@PathVariable int id) {
		List<Categories> listParent = new ArrayList<>();
		List<Categories> listChildren = new ArrayList<>();
		List<TreeNode> listTreeNode = new ArrayList<>();
		ResponseObject reponse = new ResponseObject();
		reponse.setResultCode(200);
		try {
			listParent = categoriesService.getListParentCategories();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			listChildren = categoriesService.getListChildrenCategories();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<Categories> listCheck = listParent;
		if (listParent != null && listParent.size() > 0) {
			TreeNode tr = new TreeNode();
			int max = 0;
			Boolean disable = true;
			max = listParent.size();
			for (int i = 0; i < max; i++) {
				if (listParent.get(i).getId() == id && id > 0) {
					disable = false;
				} else {
					disable = true;
				}
				if (!listId.contains(listParent.get(i).getId())) {
					tr = new TreeNode();
					tr.setLabel(listParent.get(i).getName());
					tr.setData(listParent.get(i).getId());
					tr.setExpanded(false);
					tr.setLevel(listParent.get(i).getOrder());
					tr.setChildren(setChildren(listParent.get(i).getId(), listChildren, listCheck, disable, id));
					if (listId == null || !listId.contains(listParent.get(i).getId())) {
						tr.setSelectable(disable);
						listTreeNode.add(tr);
					}

				}

			}
		}
		reponse.setObject(listTreeNode);
		return reponse;
	}

	public List<TreeNode> setChildren(long id, List<Categories> list, List<Categories> listParentCheck, Boolean disable,
			int ids) {
		List<TreeNode> liTree = new ArrayList<>();
		if (list != null && list.size() > 0) {
			TreeNode tr = new TreeNode();
			Boolean check;
			check = disable;
			int idsss = ids;

			// for (Categories ca : list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == idsss && idsss > 0 && check) {
					check = false;
				} else {
					check = true;
				}
				if (!listId.contains(list.get(i).getId())) {
					tr = new TreeNode();
					if (list.get(i).getParentId() == id) {
						listId.add((int) list.get(i).getId());
						if (listParentCheck.contains(list.get(i)) || listId == null
								|| listId.contains(list.get(i).getId())) {
							break;
						} else {
							listId.add((int) list.get(i).getId());
							listParentCheck.add(list.get(i));
							tr.setLabel(list.get(i).getName());
							tr.setData(list.get(i).getId());
							tr.setSelectable(check);
							tr.setLevel(list.get(i).getOrder());
							tr.setChildren(setChildren(list.get(i).getId(), list, listParentCheck, check, idsss));
							tr.setExpanded(false);
							liTree.add(tr);
						}
					}
				}

			}
		}
		return liTree;
	}

	@RequestMapping(value = "/testAPI", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public boolean testAPI() {
		return true;
	}

	// tree table

	@RequestMapping(value = "/gettreeTable/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject gettreeTable(@PathVariable int id) {
		List<Categories> listParent = new ArrayList<>();
		List<Categories> listChildren = new ArrayList<>();
		List<TreeNode> listTreeNode = new ArrayList<>();
		ResponseObject reponse = new ResponseObject();
		reponse.setResultCode(200);
		try {
			listParent = categoriesService.getListParentCategories();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			listChildren = categoriesService.getListChildrenCategories();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<Categories> listCheck = listParent;
		if (listParent != null && listParent.size() > 0) {
			TreeNode tr = new TreeNode();
			int max = 0;
			Boolean disable = true;
			max = listParent.size();
			for (int i = 0; i < max; i++) {
				if (listParent.get(i).getId() == id && id > 0) {
					disable = false;
				} else {
					disable = true;
				}
				if (!listId.contains(listParent.get(i).getId())) {
					tr = new TreeNode();
					tr.setLabel(listParent.get(i).getName());
					TreeCategory trDTO = new TreeCategory();
					trDTO.setId(listParent.get(i).getId());
					trDTO.setName(listParent.get(i).getName());
					trDTO.setSize(listParent.get(i).getCountProduct());
					trDTO.setNameEn(listParent.get(i).getNameEn());
					tr.setData(trDTO);
					tr.setExpanded(false);
					
					tr.setLevel(listParent.get(i).getOrder());
					tr.setChildren(setChildrenTable(listParent.get(i).getId(), listChildren, listCheck, disable, id));
					if (listId == null || !listId.contains(listParent.get(i).getId())) {
						tr.setSelectable(disable);
						listTreeNode.add(tr);
					}

				}

			}
		}
		reponse.setObject(listTreeNode);
		return reponse;
	}

	public List<TreeNode> setChildrenTable(long id, List<Categories> list, List<Categories> listParentCheck,
			Boolean disable, int ids) {
		List<TreeNode> liTree = new ArrayList<>();
		if (list != null && list.size() > 0) {
			TreeNode tr = new TreeNode();
			Boolean check;
			check = disable;
			int idsss = ids;

			// for (Categories ca : list) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getId() == idsss && idsss > 0 && check) {
					check = false;
				} else {
					check = true;
				}
				if (!listId.contains(list.get(i).getId())) {
					tr = new TreeNode();
					if (list.get(i).getParentId() == id) {
						listId.add((int) list.get(i).getId());
						if (listParentCheck.contains(list.get(i)) || listId == null
								|| listId.contains(list.get(i).getId())) {
							break;
						} else {
							listId.add((int) list.get(i).getId());
							listParentCheck.add(list.get(i));
							tr.setLabel(list.get(i).getName());
//							tr.setData(list.get(i));
							TreeCategory trDTO = new TreeCategory();
							trDTO.setId(list.get(i).getId());
							trDTO.setName(list.get(i).getName());
							trDTO.setSize(list.get(i).getCountProduct());
							trDTO.setNameEn(list.get(i).getNameEn());
							tr.setData(trDTO);
							tr.setSelectable(check);
							tr.setLevel(list.get(i).getOrder());
							tr.setChildren(setChildren(list.get(i).getId(), list, listParentCheck, check, idsss));
							tr.setExpanded(false);
							liTree.add(tr);
						}
					}
				}

			}
		}
		return liTree;
	}
	@RequestMapping(value = "/getParentTree", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getParentTree() {
		List<Categories> listParent = new ArrayList<>();
		List<TreeNode> listTreeNode = new ArrayList<>();
		ResponseObject reponse = new ResponseObject();
		reponse.setResultCode(200);
		try {
			listParent = categoriesService.getListParentCategories();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<Categories> listCheck = listParent;
		if (listParent != null && listParent.size() > 0) {
			TreeNode tr = new TreeNode();
			int max = 0;
			Boolean disable = true;
			max = listParent.size();
			for (int i = 0; i < max; i++) {
				if (!listId.contains(listParent.get(i).getId())) {
					tr = new TreeNode();
					tr.setLabel(listParent.get(i).getName());
					TreeCategory trDTO = new TreeCategory();
					trDTO.setId(listParent.get(i).getId());
					trDTO.setName(listParent.get(i).getName());
					trDTO.setSize(listParent.get(i).getCountProduct());
					trDTO.setNameEn(listParent.get(i).getNameEn());
					tr.setData(trDTO);
					tr.setExpanded(false);			
					tr.setLevel(listParent.get(i).getOrder());
					if (listId == null || !listId.contains(listParent.get(i).getId())) {
						listTreeNode.add(tr);
					}

				}

			}
		}
		reponse.setObject(listTreeNode);
		return reponse;
	}
	@RequestMapping(value = "/getParentTreeChilden/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getParentTreeChilden(@PathVariable int id) {
		List<Categories> listParent = new ArrayList<>();
		List<TreeNode> listTreeNode = new ArrayList<>();
		ResponseObject reponse = new ResponseObject();
		reponse.setResultCode(200);
		try {
			listParent = categoriesService.getParentTreeChildenById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<Categories> listCheck = listParent;
		if (listParent != null && listParent.size() > 0) {
			TreeNode tr = new TreeNode();
			int max = 0;
			Boolean disable = true;
			max = listParent.size();
			for (int i = 0; i < max; i++) {
				if (!listId.contains(listParent.get(i).getId())) {
					tr = new TreeNode();
					tr.setLabel(listParent.get(i).getName());
					TreeCategory trDTO = new TreeCategory();
					trDTO.setId(listParent.get(i).getId());
					trDTO.setName(listParent.get(i).getName());
					trDTO.setSize(listParent.get(i).getCountProduct());
					trDTO.setNameEn(listParent.get(i).getNameEn());
					tr.setData(trDTO);
					tr.setExpanded(false);			
					tr.setLevel(listParent.get(i).getOrder());
					if (listId == null || !listId.contains(listParent.get(i).getId())) {
						listTreeNode.add(tr);
					}

				}

			}
		}
		reponse.setObject(listTreeNode);
		return reponse;
	}


}
