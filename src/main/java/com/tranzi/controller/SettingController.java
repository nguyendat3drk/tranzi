package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PagerData;
import com.tranzi.model.Setting;
import com.tranzi.service.SettingService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/setting")
public class SettingController {

	@Autowired
	SettingService settingService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> getAlls() {
		List<Setting> list = settingService.getAlls();
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/pagerAllSetting", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(settingService.getPageSetting(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(settingService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseObject createSetting(@RequestBody Setting dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(settingService.createSetting(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseObject updateSetting(@RequestBody Setting dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(settingService.updateSetting(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(settingService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}
	//
	//
	// @RequestMapping(value = "/update", method = RequestMethod.PUT, headers =
	// "Accept=application/json")
	// @ResponseBody
	// public ResponseObject updateCategories(@RequestBody Categories dto) {
	// ResponseObject res = new ResponseObject();
	// try {
	// if (dto.getSlug() != null) {
	// dto.setSlug(dto.getSlug().trim());
	// }
	// res.setObject(categoriesService.update(dto));
	// res.setResultCode(200);
	// } catch (Exception e) {
	// // TODO: handle exception
	// res.setResultCode(204);
	// e.printStackTrace();
	// }
	//
	// return res;
	// }
	//

	//
	// return res;
	// }
	//
	// @RequestMapping(value = "/deleteALL", method = RequestMethod.DELETE,
	// headers = "Accept=application/json")
	// @ResponseBody
	// public ResponseObject getDeleteAllCategori() {
	// ResponseObject res = new ResponseObject();
	// try {
	// return categoriesService.deleteAllCategori();
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// res.setResultCode(204);
	// res.setErrorMessage(e.getMessage());
	// }
	// return res;
	// }
	//
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject deleteCategori(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return settingService.deleteByID(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}
		return res;
	}

}
