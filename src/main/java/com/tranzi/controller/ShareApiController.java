package com.tranzi.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tranzi.common.AddImageWatermark;
import com.tranzi.common.CommonConfig;
import com.tranzi.common.ConvetSlug;
import com.tranzi.common.ExcelWriter;
import com.tranzi.dao.AuthenticationDAO;
import com.tranzi.entity.KeyValue;
import com.tranzi.entity.KeyValueDTO;
import com.tranzi.entity.LoginView;
import com.tranzi.entity.ResponseObject;
import com.tranzi.entity.Token;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.model.Account;
import com.tranzi.model.Categories;
import com.tranzi.model.CustomerOrderProduct;
import com.tranzi.model.GroupACD;
import com.tranzi.model.ImportAttribute;
import com.tranzi.model.Media;
import com.tranzi.model.Product;
import com.tranzi.model.ProductImportData;
import com.tranzi.model.RequestCategory;
import com.tranzi.model.RequestProduct;
import com.tranzi.model.User;
import com.tranzi.service.AccountService;
import com.tranzi.service.AuthenticationService;
import com.tranzi.service.CategoriesService;
import com.tranzi.service.CustomerOrderProductService;
import com.tranzi.service.CustomerService;
import com.tranzi.service.GroupService;
import com.tranzi.service.ProductService;
import com.tranzi.service.RoleService;
import com.tranzi.service.SettingItemService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/share-api")
public class ShareApiController {

	@Autowired
	AuthenticationDAO authenticationDao;

	@Autowired
	CustomerService queueService;

	@Autowired
	AccountService accountService;

	@Autowired
	RoleService roleService;

	@Autowired
	GroupService groupService;

	@Autowired
	SettingItemService contactCenterService;

	@Autowired
	AuthenticationService authenService;

	@Autowired
	ProductService productService;

	@Autowired
	CategoriesService categoriesService;

	@Autowired
	CustomerOrderProductService customerOrderProductService;

	@RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseObject addAccount(@RequestBody Account account) {
		// System.out.println("My Header: " + authoToken);
		ResponseObject re = new ResponseObject();
		re.setResultCode(201);
		re.setObject(account);
		int intCheck = 0;
		try {
			return accountService.addAccount(account);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return re;

	}

	@RequestMapping(value = "/getRowCountSearchExceptList", method = RequestMethod.GET, headers = "Accept=application/json")
	public int getRowCountSearchExceptList(@RequestParam(value = "userSearch", required = false) String userSearch,
			@RequestParam(value = "lstExceptIds", required = false) String exceptList) {
		// return accountService.getRowCountSearchExceptList(userSearch,
		// exceptList);
		return 10000;
	}

	@RequestMapping(value = "/getAllsGroupFull", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<GroupACD> getAllsGroupFull() {
		return groupService.getAllsGroupFull();
	}

	@RequestMapping(value = "/getAllsGroup", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<GroupACD> getAllsGroup() {
		return groupService.getAllsGroup();
	}

	@RequestMapping(value = "/getGroupCombo", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<KeyValueDTO> getGroupCombo() {
		return groupService.getGroupCombo();
	}

	@RequestMapping(value = "/getAllRoles", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> getAllAgents() {
		if (roleService.getAllAgents(1) != null)
			return new ResponseEntity(roleService.getAllAgents(1), HttpStatus.OK);
		else
			return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/checkUsedPassword", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getUsedPassword(@RequestBody User account) {
		return accountService.checkUsedPassword(account);
	}

	// change password
	@RequestMapping(value = "/change-password", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseObject changePassword(@RequestBody User account) {
		return accountService.changePassword(account);
	}

	@RequestMapping(value = "/getUsersByGroupId/{groupId}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public List<User> getUsersByGroupId(@PathVariable int groupId) {
		List<User> listUsersOfGroup = groupService.getUsersByGroupId(groupId);
		return listUsersOfGroup;
	}

	public Token checkAuthor(String myHeader) {
		if (myHeader == null || myHeader.equals("")) {
			return null;
		} else {
			Token myToken = new JWTokenUtility().validate(myHeader);
			if (myToken == null) {
				return null;
			} else {
				return myToken;
			}
		}
	}

	private boolean checkAuthentication(String mToken) {
		Token token = checkAuthor(mToken);
		if (token != null) {
			String currentSession = token.getId() + token.getIssueAt();
			System.out.println("currentSession: " + currentSession);
			boolean isUserSession = authenService.checkUserSession(token.getIssuer(), currentSession);
			if (isUserSession) {
				return true;
			} else {
				// return new ResponseEntity(roleService.getAllAgents(0),
				// HttpStatus.OK);
				return false;
			}
		} else {
			return false;
		}
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET, headers = "Accept=application/json")
	public void Test() {
		new CommonConfig().loadConfig();
		System.out.println("test!");
	}

	private void skipSSL() {
		try {
			TrustManager[] trustAllCerts = { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			SSLContext sc = SSLContext.getInstance("SSL");

			HostnameVerifier hv = new HostnameVerifier() {

				public boolean verify(String arg0, SSLSession arg1) {
					return true;
				}
			};
			sc.init(null, trustAllCerts, new SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
		} catch (

		Exception localException) {
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseObject checkLogin(@RequestBody LoginView accountLogin) {
		return authenticationDao.checkLogin(accountLogin);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadFileMulti(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String nameForder, @RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();

		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadFile + parts);
				fileOuputStream.write(bytes);
				try {
					AddImageWatermark aw = new AddImageWatermark();
					aw.addLogo(CommonConfig.uploadFile + parts);

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				resetChmod(CommonConfig.uploadFile);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return re;
	}

	@RequestMapping(value = "/uploadImageManufactures", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadImageManufactures(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String nameForder, @RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();

		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadImageManufactures + parts);
				fileOuputStream.write(bytes);
				resetChmod(CommonConfig.uploadImageManufactures);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return re;
	}

	@RequestMapping(value = "/uploadImageCategory", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadImageCategory(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String nameForder, @RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();

		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadImageCategory + parts);
				fileOuputStream.write(bytes);
				resetChmod(CommonConfig.uploadImageCategory);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return re;
	}

	@RequestMapping(value = "/uploadSettingImage", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadSettingImage(@RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();

		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadFile + parts);
				fileOuputStream.write(bytes);
				resetChmod(CommonConfig.uploadFile);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return re;
	}

	@RequestMapping(value = "/uploadProduct", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadProduct(@RequestParam("files") MultipartFile[] uploadfiles) {
		UploadFile up = new UploadFile();
		return up.uploadFileProduct(uploadfiles);
	}

	@RequestMapping(value = "/uploadProductMedia", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadProductMedia(@RequestParam("files") MultipartFile[] uploadfiles) {
		UploadFile up = new UploadFile();
		return up.uploadFileProductMedia(uploadfiles);
	}

	// upload list product
	@RequestMapping(value = "/uploadxls", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject importProduct(@RequestParam("files") MultipartFile[] uploadfiles) {
		// ImportXlsxProduct up = new ImportXlsxProduct();
		return importData(uploadfiles);
	}

	public ResponseObject importData(MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<RequestProduct> list = new ArrayList<RequestProduct>();
		System.out.println("=========Start doc file import product====" + (new Date()).toString());
		for (MultipartFile file : uploadfiles) {
			try {
				Workbook workbook = new XSSFWorkbook(file.getInputStream());
				RequestProduct requestDTO = new RequestProduct();
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {

					Sheet datatypeSheet = workbook.getSheetAt(i);
					DataFormatter fmt = new DataFormatter();
					Iterator<Row> iterator = datatypeSheet.iterator();
					Row firstRow = iterator.next();
					String nameCategory = "";
					nameCategory = firstRow.getCell(0) + "";
					requestDTO = new RequestProduct();
					// add name category theo sheetName
					// requestDTO.setNameCategory(workbook.getSheetName(i));
					// add category name theo A1
					System.out.println("Check data nameCategory:" + nameCategory);

					if ("Datasheets".toUpperCase()
							.equals(nameCategory != null ? nameCategory.toUpperCase().trim() : null)) {
						continue;
					}
					requestDTO.setNameCategory(nameCategory);
					// Cell firstCell = firstRow.getCell(1);
					List<ProductImportData> listOfCustomer = new ArrayList<ProductImportData>();
					List<String> listNameAttribute = new ArrayList<String>();
					boolean check = false;
					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						ProductImportData dto = new ProductImportData();
						if (!"".equals(currentRow.getCell(4) + "") && "Datasheets".toUpperCase()
								.equals((currentRow.getCell(4) + "").toUpperCase().trim() + "")) {
							check = true;
							for (int j = 15; j < 50; j++) {
								if (currentRow.getCell(j) != null && currentRow.getCell(j).getStringCellValue() != null
										&& !"".equals(currentRow.getCell(j).getStringCellValue())) {
									String nameAttribute = currentRow.getCell(j).getStringCellValue();
									listNameAttribute.add(nameAttribute);
								}
							}
						} else if (check) {
							dto.setSeoTitlte(currentRow.getCell(1) != null ? (currentRow.getCell(1) + "").trim() : "");
							dto.setSeoKeyword(currentRow.getCell(2) != null ? (currentRow.getCell(2) + "").trim() : "");
							dto.setSeoDescription(
									currentRow.getCell(3) != null ? (currentRow.getCell(3) + "").trim() : "");

							dto.setDatasheets(currentRow.getCell(4) != null ? (currentRow.getCell(4) + "").trim() : "");
							dto.setImageUrl(currentRow.getCell(5) != null ? (currentRow.getCell(5) + "").trim() : "");
							dto.setProductName(currentRow.getCell(0) != null ? (currentRow.getCell(0) + "").trim()
									: (currentRow.getCell(6) != null ? (currentRow.getCell(6) + "").trim() : ""));
							dto.setSku(currentRow.getCell(6) != null ? (currentRow.getCell(6) + "").trim() : "");
							dto.setOrginSku(currentRow.getCell(7) != null ? (currentRow.getCell(7) + "").trim() : "");
							dto.setManufactureName(
									currentRow.getCell(8) != null ? (currentRow.getCell(8) + "").trim() : "");
							dto.setShortDescription(
									currentRow.getCell(9) != null ? (currentRow.getCell(9) + "").trim() : "");
							dto.setInStock(currentRow.getCell(10) != null ? (currentRow.getCell(10) + "").trim() : "");
							dto.setNormalPrice(
									currentRow.getCell(12) != null ? (currentRow.getCell(12) + "").trim() : "");
							dto.setMinOrder(currentRow.getCell(14) != null ? (currentRow.getCell(14) + "").trim() : "");
							List<ImportAttribute> listAttribute = new ArrayList<>();
							if (listNameAttribute != null) {
								for (int k = 0; k < listNameAttribute.size(); k++) {
									ImportAttribute d = new ImportAttribute();
									d.setParentName(listNameAttribute.get(k));
									d.setAttributeName(currentRow.getCell(15 + k) != null
											? (currentRow.getCell(15 + k) + "").trim() : "");
									listAttribute.add(d);
								}
							}
							dto.setListAttribute(listAttribute);

							if (dto.getSku() != null && !"".equals(dto.getSku()) && dto.getOrginSku() != null
									&& !"".equals(dto.getOrginSku()) && dto.getProductName() != null
									&& !"".equals(dto.getProductName())) {
								listOfCustomer.add(dto);
							}
						}

					}
					if (listOfCustomer != null && listOfCustomer.size() > 0) {
						requestDTO.setData(listOfCustomer);
					}

					list.add(requestDTO);
				}

				// System.out.println(list);
				workbook.close();
				if (list != null && list.size() > 0) {
					System.out.println("=========Bắt đầu ghi product vào database====" + (new Date()).toString());
					re = addProduct(list);
					System.out.println("=========Kết thúc File ghi product vào database====" + (new Date()).toString());
					try {
						resetChmod(CommonConfig.uploadMedia);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					System.out.println("=========Kết thúc File Import file====" + (new Date()).toString());
					return re;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		re.setResultCode(200);
		re.setObject(list != null ? list.size() : 0);
		return re;
	}

	public void resetChmod(String path) {
		try {
			Process p = Runtime.getRuntime().exec("chmod -R 777 " + path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			Process p = Runtime.getRuntime().exec("echo 1 > /proc/sys/vm/drop_caches ");
			Process p2 = Runtime.getRuntime().exec("sudo echo 1 > /proc/sys/vm/drop_caches ");
			// System.out.println("clear cache");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// Luu Object to database;
	public ResponseObject addProduct(List<RequestProduct> list) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		int countProduc = 0;
		if (list != null && list.size() > 0) {
			for (RequestProduct dto : list) {

				if (dto.getNameCategory() != null && dto.getData() != null && dto.getData().size() > 0) {
					long idCategory = 0;
					idCategory = categoriesService.addCategoryImport(dto.getNameCategory());
					if (idCategory > 0) {
						for (ProductImportData p : dto.getData()) {
							Product pro = new Product();
							pro.setName(p.getProductName());
							ConvetSlug cd = new ConvetSlug();
							Date dt = new Date();
							pro.setSlug(cd.toUrlFriendly(p.getProductName(), 1) + dt.getHours() + dt.getMinutes()
									+ dt.getSeconds());
							pro.setCategoryId(Integer.parseInt(idCategory + ""));
							pro.setCreatedTime(dt.getTime() / 1000);
							Double inStock = 0D;// 9
							try {
								inStock = Double.parseDouble(p.getInStock());
							} catch (Exception e) {
								// TODO: handle exception
							}
							Double normalPrice = 0D;// 9
							try {
								normalPrice = Double.parseDouble(p.getNormalPrice());
							} catch (Exception e) {
								// TODO: handle exception
							}
							Integer minOrder = 0;// 10
							try {
								minOrder = Integer.parseInt(p.getMinOrder());
							} catch (Exception e) {
								// TODO: handle exception
							}
							// pro.setInStock(inStock.intValue());
							// tam thoi fix = 1
							pro.setInStock(1);
							if (!"".equals(p.getSeoTitlte()) && p.getSeoTitlte().length() > 254) {
								pro.setSeoTitle(p.getSeoTitlte().substring(0, 254));
							} else {
								pro.setSeoTitle(p.getSeoTitlte());
							}
							if (!"".equals(p.getSeoKeyword()) && p.getSeoKeyword().length() > 254) {
								pro.setSeoKeywords(p.getSeoKeyword().substring(0, 254));
							} else {
								pro.setSeoKeywords(p.getSeoKeyword());
							}
							if (!"".equals(p.getSeoDescription()) && p.getSeoDescription().length() > 254) {
								pro.setSeoDescription(p.getSeoDescription().substring(0, 254));
							} else {
								pro.setSeoDescription(p.getSeoDescription());
							}
							pro.setMinOrder(minOrder);
							pro.setNormalPrice(normalPrice.intValue());
							pro.setSku(p.getSku().toUpperCase());
							pro.setOrginSku(p.getOrginSku().toUpperCase());
							pro.setActived(1);
							pro.setSeoIndex(1);
							if (p.getDatasheets() != null) {
								List<Media> listMediaFile = new ArrayList<>();
								Media media = new Media();
								media.setId(0);
								media.setUrlPath(p.getDatasheets());
								media.setUrlPath(p.getDatasheets());
								media.setType("PDF");
								listMediaFile.add(media);
								pro.setListMedia(listMediaFile);
							}
							if (p.getImageUrl() != null && !"".equals(p.getImageUrl())) {
								ImportXlsxProduct ip = new ImportXlsxProduct();
								List<String> listUrrl = new ArrayList<String>();
								listUrrl = ip.uploadImageImport(p.getImageUrl(), pro.getName());
								if (listUrrl != null && listUrrl.size() > 0) {
									pro.setThumbnailImage(listUrrl.get(0));
									List<Media> listMediaFile = new ArrayList<>();
									for (String u : listUrrl) {
										Media media = new Media();
										media.setId(0);
										media.setUrlPath(u);
										media.setUrlPath(u);
										media.setType("THUMBNAIL");
										listMediaFile.add(media);
									}
									pro.setListMediaThumbnail(listMediaFile);
								}

							}
							try {
								resetChmod(CommonConfig.uploadMedia);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							List<Long> listCategori = new ArrayList<>();
							listCategori.add(idCategory);
							pro.setListCategory(listCategori);
							pro.setShortDescription(p.getShortDescription());
							pro.setManufactureName(p.getManufactureName());
							ResponseObject repons = new ResponseObject();
							try {
								repons = productService.createImport(pro, p.getListAttribute());
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}

							if (repons != null && repons.getResultCode() == 200) {
								countProduc++;
							}

						}
					}

				} else {

					re.setResultCode(201);
					re.setErrorMessage("Cấu trúc import không đúng");
				}
			}
		}
		re.setObject(countProduc);
		return re;
	}

	// import update product by ma NSX
	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject updateProduct(@RequestParam("files") MultipartFile[] uploadfiles) {
		return updateProductByCode(uploadfiles);
	}

	public ResponseObject updateProductByCode(MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<Product> list = new ArrayList<Product>();
		for (MultipartFile file : uploadfiles) {
			try {
				Workbook workbook = new XSSFWorkbook(file.getInputStream());
				Product requestDTO = new Product();
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {

					Sheet datatypeSheet = workbook.getSheetAt(i);
					Iterator<Row> iterator = datatypeSheet.iterator();
					Row firstRow = iterator.next();
					String nameCategory = firstRow.getCell(0) + "";
					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						requestDTO = new Product();
						requestDTO.setName(currentRow.getCell(1) != null ? (currentRow.getCell(1) + "").trim() : "");
						// requestDTO.setOrginSku(
						// currentRow.getCell(0) != null ?
						// (currentRow.getCell(0) + "").toUpperCase().trim() :
						// "");
						requestDTO.setSku(
								currentRow.getCell(0) != null ? (currentRow.getCell(0) + "").toUpperCase().trim() : "");

						try {
							Double strQuantity = 0.0;
							strQuantity = currentRow.getCell(2) != null
									? Double.parseDouble((currentRow.getCell(2) + "").trim()) : 0.0;
							requestDTO.setInStock(strQuantity.intValue());
						} catch (Exception e) {
							// TODO: handle exception
							requestDTO.setInStock(0);
							e.printStackTrace();
						}
						try {
							Double strPrice = 0.0;
							strPrice = currentRow.getCell(3) != null
									? Double.parseDouble((currentRow.getCell(3) + "").trim()) : 0;
							requestDTO.setNormalPrice(strPrice.intValue());
						} catch (Exception e) {
							// TODO: handle exception
							requestDTO.setNormalPrice(0);
							e.printStackTrace();
						}
						try {
							Double strOrder = 0.0;
							strOrder = currentRow.getCell(4) != null
									? Double.parseDouble((currentRow.getCell(4) + "").trim()) : 0.0;
							requestDTO.setMinOrder(strOrder.intValue());
						} catch (Exception e) {
							// TODO: handle exception
							requestDTO.setNormalPrice(0);
							e.printStackTrace();
						}
						requestDTO.setProductStatus(
								currentRow.getCell(5) != null ? (currentRow.getCell(5) + "").toUpperCase().trim() : "");
						if (requestDTO.getSku() != null && !"".equals(requestDTO.getSku())) {
							list.add(requestDTO);
						}

					}
				}
				workbook.close();
				if (list != null && list.size() > 0) {
					return productService.updateProductByOginSku(list);
				} else {
					re.setResultCode(201);
					re.setObject(list);
					re.setErrorMessage("Không có dữ liệu hợp lệ");
					return re;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		re.setResultCode(200);
		re.setObject(list);
		return re;
	}

	// import list category
	@RequestMapping(value = "/importCategory", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject importCategory(@RequestParam("files") MultipartFile[] uploadfiles) {
		ImportXlsxProduct up = new ImportXlsxProduct();
		return importDataCategory(uploadfiles);
	}

	public ResponseObject importDataCategory(MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<RequestCategory> list = new ArrayList<RequestCategory>();
		for (MultipartFile file : uploadfiles) {
			try {
				Workbook workbook = new XSSFWorkbook(file.getInputStream());
				RequestCategory requestDTO = new RequestCategory();
				for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
					requestDTO = new RequestCategory();
					requestDTO.setCategoryName(workbook.getSheetName(i) != null ? workbook.getSheetName(i).trim() : "");
					Sheet datatypeSheet = workbook.getSheetAt(i);
					DataFormatter fmt = new DataFormatter();
					Iterator<Row> iterator = datatypeSheet.iterator();
					Row firstRow = iterator.next();
					Cell firstCell = firstRow.getCell(1);
					List<String> listCategory = new ArrayList<String>();
					boolean check = false;
					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						String dto = null;
						dto = currentRow.getCell(1) != null ? (currentRow.getCell(1) + "").trim() : "";
						if (dto != null && !"".equals(dto)) {
							listCategory.add(dto);
						}

					}
					requestDTO.setListchildren(listCategory);
					list.add(requestDTO);
				}

				workbook.close();
				if (list != null && list.size() > 0) {
					re = saveCategory(list);
					return re;
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		re.setResultCode(200);
		re.setObject(list);
		return re;
	}

	// Luu Object to database;
	public ResponseObject saveCategory(List<RequestCategory> list) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		int countProduc = 0;
		List<KeyValue> listSetLog = new ArrayList<>();
		if (list != null && list.size() > 0) {

			for (RequestCategory dto : list) {
				if (dto.getCategoryName() != null && dto.getListchildren() != null
						&& dto.getListchildren().size() > 0) {
					long idCategory = 0;

					idCategory = categoriesService.addCategoryImportNotCheck(dto.getCategoryName());
					List<KeyValue> logs = new ArrayList<>();
					if (idCategory > 0) {
						for (String p : dto.getListchildren()) {
							Categories pro = new Categories();
							pro.setName(p);
							ConvetSlug cd = new ConvetSlug();
							Date dt = new Date();
							pro.setSlug(cd.toUrlFriendly(p, 1) + dt.getHours() + dt.getMinutes() + dt.getSeconds());
							pro.setParentId(Integer.parseInt(idCategory + ""));
							pro.setCreatedTime(dt.getTime() / 1000);
							pro.setSeoIndex(1);
							pro.setOrder(1);
							pro.setId(0);
							Categories repons = new Categories();
							try {
								repons = categoriesService.addCategoryName(pro);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}

							if (repons != null && repons.getId() > 0) {
								countProduc++;
								KeyValue ke = new KeyValue();
								ke.setValue(repons.getId());
								ke.setName(p);
								logs.add(ke);
							}

						}
					}
					KeyValue ke = new KeyValue();
					ke.setData(logs);
					ke.setValue(idCategory);
					ke.setName(dto.getCategoryName());
					listSetLog.add(ke);

				}
			}
		}
		re.setObject(countProduc);
		re.setData(listSetLog);
		return re;
	}

	@RequestMapping(value = "/uploadIcon", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadIcon(@RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();

		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadImageCategory + parts);
				fileOuputStream.write(bytes);
				resetChmod(CommonConfig.uploadImageCategory);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return re;
	}

	public static void main(String[] args) {
		String a = "lkk";
		Double c = 0D;
		Integer d = 0;
		try {
			c = Double.parseDouble(a);
			System.out.println(c);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println(c);
		System.out.println(c.intValue());
	}

	@RequestMapping(value = "/fix", method = RequestMethod.GET, headers = "Accept=application/json")
	public void fix() {
		// return accountService.getRowCountSearchExceptList(userSearch,
		// exceptList);
		productService.fixSlug();
	}

	@RequestMapping(value = "/uploadArticles", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadFileArticles(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String nameForder, @RequestParam("files") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<String> listPath = new ArrayList<String>();
		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 100 * 1024 * 1024) {
					re.setErrorMessage("max file 100M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				listPath.add(parts);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadFileArticle + parts);
				fileOuputStream.write(bytes);
				// try {
				// AddImageWatermark aw = new AddImageWatermark();
				// aw.addLogo(CommonConfig.uploadFile + parts);
				//
				// } catch (Exception e) {
				// // TODO: handle exception
				// e.printStackTrace();
				// }
				resetChmod(CommonConfig.uploadFileArticle);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		re.setObject(listPath);
		re.setResultCode(200);

		return re;
	}

	// @SuppressWarnings({ "unchecked", "rawtypes" })
	// @RequestMapping(value = "/deleteAllDat09", method = RequestMethod.GET,
	// headers = "Accept=application/json")
	// public String getAllRoleStatus() {
	// productService.deleteAllProduct();
	// return "thành công";
	// }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/fixImport2", method = RequestMethod.GET, headers = "Accept=application/json")
	public String FixImport2() {
		FixImport("/home/upload/data2/");
		return "Thanh Cong";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/fixImport3", method = RequestMethod.GET, headers = "Accept=application/json")
	public String FixImport3() {
		FixImport("/home/upload/data3/");
		return "Thanh Cong";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/fixImport1", method = RequestMethod.GET, headers = "Accept=application/json")
	public String FixImport1() {
		FixImport("/home/upload/data1/");
		return "Thanh Cong";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/fixImport4", method = RequestMethod.GET, headers = "Accept=application/json")
	public String FixImport4() {
		FixImport("/home/upload/data4/");
		return "Thanh Cong";
	}

	public String FixImport(String path) {
		try {
			List<String> results = new ArrayList<String>();
			File[] files = new File(path).listFiles();
			for (File file : files) {
				if (file.isFile()) {
					results.add(file.getName());
					try {
						FileInputStream input = new FileInputStream(file);

						List<RequestProduct> list = new ArrayList<RequestProduct>();
						try {
							Workbook workbook = new XSSFWorkbook(input);
							RequestProduct requestDTO = new RequestProduct();
							for (int i = 0; i < workbook.getNumberOfSheets(); i++) {

								Sheet datatypeSheet = workbook.getSheetAt(i);
								DataFormatter fmt = new DataFormatter();
								Iterator<Row> iterator = datatypeSheet.iterator();
								Row firstRow = iterator.next();
								String nameCategory = "";
								nameCategory = firstRow.getCell(0) + "";
								requestDTO = new RequestProduct();
								// add name category theo sheetName
								// requestDTO.setNameCategory(workbook.getSheetName(i));
								// add category name theo A1
								System.out.println("Check data nameCategory:" + nameCategory);

								if ("Datasheets".toUpperCase()
										.equals(nameCategory != null ? nameCategory.toUpperCase().trim() : null)) {
									continue;
								}
								requestDTO.setNameCategory(nameCategory);
								// Cell firstCell = firstRow.getCell(1);
								List<ProductImportData> listOfCustomer = new ArrayList<ProductImportData>();
								List<String> listNameAttribute = new ArrayList<String>();
								boolean check = false;
								while (iterator.hasNext()) {
									Row currentRow = iterator.next();
									ProductImportData dto = new ProductImportData();
									if (!"".equals(currentRow.getCell(4) + "") && "Datasheets".toUpperCase()
											.equals((currentRow.getCell(4) + "").toUpperCase().trim() + "")) {
										check = true;
										for (int j = 15; j < 50; j++) {
											if (currentRow.getCell(j) != null
													&& currentRow.getCell(j).getStringCellValue() != null
													&& !"".equals(currentRow.getCell(j).getStringCellValue())) {
												String nameAttribute = currentRow.getCell(j).getStringCellValue();
												listNameAttribute.add(nameAttribute);
											}
										}
									} else if (check) {
										dto.setSeoTitlte(currentRow.getCell(1) != null
												? (currentRow.getCell(1) + "").trim() : "");
										dto.setSeoKeyword(currentRow.getCell(2) != null
												? (currentRow.getCell(2) + "").trim() : "");
										dto.setSeoDescription(currentRow.getCell(3) != null
												? (currentRow.getCell(3) + "").trim() : "");

										dto.setDatasheets(currentRow.getCell(4) != null
												? (currentRow.getCell(4) + "").trim() : "");
										dto.setImageUrl(currentRow.getCell(5) != null
												? (currentRow.getCell(5) + "").trim() : "");
										dto.setProductName(
												currentRow.getCell(0) != null ? (currentRow.getCell(0) + "").trim()
														: (currentRow.getCell(6) != null
																? (currentRow.getCell(6) + "").trim() : ""));
										dto.setSku(currentRow.getCell(6) != null ? (currentRow.getCell(6) + "").trim()
												: "");
										dto.setOrginSku(currentRow.getCell(7) != null
												? (currentRow.getCell(7) + "").trim() : "");
										dto.setManufactureName(currentRow.getCell(8) != null
												? (currentRow.getCell(8) + "").trim() : "");
										dto.setShortDescription(currentRow.getCell(9) != null
												? (currentRow.getCell(9) + "").trim() : "");
										dto.setInStock(currentRow.getCell(10) != null
												? (currentRow.getCell(10) + "").trim() : "");
										dto.setNormalPrice(currentRow.getCell(12) != null
												? (currentRow.getCell(12) + "").trim() : "");
										dto.setMinOrder(currentRow.getCell(14) != null
												? (currentRow.getCell(14) + "").trim() : "");
										List<ImportAttribute> listAttribute = new ArrayList<>();
										if (listNameAttribute != null) {
											for (int k = 0; k < listNameAttribute.size(); k++) {
												ImportAttribute d = new ImportAttribute();
												d.setParentName(listNameAttribute.get(k));
												d.setAttributeName(currentRow.getCell(15 + k) != null
														? (currentRow.getCell(15 + k) + "").trim() : "");
												listAttribute.add(d);
											}
										}
										dto.setListAttribute(listAttribute);

										if (dto.getSku() != null && !"".equals(dto.getSku())
												&& dto.getOrginSku() != null && !"".equals(dto.getOrginSku())
												&& dto.getProductName() != null && !"".equals(dto.getProductName())) {
											listOfCustomer.add(dto);
										}
									}

								}
								if (listOfCustomer != null && listOfCustomer.size() > 0) {
									requestDTO.setData(listOfCustomer);
								}
								list.add(requestDTO);
							}
							workbook.close();
							if (list != null && list.size() > 0) {
								addProduct(list);
								try {
									resetChmod(CommonConfig.uploadMedia);
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}
							}

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return "Thành Công";
	}

	@RequestMapping(value = "/clearCache", method = RequestMethod.GET, headers = "Accept=application/json")
	public String clearCache() {
		try {
			System.out.println("================Start Clear Cache==============");
			Process p = Runtime.getRuntime().exec("sudo echo 1 > /proc/sys/vm/drop_caches");
			System.out.println("===================End Clear Cache====================");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "That bai";
		}
		return "Thanh cong";
	}

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public void getDownload(@PathVariable long id, HttpServletResponse response) throws IOException {
		List<CustomerOrderProduct> list = new ArrayList<CustomerOrderProduct>();
		list = customerOrderProductService.getByCustomerOrderIDExport(id);
		if (!list.isEmpty()) {
			ExcelWriter exFile = new ExcelWriter();
			FileOutputStream file = exFile.exportDetailOrder(list);
			if (file != null) {
				OutputStream out = response.getOutputStream();
				out = file;
			}
		}
		
	}
	@RequestMapping(value = "/testexport", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody ResponseObject approveRepairPlan(@RequestParam(required = false, name = "format") String format2) {
		ResponseObject responseObject = new ResponseObject();
		responseObject.setResultCode(200); 
		responseObject.setObject(null);
		responseObject.setErrorMessage("OK");
		byte[] responseData = new byte[0];
		try {

		} catch (Exception e) {
			return responseObject;
		}
		return responseObject;
	}
	@RequestMapping(value = "/uploadRIP", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public ResponseObject uploadFileMultiRIP(@RequestParam(required = false) Long id,
			@RequestParam(required = false) String nameForder, @RequestParam("filesRIP") MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<String> listFile = new ArrayList<String>();
		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 500 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile, "UTF-8"), 0);
				String name = cv.toUrlFriendly(name1, 0);
				String parts = name.replace(".", dt.getHours() + dt.getSeconds() + ".");
				re.setObject(parts);
				re.setResultCode(200);
				String nameFullPathFile = "http://media.tranzi.vn/assets/RIP/"+ parts;
				fileOuputStream = new FileOutputStream(CommonConfig.uploadFileRIP + parts);
				fileOuputStream.write(bytes);
				listFile.add(nameFullPathFile);
//				try {
//					AddImageWatermark aw = new AddImageWatermark();
//					aw.addLogo(CommonConfig.uploadFileRIP + parts);
//
//				} catch (Exception e) {
//					// TODO: handle exception
//					e.printStackTrace();
//				}
				resetChmod(CommonConfig.uploadFileRIP);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if(listFile!=null && listFile.size()>0){
			re.setData(listFile);
		}

		return re;
	}
}
