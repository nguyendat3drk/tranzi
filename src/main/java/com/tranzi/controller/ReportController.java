package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.entity.Token;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.model.AgentReport;
import com.tranzi.model.ContactReport;
import com.tranzi.model.Queue;
import com.tranzi.model.QueueReport;
import com.tranzi.model.User;
import com.tranzi.service.AccountService;
import com.tranzi.service.AuthenticationService;
import com.tranzi.service.CustomerService;
import com.tranzi.service.ReportService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/report")
public class ReportController {
	@Autowired
	ReportService reportService;
	
	@Autowired
	AccountService accountService;
	
	
	
	@Autowired
	AuthenticationService authenService;

	@RequestMapping(value = "/getContact", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getSummaryReport(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fromDate", required = true) String fromDate,
			@RequestParam(value = "toDate", required = true) String toDate,
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "oder", required = true) String oder,
			@RequestParam(value = "timeUTC", required = true) float timeUTC,
			@RequestHeader(value = "Authorization", required = true) String authenToken) {
		ResponseObject responseObject = new ResponseObject();
		if(checkAuthentication(authenToken)){
			List<ContactReport> lst = reportService.getContact(type,fromDate,toDate,userName,oder,timeUTC);
			responseObject.setResultCode(200);
			responseObject.setObject(lst);
		}else{
			responseObject.setResultCode(405);
			responseObject.setObject(null);
			responseObject.setErrorMessage(null);
		}
		return responseObject;
	}
	
	@RequestMapping(value = "/getQueueReport", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getQueueReport(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fromDate", required = true) String fromDate,
			@RequestParam(value = "toDate", required = true) String toDate,
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "oder", required = true) String oder,
			@RequestParam(value = "queue", required = false) String queue,
			@RequestParam(value = "timeUTC", required = true) float timeUTC,
			@RequestHeader(value = "Authorization", required = true) String authenToken) {
		ResponseObject responseObject = null;
		if(checkAuthentication(authenToken)){
			responseObject = reportService.getQueueReport(type,fromDate,toDate,userName,oder,queue,timeUTC);
		}else{
			responseObject = new ResponseObject();
			responseObject.setResultCode(405);
			responseObject.setObject(null);
			responseObject.setErrorMessage(null);
		} 
		return responseObject;
	}
	
	@RequestMapping(value = "/getAgentReport", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAgentReport(@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "fromDate", required = true) String fromDate,
			@RequestParam(value = "toDate", required = true) String toDate,
			@RequestParam(value = "userName", required = true) String userName,
			@RequestParam(value = "oder", required = true) String oder,
			@RequestParam(value = "agent", required = false) String agent,
			@RequestParam(value = "timeUTC", required = true) float timeUTC,
			@RequestHeader(value = "Authorization", required = true) String authenToken) {
		ResponseObject responseObject = null;
		if(checkAuthentication(authenToken)){
			responseObject = reportService.getAgentReport(type,fromDate,toDate,userName,oder,agent,timeUTC);
		}else{
			responseObject = new ResponseObject();
			responseObject.setResultCode(405);
			responseObject.setObject(null);
			responseObject.setErrorMessage(null);
		}
		return responseObject;
	}
	

	public Token checkAuthor(String myHeader) {
		if (myHeader == null || myHeader.equals("")) {
			return null;
		} else {
			Token myToken = new JWTokenUtility().validate(myHeader);
			if (myToken == null) {
				return null;
			} else {
				return myToken;
			}
		}
	}
	
	private boolean checkAuthentication(String mToken){
		Token token = checkAuthor(mToken);
		if (token != null) {
			String currentSession = token.getId() + token.getIssueAt();
			System.out.println("currentSession: " + currentSession);
			boolean isUserSession = authenService.checkUserSession(token.getIssuer(), currentSession);
			if(isUserSession){
				return true;
			}else{
//				return new ResponseEntity(roleService.getAllAgents(0), HttpStatus.OK);
				return false;
			}
		}else{
			return false;
		}
	}

}
