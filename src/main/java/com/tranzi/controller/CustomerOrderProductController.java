package com.tranzi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.CustomerOrder;
import com.tranzi.model.PagerData;
import com.tranzi.service.CustomerOrderProductService;
import com.tranzi.service.CustomerOrderService;
import com.tranzi.service.CustomerService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/customerorderproduct")
public class CustomerOrderProductController {

	@Autowired
	CustomerOrderProductService customerOrderProductService;

	
	@RequestMapping(value = "/getByCustomerOrderID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getByCustomerOrderID(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(customerOrderProductService.getByCustomerOrderID(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}
	
}
