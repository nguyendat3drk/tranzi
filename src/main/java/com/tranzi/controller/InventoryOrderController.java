package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Inventory;
import com.tranzi.model.InventoryOrder;
import com.tranzi.model.PagerData;
import com.tranzi.service.InventoryOrderService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/inventoryorder")
public class InventoryOrderController {
	@Autowired
	InventoryOrderService inventoryOrderService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			List<InventoryOrder> list = inventoryOrderService.getAlls();
			re.setObject(list);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
		}
		
		return re;
	}
	

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creat(@RequestBody InventoryOrder dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(inventoryOrderService.create(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}


	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject update(@RequestBody InventoryOrder dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(inventoryOrderService.update(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}
	@RequestMapping(value = "/pagerAllInventoryOrder", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search,
			@RequestParam(value = "idInventory", required = false) int idInventory) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(inventoryOrderService.getPageInventory(page, pageSize, columnName, typeOrder, search,idInventory));
			pa.setTotalRow(inventoryOrderService.getCountTotalPage(page, pageSize, columnName, typeOrder, search,idInventory));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}


}
