package com.tranzi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AdminBom;
import com.tranzi.model.PagerData;
import com.tranzi.service.AdminBomService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/adminbom")
public class AdminBomController {
	@Autowired
	AdminBomService adminBomService;

	@RequestMapping(value = "/getAlls", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getAlls() {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		try {
			List<AdminBom> list = adminBomService.getAlls();
			re.setObject(list);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
		}

		return re;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject creatAdminBom(@RequestBody AdminBom dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(adminBomService.create(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}
		return res;
	}

	@RequestMapping(value = "/pagerAllAdminBom", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getPager(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "pageSize", required = true) int pageSize,
			@RequestParam(value = "columnName", required = false) String columnName,
			@RequestParam(value = "typeOrder", required = false) String typeOrder,
			@RequestParam(value = "search", required = false) String search) {
		PagerData pa = new PagerData();
		ResponseObject re = new ResponseObject();
		pa.setPager(page);
		pa.setPageSize(pageSize);
		re.setResultCode(200);
		try {
			pa.setContent(adminBomService.getPageAdminBom(page, pageSize, columnName, typeOrder, search));
			pa.setTotalRow(adminBomService.getCountTotalPage(page, pageSize, columnName, typeOrder, search));
			re.setObject(pa);
		} catch (Exception e) {
			// TODO: handle exception
			re.setResultCode(201);
		}
		return re;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject updateAdminBom(@RequestBody AdminBom dto) {
		ResponseObject res = new ResponseObject();
		try {
			res.setObject(adminBomService.update(dto));
			res.setResultCode(200);
		} catch (Exception e) {
			// TODO: handle exception
			res.setResultCode(204);
			e.printStackTrace();
		}

		return res;
	}

	@RequestMapping(value = "/getAdminBomId/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getById(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			res.setResultCode(200);
			res.setObject(adminBomService.getId(id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
		}

		return res;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseObject getDeleteAdminBom(@PathVariable int id) {
		ResponseObject res = new ResponseObject();
		try {
			return adminBomService.deleteAdminBom(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			res.setResultCode(204);
			res.setErrorMessage(e.getMessage());
		}

		return res;
	}

}
