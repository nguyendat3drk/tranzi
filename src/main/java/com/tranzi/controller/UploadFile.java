package com.tranzi.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.tranzi.common.AddImageWatermark;
import com.tranzi.common.CommonConfig;
import com.tranzi.common.ConvetSlug;
import com.tranzi.entity.ResponseObject;

public class UploadFile {
	public void  resetChmod(String path){
		try {
			Process p = Runtime.getRuntime().exec("chmod -R 777 "+ path);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public ResponseObject uploadFileProduct( MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<String> list = new ArrayList<>();
		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile,"UTF-8"),0);
				String name = cv.toUrlFriendly(name1,0);
				String parts = name.replace(".", dt.getHours()+dt.getSeconds() + ".");
				list.add(parts);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadMedia  +parts);
				fileOuputStream.write(bytes);
				try {
					AddImageWatermark ad = new AddImageWatermark();
					ad.addLogo(CommonConfig.uploadMedia  +parts);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				resetChmod(CommonConfig.uploadMedia);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		re.setResultCode(200);
		re.setObject(list);
		return re;
	}
	public ResponseObject uploadFileProductMedia( MultipartFile[] uploadfiles) {
		ResponseObject re = new ResponseObject();
		List<String> list = new ArrayList<>();
		for (MultipartFile file : uploadfiles) {
			FileOutputStream fileOuputStream = null;
			try {
				String fileExtension = file.getContentType().toLowerCase();
				if (file.getSize() > 10 * 1024 * 1024) {
					re.setErrorMessage("max file 10M");
					re.setResultCode(201);
				}

				byte[] bytes = file.getBytes();
				Date dt = new Date();
				String typeFile = "";
				typeFile = file.getOriginalFilename();
				ConvetSlug cv = new ConvetSlug();
				String name1 = cv.toUrlFriendly(URLEncoder.encode(typeFile,"UTF-8"),0);
				String name = cv.toUrlFriendly(name1,0);
				String parts = name.replace(".", dt.getHours()+dt.getSeconds() + ".");
				list.add(parts);
				fileOuputStream = new FileOutputStream(CommonConfig.uploadProductFile  +parts);
				fileOuputStream.write(bytes);
				
				resetChmod(CommonConfig.uploadProductFile);

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fileOuputStream != null) {
					try {
						fileOuputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		re.setResultCode(200);
		re.setObject(list);
		return re;
	}
}
