package com.tranzi.webconfig;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tranzi.common.CommonConfig;
import com.tranzi.common.CommonConfig.Method;
import com.tranzi.entity.MenuPerItem;
import com.tranzi.entity.Token;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.service.AccountService;
import com.tranzi.service.AuthenticationService;
import com.tranzi.service.MenuService;

public class LogInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private AuthenticationService authenticationService;
	
	@Autowired
	private AccountService userService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
			response.setStatus(200);
			System.out.println("Vao roi nhe"+200);
			return true;
		} else {
			String myToken = request.getHeader("Authorization");
			String url = request.getRequestURI();
			if (myToken == null || myToken.equals("")) {
				response.setStatus(403);
				return false;
			} else {
				Token token = checkAuthor(myToken);
				if (token != null) {
					String currentSession = myToken;
					System.out.println("currentSession: " + currentSession);
					boolean isUserSession = authenticationService.checkUserSession(token.getIssuer(), currentSession);
					
					return isUserSession;
				} else {
					//truong hop loi token
					response.getWriter().write("something");
					System.out.println("loi token"+200);
					response.setStatus(405);
					return false;
				}
			}
		}
	}

	private boolean checkPri(List<MenuPerItem> menuPerItems, String method, String menuCode) {
		for (MenuPerItem item : menuPerItems) {
			if (item.getMenuCode().equals(menuCode)) {
				if (item.getQueryPri() == 1 && method.equalsIgnoreCase(Method.GET.toString())) {
					return true;
				}
				if (item.getInsertPri() == 1 && method.equalsIgnoreCase(Method.POST.toString())) {
					return true;
				}
				if (item.getUpdatePri() == 1 && method.equalsIgnoreCase(Method.PUT.toString())) {
					return true;
				}
				if (item.getDeletePri() == 1 && method.equalsIgnoreCase(Method.DELETE.toString())) {
					return true;
				}
			}
		}
		return false;
	}

	public Token checkAuthor(String myHeader) {
		if (myHeader == null || myHeader.equals("")) {
			return null;
		} else {
			Token myToken = new JWTokenUtility().validate(myHeader);
			if (myToken == null) {
				return null;
			} else {
				return myToken;
			}
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// Ở đây, bạn có thể add các attribute vào modelAndView
		// Và sử dụng nó trong các View (jsp,..)
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		System.out.println(response.getStatus() + "");;
	}
}
