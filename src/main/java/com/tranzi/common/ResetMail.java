package com.tranzi.common;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class ResetMail {
	
//	final static String username = CommonConfig.username;
//	final static String password = CommonConfig.password;
	
	//send mail to an account
	//return 1 if success and return 0 if fail
	public int sendMail(String mailReceive, String subject, String conntent){
		new CommonConfig().loadConfig();
		Properties props = new Properties();
		final String username = CommonConfig.username;
		final String password = CommonConfig.password;
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", CommonConfig.serverMail);
		props.put("mail.smtp.port", CommonConfig.port);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(CommonConfig.username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailReceive));
			message.setSubject(subject);
			message.setContent(conntent, "text/html; charset=utf-8");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
			return 0;
		}
		
		return 1;
	}
	
	/*public static void main(String[] arg){
		sendMail("huutuyen91@gmail.com", "sub test", "This is a content test bla bla bla bla!");
	}*/
}