package com.tranzi.common;

import java.security.Key;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import com.tranzi.entity.Token;
import com.tranzi.jwt.RsaKeyProducer;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Test {
	public static void main(String [] args){
		int n, m;
//		Scanner sc = new Scanner(System.in);
//		m = sc.nextInt();
//		n = sc.nextInt();
//		if(m)
//		System.out.print(";;;5;;6;;;;;;;;".split(";").length);
	}
	
	/*private static String randomId() {
		Integer[] arr = new Integer[1000];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i;
		}
		Collections.shuffle(Arrays.asList(arr));
		return Arrays.toString(arr);
	}*/
	private static String randomId(){
		String SALTCHARS = "ABCDEFGHI012345";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
	    return saltStr;
	}
	/*public static String buildJWT(String id, String subject, int scope, String issuer,long issueAt, long ttlMillis) {
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();
//		System.out.println("RSA hash code... " + rsaJsonWebKey.hashCode());

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		JwtClaims claims = new JwtClaims();
		claims.setJwtId(id);
		claims.setSubject(subject); // the subject/principal is whom the token
		claims.setIssuer(issuer);
		claims.setStringClaim("issueAt", issueAt + "");
		claims.setStringClaim("scope", scope + "");
//		claims.setStringClaim("expire", ttlMillis + "");
		claims.setExpirationTimeMinutesInTheFuture(ttlMillis);

		JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setKey(rsaJsonWebKey.getPrivateKey());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

		String jwt = null;
		try {
			jwt = jws.getCompactSerialization();
		} catch (Exception ex) {
			// Logger.getLogger(JWTAuthFilter.class.getName()).log(Level.SEVERE,
			// null, ex);
			ex.printStackTrace();
		}

		System.out.println("Claim:\n" + claims);
		System.out.println("JWS:\n" + jws);
		System.out.println("JWT:\n" + jwt);

		return jwt;
	}
	
	private static String createJWT(String id, String issuer, String subject, long ttlMillis) {
		 
	    //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	 
	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("mysecret_key");
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	 
	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(id)
	                                .setIssuedAt(now)
	                                .setSubject(subject)
	                                .setIssuer(issuer)
	                                .signWith(signatureAlgorithm, signingKey);
	 
	    //if it has been specified, let's add the expiration
	    if (ttlMillis >= 0) {
	    long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }
	 
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
	
	private static void parseJWT(String jwt) {
		 
	    //This line will throw an exception if it is not a signed JWS (as expected)
	    Claims claims = Jwts.parser()         
	       .setSigningKey(DatatypeConverter.parseBase64Binary("mysecret_key"))
	       .parseClaimsJws(jwt).getBody();
	    System.out.println("ID: " + claims.getId());
	    System.out.println("Subject: " + claims.getSubject());
	    System.out.println("Issuer: " + claims.getIssuer());
	    System.out.println("Expiration: " + claims.getExpiration());
	}*/
	
	public static String buildJWT(String id, String subject, int scope, String issuer,long issueAt, long ttlMillis) {
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();

		JwtClaims claims = new JwtClaims();
		claims.setJwtId(id);
		claims.setSubject(subject); // the subject/principal is whom the token
		claims.setIssuer(issuer);
		claims.setStringClaim("issueAt", issueAt + "");
		claims.setStringClaim("scope", scope + "");
//		claims.setStringClaim("expire", ttlMillis + "");
		claims.setExpirationTimeMinutesInTheFuture(ttlMillis);

		JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setKey(rsaJsonWebKey.getPrivateKey());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

		String jwt = null;
		try {
			jwt = jws.getCompactSerialization();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Claim:\n" + claims);
		System.out.println("JWS:\n" + jws);
		System.out.println("JWT:\n" + jwt);

		return jwt;
	}

	public static void validate(String jwt) {
		String subject = null;
		String jwtId = null;
		String issuer = null;
		String scope = null;
		long expire = 0;
		long issueAt = 0;
		Token token = null;
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();

//		System.out.println("RSA hash code... " + rsaJsonWebKey.hashCode());

		JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireSubject()
				.setVerificationKey(rsaJsonWebKey.getKey())
				.build(); // create the JwtConsumer instance
		
		try {
			// Validate the JWT and process it to the Claims
			JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
			subject = (String) jwtClaims.getClaimValue("sub");
			jwtId = (String) jwtClaims.getClaimValue("jti");
			issuer = (String) jwtClaims.getClaimValue("iss");
			scope = (String) jwtClaims.getClaimValue("scope");
			expire = (long) jwtClaims.getClaimValue("exp");
			issueAt = Long.valueOf((String) jwtClaims.getClaimValue("issueAt"));
//			token = new Token(jwtId, issuer, subject,issueAt, expire, scope); 
			System.out.println("JWT validation succeeded! " + subject);
			System.out.println("expire" + expire);
		} catch (Exception e) {
			e.printStackTrace(); // on purpose
		}
//		return token;
	}
}
