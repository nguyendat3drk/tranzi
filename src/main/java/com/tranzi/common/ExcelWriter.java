package com.tranzi.common;

import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.tranzi.model.CustomerOrderProduct;

public class ExcelWriter {

    private static String[] columns = {"Sản phẩm", "Mã Sản phẩm	", "Mã NSX", "Số lượng", "Thành tiền"};
    public FileOutputStream exportDetailOrder(List<CustomerOrderProduct> listData){
    	try {
    		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

            /* CreationHelper helps us create instances of various things like DataFormat, 
               Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
            CreationHelper createHelper = workbook.getCreationHelper();

            // Create a Sheet
            Sheet sheet = workbook.createSheet("Customer Order Product");

            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.RED.getIndex());

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(0);

            // Create cells
            for(int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            // Create Cell Style for formatting Date
            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

            // Create Other rows and cells with employees data
            int rowNum = 1;
            for(CustomerOrderProduct employee: listData) {
                Row row = sheet.createRow(rowNum++);

                row.createCell(0)
                        .setCellValue(employee.getProductName());
                row.createCell(1)
                .setCellValue(employee.getSku());
                row.createCell(2)
                .setCellValue(employee.getOrginSku());
                row.createCell(3)
                .setCellValue(employee.getQuantity());
                row.createCell(4)
                .setCellValue(employee.getUnitPrice());
                row.createCell(5)
                .setCellValue(employee.getTotal());
                

//                row.createCell(1)
//                        .setCellValue(employee.getEmail());

//                Cell dateOfBirthCell = row.createCell(2);
//                dateOfBirthCell.setCellValue(employee.getDateOfBirth());
//                dateOfBirthCell.setCellStyle(dateCellStyle);

//                row.createCell(3)
//                        .setCellValue(employee.getSalary());
            }

    		// Resize all columns to fit the content size
            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream("poi-generated-file.xlsx");
            workbook.write(fileOut);
            fileOut.close();
            // Closing the workbook
            workbook.close();
            return fileOut;
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
        
    }
}