package com.tranzi.common;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvetSlug {
	private static char[] SPECIAL_CHARACTERS = { ' ', '!', '"', '#', '$', '%', '*', '+', ',', ':', '<', '=', '>', '?',
			'@', '[', '\\', ']', '^', '`', '|', '~', 'À', 'Á', 'Â', 'Ã', 'È', 'É', 'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ',
			'Ù', 'Ú', 'Ý', 'à', 'á', 'â', 'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý', 'Ă', 'ă',
			'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ', 'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ',
			'ẫ', 'Ậ', 'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế',
			'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ',
			'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ',
			'Ừ', 'ừ', 'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.','S','T','A',
			'B','C','D','E','F','G','H','J','K','L','M','N','O','P','Q','R','W','Z','X','V','Y','I','U','/',')','('};

	private static char[] REPLACEMENTS = { '-', '\0', '\0', '\0', '\0', '\0', '\0', '_', '\0', '_', '\0', '\0', '\0',
			'\0', '\0', '\0', '_', '\0', '\0', '\0', '\0', '\0', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o',
			'o', 'o', 'u', 'u', 'y', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y',
			'a', 'a', 'd', 'd', 'i', 'i', 'u', 'u', 'o', 'o', 'u', 'u', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
			'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e',
			'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
			'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
			'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.','s','t','a',
			'b','c','d','e','f','g','h','j','k','l','m','n','o','p','q','r','w','z','x','v','y','i','u','\0','\0','\0'};

	// public static void main(String[] args) {
	// ConvetSlug su = new ConvetSlug();
	// System.out.println(su.toUrlFriendly("nguyá»n huy Äáº¡t.jpg"));
	// }
	public String toUrlFriendly(String s,int type ) {
		int maxLength = Math.min(s.length(), 236);
		char[] buffer = new char[maxLength];
		int n = 0;
		for (int i = 0; i < maxLength; i++) {
			char ch = s.charAt(i);
			buffer[n] = removeAccent(ch, type);
			// skip not printable characters
			if (buffer[n] > 31) {
				n++;
			}
		}
		// skip trailing slashes
		while (n > 0 && buffer[n - 1] == '/') {
			n--;
		}
		return String.valueOf(buffer, 0, n);
	}

	public char removeAccent(char ch, int type) {
		int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
		if (index >= 0) {
			ch = REPLACEMENTS[index];
		} else {
			ch = Character.toLowerCase(ch);
			if ("Ä".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("º".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("»".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("�".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("¤".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("†".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("¢".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("‡".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("£".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("•".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			} else if ("¥".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}else if ("ự".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}else if ("±".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}
			else if ("µ".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}
			else if (".".equals(ch+"") && type == 1) {
				ch = REPLACEMENTS[1];
			}else if (")".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}else if ("(".equals(ch+"")) {
				ch = REPLACEMENTS[1];
			}
			else if(!checkPatern(ch)){
				ch = REPLACEMENTS[1];
			}
			else{
				ch = Character.toLowerCase(ch);
			}


		}
		return ch;
	}
	public boolean checkPatern(char ch){
		Pattern pattern = Pattern.compile("[A-Za-z0-9_.]");
		String str = (ch +"").toLowerCase();
		Matcher matcher = pattern.matcher(str);
		 
		boolean match = matcher.matches();
		
		return match;
	}

	public String removeAccent(String s, int type) {
		StringBuilder sb = new StringBuilder(s);
		for (int i = 0; i < sb.length(); i++) {
			sb.setCharAt(i, removeAccent(sb.charAt(i),type));
		}
		return sb.toString();
	}
	public static void main(String[] args) {
		ConvetSlug cv = new ConvetSlug();
		System.out.println(cv.toUrlFriendly("Tụ Nhôm Phân Cực Chân Dán SMD 10µF 16V Sai Số ±20% Kích Thước  4.30 x 4.30 Cao (5.65mm)",1));
	}

}
