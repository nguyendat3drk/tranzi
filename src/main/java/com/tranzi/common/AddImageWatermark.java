package com.tranzi.common;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public  class AddImageWatermark {
	public static void main(String... args) throws IOException {

		// overlay settings
		File input = new File("C:/Users/Admin/Desktop/up/222222tfasdfasdf5613.png");
		CommonConfig cd = new CommonConfig();
		File overlay = new File("C:/Users/Admin/Desktop/up/logotranzi.png");
		File output = new File("C:/Users/Admin/Desktop/up/out1.png");

		// adding text as overlay to an image
		addImageWatermark(overlay, "png", input, output);
	}

	public String addLogo(String path) {
		try {
			File input = new File(path);
			CommonConfig cd = new CommonConfig();
			File overlay = new File(cd.uploadFile + "logotranzi.png");
			File output = new File(path);
			// adding text as overlay to an image
			addImageWatermark(overlay, "png", input, output);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return path;

	}

	private static void addImageWatermark(File watermark, String type, File source, File destination)
			throws IOException {
		BufferedImage image = ImageIO.read(source);
		BufferedImage overlay = resize(ImageIO.read(watermark), 55, 280);

		// determine image type and handle correct transparency
		int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
		BufferedImage watermarked = new BufferedImage(image.getWidth(), image.getHeight(), imageType);

		// initializes necessary graphic properties
		Graphics2D w = (Graphics2D) watermarked.getGraphics();
		w.drawImage(image, 0, 0, null);
//		điều chỉnh áng tối ảnh logo
//		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
		w.setComposite(alphaChannel);

		// calculates the coordinate where the String is painted
		int centerX = image.getWidth() / 2;
		int centerY = image.getHeight() / 2;

		// add text watermark to the image
		// w.drawImage(overlay, centerX, centerY, null);
		w.drawImage(overlay, 2, 2, null);
		ImageIO.write(watermarked, type, destination);
		w.dispose();
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}
}
