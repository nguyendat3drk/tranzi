package com.tranzi.common;

import java.io.ByteArrayOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

//import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.export.JRPdfExporter;
//import net.sf.jasperreports.engine.export.JRXlsExporter;
//import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
//import net.sf.jasperreports.export.SimpleExporterInput;
//import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Component
public class TranziReportServlet {

	private static final String CONTENT = "Content-Disposition";
	private static final String ATTACH_FILENAME = "attachment; filename=";

	protected String rootpath;

//	public byte[] getPdfReport(JasperPrint jasperPrint, HttpServletResponse response, String fileName)
//			throws JRException {
//		// preparation
//		response.setContentType("application/pdf; charset=UTF-8");
//		response.setHeader(CONTENT, getAttachFileName(fileName, "pdf"));
//		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
//
//		JRPdfExporter pdfExporter = new JRPdfExporter();
//
//		pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//		pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outstream));
//		pdfExporter.exportReport();
//		return outstream.toByteArray();
//	}
//
//	public byte[] getXlsReport(JasperPrint jasperPrint, HttpServletResponse response, String fileName)
//			throws JRException {
//		// preparation
//		response.setContentType("application/vnd.ms-excel; charset=UTF-8");
//		response.setHeader(CONTENT, getAttachFileName(fileName, "xls"));
//		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
//
//		JRXlsExporter xlsExporter = new JRXlsExporter();
//		xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//		xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outstream));
//
//		xlsExporter.exportReport();
//		return outstream.toByteArray();
//	}
//
//	public byte[] getDocReport(JasperPrint jasperPrint, HttpServletResponse response, String fileName)
//			throws JRException {
//		response.setContentType("application/vnd.ms-word; charset=UTF-8");
//		response.setHeader(CONTENT, getAttachFileName(fileName, "docx"));
//		ByteArrayOutputStream outstream = new ByteArrayOutputStream();
//
//		JRDocxExporter exporter = new JRDocxExporter();
//		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outstream));
//		exporter.exportReport();
//		return outstream.toByteArray();
//	}
//
//	private String getAttachFileName(String fileName, String ext) {
//		StringBuilder str = new StringBuilder();
//		str.append(ATTACH_FILENAME);
//		str.append(fileName);
//		str.append(".");
//		str.append(ext);
//		return str.toString();
//	}
}
