package com.tranzi.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
public class CompareWord {
	public boolean readDocxFile(String path1, String path2) {
		Boolean check = true;
        try {
            File file = new File(path1);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> paragraphs = document.getParagraphs();
            File file2 = new File(path2);
            FileInputStream fis2 = new FileInputStream(file2.getAbsolutePath());
            XWPFDocument document2 = new XWPFDocument(fis2);
            List<XWPFParagraph> paragraphs2 = document2.getParagraphs();
            check =true; 
            if(paragraphs.size() != paragraphs.size()){
            	return false;
            }
            for (int i=0;i<paragraphs.size();i++) {
            	if(!paragraphs.get(0).getParagraphText().equals(paragraphs2.get(0).getParagraphText())){
            		check = false;
            	}
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            check = false;
        }
        return check;
    }
	public static void main(String[] args) {
		CompareWord cd = new CompareWord();
		cd.readDocxFile("C:/Users/Admin/Desktop/up/huydat.docx","C:/Users/Admin/Desktop/up/huydat1.docx");
	}
}
    