package com.tranzi.common;

import com.tranzi.entity.Addresses;
import com.google.gson.Gson;

public class GsonHelper {
	Gson gson = new Gson();

	public GsonHelper() {
		super();
	}

	// method to convert string json to list addresses object
	public Addresses JsonToAddresses(String jsonStr) {
		Addresses responseObj;
		responseObj = gson.fromJson(jsonStr, Addresses.class);
		return responseObj;
	}

	// method to convert list addresses object to string json
	public String ResponseRouteObjToJson(Addresses responseObj) {
		String jsonInString = gson.toJson(responseObj);
		return jsonInString;
	}
}
