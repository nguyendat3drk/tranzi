package com.tranzi.common;

public class CommonConfig {
	public static String uploadFile = "/home/deploy/html/tranzi-admin/dist/assets/banner/";
	public static String uploadFileArticle = "/home/deploy/html/tranzi-admin/dist/assets/articles/";
	public static String uploadMedia = "/home/deploy/html/tranzi-admin/dist/assets/images/product/media/";
	public static String uploadProductFile = "/home/deploy/html/tranzi-admin/dist/assets/images/product/file/";
	public static String domainWeb = "http://media.tranzi.vn/assets/images/product/media/";
	public static String uploadImageCategory = "/home/deploy/html/tranzi-admin/dist/assets/images/category/";
	public static String uploadImageManufactures = "/home/deploy/html/tranzi-admin/dist/assets/images/manufactures/";
	
	//export order and qrCode
	public static String pathExportTempate = "/home/deploy/html/tranzi-admin/exportFile/Templates/order/";
	public static String pathExportOrderCustomer = "/home/deploy/html/tranzi-admin/dist/assets/exportFile/CustomerOrderExport/";
	public static String domainWebCustomerOrder = "http://media.tranzi.vn/assets/exportFile/CustomerOrderExport/";
	public static String pathExportQRCode = "/home/deploy/html/tranzi-admin/exportFile/QRImage/";
	// Export BarCode product
	public static String pathExportBarCodeProduct = "/home/deploy/html/tranzi-admin/dist/assets/exportFile/BarCodeProduct/";
	public static String domainWebBarCodeProduct = "http://media.tranzi.vn/assets/exportFile/BarCodeProduct/";
	public static String pathExportBarCode = "/home/deploy/html/tranzi-admin/exportFile/BarImage/";
	// Upload File Dinh kem cho cac muc 
	public static String uploadFileRIP = "/home/deploy/html/tranzi-admin/dist/assets/RIP/";
//		public static String uploadFileRIP = "D:/SETUP/rest/";	
	
//	public static String uploadFileArticle = "E:\\TaiLieu\\";
//	public static String pathExportTempate = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/";
//	public static String pathExportOrderCustomer = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/";
//	public static String pathExportQRCode = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/QR/";
//	public static String domainWebCustomerOrder = "http://media.tranzi.vn/assets/exportFile/CustomerOrderExport/";
//	
//	// Export BarCode product
//	public static String pathExportBarCodeProduct = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/";
//	public static String domainWebBarCodeProduct = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/report/";
//	public static String pathExportBarCode = "E:/TaiLieu/app/tranzi/tranzi/src/main/resources/templates/QR/";
	
//	public static String uploadFile = "D:\\AHUY\\uploadtest\\";
//	// upload anh san pham
//	public static String uploadMedia = "D:\\AHUY\\uploadtest\\";
//	// upload file san pham
//	public static String uploadProductFile = "C:\\Users\\Admin\\Desktop\\up\\";
//	public static String domainWeb = "http://aduploadImageManufacturesmin.tranzi.vn/assets/images/product/media/";
	
	
	
	//SMTP mail infor
	public static String serverMail = "";
	public static String username = "";
	public static String password = "";
	public static String port = "";
	public static String linkResetPass = "";
	public static String path = "";
	
	//method service
	public enum Method {
		GET, POST, PUT, DELETE;
	}
	
	public void loadConfig() {

		
	}
}
