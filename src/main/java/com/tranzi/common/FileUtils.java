package com.tranzi.common;



import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * @author dat.nguyenhuy
 *
 */
public class FileUtils {

	private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);
	
	public static byte[] getByteArrayFromUrl(String url) throws IOException {

		URL oracle = new URL(url);
		try (InputStream in = oracle.openStream()) {
			int length = -1;
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			byte[] tempBr = new byte[1024];// buffer for portion of data from connection
			while ((length = in.read(tempBr)) > -1) {
				buffer.write(tempBr, 0, length);
			}
			return buffer.toByteArray();
		}
	}
	
	/**
	 * Get Inputstream file
	 * 
	 * @param filePath
	 * @return
	 */
	public static InputStream getFileClassPath(String filePath) {
		try {
			Resource resource = new ClassPathResource(filePath);
			return resource.getInputStream();
		} catch (IOException e) {
			LOG.info("Error! Cannot read file ", e);
		}
		return null;
	}
}
