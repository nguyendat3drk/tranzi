package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.common.ConvetSlug;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Categories;
import com.tranzi.model.Product;

@Repository
public class CategoriesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Categories getById(int id) {
		List<Categories> listCategories = new ArrayList<Categories>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from categories where id = ? ").setParameter(0,
					id);
			query.addEntity(Categories.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories.get(0);
	}

	public ResponseObject deleteCategori(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			// if (!checkDeleteCategori(id)) {
			// re.setResultCode(205);
			// re.setErrorMessage("Thể loại đã liên kết Sản phẩn. không thể
			// xóa");
			// }else{
			session.createSQLQuery("delete from categories where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành công");
			// }
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public ResponseObject deleteAllCategori() {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			session.createSQLQuery("delete from categories ").executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public boolean checkDeleteCategori(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (id > 0) {
				Criteria criteria = session.createCriteria(Categories.class);
				criteria.add(Restrictions.sqlRestriction(" id = " + id + " and count_product > 0  "));
				List<Categories> list = criteria.list();
				if (list != null && list.size() > 0) {
					return false;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}

	public List<Categories> getAlls() {
		List<Categories> listCategories = new ArrayList<Categories>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from categories  where 1 =1 order by orders asc ");
			query.addEntity(Categories.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories;
	}

	public int getIdmax() {
		Session session = this.sessionFactory.getCurrentSession();

		int id = 1;
		try {
			String sqls = "select max(id)+1 from categories where 1=1";
			Query query3 = session.createSQLQuery(sqls);
			List<BigInteger> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				BigInteger idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					id = 1;
				} else {
					id = Integer.parseInt(idm.toString());
				}

			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Categories create(Categories re) {
		Categories dto = new Categories();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}
		if (re.getNameEn() != null && re.getNameEn().length() >= 254) {
			re.setNameEn(re.getNameEn().substring(0, 253));
		}
		List<Categories> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}

		Date dt = new Date();
		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug().replace("--", "-") + dt.getSeconds());
		}
		// int id = getIdmax();
		try {
			if (dto != null && !dto.equals("")) {
				dto.setCreatedTime(dt.getTime() / 1000);
				dto.setType(0);
				session.persist(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public Categories createImport(Categories re) {
		Categories dto = new Categories();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}
		if (re.getNameEn() != null && re.getNameEn().length() >= 254) {
			re.setNameEn(re.getNameEn().substring(0, 253));
		}
		List<Categories> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size() + ")"));
		}
		// int id = getIdmax();
		try {
			if (dto != null && !dto.equals("")) {
				// dto.setId(id);
				dto.setId(0);
				Date dt = new Date();
				dto.setType(1);
				// dto.setBannerImage(bannerImage);
				dto.setCreatedTime(dt.getTime() / 1000);
				session.persist(dto);
				session.flush();
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public List<Categories> checkSlug(String slug, long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Categories.class);

		if (slug != null && !slug.equals("") && id <= 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) like '" + slug.toUpperCase().trim() + "%') "));
		} else if (slug != null && !slug.equals("") && id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) = '" + slug.toUpperCase().trim() + "%') "));
		}
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
		}
		List<Categories> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Categories> getAllCategoryPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Categories.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		criteria.addOrder(Order.asc("orders"));

		List<Categories> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllCategory(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Categories.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Categories> list = criteria.list();
		return list.size();
	}

	public Categories update(Categories re) {
		Categories dto = new Categories();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}
		if (re.getNameEn() != null && re.getNameEn().length() >= 254) {
			re.setNameEn(re.getNameEn().substring(0, 253));
		}
		List<Categories> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size()) + ")");
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				session.update(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public List<Categories> getListParentCategories() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session
					.createSQLQuery("select * from categories where parent_id is null Or parent_id =0 order by orders asc ");
			query.addEntity(Categories.class);
			return query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public List<Categories> getParentTreeChildenById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from categories where  parent_id = " + id + " order by orders asc ");
			query.addEntity(Categories.class);
			return query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public List<Categories> getListChildrenCategories() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session
					.createSQLQuery("select * from categories where parent_id is not null  and  parent_id != 0 order by orders asc ");
			query.addEntity(Categories.class);
			return query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public long addCategoryImport(String name, long parentID) {
		long id = 0;
		// check name category

		try {
			Session session = this.sessionFactory.getCurrentSession();

			if (name != null && !name.equals("")) {

				Criteria criteria = session.createCriteria(Categories.class);
				criteria.add(Restrictions.sqlRestriction(" (UPPER(name_en) = '" + name.toUpperCase().trim() + "')"));
				if (parentID > 0) {
					criteria.add(Restrictions.sqlRestriction(" (parent_id = " + parentID + ")"));
				}
				List<Categories> list = criteria.list();
				session.flush();
				if (list != null && list.size() > 0) {
					return list.get(0).getId();
				} else {
					Categories dto = new Categories();
					ConvetSlug cd = new ConvetSlug();
					dto.setId(0);
					Date dt = new Date();
					dto.setSlug(cd.toUrlFriendly(name, 1));
					dto.setName(name);
					dto.setNameEn(name);
					dto.setCreatedTime(dt.getTime() / 1000);
					dto.setSeoIndex(1);
					dto.setOrder(1);
					dto.setParentId(Integer.parseInt(parentID + ""));
					Categories re = new Categories();
					re = createImport(dto);
					return re.getId();
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	public long addCategoryImportNotCheck(String name) {
		long id = 0;
		// check name category

		try {
			Session session = this.sessionFactory.getCurrentSession();

			Categories dto = new Categories();
			ConvetSlug cd = new ConvetSlug();
			dto.setId(0);
			Date dt = new Date();
			dto.setSlug(cd.toUrlFriendly(name, 1).replace("--", "-"));
			dto.setName(name);
			dto.setNameEn(name);
			dto.setCreatedTime(dt.getTime() / 1000);
			dto.setSeoIndex(1);
			dto.setOrder(1);
			Categories re = new Categories();
			re = createImport(dto);
			return re.getId();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return id;
	}

	public Categories updateOrder(List<Categories> list) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (list != null && list.size() > 0) {
				for (Categories pro : list) {
					updateCategoryOrder(pro);
				}
				System.out.println("ok");
				return list.get(0);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	public void updateCategoryOrder(Categories dto) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Categories.class);
			criteria.add(Restrictions.sqlRestriction(" id = " + dto.getId() + " "));
			List<Categories> list = criteria.list();
			if (list != null && list.size() > 0) {
				Categories re = new Categories();
				re = list.get(0);
				re.setOrder(dto.getOrder());
				session.update(re);
				session.flush();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}

	}

}
