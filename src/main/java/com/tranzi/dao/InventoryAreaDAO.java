package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.Inventory;
import com.tranzi.model.InventoryArea;
import com.tranzi.model.PackageTypes;

@Repository
public class InventoryAreaDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public InventoryArea getById(long id) {
		List<InventoryArea> list = new ArrayList<InventoryArea>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from inventory_area where id = ? ").setParameter(0,
					id);
			query.addEntity(InventoryArea.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public ResponseObject delete(long id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		if (!checkDeleteInventoryArea(id)) {
			re.setResultCode(205);
			re.setErrorMessage("Địa chỉ kho đã có sản phẩm, không thể xóa");
			;
			return re;
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from inventory_area where id = ? ").setParameter(0, id).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
		}
		return re;
	}

	public boolean checkDeleteInventoryArea(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Inventory> list = new ArrayList<>();
		try {
			// session.createSQLQuery("select * from attribute_values where
			// attribute_id = ? ").setParameter(0, id);
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from inventory where inventory_area_id = ? ")
					.setParameter(0, id);
			query.addEntity(Inventory.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<InventoryArea> getAlls() {
		List<InventoryArea> list = new ArrayList<InventoryArea>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from inventory_area  where 1 =1 ");
			query.addEntity(InventoryArea.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject addupdate(InventoryArea re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		InventoryArea dto = new InventoryArea();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("") && re.getId()!=null && re.getId()>0) {
				session.update(dto);
				respon.setObject(dto);
			}else{
				session.persist(dto);
				respon.setObject(dto);
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}

	// chua dung
	@SuppressWarnings("unchecked")
	public List<InventoryArea> getAllpageding(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryArea.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) like '%" + keySearch.replace("%", "^") + "%' OR UPPER(address) LIKE '%"
					+ keySearch.replace("%", "^") + "%')"));
		}
		List<InventoryArea> list = criteria.list();
		return list;
	}


	@SuppressWarnings("unchecked")
	public int getCountPageAll(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryArea.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc("id"));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc("id"));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) like '%" + keySearch.replace("%", "^") + "%' OR UPPER(address) LIKE '%"
					+ keySearch.replace("%", "^") + "%')"));
		}
		List<InventoryArea> list = criteria.list();
		return list.size();
	}

}
