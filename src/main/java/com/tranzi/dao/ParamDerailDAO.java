package com.tranzi.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.model.MenuPermission;
import com.tranzi.model.ParamDetail;
import com.tranzi.model.User;

@Repository
public class ParamDerailDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	public List<ParamDetail> getParamDetailType(String type) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(ParamDetail.class);
		if (type != null && !type.equals("") && !type.equals("null")) {
			criteria.add(Restrictions.eq("type", type));
		}
		return criteria.list();
		
	}

}
