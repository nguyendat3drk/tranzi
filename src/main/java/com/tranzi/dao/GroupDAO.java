package com.tranzi.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.KeyValueDTO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.GroupACD;
import com.tranzi.model.Role;
import com.tranzi.model.User;

@Repository
public class GroupDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<GroupACD> getAllsGroup(){
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		criteria.add(Restrictions.eq("status", 1));
		criteria.add(Restrictions.eq("deleteState", 0));
		criteria.addOrder(Order.asc("groupName"));
		List<GroupACD> result = criteria.list();
		for (GroupACD group : result) {
			session.evict(group);
			group.setCreatedTime(convertUTCtoLocalTime(group.getCreatedTime()));
		}
		return result;
	}
	public List<GroupACD> getAllsGroupFull(){
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		criteria.add(Restrictions.eq("deleteState", 0));
		criteria.addOrder(Order.asc("groupName"));
		List<GroupACD> result = criteria.list();
//		for (GroupACD group : result) {
//			session.evict(group);
//			group.setCreatedTime(convertUTCtoLocalTime(group.getCreatedTime()));
//		}
		return result;
	}
	

	public boolean isGroupExists(GroupACD group) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session
				.createSQLQuery("select (1) from groupacd a where a.group_name = :groupName and a.id <> :id and delete_state = 0")
				.setParameter("groupName", group.getGroupName().trim()).setParameter("id", group.getId());
		List result = query.list();
		if (result.size() > 0)
			return true;
		return false;
	}
	
	private Date getUTCDateTime() {
		Date mDate = null;
		  try {
		   String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
		   final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		   final String utcTime = sdf.format(new Date());
		   mDate = dateFormat.parse(utcTime);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return mDate;
	}

	public ResponseObject deleteListGroup(String ids) {
		String groupNames = getListGroupByIds(ids);
		if (groupNames == null) {
			return null;
		}
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(
				"select group_name from groupacd where id in (select distinct group_id from user_group_granted where group_id in (:ids)) and delete_state = 0");
		String[] tempGroupIds;
		tempGroupIds = ids.split(",");
		query.setParameterList("ids", tempGroupIds);
		List result = query.list();
		if (result.size() > 0) {
			responseObject.setResultCode(500);
			responseObject.setObject(result.toString());
			responseObject.setErrorMessage("The user group " + result.toString() + " are not empty. Cannot delete");
			return responseObject;
		} else {
			SQLQuery strQuery = session.createSQLQuery("UPDATE  groupacd SET delete_state = 1, last_modify = :lastModify WHERE id IN (:groupIds)");
			strQuery.setParameterList("groupIds", tempGroupIds).setParameter("lastModify", getUTCDateTime());
			strQuery.executeUpdate();

			responseObject.setResultCode(200);
			responseObject.setObject(groupNames);
			responseObject.setErrorMessage("The user group " + groupNames + " is deleted successfully!");
			return responseObject;
		}
	}

	public List<GroupACD> getAlls(int page, int pageSize, String columnName, String typeOrder) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}

		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		criteria.add(Restrictions.eq("deleteState", 0));
		criteria.addOrder(Order.desc("createdTime"));
		List<GroupACD> result = criteria.list();
		for (GroupACD group : result) {
			session.evict(group);
//			System.out.println(">>>>>>>>>>>>" + group.getCreatedTime());
			group.setCreatedTime(convertUTCtoLocalTime(group.getCreatedTime()));
		}
		return result;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		// criteria.add(Restrictions.eq("status", 1));
		criteria.add(Restrictions.eq("deleteState", 0));
		return criteria.list().size();
	}
	
	public ResponseObject getUsersByGroupIdPaging(int page, int pageSize, int id) {
		ResponseObject responseObject = new ResponseObject();
		List<User> listUsers = new ArrayList<>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
		SQLQuery query = session.createSQLQuery(
				"SELECT * FROM user u INNER JOIN user_group_granted ugg ON u.id = ugg.user_id WHERE ugg.group_id = :id AND u.delete_state = 0 LIMIT :limit OFFSET :offset");
		query.addEntity(User.class);		
		query.setParameter("id", id);
		query.setParameter("limit", pageSize);
		query.setParameter("offset", (page - 1) * pageSize);
		listUsers = query.list();
		responseObject.setResultCode(200);
		responseObject.setObject(listUsers);
		responseObject.setErrorMessage(null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			responseObject.setResultCode(500);
			responseObject.setObject(null);
			responseObject.setErrorMessage(null);
		}
//		session.close();
		return responseObject;
	}
	
	public ResponseObject getRowCountUserByGroup(int groupId) {
		// TODO Auto-generated method stub
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT COUNT(1) FROM user_group_granted WHERE group_id = :id";
			SQLQuery query = session.createSQLQuery(sql);
			System.out.println(query);
			query.setParameter("id", groupId);
			BigInteger count = (BigInteger) query.uniqueResult();
			int result = count.intValue();
			responseObject.setResultCode(200);
			responseObject.setObject(result);
			responseObject.setErrorMessage(null);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			responseObject.setResultCode(500);
			responseObject.setObject(null);
			responseObject.setErrorMessage(null);
		}
		return responseObject;
	}

	public GroupACD getGroup(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		GroupACD group = (GroupACD) session.createCriteria(GroupACD.class).add(Restrictions.eq("deleteState", 0)).add(Restrictions.eq("id", id)).uniqueResult();
		return group;
	}

	public ResponseObject addGroup(GroupACD group) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject responseObject = new ResponseObject();
		try {
			if (isGroupExists(group)) {
				responseObject.setResultCode(201);
				responseObject
						.setErrorMessage("Group " + group.getGroupName() + " is existed. Please choose another name");
				responseObject.setObject(group);
			} else {
				/*SimpleDateFormat sdf = new				 SimpleDateFormat();
				sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
				Date date = sdf.parse(String.valueOf(new Date()));*/
				group.setCreatedTime(getUTCDateTime());
				group.setGroupName(group.getGroupName().trim());
				session.persist(group);
				addUserGroupGranted(group);
				responseObject.setResultCode(200);
				responseObject.setObject(group);
				responseObject.setErrorMessage("Group " + group.getGroupName() + " is created");
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObject.setResultCode(500);
			responseObject.setObject(null);
			responseObject.setErrorMessage("Something were wrong!");
		}
		return responseObject;
	}

	public void addUserGroupGranted(GroupACD group) {
		Session session = this.sessionFactory.getCurrentSession();
		String[] userIds;
		if (group.getUserIds() != null && !group.getUserIds().equals("")) {
			if (group.getUserIds().length() == 1) {
				userIds = new String[1];
				userIds[0] = group.getUserIds();
			} else {
				userIds = group.getUserIds().split(",");
			}
			for (int i = 0; i < userIds.length; i++) {
				SQLQuery query = session
						.createSQLQuery("insert into user_group_granted (user_id, group_id) values(:userId,:groupId)");
				query.setParameter("groupId", group.getId());
				query.setParameter("userId", userIds[i]);
				query.executeUpdate();
			}
		}
	}

	public ResponseObject updateGroup(GroupACD group) {
		ResponseObject responseObject = new ResponseObject();
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (isGroupExists(group)) {
				responseObject.setResultCode(201);
				responseObject.setErrorMessage("Group " + group.getGroupName() + " is existed. Please choose another name");
				responseObject.setObject(null);
			} else {
				group.setGroupName(group.getGroupName().trim());
				group.setLastModify(getUTCDateTime());
				updateUserGroup(group);
				session.update(group);
				responseObject.setResultCode(200);
				responseObject.setErrorMessage("User Group " + group.getGroupName() + " is updated");
				responseObject.setObject(null);
			}
			return responseObject;
		} catch (Exception ex) {
			responseObject.setResultCode(500);
			responseObject.setErrorMessage("Error when update " + group.getGroupName() + ". Please check log");
			responseObject.setObject(null);
			return responseObject;
		}
	}
	
	public int getCurrentStatusGroup(int groupId){
		GroupACD groupAcd = getGroup(groupId);
		if(groupAcd != null){
			return groupAcd.getStatus();
		}else{
			return -1;
		} 
	}

	public GroupACD changeGroupStatus(GroupACD group) {
		Session session = this.sessionFactory.getCurrentSession();
		List<User> lstUserOfGroup = getUsersByGroupId(group.getId());
		int currentStatus = getCurrentStatusGroup(group.getId());
		if(lstUserOfGroup.size()>0 && currentStatus == 1 && group.getStatus() == 0){
			return null;
		}else{
			SQLQuery query = session.createSQLQuery("update groupacd set status =:status, last_modify = :lastModify where id = :groupId and delete_state = 0");
			query.setParameter("groupId", group.getId());
			query.setParameter("status", group.getStatus());
			query.setParameter("lastModify", getUTCDateTime());
			query.executeUpdate();
			return group;
		}
	}

	public void updateUserGroup(GroupACD group) {
		String[] userIds;
		Session session = this.sessionFactory.getCurrentSession();
		// delete all user of group in user_group_granted table
		SQLQuery delQuery = session.createSQLQuery("delete from user_group_granted where group_id=:groupId");
		delQuery.setParameter("groupId", group.getId());
		delQuery.executeUpdate();
		if (group.getUserIds() != null && !group.getUserIds().equals("")) {
			if (group.getUserIds().length() == 1) {
				userIds = new String[1];
				userIds[0] = group.getUserIds();
			} else {
				userIds = group.getUserIds().split(",");
			}
			for (int i = 0; i < userIds.length; i++) {
				SQLQuery query = session
						.createSQLQuery("insert into user_group_granted (user_id, group_id) values(:userId,:groupId)");
				query.setParameter("groupId", group.getId());
				query.setParameter("userId", userIds[i]);
				query.executeUpdate();
			}
		}
	}

	public ResponseObject deleteGroup(int id) {
		GroupACD group = getGroupById(id);
		String groupName = "";
		if (group != null) {
			groupName = group.getGroupName();
		}
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select distinct group_id from user_group_granted where group_id=:groupId")
				.setParameter("groupId", id);
		List result = query.list();
		if (result.size() > 0) {
			responseObject.setResultCode(500);
			responseObject.setObject(groupName);
			responseObject.setErrorMessage("The user group [" + groupName + "] are not empty. Cannot delete");
		} else {
			GroupACD p = (GroupACD) session.load(GroupACD.class, new Integer(id));
			if (null != p) {
				session.createSQLQuery("UPDATE groupacd SET delete_state = 1, last_modify = :lastModify WHERE id = :id")
				.setParameter("id", p.getId()).setParameter("lastModify", getUTCDateTime())
				.executeUpdate();
			}
			responseObject.setResultCode(200);
			responseObject.setObject(groupName);
			responseObject.setErrorMessage("The user group [" + groupName + "] is deleted successfully!");
		}
		return responseObject;
	}

	public GroupACD getGroupById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		criteria.add(Restrictions.eq("deleteState", 0));
		if (id > 0) {
			criteria.add(Restrictions.eq("id", id));
		}
		if (criteria.list().size() > 0){
			GroupACD result = (GroupACD) criteria.list().get(0);
			return result;
		}else
			return null;
	}
	
	@SuppressWarnings("unchecked")
	public ResponseObject getUserIdsByGroupId(int groupId){
		String result ="";
		ResponseObject responseObj = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createSQLQuery("select user_id from user_group_granted a where a.group_id = :groupId").setParameter("groupId", groupId);
		List<Integer> lstResult =(List<Integer>) query.list();
		if(lstResult.size()>0){
			for(int i=0; i<lstResult.size(); i++){
				result += lstResult.get(i) + ",";
			}
			result = result.substring(0,result.length()-1);
			responseObj.setResultCode(200);
			responseObj.setObject(result);
			responseObj.setErrorMessage(null);
			return responseObj;
		}else{
			responseObj.setResultCode(500);
			responseObj.setObject(null);
			responseObj.setErrorMessage("error");
			return responseObj;
		}
	}

	public String getListGroupByIds(String ids) {
		String[] lstIds = ids.split(",");
		List<Long> arrIds = new ArrayList<>();
		for (String idItem : lstIds) {
			long idTemp = Long.valueOf(idItem);
			arrIds.add(idTemp);
		}
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select group_name from groupacd g where g.id in :ids and g.delete_state = 0");
		query.setParameterList("ids", arrIds);
		List result = query.list();
		return result.toString();
	}

	public List<GroupACD> searchGroup(String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(GroupACD.class);
		criteria.add(Restrictions.eq("deleteState", 0));
		if (name != null) {
			criteria.add(Restrictions.eq("groupName", name));
		}
		/*
		 * if(population >=0){ criteria.add(Restrictions.eq("population",
		 * population)); }
		 */
		List<GroupACD> result = criteria.list();
//		for (GroupACD group : result) {
//			session.evict(group);
//			group.setCreatedTime(convertUTCtoLocalTime(group.getCreatedTime()));
//		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsersByGroupId(int groupId) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria roleCriteria = session.createCriteria(Role.class);
		List<Role> roleList = roleCriteria.list();
		
		SQLQuery query = session.createSQLQuery(
				"select * from user where id in (select user_id from user_group_granted where group_id = :groupId) and delete_state = 0");
		query.addEntity(User.class);
		
		query.setParameter("groupId", groupId);
		List<User> lstUsers = query.list();
		
		if(lstUsers!=null && lstUsers.size()>0 && roleList!=null && roleList.size()>0){
			for(User dto:lstUsers){
				for(Role dt:roleList){
					if(0 ==dt.getId()){
						continue;
					}
				}
			}
		}
		return lstUsers;
	}

	public List<User> getUsersNotInGroup(int groupId) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(
				"select * from user where id not in (select user_id from user_group_granted where group_id = :groupId) and delete_state = 0");
		query.addEntity(User.class);
		query.setParameter("groupId", groupId);
		return query.list();
	}

	public List<KeyValueDTO> getGroupCombo() {
		// TODO Auto-generated method stub
		List<KeyValueDTO> keyValuelst = new ArrayList<>();
		List<GroupACD> groups = getAllsGroup();
		for (GroupACD groupACD : groups) {
			KeyValueDTO keyValue = new KeyValueDTO();
			keyValue.setKey(groupACD.getId());
			keyValue.setDisplay(groupACD.getGroupName());
			keyValue.setValue(groupACD.getId());
			keyValuelst.add(keyValue);
		}
		return keyValuelst;
	}
	
	private Date convertUTCtoLocalTime(Date date) {
		Date convertedDate = null;
		try {
	         String utcTimeString = date.toString();

	         DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	         utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	         Date utcTime = utcFormat.parse(utcTimeString);


	         DateFormat localFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	         localFormat.setTimeZone(TimeZone.getDefault());
	         String convertedString = localFormat.format(utcTime);
	         convertedDate = localFormat.parse(convertedString);
	        } catch (ParseException e) {

	        }
		
		return convertedDate;
	}
	
}
