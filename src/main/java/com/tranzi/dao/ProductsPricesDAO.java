package com.tranzi.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ProductsPrices;

@Repository
public class ProductsPricesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public ResponseObject create(ProductsPrices re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		ProductsPrices dto = new ProductsPrices();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		List<ProductsPrices> list = new ArrayList<>();

		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				session.persist(dto);
				respon.setObject(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}
	public ResponseObject createList(List<ProductsPrices> re) {
		ResponseObject respon = new ResponseObject();
		try {
			List<ProductsPrices> list = new ArrayList<>();
			list = re;
			respon.setResultCode(200);
			ProductsPrices newDto;
			respon.setObject(list);
			
			if(list !=null && list.size()>0){
				int id =  list.get(0).getProductId();
				if(id>0){
					deleteProductPriceByID(id);
				}
				Session session = this.sessionFactory.getCurrentSession();
				for(ProductsPrices p: list){
					newDto = new ProductsPrices();
					newDto = p;
					newDto.setId(0);
					session.persist(newDto);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}
	public ResponseObject deleteProductPriceByID(int id){
		ResponseObject respon = new ResponseObject();
		try {
			if(id>0){
				Session session = this.sessionFactory.getCurrentSession();
				session.createSQLQuery("delete from products_prices where product_id = ? ")
				.setParameter(0, id).executeUpdate();
				respon.setResultCode(200);
				respon.setObject("THành công");
				return respon;
			}else{
				respon.setResultCode(204);
				respon.setErrorMessage("có loi xay ra");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
		
	}
	public ResponseObject deleteByProductSku(String sku){
		ResponseObject respon = new ResponseObject();
		try {
			if(!"".equals(sku) && sku!=null){
				Session session = this.sessionFactory.getCurrentSession();
				session.createSQLQuery("delete from products_prices  where product_id in (select p.id from products p where  UPPER(p.sku) = ?) ")
				.setParameter(0, sku.toUpperCase().trim()).executeUpdate();
				respon.setResultCode(200);
				respon.setObject("THành công");
				return respon;
			}else{
				respon.setResultCode(204);
				respon.setErrorMessage("có loi xay ra");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
		
	}

	public ResponseObject update(ProductsPrices re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		ProductsPrices dto = new ProductsPrices();
		dto = re;
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if(re.getProductId() !=null && re.getProductId()>0){
				session.createSQLQuery("UPDATE products_prices set min_quantity = ?,price = ? where product_id = ?  ")
				.setParameter(0, re.getMinQuantity()).setParameter(1, re.getPrice()).setParameter(2, re.getProductId()).executeUpdate();
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}
	public List<ProductsPrices> getALlByProductID(int id){
		
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(ProductsPrices.class);
			
			if (id>0) {
				criteria.add(Restrictions.sqlRestriction(" (product_id = "+id+")"));
			}else{
				return null;
			}
			List<ProductsPrices> list = criteria.list();
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}

}
