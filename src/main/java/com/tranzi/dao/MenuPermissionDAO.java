package com.tranzi.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.tranzi.model.MenuPermission;
import com.tranzi.model.Role;

@Repository
public class MenuPermissionDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<MenuPermission> getAllMenuPermissionsList() {
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuPermission> MenuPermissionList = session.createQuery("from menu_permission ").list();
		return MenuPermissionList;
	}

	public ResponseEntity<?> addMenuPermission(MenuPermission menuPermission) {
		Session session = this.sessionFactory.getCurrentSession();
		MenuPermission permission = new MenuPermission();
		permission = menuPermission;
		if (permission != null) {
			String sql = "insert into menu_permission (role_id, menu_id, status, insert_pri, delete_pri, update_pri, query_pri) values ("
					+ permission.getRoleId() + "," + permission.getMenuId() + "," + permission.getStatus() + ","
					+ permission.getInsertPri() + "," + permission.getDeletePri() + "," + permission.getUpdatePri()
					+ "," + permission.getQueryPri() + ");";
			try {
				Query query = session.createSQLQuery(sql);
				System.out.println(sql);
				query.executeUpdate();
				return new ResponseEntity(HttpStatus.OK);

			} catch (Exception e) {
				// TODO: handle exception
				return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
			}
		} else {
			return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
		}
	}

	public ResponseEntity<?> deleteMenuPermission(Integer[] ids) {
		Session session = this.sessionFactory.getCurrentSession();
		MenuPermission p = new MenuPermission();
		for (Integer id : ids) {
			p = (MenuPermission) session.load(MenuPermission.class, new Integer(id));
			if (null != p) {
				session.delete(p);
				return new ResponseEntity(HttpStatus.OK);
			}
			return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
		}
		return null;
	}

	public ResponseEntity<?> deletePermission(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		// MenuPermission p = (MenuPermission)
		// session.load(MenuPermission.class, id);
		String sql = "delete from menu_permission where role_id=" + id + "";
		try {
			Query query = session.createSQLQuery(sql);
			query.executeUpdate();
			return new ResponseEntity(HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
		}
	}
	public List<MenuPermission> getPermissionRealTime(int id){
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		list =getAllPermissionMenuCode(id,"REALTIME");
		
		return list;
	}
	public List<MenuPermission> getAllPermissionRole(int id) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		list =getAllPermissionMenuCode(id,"CONFIG");
		
		return list;
	}
	public List<MenuPermission> getPerDashBoard(int id) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		list =getPermissionMenuDashBoard(id,"DASHBOARD");
		
		return list;
	}
	public List<MenuPermission> getPerStatistic(int id) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		list =getPerStatistic(id,"REPORT_MANAGEMENT");
		
		return list;
	}
	public List<MenuPermission> getAllPermissionMenuCode(int id,String code) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		int roleId = 0;
		MenuPermission dto;
		roleId = id;
		if (roleId >= 0) {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				String sql = " select a.menu_id,a.menu_name,coalesce(a.role_id,0) as role_id,a.status ,b.update_pri,b.delete_pri,b.insert_pri,b.query_pri from( "
						+" select  m.id as menu_id,m.menu_name,m.id as role_id,m.status ,0 update_pri,0 delete_pri,0 insert_pri,0 query_pri,0 parent_Code "
						+" from menu m where m.status = 1 and parent_code = '"+code+"' "
						+" ) a left join (select mp.menu_id,m.menu_name,mp.role_id,mp.status ,mp.update_pri,mp.delete_pri,mp.insert_pri,mp.query_pri,m.parent_Code "
						+" from menu m left OUTER  join menu_permission mp ON m.id=mp.menu_id "
						+" where m.status=1 and mp.status=1 and mp.role_id = ? and  m.parent_Code = '"+code+"' )b On a.menu_id = b.menu_id; "
;
				Query query = session.createSQLQuery(sql);
				query.setParameter(0, roleId);
				List<Object[]> rows = query.list();
				if (rows != null && rows.size() > 0) {
					list = new ArrayList<MenuPermission>();
					for (Object[] row : rows) {
						dto = new MenuPermission();
						if(row[2]!=null && !row[2].equals("")){
							dto.setRoleId(Integer.parseInt(row[2].toString()));
						}else{
							dto.setRoleId(0);
						}
						if(row[0]!=null && !row[0].equals("")){
							dto.setMenuId(Integer.parseInt( row[0].toString()));
						}else{
							dto.setMenuId(0);
						}
						if(row[3]!=null && !row[3].equals("")){
							dto.setStatus(Integer.parseInt( row[3].toString()));
						}else{
							dto.setStatus(1);
						}
						if(row[4]!=null && !row[4].equals("")){
							dto.setUpdatePri(Integer.parseInt( row[4].toString()));
						}else{
							dto.setUpdatePri(0);
						}
						if(row[5]!=null && !row[5].equals("")){
							dto.setDeletePri(Integer.parseInt( row[5].toString()));
						}else{
							dto.setDeletePri(0);
						}
						if(row[6]!=null && !row[6].equals("")){
							dto.setInsertPri(Integer.parseInt( row[6].toString()));
						}else{
							dto.setInsertPri(0);
						}
						if(row[7]!=null && !row[7].equals("")){
							dto.setQueryPri(Integer.parseInt( row[7].toString()));
						}else{
							dto.setQueryPri(0);
						}
						dto.setMenuName(row[1].toString());
						list.add(dto);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				
			}
		}
		return list;
	}
	public MenuPermission updatePermission(List<MenuPermission> menupermission) {
		List<MenuPermission> permission = new ArrayList<MenuPermission>();
		permission = menupermission;
		MenuPermission dto = new MenuPermission();
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if(delete(permission.get(0).getRoleId())){
				add(permission);
			}
//			session.update(menupermission);
			dto = menupermission.get(0);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
		
	}
	public boolean delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		// MenuPermission p = (MenuPermission)
		// session.load(MenuPermission.class, id);
		String sql = "delete from menu_permission where role_id=" + id + "";
		try {
			Query query = session.createSQLQuery(sql);
			query.executeUpdate();
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	public void add(List<MenuPermission>  menuPermission) {
		Session session = this.sessionFactory.getCurrentSession();
		List<MenuPermission>  listpermission = new ArrayList<MenuPermission> ();
		listpermission = menuPermission;
		if(listpermission!=null && listpermission.size()>0){
			for(MenuPermission permission:listpermission){
				if (permission != null) {
					if((permission.getInsertPri()!=0 ||permission.getUpdatePri()!=0 ||permission.getDeletePri()!=0 ||permission.getQueryPri()!=0 ) && permission.getRoleId()!=0){
					
						String sql = "insert into menu_permission (role_id, menu_id, status, insert_pri, delete_pri, update_pri, query_pri) values ("
								+ permission.getRoleId() + "," + permission.getMenuId() + "," + permission.getStatus() + ","
								+ permission.getInsertPri() + "," + permission.getDeletePri() + "," + permission.getUpdatePri()
								+ "," + permission.getQueryPri() + ");";
						try {
							Query query = session.createSQLQuery(sql);
							System.out.println(sql);
							query.executeUpdate();
	
						} catch (Exception e) {
							e.printStackTrace();
						}
						
					}
				}
				
			}
		}
		
	}

	public List<MenuPermission> getPermissionMenuDashBoard(int id,String code) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		int roleId = 0;
		MenuPermission dto;
		roleId = id;
		if (roleId >= 0) {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				String sql = " select a.menu_id,a.menu_name,? as role_id,a.status ,b.update_pri,b.delete_pri,b.insert_pri,b.query_pri from( "
						+" select  m.id as menu_id,m.menu_name,m.id as role_id,m.status ,0 update_pri,0 delete_pri,0 insert_pri,0 query_pri,0 parent_Code "
						+" from menu m where m.status = 1 and menu_code = '"+code+"' "
						+" ) a left join (select mp.menu_id,m.menu_name,mp.role_id,mp.status ,mp.update_pri,mp.delete_pri,mp.insert_pri,mp.query_pri,m.parent_Code "
						+" from menu m left OUTER  join menu_permission mp ON m.id=mp.menu_id "
						+" where m.status=1 and mp.status=1 and mp.role_id = ? and  m.menu_code = '"+code+"' )b On a.menu_id = b.menu_id; "
;
				Query query = session.createSQLQuery(sql);
				query.setParameter(0, roleId);
				query.setParameter(1, roleId);
				List<Object[]> rows = query.list();
				if (rows != null && rows.size() > 0) {
					list = new ArrayList<MenuPermission>();
					for (Object[] row : rows) {
						dto = new MenuPermission();
						if(row[2]!=null && !row[2].equals("")){
							dto.setRoleId(Integer.parseInt(row[2].toString()));
						}else{
							dto.setRoleId(0);
						}
						if(row[0]!=null && !row[0].equals("")){
							dto.setMenuId(Integer.parseInt( row[0].toString()));
						}else{
							dto.setMenuId(0);
						}
						if(row[3]!=null && !row[3].equals("")){
							dto.setStatus(Integer.parseInt( row[3].toString()));
						}else{
							dto.setStatus(1);
						}
						if(row[4]!=null && !row[4].equals("")){
							dto.setUpdatePri(Integer.parseInt( row[4].toString()));
						}else{
							dto.setUpdatePri(0);
						}
						if(row[5]!=null && !row[5].equals("")){
							dto.setDeletePri(Integer.parseInt( row[5].toString()));
						}else{
							dto.setDeletePri(0);
						}
						if(row[6]!=null && !row[6].equals("")){
							dto.setInsertPri(Integer.parseInt( row[6].toString()));
						}else{
							dto.setInsertPri(0);
						}
						if(row[7]!=null && !row[7].equals("")){
							dto.setQueryPri(Integer.parseInt( row[7].toString()));
						}else{
							dto.setQueryPri(0);
						}
						dto.setMenuName(row[1].toString());
						list.add(dto);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				
			}
		}
		return list;
	}
	public List<MenuPermission> getPerStatistic(int id,String code) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		int roleId = 0;
		MenuPermission dto;
		roleId = id;
		if (roleId >= 0) {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				String sql = " select a.menu_id,a.menu_name,? as role_id,a.status ,b.update_pri,b.delete_pri,b.insert_pri,b.query_pri from( "
						+" select  m.id as menu_id,m.menu_name,m.id as role_id,m.status ,0 update_pri,0 delete_pri,0 insert_pri,0 query_pri,0 parent_Code "
						+" from menu m where m.status = 1 and parent_code = '"+code+"' "
						+" ) a left join (select mp.menu_id,m.menu_name,mp.role_id,mp.status ,mp.update_pri,mp.delete_pri,mp.insert_pri,mp.query_pri,m.parent_Code "
						+" from menu m left OUTER  join menu_permission mp ON m.id=mp.menu_id "
						+" where m.status=1 and mp.status=1 and mp.role_id = ? and  m.parent_code = '"+code+"' )b On a.menu_id = b.menu_id; "
;
				Query query = session.createSQLQuery(sql);
				query.setParameter(0, roleId);
				query.setParameter(1, roleId);
				List<Object[]> rows = query.list();
				if (rows != null && rows.size() > 0) {
					list = new ArrayList<MenuPermission>();
					for (Object[] row : rows) {
						dto = new MenuPermission();
						if(row[2]!=null && !row[2].equals("")){
							dto.setRoleId(Integer.parseInt(row[2].toString()));
						}else{
							dto.setRoleId(0);
						}
						if(row[0]!=null && !row[0].equals("")){
							dto.setMenuId(Integer.parseInt( row[0].toString()));
						}else{
							dto.setMenuId(0);
						}
						if(row[3]!=null && !row[3].equals("")){
							dto.setStatus(Integer.parseInt( row[3].toString()));
						}else{
							dto.setStatus(1);
						}
						if(row[4]!=null && !row[4].equals("")){
							dto.setUpdatePri(Integer.parseInt( row[4].toString()));
						}else{
							dto.setUpdatePri(0);
						}
						if(row[5]!=null && !row[5].equals("")){
							dto.setDeletePri(Integer.parseInt( row[5].toString()));
						}else{
							dto.setDeletePri(0);
						}
						if(row[6]!=null && !row[6].equals("")){
							dto.setInsertPri(Integer.parseInt( row[6].toString()));
						}else{
							dto.setInsertPri(0);
						}
						if(row[7]!=null && !row[7].equals("")){
							dto.setQueryPri(Integer.parseInt( row[7].toString()));
						}else{
							dto.setQueryPri(0);
						}
						dto.setMenuName(row[1].toString());
						list.add(dto);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				
			}
		}
		return list;
	}


}
