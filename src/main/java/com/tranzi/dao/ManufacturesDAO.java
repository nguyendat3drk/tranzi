package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.common.ConvetSlug;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Manufactures;
import com.tranzi.model.Product;

@Repository
public class ManufacturesDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Manufactures getById(int id) {
		List<Manufactures> listCategories = new ArrayList<Manufactures>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from manufactures where id = ? ")
					.setParameter(0, id);
			query.addEntity(Manufactures.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories.get(0);
	}

	public ResponseObject getDeleteManufactures(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			if(!checkDeleteManufactures(id)){
				re.setResultCode(205);
				re.setErrorMessage("Nhà sản xuất đã gán cho sản phẩm. Không thể xóa.");
				return re;
			}
			session.createSQLQuery("delete from manufactures where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành Công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}
	public boolean checkDeleteManufactures(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Product> list = new ArrayList<>();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from products where manufacture_id = ? ")
					.setParameter(0, id);
			query.addEntity(Product.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public List<Manufactures> getAlls() {
		List<Manufactures> listCategories = new ArrayList<Manufactures>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from manufactures  where 1 =1 ");
			query.addEntity(Manufactures.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories;
	}

	public int getIdmax() {
		Session session = this.sessionFactory.getCurrentSession();

		int id = 1;
		try {
			String sqls = "select max(id)+1 from manufactures where 1=1";
			Query query3 = session.createSQLQuery(sqls);
			List<BigInteger> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				BigInteger idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					id = 1;
				} else {
					id = Integer.parseInt(idm.toString());
				}

			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Manufactures create(Manufactures re) {
		Manufactures dto = new Manufactures();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		if(re.getSlug()!=null && re.getSlug().length()>=254){
			re.setSlug(re.getSlug().substring(0, 253));
		}
		List<Manufactures> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(),0);
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size()) + ")");
		}
		try {
			if (dto != null && !dto.equals("")) {
				// dto.setId(id);
				Date dt = new Date();
				// dto.setBannerImage(bannerImage);
				dto.setCreatedTime(dt.getTime() / 1000);
				session.persist(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public List<Manufactures> checkSlug(String slug, long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Manufactures.class);

		if (slug != null && !slug.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (slug like '" + slug + "%') "));
		}
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id  != '" + id + "') "));
		}
		List<Manufactures> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Manufactures> getAllManufacturesPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Manufactures.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or "
					+ " UPPER(address) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or "
							+ "UPPER(website) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Manufactures> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllManufactures(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Manufactures.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or "
					+ " UPPER(address) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or "
							+ "UPPER(website) LIKE '%"+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Manufactures> list = criteria.list();
		return list.size();
	}

	public Manufactures update(Manufactures re) {
		Manufactures dto = new Manufactures();
		dto = re;
		if(re.getSlug()!=null && re.getSlug().length()>=254){
			re.setSlug(re.getSlug().substring(0, 253));
		}
		List<Manufactures> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(),dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size() + ")"));
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				session.update(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}
	public long getIdManufacturesByName(String name){
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Manufactures.class);

			if (name != null && !name.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (UPPER(name) = '" + name.toUpperCase().trim() + "') "));
			}
			List<Manufactures> list = criteria.list();
			if (list != null && list.size() > 0) {
				return list.get(0).getId();
			}	
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}
	public long manufactureByName(String name) {
		long id =0;
		if(name!=null && !"".equals(name) && !"-".equals(name)){
			id = getIdManufacturesByName(name);
			if (id > 0) {
				return id;
			} else {
				Session session = this.sessionFactory.getCurrentSession();
				try {
					Manufactures mn = new Manufactures();
					mn.setId(0);
					mn.setName(name);
					ConvetSlug cv = new ConvetSlug();
					mn.setSlug(cv.toUrlFriendly(name,1));
					mn.setSeoIndex(1);
					Date d = new Date();
					mn.setCreatedTime(d.getTime() / 1000);
					mn.setSeoDescription(name);
					mn.setSeoKeywords(name);
					mn.setSeoTitle(name);
					session.persist(mn);
					session.flush();
					return mn.getId();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				// add manufactureByName
			}
		}
		
		return 0;
	}

}
