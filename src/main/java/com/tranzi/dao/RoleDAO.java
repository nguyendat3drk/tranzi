package com.tranzi.dao;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.NullPrecedence;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Role;
@Repository
public class RoleDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	
	public List<Role> getAllRolesList() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Role> RoleList = session.createQuery("from Role WHERE delete_state = 0").list();
		
		/*final ResponseBuilder responseBuilder = Response.status(200);
		responseBuilder.entity(RoleList.get(0));
		responseBuilder.status(202);*/
//	    return Response.status(201).entity(RoleList.get(0)).build();
//		for (Role role : RoleList) {
//			session.evict(role);
//			role.setCreatedate(convertUTCtoLocalTime(role.getCreatedate()));
//		}
		return RoleList;
	}

	public Role getRole(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Role> roleList = session.createQuery("from Role where id = "+id+" and delete_state = 0").list();
			Role dt = new Role();
			if(roleList!=null && roleList.size()>0){
				dt = roleList.get(0);
			}
			return dt;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}
	public String validateRole(Role role){
		if(role!=null){
			if(role.getName().isEmpty()){
				return "Role Name not null.";
			}else if(role.getName().length()>50){
				return "Role Name max length 50.";
			}
			if(role.getDescription()!=null &&role.getDescription().length()>254){
				return "Role Description max length 254.";
			}
		}
		return null;	
	}
	public int getIdRole(){
		try {
			int  id = 0;
			Session session = this.sessionFactory.getCurrentSession();
			String sql = " SELECT AUTO_INCREMENT as id "
						+" FROM information_schema.tables "
						+" WHERE table_name = 'role' "
						+" AND table_schema = DATABASE( )  ";
			Query query = session.createSQLQuery(sql);
			List<BigInteger> rows = query.list();
			if (rows != null && rows.size() > 0) {
				BigInteger row;
				row = rows.get(0);
					id= Integer.parseInt(row.toString());
					}
			
			return id;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}
	
	private Date getUTCDateTime() {
		Date mDate = null;
		  try {
		   String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
		   final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		   sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		   final String utcTime = sdf.format(new Date());
		   mDate = dateFormat.parse(utcTime);
		  } catch (Exception e) {
		   e.printStackTrace();
		  }
		  return mDate;
	}
	
	public int addRole(Role role) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String strValidate = "";
			strValidate= validateRole(role);
			if(strValidate!=null && !strValidate.equals("")){
				return 0;
			}
			int id =0;
			id = getIdRole();
			
			if(id>0){
				role.setId(id);
			}
			Date date = new Date();
			role.setCreatedate(date);
//			role.setCreatedate(date);
			
//			Session session = this.sessionFactory.getCurrentSession();
			String sql = "insert into role (id, role_name,status,description,created_date,type) values (:id,:name,:st,:de,:date,:type)";
			Query query = session.createSQLQuery(sql);
			System.out.println(sql);
			query.setParameter("id", role.getId());
			query.setParameter("name", role.getName().trim());
			query.setParameter("st", role.getStatus());
			query.setParameter("de", role.getDescription());
			query.setParameter("date", getUTCDateTime());
			query.setParameter("type", role.getType());
			query.executeUpdate();
//			session.persist(role);
			return id;	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public ResponseEntity<?> updateStatusRole(Role role) {
		Session session = this.sessionFactory.getCurrentSession();
		role.setLastModify(getUTCDateTime());
		role.setCreatedate(role.getCreatedate());
		session.update(role);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	public ResponseEntity<?> saveRole(Role role) {
		Session session = this.sessionFactory.getCurrentSession();
		role.setLastModify(getUTCDateTime());
		session.update(role);
		return new ResponseEntity(HttpStatus.OK);
	}
	
//	public ResponseEntity<?> updateRole(Role role) {
//		Session session = this.sessionFactory.getCurrentSession();
//		role.setLastModify(getUTCDateTime());
//		role.setCreatedate(convertLocalTimeToUTC(role.getCreatedate()));
//		session.update(role);
//		return new ResponseEntity(HttpStatus.OK);
//	}
	
	public ResponseObject deleteRoleList(String roles) {
		  Session session = this.sessionFactory.getCurrentSession();
		  ResponseObject responseObject = new ResponseObject();
          try {
        	  SQLQuery query = session.createSQLQuery("DELETE FROM menu_permission WHERE role_id IN (" + roles + ")");
              query.executeUpdate();
         
              SQLQuery strQuery = session.createSQLQuery("UPDATE role SET delete_state = 1, last_modify = :lastModify WHERE id IN (" + roles + ")");
              	strQuery.setParameter("lastModify", getUTCDateTime());
                strQuery.executeUpdate();

                responseObject.setResultCode(200);
                responseObject.setObject(roles);
                responseObject.setErrorMessage("Success!");
          } catch(Exception e) {
        	  e.printStackTrace();
        	  responseObject.setResultCode(500);
              responseObject.setObject(null);
              responseObject.setErrorMessage("Something Wrong!");
          }
            return responseObject;
	 }

//	public ResponseEntity<?> deleteRole(Integer[] ids) {
//		Session session = this.sessionFactory.getCurrentSession();
//		Role p = new Role();
//		if (null != p) {
//			String sql = "delete from menu_permission WHERE menu_permission.role_id IN (:ids); delete from menu_permission WHERE menu_permission.role_id IN (:role_ids)";
//			Query query = session.createSQLQuery(sql);
//			query.setParameter("ids", ids);
//			query.setParameter("role_ids", ids);
//			query.executeUpdate();
//			return new ResponseEntity(HttpStatus.OK);
//		}
//		return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
//	}

	//can search Role by role or id or name or both
	public List<Role> searchRoles(Integer role, Integer id, String name) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.add(Restrictions.eq("deleteState", 0));
		if(id != null && id >=0){
			criteria.add(Restrictions.eq("id", id));
		}
		if(name != null){
			criteria.add(Restrictions.eq("name", name));
		}
		List<Role> result = criteria.list();
		for (Role eachRole : result) {
			session.evict(eachRole);
			if(eachRole.getCreatedate() == null){
				continue;
			}
			eachRole.setCreatedate(convertUTCtoLocalTime(eachRole.getCreatedate()));
		}
		return result;
	}
	
	//get Role paging
	//input page size and page number
	public List<Role> getRolePagingList(int page, int pageSize, String columnName, String typeOrder){
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.add(Restrictions.eq("deleteState", 0));
		if(typeOrder !=null && !typeOrder.equals("")){
			if(typeOrder.equals("desc")){
				if(columnName.equals("createdate")){
					criteria.addOrder(Order.desc(columnName).nulls(NullPrecedence.FIRST));
				}else{
				   criteria.addOrder(Order.desc(columnName));
				}
			}else if(typeOrder.equals("asc")){
				criteria.addOrder(Order.asc(columnName));
			}
		}
		
		if(page>=0 && pageSize>=0){
			criteria.setFirstResult((page-1)*pageSize);
			criteria.setMaxResults(pageSize);
		}
		List<Role> result = criteria.list();
		for (Role eachRole : result) {
			session.evict(eachRole);
			if(eachRole.getCreatedate() == null){
				continue;
			}
			eachRole.setCreatedate(convertUTCtoLocalTime(eachRole.getCreatedate()));
		}
		return result;
	}
	
	//get all agent
	public List<Role> getAllRoles(int id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		if(id==1){
		   criteria.add(Restrictions.eq("status", 1));
		}
		criteria.add(Restrictions.eq("deleteState", 0));
		List<Role> result = criteria.list();
//		for (Role eachRole : result) {
//			session.evict(eachRole);
//			if(eachRole.getCreatedate() == null){
//				continue;
//			}
//			eachRole.setCreatedate(convertUTCtoLocalTime(eachRole.getCreatedate()));
//		}
		return result;
	}
	
	//get all supervisor
	public List<Role> getAllSupervisors() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
		criteria.add(Restrictions.eq("status", 1));
		criteria.add(Restrictions.eq("deleteState", 0));
		return criteria.list();
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Role.class);
//		criteria.add(Restrictions.eq("status", 1));
		criteria.add(Restrictions.eq("deleteState", 0));
		return criteria.list().size();
	}
	public int checkUnique(Role role){
		Session session = this.sessionFactory.getCurrentSession();
		int isCheck = 0;
		try {
			String nameC ="";
			nameC =role.getName().trim();
			 String sql = "select count(1) from role where UPPER(role_name) = UPPER('"+nameC+"') and id != "+role.getId()+" AND delete_state = 0";
			Query query = session.createSQLQuery(sql);
			BigInteger count = (BigInteger) query.uniqueResult();
			isCheck = count.intValue();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
		return isCheck;
	}
	
	public int checkDelete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "select count(*) from user where user.role = :id and user.status = 1 and user.delete_state = 0";
			Query query = session.createSQLQuery(sql);
			query.setParameter("id", id);
			BigInteger count = (BigInteger) query.uniqueResult();
			int result = count.intValue();
			return result;	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}
	
	public ResponseObject checkDeleteList(String roles) {
		Session session = this.sessionFactory.getCurrentSession();
		BigInteger count = null;
		ResponseObject responseObject = new ResponseObject();
		try {
			SQLQuery query = session.createSQLQuery("select count(1) from user where user.role IN (:roles)  and user.delete_state = 0");
			query.setParameter("roles", roles);
			count = (BigInteger) query.uniqueResult();
			int result = count.intValue();
			String errr = "Count available delete list";
			if(result>0){
				errr = checkDeleteListName(roles);
			}
			responseObject.setResultCode(200);
			responseObject.setObject(result);
			responseObject.setErrorMessage(errr);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		return responseObject;
	}
	public String checkDeleteListName(String roles) {
		Session session = this.sessionFactory.getCurrentSession();
		List<String> count = null;
		ResponseObject responseObject = new ResponseObject();
		String re = "";
		try {
			SQLQuery query = session.createSQLQuery("select r.role_name from user u left join role r on u.role = r.id where u.role IN (:roles)  ");
			query.setParameter("roles", roles);
			count =  (List<String>) query.list();
			if(count!=null && count.size()>0){
				for(String ot: count){
					if(re==null || re.equals("")){
						re = ot;
					}else{
						re =re+ ", "+ot;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		return re;
	}
	
	private Date convertUTCtoLocalTime(Date date) {
		Date convertedDate = null;
		try {
	         String utcTimeString = date.toString();

	         DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	         utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	         Date utcTime = utcFormat.parse(utcTimeString);


	         DateFormat localFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	         localFormat.setTimeZone(TimeZone.getDefault());
	         String convertedString = localFormat.format(utcTime);
	         convertedDate = localFormat.parse(convertedString);
	         System.out.println("Local: " + localFormat.format(utcTime));

	        } catch (ParseException e) {

	        }
		
		return convertedDate;
	}
	
//	private Date convertLocalTimeToUTC(Date date) {
//		Date convertedDate = null;
//		try {
//	         DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	         utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//	         String convertedString = utcFormat.format(date);
//	         DateFormat receiveFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	         convertedDate = receiveFormat.parse(convertedString);
//	        } catch (ParseException e) {
//
//	        }
//		
//		return convertedDate;
//	}
	
}
