package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.CustomerOrder;

@Repository
public class CustomerDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	public Customer getById(long id) {
		List<Customer> list = new ArrayList<Customer>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from customer where id = ? ").setParameter(0,
					id);
			query.addEntity(Customer.class);
			list = query.list();
			if(list!=null && list.size()>0){
				return list.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public ResponseObject delete(long id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		if(!checkDeleteCustomer(id)){
			re.setResultCode(205);
			re.setErrorMessage("Customer đã có chi tiết, không thể xóa");;
			return re;
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from customer where id = ? ").setParameter(0, id).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
		}
		return re;
	}

	public boolean checkDeleteCustomer(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<CustomerOrder> list = new ArrayList<CustomerOrder>();
		try {
			// session.createSQLQuery("select * from Customer_values where
			// Customer_id = ? ").setParameter(0, id);
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from customerorder where customer_id = ? ")
					.setParameter(0, id);
			query.addEntity(CustomerOrder.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Customer> getAlls() {
		List<Customer> list = new ArrayList<Customer>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from customer  where 1 =1 ");
			query.addEntity(Customer.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject create(Customer re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Customer dto = new Customer();
		
		dto = re;
		if (isAccountEmailExists(dto)) {
			respon.setResultCode(201);
			respon.setObject(dto);
			respon.setErrorMessage("The email " + dto.getEmail() + " existed. Please choose another email");
		} else {
			try {
				if (dto != null && !dto.equals("")) {
					Session session = this.sessionFactory.getCurrentSession();
					dto.setId(0);
					Date dt = new Date();
					dto.setCreatedAt(Integer.parseInt((dt.getTime()/1000)+""));
					dto.setIsEmailVerified(0);
					session.persist(dto);
					respon.setObject(dto);
					respon.setResultCode(200);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				respon.setResultCode(204);
				respon.setErrorMessage("có loi xay ra");
				return respon;
			}
		}
		

		
		return respon;
	}

	public ResponseObject update(Customer re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Customer dto = new Customer();
		dto = re;
//		String pass = "";
//		try {
//			pass = checkUpdatePass(account);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
		if (isAccountEmailExists(dto)) {
			respon.setResultCode(201);
			respon.setObject(dto);
			respon.setErrorMessage("The email " + dto.getEmail() + " existed. Please choose another email");
		} else {
			try {
				Session session = this.sessionFactory.getCurrentSession();
//				dto.setPassword(pass);
				Date dt = new Date();
				dto.setUpdatedAt(Integer.parseInt((dt.getTime()/1000)+""));
				session.update(dto);
				respon.setObject(dto);
				respon.setResultCode(200);
				// responseObject.setObject(putAccount(account));

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				respon.setResultCode(204);
				respon.setObject(e.getMessage());
			}
		}
		return respon;
	}

	@SuppressWarnings("unchecked")
	public List<Customer> getAllCustomer(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Customer.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(full_name) LIKE '%" + keySearch.replace("%", "^").toUpperCase().trim() + "' OR UPPER(email) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Customer> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountCustomer(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Customer.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(full_name) LIKE '%" + keySearch.replace("%", "^").toUpperCase().trim() + "' OR UPPER(email) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Customer> list = criteria.list();
		return list.size();
	}
	private boolean isAccountEmailExists(Customer customer) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (customer != null && customer.getId() > 0) {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Customer.class);
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(email) = '" + customer.getEmail().toUpperCase() + "')"));
				criteria.add(Restrictions.sqlRestriction(" (id != " + customer.getId() + ")"));
				List<Customer> list = criteria.list();
				session.flush();
				if (list.size() > 0)
					return true;

			} else {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Customer.class);
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(email) = '" + customer.getEmail().toUpperCase() + "')"));
				List<Customer> list = criteria.list();
				session.flush();
				if (list.size() > 0)
					return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;
	}
	
}
