package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import com.tranzi.common.CommonConfig;
import com.tranzi.common.ConvetSlug;
import com.tranzi.entity.KeyValue;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ArticleLinkUpload;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.ImportAttribute;
import com.tranzi.model.Inventory;
import com.tranzi.model.ListAttributeAndValues;
import com.tranzi.model.ManufacturesCategory;
import com.tranzi.model.Product;
import com.tranzi.model.ProductSearch;
import com.tranzi.model.ProductsCategories;
import com.tranzi.model.ProductsPrices;
import com.tranzi.model.Tags;

@Repository
public class ProductDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private ProductCategoryDAO productCategoryDAO;

	@Autowired
	private ProductsPricesDAO productsPricesDAO;

	@Autowired
	private MediaDAO mediaDAO;

	@Autowired
	private TagsDAO tagsDAO;

	@Autowired
	private AttributesDAO attributesDAO;

	@Autowired
	private InventoryDAO inventoryDAO;

	@Autowired
	private ManufacturesDAO manufacturesDAO;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Product getById(int id) {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from products where id = ? ").setParameter(0,
					id);
			query.addEntity(Product.class);
			list = query.list();
			// session.flush();
			if (list != null && list.size() > 0) {
				list.get(0).setListCategory(getListCategoryByProduct(id));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public int getCategoryByProduct(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sqls = "select category_id from products_categories where product_id = ? ";
			Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
			List<Integer> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					return 0;
				} else {
					return Integer.parseInt(idm.toString());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return 0;
	}

	public List<Long> getListCategoryByProduct(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Long> list = new ArrayList<>();
		try {
			String sqls = "select category_id from products_categories where product_id = ? ";
			Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
			List<Integer> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				for (Integer in : idMax) {
					idm = in;
					list.add(Long.parseLong(idm + ""));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return list;
	}

	public ResponseObject deleteProduct(int id) {
		ResponseObject re = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		re.setResultCode(200);
		try {
			KeyValue check = validateDeleteProduct(id);
			re.setData(check);
			if (check.getValue().equals("0")) {
				session.createSQLQuery("delete from products where id = ? ").setParameter(0, id).executeUpdate();
				// xoa product ra khoi count category
				try {
					removeCountProduct(Long.parseLong(id + ""));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else {
				re.setResultCode(201);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(201);
		}
		return re;
	}

	public ResponseObject deleteListID(@RequestBody List<Integer> list) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		List<KeyValue> listData = new ArrayList<KeyValue>();

		if (list != null && list.size() > 0) {
			for (Integer dt : list) {
				KeyValue check = new KeyValue();
				try {
					check = validateDeleteProduct(dt);
					if (check.getValue().equals("0")) {
						deleteProduct(dt);
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					check.setData("Xóa thất bại");
				}
				listData.add(check);

			}
		}

		re.setData(listData);
		return re;

	}

	public KeyValue validateDeleteProduct(int id) {
		KeyValue dt = new KeyValue();
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Product.class);
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id = " + id + ") "));
		}
		List<Product> list = criteria.list();
		dt.setName(list.get(0).getSku());
		if (list != null && list.size() > 0) {
			dt.setValue("0");
		}
		dt.setData("Xóa thành công");
		// try {
		// String sqls = "select count(1) as cou from customer_order_product cup
		// where cup.product_id = ? ";
		// Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
		// List<Object> idMax = query3.list();
		// if (idMax != null && idMax.size() > 0) {
		// Integer idm;
		// for (Object in : idMax) {
		// if (Integer.parseInt(in + "") > 0) {
		// // Đã có đơn đặt hàng
		// dt.setData("Đã có đơn đặt hàng không thể xóa");
		// dt.setValue("1");
		// }
		// }
		// }
		// } catch (Exception e) {
		// // TODO: handle exception
		// e.printStackTrace();
		// }
		//
		// try {
		// String sqls = "select count(1) as cou from inventory ito where
		// ito.product_id = ? ";
		// Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
		// List<Object> idMax = query3.list();
		// if (idMax != null && idMax.size() > 0) {
		// Integer idm;
		// for (Object in : idMax) {
		// if (Integer.parseInt(in + "") > 0) {
		// // San phẩm đã add vào kho
		// dt.setData("Sản phẩm đã được thêm vào kho. Không thể xóa");
		// dt.setValue("2");
		// }
		// }
		// }
		// } catch (Exception e) {
		// // TODO: handle exception
		// e.printStackTrace();
		// }
		return dt;
	}

	public int deleteAllProduct() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			try {
				System.out.println("=========Start Xoa All====" + (new Date()).toString());
				// session.createSQLQuery("delete from
				// products").executeUpdate();
				// session.createSQLQuery("delete from
				// products_files").executeUpdate();
				// session.createSQLQuery("delete from
				// products_media").executeUpdate();
				// session.createSQLQuery("delete from
				// products_prices").executeUpdate();
				// session.createSQLQuery("delete from
				// products_related").executeUpdate();
				// session.createSQLQuery("delete from
				// products_attribute_values").executeUpdate();
				// session.createSQLQuery("delete from
				// attributes").executeUpdate();
				// session.createSQLQuery("delete from
				// attribute_values").executeUpdate();
				// session.createSQLQuery("delete from
				// products_categories").executeUpdate();
				// session.createSQLQuery("delete from
				// categories_attributes").executeUpdate();
				// session.createSQLQuery("delete from
				// categories").executeUpdate();
				// session.createSQLQuery("delete from media").executeUpdate();
				// session.createSQLQuery("delete from
				// manufactures").executeUpdate();
				// session.createSQLQuery("delete from
				// customer_order").executeUpdate();
				// session.createSQLQuery("delete from
				// customer_order_product").executeUpdate();
				// session.createSQLQuery("delete from
				// inventory").executeUpdate();
				System.out.println("=========End Xoa All====" + (new Date()).toString());
			} catch (Exception e) {
				// TODO: handle exception
			}

			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public List<ListAttributeAndValues> getListIDAttributeValue(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Attributes> listARespent = new ArrayList<>();
		List<ListAttributeAndValues> respon = new ArrayList<>();
		try {
			// List<AttributeValues> list = session
			// .createSQLQuery(
			// "SELECT * FROM attribute_values where id in (select
			// attribute_value_id from products_attribute_values where
			// product_id = ? )")
			// .addEntity(AttributeValues.class).setParameter(0, id).list();

			List<AttributeValues> list = session
					.createSQLQuery(
							"SELECT * FROM attribute_values where id in (SELECT attribute_value_id FROM  products_attribute_values where  product_id = ?)")
					.addEntity(AttributeValues.class).setParameter(0, id).list();
			// System.out.println(list);
			List<Attributes> listAt = getListAttribute();
			if (list != null && list.size() > 0 && listAt != null && listAt.size() > 0) {
				for (AttributeValues dto : list) {
					if (dto.getAttributeId() > 0) {
						for (Attributes c : listAt) {
							if (dto.getAttributeId() == c.getId()) {
								if (listARespent == null || listARespent.size() <= 0) {
									listARespent.add(c);
									break;
								} else {
									boolean check = true;
									for (Attributes chck : listARespent) {
										if (dto.getAttributeId() == chck.getId()) {
											check = false;
											break;
										}
									}
									if (check) {
										listARespent.add(c);
										break;
									}
								}
							}
						}
					}
				}
				if (listARespent != null && listARespent.size() > 0) {
					ListAttributeAndValues r = new ListAttributeAndValues();
					List<AttributeValues> listDetail = new ArrayList<>();
					for (Attributes dto : listARespent) {
						r = new ListAttributeAndValues();
						listDetail = new ArrayList<>();
						r.setAttribute(dto);
						for (AttributeValues dtoDetail : list) {
							if (dtoDetail.getAttributeId() == dto.getId()) {
								listDetail.add(dtoDetail);
							}

						}
						r.setListAttributeValues(listDetail);
						respon.add(r);
					}
				}
			}

			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public List<Attributes> getListAttribute() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Attributes.class);
		List<Attributes> list = criteria.list();
		return list;
	}

	public List<Product> getAlls() {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from products  where 1 =1 ");
			query.addEntity(Product.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public List<Product> getAllsNotDetail() {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from products  where 1 =1 ");
			query.addEntity(Product.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public List<Product> getAllProduct() {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session
					.createSQLQuery("select products.*,categories.name as categoriesName  from products "
							+ " left join products_categories " + " on products.id = products_categories.product_id "
							+ " left join categories  on products_categories.category_id = categories.id "
							+ " where 1 =1");
			query.addEntity(Product.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Product> getAllProductPagerDuphong(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(Product.class);
			if (typeOrder != null && !typeOrder.equals("")) {
				if (typeOrder.equals("desc")) {
					criteria.addOrder(Order.desc(columnName));
				} else if (typeOrder.equals("asc")) {
					criteria.addOrder(Order.asc(columnName));
				}
			}
			if (page >= 0 && pageSize >= 0) {
				criteria.setFirstResult((page - 1) * pageSize);
				criteria.setMaxResults(pageSize);
			}
			if (keySearch != null && !keySearch.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (name = '" + keySearch.replace("%", "^")
						+ "' OR name LIKE '%" + keySearch.replace("%", "^") + "%')"));
			}
			List<Product> list = criteria.list();
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Product> getAllProductPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Product> list = new ArrayList<Product>();
			String sql = "SELECT p.id, " + "   p.name, " + "   p.created_time, " + "   p.normal_price, "
					+ "   p.sale_price, " + "   p.sale_start, " + "   p.sale_end, " + "   p.sku, " + "   p.orgin_sku, "
					+ "   p.manufacture_id, " + "   p.in_stock, " + "   p.orgin_from, "
					+ "   case when actived=1 then '1'else '0' end, " + "   p.category_id, "
					+ "   ca.name as categoryName, "
					+ "   ma.name as manufactureName,thumbnail_image, p.product_status as product_status"
					+ " FROM products p " + " left join categories ca on p.category_id = ca.id  "
					+ " left  join manufactures ma on p.manufacture_id = ma.id " + " where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(ca.name) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(p.sku) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(ma.name) like '%" + keySearch.toUpperCase().trim() + "%' )";
			}
			sql = sql + " order by p.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Product dto;
				list = new ArrayList<Product>();
				for (Object[] row : rows) {
					dto = new Product();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
						dto.setName(row[1] != null ? row[1].toString() : "");
						dto.setCreatedTime(Integer.parseInt(row[2] != null ? row[2].toString() : "0"));
						dto.setNormalPrice(Integer.parseInt(row[3] != null ? row[3].toString() : "0"));
						dto.setSalePrice(Integer.parseInt(row[4] != null ? row[4].toString() : "0"));
						dto.setSaleStart(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
						dto.setSaleEnd(Integer.parseInt(row[6] != null ? row[6].toString() : "0"));
						dto.setSku(row[7] != null ? row[7].toString() : "");
						dto.setOrginSku(row[8] != null ? row[8].toString() : "");
						dto.setManufactureId(Integer.parseInt(row[9] != null ? row[9].toString() : "0"));
						dto.setInStock(Integer.parseInt(row[10] != null ? row[10].toString() : "0"));
						dto.setOrginFrom(row[11] != null ? row[11].toString() : "");
						dto.setActived(Integer.parseInt(row[12] != null ? row[12].toString() : "0"));
						dto.setCategoryId(Integer.parseInt(row[13] != null ? row[13].toString() : "0"));
						dto.setCategoriesName(row[14] != null ? row[14].toString() : "");
						dto.setManufactureName(row[15] != null ? row[15].toString() : "");
						dto.setThumbnailImage(row[16] != null ? row[16].toString() : "");
						dto.setProductStatus(row[17] != null ? row[17].toString() : "");
					}

					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public int getCountAllProduct(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Object[]> list = new ArrayList<>();

		try {
			String sql = "SELECT p.id" + " FROM products p " + " left join categories ca on p.category_id = ca.id  "
					+ " left  join manufactures ma on p.manufacture_id = ma.id " + " where 1=1 ";

			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(ca.name) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(p.sku) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(ma.name) like '%" + keySearch.toUpperCase().trim() + "%' )";
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (list == null) {
			return 0;
		}
		return list.size();
	}

	public ResponseObject create(Product re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Product dto = new Product();
		dto = re;
		List<Product> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (!validaeSku(dto.getSku(), null, 0)) {
			respon.setResultCode(204);
			respon.setErrorMessage("Mã sản phẩm đã tồn tại");
			return respon;
		}
		// if (!validaeSku(null, dto.getOrginSku(), 0)) {
		// respon.setResultCode(204);
		// respon.setErrorMessage("Mã sản phẩm từ NSX đã tồn tại");
		// return respon;
		// }
		if (dto.getCategoryId() == null && dto.getListCategory() != null && dto.getListCategory().size() > 0) {
			dto.setCategoryId(dto.getListCategory().get(0).intValue());
		}
		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "-" + (list.size() + 1));
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				dto.setAuthorId(1);
				dto.setCreatedTime(dt.getTime() / 1000);
				session.persist(dto);
				respon.setObject(dto);
				session.flush();
				// session.
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		if (dto.getId() > 0) {
			//	add linkUpload file
			if (dto.getId() > 0 && dto.getListArticleLinkUpload() != null
					&& dto.getListArticleLinkUpload().size() > 0) {
				for (int i = 0; i < dto.getListArticleLinkUpload().size(); i++) {
					ArticleLinkUpload d = new ArticleLinkUpload();
					d = dto.getListArticleLinkUpload().get(i);
					if (d.getStatus() > 0) {
						d.setType(2);
						d.setArticleId(dto.getId());
						createArticleLinkUpload(d);
					}
				}
			}

			// add ProductsPrices
			try {
				ProductsPrices pp = new ProductsPrices();
				pp.setId(0);
				pp.setProductId((int) dto.getId());
				pp.setMinQuantity((int) dto.getMinOrder());
				pp.setPrice(dto.getNormalPrice());
				pp.setSalePrice(dto.getSalePrice());
				productsPricesDAO.create(pp);
			} catch (Exception e) {
				// TODO: handle exception
			}
			// add product Media
			try {
				mediaDAO.createListMediaAndelete(dto.getListMediaThumbnail(), dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
			}
			// add product file
			try {
				mediaDAO.createListMediaAndeleteFile(dto.getListMedia(), dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add attribute values
			try {
				if (dto.getListAttributeAndValues() != null && dto.getListAttributeAndValues().size() > 0) {
					deleteAttributeValueById(dto.getId());
					addAttributeValueProduct(dto.getListAttributeAndValues(), dto.getId(), dto.getCategoryId());
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			// add tags
			try {
				if (dto.getListTags() != null && dto.getListTags().size() > 0) {
					for (Tags ta : dto.getListTags()) {
						ta.setProductId(Integer.parseInt(dto.getId() + ""));
					}
					tagsDAO.addTagByProduct(dto.getListTags(), dto.getId());
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
			// add ProductsCategories
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					if (productCategoryDAO.updateProduct(dto.getId(), 0)) {
						for (long cate : dto.getListCategory()) {
							ProductsCategories po = new ProductsCategories();
							po.setProductId(Integer.parseInt(dto.getId() + ""));
							po.setCategoryId(Integer.parseInt(cate + ""));
							productCategoryDAO.addProduct(po, 1);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					addCountProductCategory(dto.getListCategory());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				Inventory in = new Inventory();
				in.setId(0);
				in.setProductId(dto.getId());
				in.setStock(dto.getInStock());
				in.setCurrentStock(dto.getInStock());
				Date d = new Date();
				in.setCreatedAt(d.getTime() / 1000);
				in.setUpdatedAt(d.getTime() / 1000);
				inventoryDAO.create(in);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if (dto.getCategoryId() > 0 && dto.getManufactureId() > 0) {
					addCategoryManfactory(dto.getCategoryId(), dto.getManufactureId());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}return respon;

	}

	public void addCountProductCategory(List<Long> list) {
		if (list != null && list.size() > 0) {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Categories.class);

				criteria.add(Restrictions.in("id", list));
				List<Categories> listCategory = criteria.list();
				if (listCategory != null && listCategory.size() > 0) {
					String listID = "";
					for (Categories dto : listCategory) {
						if (listID == null || "".equals(listID)) {
							listID = dto.getId() + "";
						} else {
							listID = listID + "," + dto.getId();
						}
						if (dto.getParentId() != null && dto.getParentId() > 0) {
							if (listID == null || "".equals(listID)) {
								listID = dto.getParentId() + "";
							} else {
								listID = listID + "," + dto.getParentId();
							}
						}

					}
					if (listID != null && listID != "") {
						session.createSQLQuery(
								"UPDATE categories set count_product = count_product +1 where id in (" + listID + ")")
								.executeUpdate();
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public List<Product> checkSlug(String slug, long id) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Product.class);

			if (slug != null && !slug.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (slug = '" + slug + "') "));
			}
			if (id > 0) {
				criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
			}
			List<Product> list = criteria.list();
			return list;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	public ResponseObject updateProduct(Product re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Product dto = new Product();
		dto = re;
		Date st = new Date();
		if (dto.getSaleStart() != null && dto.getSaleStart() > 0) {
			st.setTime(dto.getSaleStart());
			dto.setSaleStart((int) st.getTime());
		}

		Date ed = new Date();
		if (dto.getSaleEnd() != null && dto.getSaleEnd() > 0) {
			ed.setTime(dto.getSaleEnd());
			dto.setSaleEnd((int) ed.getTime());
		}
		if (!validaeSku(dto.getSku(), null, dto.getId())) {
			respon.setResultCode(204);
			respon.setErrorMessage("Mã sản phẩm đã tồn tại");
			return respon;
		}
		// if (!validaeSku(null, dto.getOrginSku(), dto.getId())) {
		// respon.setResultCode(204);
		// respon.setErrorMessage("Mã sản phẩm từ NSX đã tồn tại");
		// return respon;
		// }
		List<Product> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (dto.getCategoryId() == null && dto.getListCategory() != null && dto.getListCategory().size() > 0) {
			dto.setCategoryId(dto.getListCategory().get(0).intValue());
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "-" + (list.size() + 1));
		}
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				session.flush();
				respon.setObject(dto);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		if (dto.getId() > 0) {
//			add linkUpload file
			if (dto.getId() > 0 && dto.getListArticleLinkUpload() != null
					&& dto.getListArticleLinkUpload().size() > 0) {
				for (int i = 0; i < dto.getListArticleLinkUpload().size(); i++) {
					ArticleLinkUpload d = new ArticleLinkUpload();
					d = dto.getListArticleLinkUpload().get(i);
					if (d.getStatus() > 0) {
						d.setType(2);
						d.setArticleId(dto.getId());
						createArticleLinkUpload(d);
					}
				}
			}
			// add ProductsPrices
			try {
				ProductsPrices pp = new ProductsPrices();
				pp.setId(0);
				pp.setProductId((int) dto.getId());
				pp.setMinQuantity((int) dto.getMinOrder());
				pp.setPrice(dto.getNormalPrice());
				productsPricesDAO.update(pp);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add product Media
			try {
				mediaDAO.createListMediaAndelete(dto.getListMediaThumbnail(), dto.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// add product file
			try {
				mediaDAO.createListMediaAndeleteFile(dto.getListMedia(), dto.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			// add attribute values
			try {
				deleteAttributeValueById(dto.getId());
				addAttributeValueProduct(dto.getListAttributeAndValues(), dto.getId(), dto.getCategoryId());

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add tags
			try {
				if (dto.getListTags() != null && dto.getListTags().size() > 0) {
					for (Tags ta : dto.getListTags()) {
						ta.setProductId(Integer.parseInt(dto.getId() + ""));
					}
					tagsDAO.addTagByProduct(dto.getListTags(), dto.getId());
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// delete caount product count category
			try {
				removeCountProduct(dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add count product count category
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					addCountProductCategory(dto.getListCategory());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add ProductsCategories
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					if (productCategoryDAO.updateProduct(dto.getId(), 0)) {
						for (long cate : dto.getListCategory()) {
							ProductsCategories po = new ProductsCategories();
							po.setProductId(Integer.parseInt(dto.getId() + ""));
							po.setCategoryId(Integer.parseInt(cate + ""));
							productCategoryDAO.addProduct(po, 1);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if (dto.getCategoryId() > 0 && dto.getManufactureId() > 0) {
					addCategoryManfactory(dto.getCategoryId(), dto.getManufactureId());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
		return respon;
	}

	public boolean validaeSku(String sku, String skuOgrin, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(Product.class);

			if (sku != null && !sku.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (UPPER(sku) = '" + sku.toUpperCase().trim() + "') "));
			}
			if (skuOgrin != null && !skuOgrin.equals("")) {
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(orgin_sku) = '" + skuOgrin.toUpperCase().trim() + "') "));
			}
			if (id > 0) {
				criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
			}
			List<Product> list = criteria.list();
			if (list != null && list.size() > 0) {

				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}

	// validate SkuAnd update
	public Product validaeSkuUpdate(String sku, String skuOgrin, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(Product.class);

			if (sku != null && !sku.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (UPPER(sku) = '" + sku.toUpperCase().trim() + "') "));
			}
			if (skuOgrin != null && !skuOgrin.equals("")) {
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(orgin_sku) = '" + skuOgrin.toUpperCase().trim() + "') "));
			}
			if (id > 0) {
				criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
			}
			List<Product> list = criteria.list();
			if (list != null && list.size() > 0) {

				return list.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	// validate SkuAnd update Import
	public Product validaeSkuUpdateImport(String sku, String skuOgrin, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(Product.class);

			if (sku != null && !sku.equals("")) {
				criteria.add(Restrictions.sqlRestriction(" (UPPER(sku) = '" + sku.toUpperCase().trim() + "') "));
			}
			if (skuOgrin != null && !skuOgrin.equals("")) {
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(orgin_sku) = '" + skuOgrin.toUpperCase().trim() + "') "));
			}
			if (id > 0) {
				criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
			}
			List<Product> list = criteria.list();
			if (list != null && list.size() > 0) {
				session.flush();
				return list.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	// add list attribute value and product
	public void addAttributeValueProduct(List<ListAttributeAndValues> list, long id, Integer idCategory) {
		if (list != null && list.size() > 0) {
			for (ListAttributeAndValues dto : list) {
				if (dto != null && dto.getAttribute() != null && dto.getAttribute().getId() >= 0) {
					Attributes at = new Attributes();
					at.setId(dto.getAttribute().getId());
					at.setName(dto.getAttribute().getName());
					at.setNameEn((dto.getAttribute().getNameEn() != null
							&& !"".equals(dto.getAttribute().getNameEn() != null)) ? dto.getAttribute().getNameEn()
									: dto.getAttribute().getName());
					try {
						Attributes atnew = new Attributes();
						if (at.getId() == 0) {
							atnew = addAttributes(at);
						}else{
							atnew =at; 
						}
						if (atnew.getId() > 0 && dto.getListAttributeValues() != null
								&& dto.getListAttributeValues().size() > 0) {
							int newAttributeValue = 0;
							for (AttributeValues avlue : dto.getListAttributeValues()) {
								// avlue.setId(avlue.getId());
								if (avlue.getId() > 0) {
									deleteAttibuteValueId(avlue.getId());
								}
								avlue.setId(0);
								avlue.setCategoryId(idCategory != null ? idCategory : 0);
								avlue.setAttributeId(atnew.getId());
								// avlue.setProductId(Integer.parseInt(id+""));
								AttributeValues avlueNew = new AttributeValues();
								avlueNew = addAttributeValues(avlue);
								// avlueNew = avlue;
								if (avlueNew.getId() > 0) {
									addAttributeValuesProduct(avlueNew, id, avlueNew.getId());
								}
								newAttributeValue++;
							}
							System.out.println("--------Add được atribute value:" + newAttributeValue);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
		}

	}

	public Attributes addAttributes(Attributes aty) {
		Attributes dto = new Attributes();
		dto = aty;
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.persist(dto);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return dto;
	}

	public AttributeValues addAttributeValues(AttributeValues aty) {
		AttributeValues dto = new AttributeValues();
		dto = aty;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			// if (dto.getId() > 0) {
			// session.merge(dto);
			// } else {
			session.persist(dto);
			// }
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public void addAttributeValuesProduct(AttributeValues aty, long id, long attibutesID) {
		AttributeValues avlueNew = new AttributeValues();
		long ids = 0;
		ids = id;
		avlueNew = aty;

		try {
			if (checkProductAttributeValue(ids, avlueNew.getId(), attibutesID) <= 0) {
				Session session = this.sessionFactory.getCurrentSession();
				session.createSQLQuery(
						"INSERT INTO products_attribute_values (product_id,attribute_value_id,attribute_id) VALUES  (?,?,?) ")
						.setParameter(0, ids).setParameter(1, avlueNew.getId()).setParameter(2, attibutesID)
						.executeUpdate();
				session.flush();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// add list attribute value and product
	public void deleteAttributeValueProduct(long productID, long attibutesID) {
		Session session = this.sessionFactory.getCurrentSession();
		if (productID > 0) {
			try {
				session.createSQLQuery("DELETE FROM  products_attribute_values where attribute_id = " + attibutesID
						+ " and product_id = " + productID + "").executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}
	}
	public void deleteAttributeValueByIdImport(long attibutesID, long idCategory, long productID) {
		try {
			if (attibutesID > 0) {
				if (attibutesID > 0 && productID > 0) {
					String sql2 = "DELETE FROM  products_attribute_values where attribute_id = " + attibutesID
							+ " and product_id = " + productID + "";
					System.out.println(sql2);
					Session session = this.sessionFactory.getCurrentSession();
					session.createSQLQuery(sql2).executeUpdate();
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public ResponseObject createImport(Product re, List<ImportAttribute> listAttributes) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Product dto = new Product();
		dto = re;
		List<Product> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		long manufacory = 0;
		try {
			if (dto.getManufactureName() != null && !"".equals(dto.getManufactureName())) {
				manufacory = manufacturesDAO.manufactureByName(dto.getManufactureName());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		dto.setManufactureId(Integer.parseInt(manufacory + ""));
		Product pr = validaeSkuUpdateImport(dto.getSku(), null, 0);
		dto.setSku(dto.getSku() != null ? dto.getSku().trim() : "");
		dto.setId(0);
		if (pr != null && pr.getId() > 0) {
			respon.setResultCode(204);
			respon.setErrorMessage("Mã sản phẩm đã tồn tại");
			// Date dt = new Date();
			// System.out.println("Ma san pham ton tai:" + dt.toString());
			// System.out.println("Update san pham:" + dt.toString());
			dto.setId(pr.getId());
			// return respon;
		}
		// Không bắt trùng mã sản xuất
		// if (!validaeSkuImport(dto)) {
		// respon.setResultCode(204);
		// respon.setErrorMessage("Mã sản phẩm từ NSX đã tồn tại");
		// return respon;
		// }

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "-" + dto.getSku().toLowerCase());
		}
		int checkInsert = 0;
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (dto != null && !dto.equals("")) {

				Date dt = new Date();
				if (dto.getCategoryId() == null && dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					dto.setCategoryId(dto.getListCategory().get(0).intValue());
				}
				dto.setAuthorId(1);
				dto.setCreatedTime(dt.getTime() / 1000);
				// if (dto.getThumbnailImage() != null &&
				// !"".equals(dto.getThumbnailImage())) {
				if (dto.getId() <= 0) {
					session.persist(dto);
					checkInsert = 1;
				} else {
					session.merge(dto);
					checkInsert = 0;
				}

				// }

				respon.setObject(dto);
				session.flush();
				// session.
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		if (dto.getId() > 0) {

			// add ProductsPrices
			try {
				// ProductsPrices pp = new ProductsPrices();
				// pp.setId(0);
				// pp.setProductId((int) dto.getId());
				// pp.setMinQuantity((int) dto.getMinOrder());
				// pp.setPrice((dto.getNormalPrice() == null ||
				// dto.getNormalPrice() <= 0) ? 1 : dto.getNormalPrice());
				// productsPricesDAO.create(pp);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add product Media
			try {
				mediaDAO.createListMediaAndelete(dto.getListMediaThumbnail(), dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add product file
			try {
				mediaDAO.createListMediaAndeleteFile(dto.getListMedia(), dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			System.out.println("product LK:" + dto.getSku());
			// add attribute values
			List<ImportAttribute> listAttribute = new ArrayList<>();
			listAttribute = listAttributes;
			try {
				if (listAttribute != null && listAttribute.size() > 0 && dto.getId() > 0) {
					addAttributeValueProductImport(listAttribute, dto.getId(), dto.getCategoryId(), checkInsert);
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			System.out.println("End product" + dto.getSku());
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0 && checkInsert > 0) {
					addCountProductCategory(dto.getListCategory());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add tags
			try {
				if (dto.getListTags() != null && dto.getListTags().size() > 0) {
					for (Tags ta : dto.getListTags()) {
						ta.setProductId(Integer.parseInt(dto.getId() + ""));
					}
					tagsDAO.addTagByProduct(dto.getListTags(), dto.getId());
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add ProductsCategories
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					if (productCategoryDAO.updateProduct(dto.getId(), 0)) {
						for (long cate : dto.getListCategory()) {
							ProductsCategories po = new ProductsCategories();
							po.setProductId(Integer.parseInt(dto.getId() + ""));
							po.setCategoryId(Integer.parseInt(cate + ""));
							productCategoryDAO.addProduct(po, 1);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				Inventory in = new Inventory();
				in.setId(0);
				in.setProductId(dto.getId());
				in.setStock(dto.getInStock());
				in.setCurrentStock(dto.getInStock());
				Date d = new Date();
				in.setCreatedAt(d.getTime() / 1000);
				in.setUpdatedAt(d.getTime() / 1000);
				inventoryDAO.create(in);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// xoa product ra khoi count category
			try {
				removeCountProduct(dto.getId());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add lại product count cho category moi
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					addCountProductCategory(dto.getListCategory());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if (dto.getCategoryId() > 0 && dto.getManufactureId() > 0) {
					addCategoryManfactory(dto.getCategoryId(), dto.getManufactureId());
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		try {
			resetChmod(CommonConfig.uploadMedia);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return respon;
	}

	public ResponseObject getIDAttributesByName(String name) {

		ResponseObject re = new ResponseObject();
		re.setResultCode(0);
		re.setObject(0);
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Attributes.class);
			String nameCheck = "";
			String nameVNUpdate = "";
			if (name != null) {
				String[] list = name.split(">");
				if (list != null && list.length > 1) {
					nameCheck = list[0];
					if (list.length > 1) {
						nameVNUpdate = list[1] != null ? list[1] : list[0];
					} else {
						nameVNUpdate = nameCheck;
					}

				}
			}
			if ("Impedance".toUpperCase().trim().equals(nameCheck.toUpperCase().trim())) {
				System.out.println(nameCheck);
			}
			if (name != null && !name.equals("")) {
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(name_en) = '" + nameCheck.toUpperCase().trim() + "') "));
				criteria.addOrder(Order.desc("id"));
			} else {
				re.setObject(0);
				return re;
			}
			List<Attributes> list = criteria.list();
			session.flush();
			Attributes dto = new Attributes();
			dto.setId(0);
			if (list != null && list.size() > 0) {
				re.setResultCode(1);
				re.setObject(list.get(0).getId());
				dto = list.get(0);
				dto.setName(nameVNUpdate.trim());
				re.setErrorMessage("update");
				// return re;
			} else {
				re.setResultCode(2);
				dto.setName(nameVNUpdate.trim());
				dto.setNameEn(nameCheck.trim());
				dto.setDescription(nameCheck.trim());
				dto.setType(1);
			}
			Attributes dtoRes = new Attributes();
			dtoRes = attributesDAO.createImport(dto);
			re.setObject(dtoRes.getId());
			re.setErrorMessage("add");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return re;
	}

	public void addAttributesCategory(long idAttribute, Integer IdCa) {
		try {
			long ic = 0;
			ic = checkAttributesCategory(idAttribute, IdCa);
			if (ic <= 0) {
				Session session = this.sessionFactory.getCurrentSession();
				session.createSQLQuery("INSERT INTO categories_attributes (attribute_id,category_id) VALUES  (?,?) ")
						.setParameter(0, idAttribute).setParameter(1, IdCa).executeUpdate();
				session.flush();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public long getIDAttributesVaqlueByName(String name, long id, Integer idCategory, long productID) {
		Session session = this.sessionFactory.getCurrentSession();
		// type ==1 update co check/ =2 them luon
		try {
			Criteria criteria = session.createCriteria(AttributeValues.class);
			List<AttributeValues> list = new ArrayList<AttributeValues>();
			if (name != null && !name.equals("") && id > 0) {
				if (name != null && name.length() > 2) {
					if (".0".equals(name.substring(name.length() - 2, name.length()))) {
						name = (name.substring(0, name.length() - 2));
					}
				}
				criteria.add(Restrictions.sqlRestriction(" (UPPER(value) = '" + name.toUpperCase().trim() + "') "));
				criteria.add(Restrictions.sqlRestriction(" (attribute_id = " + id + ") "));
				criteria.add(Restrictions.sqlRestriction(" (category_id = " + idCategory + ") "));
				criteria.addOrder(Order.desc("id"));
				list = criteria.list();
			}
			if (list != null && list.size() > 0 && id > 0 && name != null && !name.equals("") && idCategory > 0) {
				System.out.println("dung chung attribute:" + list.get(0).getValue());
				return list.get(0).getId();
			} else {
				AttributeValues dto = new AttributeValues();
				dto.setId(0);
				dto.setValue(name.trim());
				if (dto.getValue() != null && dto.getValue().length() > 2) {
					if (".0".equals(dto.getValue().substring(dto.getValue().length() - 2, dto.getValue().length()))) {
						dto.setValue(dto.getValue().substring(0, dto.getValue().length() - 2));
					}
				}
				dto.setCategoryId(idCategory);
				dto.setAttributeId(id);
				session.persist(dto);
				return dto.getId();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public long checkAttributesCategory(long idAttribue, Integer cate) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "select * from categories_attributes where category_id = " + cate + " and attribute_id = "
					+ idAttribue + "";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				return 1;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public void addAttributeValueProductImport(List<ImportAttribute> listAttribute, long id, Integer idCategory,
			Integer insertUpdateProductTye) {
		List<ImportAttribute> list = new ArrayList<ImportAttribute>();
		list = listAttribute;
		// deleteAttributeValueById(0, id);
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				String attribusPatern = list.get(i).getParentName();
				// check ton tai thuoc tinh
				if (attribusPatern != null && !"".equals(attribusPatern)) {
					long AttibutesID = 0;
					ResponseObject recheck = new ResponseObject();
					recheck = getIDAttributesByName(attribusPatern);
					AttibutesID = Long.parseLong(recheck.getObject() + "");
					try {
						if (AttibutesID > 0) {
							addAttributesCategory(AttibutesID, idCategory);
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					if (AttibutesID > 0) {
						if (recheck.getResultCode() == 1 && insertUpdateProductTye == 0) {
							 deleteAttributeValueByIdImport(AttibutesID, idCategory,id);
						}
						String name = "";
						name = list.get(i).getAttributeName();
						String listName[] = name.split(";");
						// System.out.println("listName Ban dau:" + name);
						for (String s : listName) {
							if (s != null && !"".equals(s)) {
								// check attributes value by name
								long idValue = 0;
								idValue = getIDAttributesVaqlueByName(s, AttibutesID, idCategory, id);
								// System.out.println("idValueidValueidValue:" +
								// idValue);
								try {
									if (checkProductAttributeValue(id, idValue, AttibutesID) <= 0) {
										Session session = this.sessionFactory.getCurrentSession();
										session.createSQLQuery(
												"INSERT INTO products_attribute_values (product_id,attribute_value_id,attribute_id) VALUES  (?,?,?) ")
												.setParameter(0, id).setParameter(1, idValue)
												.setParameter(2, AttibutesID).executeUpdate();
									}
								} catch (Exception e) {
									// TODO: handle exception
									e.printStackTrace();
								}
							}
						}
					}
				}

			}
		}

	}

	public long checkProductAttributeValue(long productId, long attributeValueId, long attibutesID) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "select * from products_attribute_values where product_id = " + productId
					+ " and attribute_value_id = " + attributeValueId + " and attribute_id = " + attibutesID + "";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				return 1;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public void deleteAttibuteValueId(long id) {
		try {
			if (id > 0) {
				String sql1 = "DELETE FROM  attribute_values where id = " + id + " ";
				Session session = this.sessionFactory.getCurrentSession();
				session.createSQLQuery(sql1).executeUpdate();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteAttributeValueById(long productID) {
		try {
			if (productID > 0) {
				System.out.println("==Vào case Xóa attribte value==");
				//
				if (productID > 0 && productID > 0) {
					String sql1 = "DELETE FROM  attribute_values " + "where attribute_values.id in ("
							+ "select attribute_value_id FROM  products_attribute_values "
							+ "where products_attribute_values.product_id = " + productID + ")";
					Session session = this.sessionFactory.getCurrentSession();
					session.createSQLQuery(sql1).executeUpdate();
					String sql2 = "DELETE FROM  products_attribute_values where product_id = " + productID + "";
					System.out.println(sql2);
					// Session session2 =
					// this.sessionFactory.getCurrentSession();
					session.createSQLQuery(sql2).executeUpdate();
					// session.flush();
				}
				// session.flush();

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Product updatePrice(Product re) {
		Product dto = new Product();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				session.flush();
				return dto;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		return null;
	}

	// xoa product trong categoru
	public void removeCountProduct(Long id) {

		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Categories.class);
			criteria.add(Restrictions.sqlRestriction(
					" id in (select category_id from products_categories where product_id = " + id + ")"));
			List<Categories> listCategory = criteria.list();
			if (listCategory != null && listCategory.size() > 0) {
				String listID = "";
				for (Categories dto : listCategory) {
					if (listID == null || "".equals(listID)) {
						listID = dto.getId() + "";
					} else {
						listID = listID + "," + dto.getId();
					}
					if (dto.getParentId() != null && dto.getParentId() > 0) {
						if (listID == null || "".equals(listID)) {
							listID = dto.getParentId() + "";
						} else {
							listID = listID + "," + dto.getParentId();
						}
					}

				}
				if (listID != null && listID != "") {
					session.createSQLQuery(
							"UPDATE categories set count_product = count_product - 1 where id in (" + listID + ")")
							.executeUpdate();
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public boolean validaeSkuImport(Product pro) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Criteria criteria = session.createCriteria(Product.class);
			if (pro != null && pro.getOrginSku() != null && !pro.getOrginSku().equals("")) {
				criteria.add(Restrictions
						.sqlRestriction(" (UPPER(orgin_sku) = '" + pro.getOrginSku().toUpperCase().trim() + "') "));
			}
			List<Product> list = criteria.list();
			if (list != null && list.size() > 0) {
				try {
					list.get(0).setName(pro.getName());
					list.get(0).setNormalPrice(pro.getNormalPrice());
					list.get(0).setInStock(pro.getInStock());
					list.get(0).setSku(pro.getSku());
					session.update(list.get(0));
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}

	public ResponseObject updateProductByOginSku(List<Product> list) {

		ResponseObject response = new ResponseObject();
		response.setResultCode(200);
		response.setObject(list);
		List<Product> listFor = new ArrayList<>();
		listFor = list;
		int i = 0;
		if (listFor != null && listFor.size() > 0) {
			for (Product pro : listFor) {
				try {
					Session session = this.sessionFactory.getCurrentSession();
					Criteria criteria = session.createCriteria(Product.class);
					if (pro != null && pro.getSku() != null && !pro.getSku().equals("")) {
						criteria.add(Restrictions
								.sqlRestriction(" (UPPER(sku) = '" + pro.getSku().toUpperCase().trim() + "') "));

						List<Product> listCheck = criteria.list();
						if (listCheck != null && listCheck.size() > 0) {
							try {
								if (pro.getName() != null && pro.getName().trim() != null
										&& !"".equals(pro.getName().trim())) {
									listCheck.get(0).setName(pro.getName().trim());
									ConvetSlug sl = new ConvetSlug();
									listCheck.get(0).setSlug(
											sl.toUrlFriendly(pro.getName().trim(), 1) + listCheck.get(0).getId());
								}
								if (pro.getNormalPrice() > 0) {
									listCheck.get(0).setNormalPrice(pro.getNormalPrice());
								}
								if (pro.getInStock() > 0) {
									listCheck.get(0).setInStock(pro.getInStock());
								}
								if (pro.getMinOrder() > 0) {
									listCheck.get(0).setMinOrder(pro.getMinOrder());
								}
								listCheck.get(0).setProductStatus(pro.getProductStatus());
								session.update(listCheck.get(0));
								// update sản pham trong kho
								// updateInStockInventory(listCheck.get(0).getId(),
								// listCheck.get(0).getInStock());
								session.flush();
								i++;
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			response.setObject(i);
		}

		return response;
	}

	public void updateInStockInventory(long idProduct, int stock) {
		try {
			if (idProduct > 0) {
				Session session = this.sessionFactory.getCurrentSession();
				int stockIn = 0;
				stockIn = stock;
				session.createSQLQuery(
						"UPDATE inventory set stock = " + stockIn + "  where product_id =  " + idProduct + "")
						.executeUpdate();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// Tìm kiếm nâng cao
	@SuppressWarnings("unchecked")
	public int getCountAllProductMore(ProductSearch pro) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Product> list = new ArrayList<Product>();
			String sql = "SELECT p.id, " + "   p.name, " + "   p.created_time, " + "   p.normal_price, "
					+ "   p.sale_price, " + "   p.sale_start, " + "   p.sale_end, " + "   p.sku, " + "   p.orgin_sku, "
					+ "   p.manufacture_id, " + "   p.in_stock, " + "   p.orgin_from, "
					+ "   case when actived=1 then '1'else '0' end, " + "   p.category_id, "
					+ "   ca.name as categoryName, " + "   ma.name as manufactureName,thumbnail_image "
					+ " FROM products p " + " left join categories ca on p.category_id = ca.id  "
					+ " left  join manufactures ma on p.manufacture_id = ma.id " + " where 1=1 ";
			if (pro.getSearch() != null && !"".equals(pro.getSearch().trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + pro.getSearch().toUpperCase().trim() + "%'"
						+ " or UPPER(ca.name) like '%" + pro.getSearch().toUpperCase().trim() + "%' "
						+ " or UPPER(p.sku) like '%" + pro.getSearch().toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + pro.getSearch().toUpperCase().trim() + "%' "
						+ " or UPPER(ma.name) like '%" + pro.getSearch().toUpperCase().trim() + "%' )";
			}
			if (pro.getPriceStart() > 0) {
				sql = sql + " and (p.normal_price >=" + pro.getPriceStart() + ") ";
			}
			if (pro.getPriceEnd() > 0) {
				sql = sql + " and (p.normal_price <=" + pro.getPriceEnd() + ") ";
			}
			if (pro.isBlInsufficient()) {
				sql = sql
						+ " and (p.name  is null or p.normal_price = 0 or p.in_stock = 0 or p.in_stock is null or p.sku is null or ma.name is null or  p.orgin_sku is null ) ";
			}
			if (pro.getListCategoriID() != null && pro.getListCategoriID().size() > 0) {
				String listId = "";
				for (Integer it : pro.getListCategoriID()) {
					if (it != null && it > 0) {
						if (listId == null || "".equals(listId)) {
							listId = it + "";
						} else {
							listId = listId + "," + it;
						}
					}

				}
				if (listId != null && !"".equals(listId)) {
					sql = sql
							+ " and (p.id in (SELECT distinct proca.product_id FROM products_categories proca where proca.category_id in ("
							+ listId + "))) ";
				}
			}
			if (pro.isBlSale()) {
				sql = sql + " and (p.sale_price is not null OR p.sale_start is not null OR p.sale_end is not null ) ";
			}
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				return rows.size();
			} else {
				return 0;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}

	}

	@SuppressWarnings("unchecked")
	public List<Product> getAllProductPagerMore(ProductSearch pro) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Product> list = new ArrayList<Product>();
			String sql = "SELECT p.id, " + "   p.name, " + "   p.created_time, " + "   p.normal_price, "
					+ "   p.sale_price, " + "   p.sale_start, " + "   p.sale_end, " + "   p.sku, " + "   p.orgin_sku, "
					+ "   p.manufacture_id, " + "   p.in_stock, " + "   p.orgin_from, "
					+ "   case when actived=1 then '1'else '0' end, " + "   p.category_id, "
					+ "   ca.name as categoryName, "
					+ "   ma.name as manufactureName,thumbnail_image, p.product_status as product_status "
					+ " FROM products p " + " left join categories ca on p.category_id = ca.id  "
					+ " left  join manufactures ma on p.manufacture_id = ma.id " + " where 1=1 ";
			if (pro.getSearch() != null && !"".equals(pro.getSearch().trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + pro.getSearch().toUpperCase().trim() + "%'"
						+ " or UPPER(ca.name) like '%" + pro.getSearch().toUpperCase().trim() + "%' "
						+ " or UPPER(p.sku) like '%" + pro.getSearch().toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + pro.getSearch().toUpperCase().trim() + "%' "
						+ " or UPPER(ma.name) like '%" + pro.getSearch().toUpperCase().trim() + "%' )";
			}
			if (pro.getPriceStart() > 0) {
				sql = sql + " and (p.normal_price >=" + pro.getPriceStart() + ") ";
			}
			if (pro.getPriceEnd() > 0) {
				sql = sql + " and (p.normal_price <=" + pro.getPriceEnd() + ") ";
			}
			if (pro.isBlInsufficient()) {
				sql = sql
						+ " and (p.name  is null or p.normal_price = 0 or p.in_stock = 0 or p.in_stock is null or p.sku is null or ma.name is null or  p.orgin_sku is null ) ";
			}
			if (pro.getListCategoriID() != null && pro.getListCategoriID().size() > 0) {
				String listId = "";
				for (Integer it : pro.getListCategoriID()) {
					if (it != null && it > 0) {
						if (listId == null || "".equals(listId)) {
							listId = it + "";
						} else {
							listId = listId + "," + it;
						}
					}
				}
				if (listId != null && !"".equals(listId)) {
					sql = sql
							+ " and (p.id in (SELECT distinct proca.product_id FROM products_categories proca where proca.category_id in ("
							+ listId + "))) ";
				}
			}
			if (pro.isBlSale()) {
				sql = sql + " and (p.sale_price is not null OR p.sale_start is not null OR p.sale_end is not null ) ";
			}
			sql = sql + " order by p.id desc" + " limit " + ((pro.getPage() - 1) * pro.getPageSize()) + ","
					+ pro.getPageSize() + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Product dto;
				list = new ArrayList<Product>();
				for (Object[] row : rows) {
					dto = new Product();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
						dto.setName(row[1] != null ? row[1].toString() : "");
						dto.setCreatedTime(Integer.parseInt(row[2] != null ? row[2].toString() : "0"));
						dto.setNormalPrice(Integer.parseInt(row[3] != null ? row[3].toString() : "0"));
						dto.setSalePrice(Integer.parseInt(row[4] != null ? row[4].toString() : "0"));
						dto.setSaleStart(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
						dto.setSaleEnd(Integer.parseInt(row[6] != null ? row[6].toString() : "0"));
						dto.setSku(row[7] != null ? row[7].toString() : "");
						dto.setOrginSku(row[8] != null ? row[8].toString() : "");
						dto.setManufactureId(Integer.parseInt(row[9] != null ? row[9].toString() : "0"));
						dto.setInStock(Integer.parseInt(row[10] != null ? row[10].toString() : "0"));
						dto.setOrginFrom(row[11] != null ? row[11].toString() : "");
						dto.setActived(Integer.parseInt(row[12] != null ? row[12].toString() : "0"));
						dto.setCategoryId(Integer.parseInt(row[13] != null ? row[13].toString() : "0"));
						dto.setCategoriesName(row[14] != null ? row[14].toString() : "");
						dto.setManufactureName(row[15] != null ? row[15].toString() : "");
						dto.setThumbnailImage(row[16] != null ? row[16].toString() : "");
						dto.setProductStatus(row[17] != null ? row[17].toString() : "");
					}

					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	// End tin kiếm nang cao

	@SuppressWarnings("unchecked")
	public int getCountAllProductByInventoryArea(int page, int pageSize, String columnName, String typeOrder,
			String keySearch, int area) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Product> list = new ArrayList<Product>();
			String sql = "SELECT distinct  p.id, " + "   p.name, " + "    p.sku, " + "   p.orgin_sku"
					+ " FROM products p " + " where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.sku) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + keySearch.toUpperCase().trim() + "%')";
			}
			if (area > 0) {
				sql = sql
						+ " and p.id not in (select distinct ia.product_id from inventory ia where ia.inventory_area_id = "
						+ area + ") ";
			}

			sql = sql + " order by p.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Product dto;
				list = new ArrayList<Product>();
				for (Object[] row : rows) {
					dto = new Product();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
						dto.setName(row[1] != null ? row[1].toString() : "");
						dto.setSku(row[2] != null ? row[2].toString() : "");
						dto.setOrginSku(row[3] != null ? row[3].toString() : "");
					}

					list.add(dto);
				}
			}
			if (list != null && list.size() > 0) {
				return list.size();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public List<Product> getPageProductByInventoryArea(int page, int pageSize, String columnName, String typeOrder,
			String keySearch, int area) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Product> list = new ArrayList<Product>();
			String sql = "SELECT distinct  p.id, " + "   p.name, " + "    p.sku, " + "   p.orgin_sku"
					+ " FROM products p " + " where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(p.name) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.sku) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(p.orgin_sku) like '%" + keySearch.toUpperCase().trim() + "%')";
			}
			if (area > 0) {
				sql = sql
						+ " and p.id not in (select distinct ia.product_id from inventory ia where ia.inventory_area_id = "
						+ area + ") ";
			}
			sql = sql + " order by p.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Product dto;
				list = new ArrayList<Product>();
				for (Object[] row : rows) {
					dto = new Product();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
						dto.setName(row[1] != null ? row[1].toString() : "");
						dto.setSku(row[2] != null ? row[2].toString() : "");
						dto.setOrginSku(row[3] != null ? row[3].toString() : "");
					}

					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		String name = "7.0";
		if (name != null && name.length() > 2) {
			if (".0".equals(name.substring(name.length() - 2, name.length()))) {
				System.out.println(name.substring(0, name.length() - 2));
			}
		}

	}

	public void fixSlug() {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			// SQLQuery query = (SQLQuery) session.createSQLQuery("select * from
			// products where 1 =1 ");
			// query.addEntity(Product.class);
			// list = query.list();
			// // session.flush();
			// if (list != null && list.size() > 0) {
			// for(Product pro: list){
			// updateproductFix(pro);
			// }
			// }
			session.createSQLQuery("delete from products  where name like '%tụ%' ").executeUpdate();
			System.out.println("ok");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void updateproductFix(Product dto) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			ConvetSlug sl = new ConvetSlug();
			dto.setSlug(sl.toUrlFriendly(dto.getSlug(), 1));
			session.update(dto);
			// session.flush();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}

	}

	public void addCategoryManfactory(Integer idCategory, Integer IdManfactory) {
		try {
			if (checkManfactoryCategory(idCategory, IdManfactory)) {
				// =true thi them moi lien ket.
				try {
					ManufacturesCategory dto = new ManufacturesCategory();
					dto.setId(0);
					dto.setManufacturesId(IdManfactory);
					dto.setNacategoryId(idCategory);
					Session session = this.sessionFactory.getCurrentSession();
					if (dto != null && !dto.equals("")) {
						session.persist(dto);
						session.flush();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public Boolean checkManfactoryCategory(Integer idCate, Integer idManu) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(ManufacturesCategory.class);
			if (idCate > 0 && idManu > 0) {
				criteria.add(Restrictions.sqlRestriction(" (manufactures_id = " + idManu + ") "));
				criteria.add(Restrictions.sqlRestriction(" (category_id = " + idCate + ") "));
			} else {
				return true;
			}
			List<ManufacturesCategory> list = criteria.list();
			if (list != null && list.size() > 0) {
				return false;
			}
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return true;
	}

	public Integer checkArticleProeduct(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery(
							"select products_articles.article_id from products left join products_articles on products.id = products_articles.product_id where products.id = ? and products_articles.article_id is not null  ")
					.setParameter(0, id);
			List<Integer> idMax = query.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					return 0;
				} else {
					return Integer.parseInt(idm.toString());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Product getByLK(String sku, String startDate, String endDate) {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from products p where  UPPER(p.sku) = ? ")
					.setParameter(0, sku.toUpperCase().trim());
			query.addEntity(Product.class);
			list = query.list();
			// session.flush();
			if (list != null && list.size() > 0) {
				Product p = new Product();
				p = list.get(0);
				try {
					Date dt = new Date(startDate);
					Date dtEnd = new Date(endDate);
					p.setSaleStart(Integer.parseInt((dt.getTime() / 1000) + ""));
					p.setSaleEnd(Integer.parseInt((dtEnd.getTime() / 1000) + ""));

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				session.update(p);
				return p;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public Product getByLKPrice(String sku, int price, int i) {
		List<Product> list = new ArrayList<Product>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (sku != null) {
				SQLQuery query = (SQLQuery) session.createSQLQuery("select * from products p where  UPPER(p.sku) = ? ")
						.setParameter(0, sku.toUpperCase().trim());
				query.addEntity(Product.class);
				list = query.list();
				// session.flush();
				if (list != null && list.size() > 0) {
					Product p = new Product();
					p = list.get(0);
					try {
						if (price > 0) {
							p.setNormalPrice(price);
						}

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					if (i <= 0) {
						session.update(p);
					}
					return p;
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public void resetChmod(String path) {
		try {
			Process p = Runtime.getRuntime().exec("echo 1 >/proc/sys/vm/drop_caches");
			Process p2 = Runtime.getRuntime().exec("sudo echo 1 > /proc/sys/vm/drop_caches");
			System.out.println("clear cache");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public void createArticleLinkUpload(ArticleLinkUpload dto) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (dto != null) {
				dto.setId(0);
				dto.setType(2);
				session.persist(dto);
				session.flush();

				// }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
