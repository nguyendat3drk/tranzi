package com.tranzi.dao;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.Dashboard;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.UserPreference;

@Repository
public class DashboardDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public static List<String> getDaysBetweenDates(Date startdate, Date enddate) {
		List<String> dates = new ArrayList<String>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {
			String result = calendar.getTime().toString();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	public List<ResponseObject> getTotalCall(List<String> dates) throws ParseException {
		String pattern = "MM/dd/yyyy hh:mm:ss a";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		List<ResponseObject> responseObject = new ArrayList<ResponseObject>();
		ResponseObject responseObjectDates = new ResponseObject();
		ResponseObject responseObjectTotalCall = new ResponseObject();
		ResponseObject responseObjectAbandonCall = new ResponseObject();
		ResponseObject responseObjectAnsweredCall = new ResponseObject();
		List<Integer> listTotalCall = new ArrayList<Integer>();
		List<String> listAbandonCall = new ArrayList<String>();
		List<String> listAnsweredCall = new ArrayList<String>();
		int result;
		List<String> listDate = getDaysBetweenDates(simpleDateFormat.parse(dates.get(0)),
				simpleDateFormat.parse(dates.get(1)));
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery sql = session.createSQLQuery("SELECT COUNT(*) FROM call_details WHERE receive_time = :date");
		for (String date : listDate) {
			sql.setParameter("date", date);
			BigInteger count = (BigInteger) sql.uniqueResult();
			result = count.intValue();
			listTotalCall.add(result);
		}

		responseObjectTotalCall.setResultCode(200);
		responseObjectTotalCall.setObject(listTotalCall);
		responseObjectTotalCall.setErrorMessage("get total count fail");

		responseObjectDates.setResultCode(200);
		responseObjectDates.setObject(listDate);
		responseObjectDates.setErrorMessage("get list dates fail");

		responseObjectAbandonCall.setResultCode(200);
		responseObjectAbandonCall.setObject(listAbandonCall);
		responseObjectAbandonCall.setErrorMessage("get list abandon fail");

		responseObjectAnsweredCall.setResultCode(200);
		responseObjectAnsweredCall.setObject(listAnsweredCall);
		responseObjectAnsweredCall.setErrorMessage("get list answered fail");

		responseObject.add(responseObjectDates);
		responseObject.add(responseObjectTotalCall);
		responseObject.add(responseObjectAbandonCall);
		responseObject.add(responseObjectAnsweredCall);

		return responseObject;
	}

	@SuppressWarnings("unchecked")
	public ResponseObject getDashboardState(int id) throws ParseException {
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("select * from user_preference a where a.user_id = :userId");
		query.setParameter("userId", id);
		query.addEntity(UserPreference.class);
		List<UserPreference> result = query.list();
		// System.out.println("result.size() " + result.size());
		if (result.size() > 0) {
			if (result.get(0).getWidgetPosition() == null) {
				result.get(0).setWidgetPosition("ALI,AOC,AAX,ALO,WC,AAV,ABT,QIS,TC,ANC,MATT,AATT,ABC,WT,MWT,AWT");
			}
			responseObject.setResultCode(200);
			responseObject.setObject(result.get(0));
			responseObject.setErrorMessage("get total count fail");
		} else {
			responseObject.setResultCode(500);
			responseObject.setObject(null);
			responseObject.setErrorMessage("can not find user preference");
		}
		return responseObject;
	}

	public ResponseObject saveDashboardState(UserPreference userPreference) {
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userPreference);
		responseObject.setResultCode(200);
		responseObject.setObject(null);
		responseObject.setErrorMessage("get total count fail");

		return responseObject;
	}
	
	public ResponseObject savePosition(UserPreference userPreference) {
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		session.update(userPreference);
		responseObject.setResultCode(200);
		responseObject.setObject(null);
		responseObject.setErrorMessage("");
		return responseObject;
	}

	// get role type of user
	public int getTypeRole(int agentId, Session session) {
		int re = 0;
		String sql = "select r.type from user u, role r "
				+ "where u.id = :agentId and u.role = r.id and u.delete_state =0 ";
		;
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("agentId", agentId);
			List<Integer> lstObj = query.list();
			if (lstObj != null && lstObj.size() > 0) {
				re = lstObj.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return re;
	}

	public ResponseObject getDataDashBoard(int agentId, long fromDate, long toDate) {
		// TODO Auto-generated method stub
		ResponseObject responseObject = new ResponseObject();
		Dashboard dasboard = new Dashboard();
		Session session = this.sessionFactory.getCurrentSession();
		int typeRole = getTypeRole(agentId, session);
		String strQuery = "";
		// get agents logged in real time
		Query query = null;
		strQuery = "select count(1) from user as u, role as r where u.delete_state =0 and r.role_name = 'Agent' and u.role = r.id "
				+ "and u.id in (select account_id from account_logged_details where log_out_time is null) ";

		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and u.id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery
					+ " and u.id in (select account_id from account_in_queue aiq where queue_id in (select queue_id from account_in_queue where account_id = "
					+ agentId + ") and account_id <> " + agentId + ")";
		}
		query = session.createSQLQuery(strQuery);
		int agentsLogin = ((BigInteger) query.uniqueResult()).intValue();

		// get agents logged out current day
		strQuery = "select count(DISTINCT u.id) from user as u, account_logged_details as ald, role as r where u.delete_state =0 and u.id = ald.account_id "
				+ "and u.id not in (select account_id from account_logged_details where log_out_time is null group by account_id) "
				+ "and u.role = r.id and r.role_name = 'Agent' and DATE_FORMAT(log_out_time,'%d/%m/%Y') = DATE_FORMAT(UTC_TIMESTAMP(),'%d/%m/%Y') ";

		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and u.id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery
					+ " and u.id in (select account_id from account_in_queue aiq where queue_id in (select queue_id from account_in_queue where account_id = "
					+ agentId + ") and account_id <> " + agentId + ")";
		}
		query = session.createSQLQuery(strQuery);
		int agentsLogout = ((BigInteger) query.uniqueResult()).intValue();

		// get agents auxiliary status
		strQuery = "select count(1) from user as u, role as r where u.delete_state =0 and u.state = 'AUXILIARY' and r.role_name = 'Agent' and u.role = r.id ";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and u.id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery
					+ " and u.id in (select account_id from account_in_queue aiq where queue_id in (select queue_id from account_in_queue where account_id = "
					+ agentId + ") and account_id <> " + agentId + ")";
		}
		query = session.createSQLQuery(strQuery);
		int agentsAuxiliary = ((BigInteger) query.uniqueResult()).intValue();

		// get all waiting calls
		strQuery = "select count(1) from call_in_queue where status = '1' ";
		// typeRole
		if (typeRole == 2) {
			// agent
			strQuery = strQuery + " and queue_id in (select queue_id from account_in_queue where account_id = " + agentId + ")";
		}else if(typeRole == 1){
			strQuery = strQuery + " and agent_id = " + agentId;
		}

		query = session.createSQLQuery(strQuery);
		int waitingCalls = ((BigInteger) query.uniqueResult()).intValue();

		// get agents on call
		strQuery = "select count(1) from call_in_queue where status = 0 and agent_id is not null and call_id in (select id from call_details where status = 1) ";
		if (typeRole == 2) {
			strQuery = strQuery + "and queue_id in (select queue_id from account_in_queue where account_id = " + agentId + ")";
		}else if(typeRole == 1){
			strQuery = strQuery + " and agent_id = " + agentId;
		}
		query = session.createSQLQuery(strQuery);
		int agentsOnCall = ((BigInteger) query.uniqueResult()).intValue();

		// get agent is available state
		strQuery = "select count(DISTINCT u.id) from user as u, role as r where u.delete_state =0 and  u.state = 'AVAILABLE' and u.status = 1 and r.type = 1 "
					+ "and (TO_SECONDS(UTC_TIMESTAMP()) - COALESCE(TO_SECONDS(u.last_call_time),0)) > COALESCE((select breaking_time from queue where id = u.last_queue), 0) "
					+ "and u.role = r.id ";
		if (typeRole == 2) {
			strQuery = strQuery + " and u.id in (select DISTINCT account_id from account_in_queue where queue_id in (select queue_id from account_in_queue where account_id = " 
								+ agentId + ") and account_id <> " + agentId + ")";
		}else if(typeRole == 1){
			strQuery = strQuery + " and u.id = " + agentId;
		}
		query = session.createSQLQuery(strQuery);
		int agentsAvailable = ((BigInteger) query.uniqueResult()).intValue();

		// get agent is breaking time state
		strQuery = "select count(DISTINCT u.id) from user as u, queue as q, account_in_queue as aiq "
				+ "where u.delete_state =0 and  u.status = 1 and u.state = 'AVAILABLE' and q.status = 1 "
				+ "and q.id = aiq.queue_id and aiq.account_id = u.id and q.id = u.last_queue "
				+ "and (TO_SECONDS(UTC_TIMESTAMP()) - TO_SECONDS(u.last_call_time)) < q.breaking_time ";
		if (typeRole == 2) {
			strQuery = strQuery + "and aiq.queue_id in (select queue_id from account_in_queue where account_id = " + agentId + ")";
		}else if(typeRole == 1){
			strQuery = strQuery + " and u.id = " + agentId;
		}
		query = session.createSQLQuery(strQuery);
		int agentsBreakingTime = ((BigInteger) query.uniqueResult()).intValue();

		// get queues in service
		strQuery = "select count(1) from queue where id in (select queue_id from call_in_queue where status = 1 ";
		if (typeRole == 1 || typeRole == 2) {
			strQuery = strQuery + "and queue_id in (select queue_id from account_in_queue where account_id = " + agentId + "))";
		}else{
			strQuery = strQuery + ")";
		}
		query = session.createSQLQuery(strQuery);
		int queues = ((BigInteger) query.uniqueResult()).intValue();

		// get dashboard item by date and agentId
		// get total call
		strQuery = "select count(1) from call_details as cd, call_in_queue as ciq " + "where cd.status = 0 "
				+ "and ciq.agent_id is not null " + "and UNIX_TIMESTAMP(cd.receive_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(cd.end_call_time)*1000 <= :toDate " + "and cd.id = ciq.call_id";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and ciq.agent_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and ciq.agent_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}

		query = session.createSQLQuery(strQuery);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		int totalCalls = ((BigInteger) query.uniqueResult()).intValue();

		// get answered by agent
		strQuery = "select count(1) from call_details as cd, call_in_queue as ciq "
				+ "where cd.status = 0 and cd.start_call_time is not null "
				+ "and UNIX_TIMESTAMP(cd.start_call_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(cd.start_call_time)*1000 <= :toDate and cd.id = ciq.call_id";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and ciq.agent_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and ciq.agent_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.delete_state =0 and u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}
		query = session.createSQLQuery(strQuery);
		// query.setParameter("agentId", agentId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		int answeredCalls = ((BigInteger) query.uniqueResult()).intValue();

		// get abandon call by agent
		strQuery = "select count(1) from call_details as cd, call_in_queue as ciq "
				+ "where cd.state_of_miss_call = 'ABANDON' and cd.id = ciq.call_id " + "and ciq.agent_id is not null "
				+ "and UNIX_TIMESTAMP(cd.end_call_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(cd.end_call_time)*1000 <= :toDate ";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and ciq.agent_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and ciq.agent_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}

		query = session.createSQLQuery(strQuery);
		// query.setParameter("agentId", agentId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		int abandonCalls = ((BigInteger) query.uniqueResult()).intValue();

		// get total working time
		strQuery = "select concat('', SEC_TO_TIME(SUM(UNIX_TIMESTAMP(COALESCE(al.log_out_time, UTC_TIMESTAMP())) - UNIX_TIMESTAMP(al.log_in_time)))) workingTime "
				+ "from account_logged_details as al, user as u, role as r "
				+ "where UNIX_TIMESTAMP(al.log_in_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(al.log_in_time)*1000 <= :toDate " + "and r.type = 1 "
				+ "and u.id = al.account_id " + "and u.role = r.id ";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and al.account_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and al.account_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}

		query = session.createSQLQuery(strQuery);
		// query.setParameter("agentId", agentId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		List<String> workingTimes = query.list();
		if (workingTimes != null && workingTimes.size() > 0) {
			if (workingTimes.get(0) != null && !workingTimes.get(0).equals("")) {
				dasboard.setWorkingTime(workingTimes.get(0));
			} else {
				dasboard.setWorkingTime("00:00");
			}
		}

		// get Max talk time
		// get AVG talk time
		strQuery = "select SEC_TO_TIME(ROUND(AVG(UNIX_TIMESTAMP(cd.end_call_time) - UNIX_TIMESTAMP(cd.start_call_time)))) avg_talk_time, "
				+ "MAX(SEC_TO_TIME(UNIX_TIMESTAMP(cd.end_call_time) - UNIX_TIMESTAMP(cd.start_call_time))) max_talk_time "
				+ "from call_details as cd, call_in_queue as ciq  where cd.start_call_time is not null "
				+ "and cd.id = ciq.call_id " + "and ciq.agent_id is not null "
				+ "and UNIX_TIMESTAMP(cd.start_call_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(cd.end_call_time)*1000 <= :toDate ";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and ciq.agent_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and ciq.agent_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}

		query = session.createSQLQuery(strQuery);
		// query.setParameter("agentId", agentId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		List<Object[]> rows = query.list();
		if (rows != null && rows.size() > 0) {
			for (Object[] row : rows) {
				if (row[0] != null && !row[0].equals("")) {
					dasboard.setAVGAgentTalkTime(row[0].toString());
				} else {
					dasboard.setAVGAgentTalkTime("00:00");
				}

				if (row[1] != null && !row[1].equals("")) {
					dasboard.setMaxAgentTalkTime(row[1].toString());
				} else {
					dasboard.setMaxAgentTalkTime("00:00");
				}
			}
		}

		// get Max wait time
		// get AVG wait time
		strQuery = "select MAX(SEC_TO_TIME(UNIX_TIMESTAMP(ciq.end_queue_time) - UNIX_TIMESTAMP(ciq.in_queue_time))) max_wait_time, "
				+ "SEC_TO_TIME(ROUND(AVG(UNIX_TIMESTAMP(ciq.end_queue_time) - UNIX_TIMESTAMP(ciq.in_queue_time)))) avg_wait_time "
				+ "from call_in_queue as ciq where ciq.status = 0 "
				+ "and UNIX_TIMESTAMP(ciq.in_queue_time)*1000 >= :fromDate "
				+ "and UNIX_TIMESTAMP(ciq.end_queue_time)*1000 <= :toDate ";
		// typeRole
		if (typeRole == 1) {
			// agent
			strQuery = strQuery + " and ciq.agent_id = " + agentId;
		} else if (typeRole == 2) {
			// queue
			strQuery = strQuery + " and ciq.agent_id in (select u.id from user u, account_in_queue aiq, role r "
					+ "where u.id = aiq.account_id and u.role = r.id "
					+ "and r.type = 1 and aiq.queue_id in (select queue_id FROM account_in_queue where account_id = "
					+ agentId + "))";
		}

		query = session.createSQLQuery(strQuery);
		// query.setParameter("agentId", agentId);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		List<Object[]> rowsResult = query.list();
		if (rowsResult != null && rowsResult.size() > 0) {
			for (Object[] row : rowsResult) {
				if (row[0] != null && !row[0].equals("")) {
					dasboard.setMaxWaitTime(row[0].toString());
				} else {
					dasboard.setMaxWaitTime("00:00");
				}

				if (row[1] != null && !row[1].equals("")) {
					dasboard.setAVGWaitTime(row[1].toString());
				} else {
					dasboard.setAVGWaitTime("00:00");
				}
			}
		}

		dasboard.setAgentsLoggedin(agentsLogin);
		dasboard.setAgentsLogout(agentsLogout);
		dasboard.setAgentsAuxiliary(agentsAuxiliary);
		dasboard.setWaitingCall(waitingCalls);
		dasboard.setAgentsOnCall(agentsOnCall);
		dasboard.setAgentAvailable(agentsAvailable);
		dasboard.setAgentBreakingTime(agentsBreakingTime);
		dasboard.setQueuesInService(queues);

		// get dashboard by agent
		dasboard.setTotalCalls(totalCalls);
		dasboard.setAnsweredCalls(answeredCalls);
		dasboard.setAbandonCalls(abandonCalls);
		// dasboard.setWorkingTime(workingTime);
		
		responseObject.setObject(dasboard);
		responseObject.setResultCode(200);
		return responseObject;
	}
}
