package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AdminBom;

@Repository
public class AdminBomDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public AdminBom getById(int id) {
		List<AdminBom> list = new ArrayList<AdminBom>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from admin_bom where id = ? ").setParameter(0,
					id);
			query.addEntity(AdminBom.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public ResponseObject deleteAdminBom(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			session.createSQLQuery("delete from admin_bom where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành Công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	// public boolean checkDeleteManufactures(long id) {
	// Session session = this.sessionFactory.getCurrentSession();
	// List<Product> list = new ArrayList<>();
	// try {
	// SQLQuery query = (SQLQuery) session.createSQLQuery("select * from
	// products where manufacture_id = ? ")
	// .setParameter(0, id);
	// query.addEntity(Product.class);
	// list = query.list();
	// if (list != null && list.size() > 0) {
	// return false;
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// return false;
	// }
	// return true;
	// }
	public List<AdminBom> getAlls() {
		List<AdminBom> list = new ArrayList<AdminBom>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from manufactures  where 1 =1 ");
			query.addEntity(AdminBom.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public AdminBom create(AdminBom re) {
		AdminBom dto = new AdminBom();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				// dto.setId(id);
				Date dt = new Date();
				// dto.setBannerImage(bannerImage);
				dto.setCreatedTime(dt.getTime() / 1000);
				session.persist(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	@SuppressWarnings("unchecked")
	public List<AdminBom> getAllAdminBomPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AdminBom.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<AdminBom> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllAdminBom(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AdminBom.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<AdminBom> list = criteria.list();
		return list.size();
	}

	public AdminBom update(AdminBom re) {
		AdminBom dto = new AdminBom();
		dto = re;
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Date dt = new Date();
			if (re != null && !re.equals("")) {
				dto.setUpdateTime(dt.getTime() / 1000);
				session.update(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

}
