package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.PackageTypes;
import com.tranzi.model.Tags;

@Repository
public class TagsDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Tags> getTagsByProductId(int id) {
		List<Tags> list = new ArrayList<Tags>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from product_tag where product_id = ? ")
					.setParameter(0, id);
			query.addEntity(Tags.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public List<Tags> getTagsByArticleId(int id) {
		List<Tags> list = new ArrayList<Tags>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from product_tag where article_id = ? ")
					.setParameter(0, id);
			query.addEntity(Tags.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public Tags getById(int id) {
		List<Tags> list = new ArrayList<Tags>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from product_tag where id = ? ")
					.setParameter(0, id);
			query.addEntity(Tags.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public int delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from product_tag where id = ? ").setParameter(0, id).executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public List<Tags> getAlls() {
		List<Tags> list = new ArrayList<Tags>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from product_tag  where 1 =1 ");
			query.addEntity(Tags.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject create(Tags re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Tags dto = new Tags();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				session.persist(dto);
				respon.setObject(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}

	public ResponseObject update(Tags re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Tags dto = new Tags();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				respon.setObject(dto);
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}

	public int deleteTagsByIdProduct(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from product_tag where product_id = ? ").setParameter(0, id).executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	// add list attribute value and product
	public void addTagByProduct(List<Tags> list, long id) {
		if (id > 0) {
			deleteTagsByIdProduct(id);
		}
		Session session = this.sessionFactory.getCurrentSession();
		if (list != null && list.size() > 0) {
			for (Tags dto : list) {
				if (dto != null) {
					try {
						dto.setId(0);
						session.persist(dto);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
		}

	}

}
