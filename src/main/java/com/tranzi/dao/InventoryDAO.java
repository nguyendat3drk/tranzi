package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Inventory;
import com.tranzi.model.InventoryOrder;

@Repository
public class InventoryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Inventory getById(int id) {
		List<Inventory> list = new ArrayList<Inventory>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from inventory where id = ? ").setParameter(0,
					id);
			query.addEntity(Inventory.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public ResponseObject getDeleteInventory(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			if (!checkDeleteInventory(id)) {
				re.setResultCode(205);
				re.setErrorMessage("Sản phẩm đã có phiếu xuất/Nhập kho. Không thể xóa.");
				return re;
			}
			session.createSQLQuery("delete from inventory where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành Công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public boolean checkDeleteInventory(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<InventoryOrder> list = new ArrayList<InventoryOrder>();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from inventoryorder where inventory_id = ? ")
					.setParameter(0, id);
			query.addEntity(InventoryOrder.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Inventory> getAlls() {
		List<Inventory> listCategories = new ArrayList<Inventory>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from inventory  where 1 =1 ");
			query.addEntity(Inventory.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories;
	}

	public Inventory create(Inventory re) {
		Inventory dto = new Inventory();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;

		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				dto.setCreatedAt(dt.getTime() / 1000);
				dto.setUpdatedAt(dt.getTime() / 1000);
				session.persist(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	@SuppressWarnings("unchecked")
	public List<Inventory> esd(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Inventory.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		List<Inventory> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Inventory> getAllInventoryPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Inventory> list = new ArrayList<Inventory>();
			String sql = "SELECT inventory.id," + " inventory.product_id," + " inventory.stock,"
					+ " inventory.current_stock," + " inventory.created_at," + " inventory.updated_at,"
					+ " ps.name, ps.sku, ia.name as nameArea"
					+ " FROM inventory left join products ps on inventory.product_id = ps.id"
					+ " left join inventory_area ia on inventory.inventory_area_id = ia.id  where 1=1  and ps.name is not null ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPUR(ps.name) like '%" + keySearch.toUpperCase().trim() + "%' OR "
						+ " UPPUR(ia.name) like '%" + keySearch.toUpperCase().trim() + "%' OR "
								+ " UPPUR(ps.sku) like '%" + keySearch.toUpperCase().trim() + "%' )";
			}
			sql = sql + " order by inventory.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Inventory dto;
				list = new ArrayList<Inventory>();
				for (Object[] row : rows) {
					dto = new Inventory();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0].toString()));
					}
					if (row[1] != null && !row[1].equals("")) {
						dto.setProductId(Integer.parseInt(row[1].toString()));
					}

					if (row[2] != null && !row[2].equals("")) {
						dto.setStock(Integer.parseInt(row[2].toString()));
					}
					if (row[3] != null && !row[3].equals("")) {
						dto.setCurrentStock(Integer.parseInt(row[3].toString()));
					}
					if (row[4] != null && !row[4].equals("")) {
						dto.setCreatedAt(Integer.parseInt(row[4].toString()));
					}
					if (row[5] != null && !row[5].equals("")) {
						dto.setUpdatedAt(Integer.parseInt(row[5].toString()));
					}
					if (row[6] != null && !row[6].equals("")) {
						dto.setProductName(row[6].toString());
					}
					if (row[7] != null && !row[7].equals("")) {
						dto.setSku(row[7].toString());
					}
					if (row[8] != null && !row[8].equals("")) {
						dto.setInventoryArea(row[8].toString());
					}
					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public int getCountAllInventorytest(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Inventory.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		List<Inventory> list = criteria.list();
		return list.size();
	}

	@SuppressWarnings("unchecked")
	public int getCountAllInventory(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Inventory> list = new ArrayList<Inventory>();
			String sql = "SELECT inventory.id," + " inventory.product_id," + " inventory.stock,"
					+ " inventory.current_stock," + " inventory.created_at," + " inventory.updated_at," + " ps.name"
					+ " FROM inventory left join products ps on inventory.product_id = ps.id  "
					+ " left join inventory_area ia on inventory.inventory_area_id = ia.id   where 1=1 and ps.name is not null ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPUR(ps.name) like '%" + keySearch.toUpperCase().trim() + "%' OR "
						+ " UPPUR(ia.name) like '%" + keySearch.toUpperCase().trim() + "%' OR "
								+ " UPPUR(ps.sku) like '%" + keySearch.toUpperCase().trim() + "%' )";
			}
			sql = sql 		+ " order by inventory.id desc";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				return rows.size();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public Inventory update(Inventory re) {
		Inventory dto = new Inventory();
		dto = re;
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				Date dt = new Date();
				dto.setUpdatedAt(dt.getTime() / 1000);
				session.update(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	// Xuất nhập kho
	public void invoiceOrder(long idInventory, int quatity, int type) {
		if (idInventory > 0) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				SQLQuery query = (SQLQuery) session.createSQLQuery("select * from inventory where id = ? ")
						.setParameter(0, idInventory);
				query.addEntity(Inventory.class);
				List<Inventory> list = query.list();
				if (list != null && list.size() > 0) {
					// nhập kho
					if (type == 1) {
						list.get(0).setCurrentStock(list.get(0).getCurrentStock() + quatity);
					} else {
						// xuất kho
						list.get(0).setCurrentStock(list.get(0).getCurrentStock() - quatity);
					}
					update(list.get(0));

				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

	}
}
