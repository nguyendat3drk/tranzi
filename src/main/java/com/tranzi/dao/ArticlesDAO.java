package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ArticleLinkUpload;
import com.tranzi.model.Articles;
import com.tranzi.model.ProductArticle;
import com.tranzi.model.TermsArticles;
import com.tranzi.model.respontOrder.SelectLinkFile;

@Repository
public class ArticlesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Articles getById(int id) {
		List<Articles> listVideos = new ArrayList<Articles>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from articles where id = ? ").setParameter(0,
					id);
			query.addEntity(Articles.class);
			listVideos = query.list();
			if (listVideos != null && listVideos.size() > 0) {
				listVideos.get(0).setListTerms(getListTermsByArticleId(id));
				listVideos.get(0).setProductId(checkArticleProeduct(id));
				listVideos.get(0).setListArticleLinkUpload(getByIdArticleLinkUpload(listVideos.get(0).getId()));
				return listVideos.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Integer checkArticleProeduct(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery(
							"select products_articles.article_id from products left join products_articles on products.id = products_articles.product_id where products.id = ? and products_articles.article_id is not null  ")
					.setParameter(0, id);
			List<Integer> idMax = query.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					return 0;
				} else {
					return Integer.parseInt(idm.toString());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public List<Integer> getListTermsByArticleId(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Integer> list = new ArrayList<>();
		try {
			String sqls = "select term_id from articles_terms where article_id = ? ";
			Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
			List<Integer> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				for (Integer in : idMax) {
					idm = in;
					list.add(idm);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return list;
	}

	public ResponseObject deleteTerm(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			// if (!checkDeleteCategori(id)) {
			// re.setResultCode(205);
			// re.setErrorMessage("Thể loại đã liên kết Sản phẩn. không thể
			// xóa");
			// }else{
			session.createSQLQuery("delete from articles where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành công");
			// }
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public boolean checkDeleteCategori(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (id > 0) {
				Criteria criteria = session.createCriteria(Articles.class);
				criteria.add(Restrictions.sqlRestriction(" id = " + id + " and count_product > 0  "));
				List<Articles> list = criteria.list();
				if (list != null && list.size() > 0) {
					return false;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}

	public List<Articles> getAlls() {
		List<Articles> listVideos = new ArrayList<Articles>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from articles  where 1 =1 order by id Desc ");
			query.addEntity(Articles.class);
			listVideos = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listVideos;
	}

	public int getIdmax() {
		Session session = this.sessionFactory.getCurrentSession();

		int id = 1;
		try {
			String sqls = "select max(id)+1 from articles where 1=1";
			Query query3 = session.createSQLQuery(sqls);
			List<BigInteger> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				BigInteger idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					id = 1;
				} else {
					id = Integer.parseInt(idm.toString());
				}

			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Articles create(Articles re) {
		Articles dto = new Articles();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}

		List<Articles> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}

		Date dt = new Date();
		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug().replace("--", "-") + dt.getSeconds());
		}
		// int id = getIdmax();
		try {
			if (dto != null && !dto.equals("")) {
				session.persist(dto);
			}
			if (dto.getId() > 0 && dto.getListArticleLinkUpload() != null
					&& dto.getListArticleLinkUpload().size() > 0) {
				for (int i = 0; i < dto.getListArticleLinkUpload().size(); i++) {
					ArticleLinkUpload d = new ArticleLinkUpload();
					d = dto.getListArticleLinkUpload().get(i);
					if (d.getStatus() > 0) {
						d.setArticleId(dto.getId());
						createArticleLinkUpload(d);
					}
				}
			}
			// add ProductsCategories
			try {
				if (dto.getListTerms() != null && dto.getListTerms().size() > 0) {
					for (long cate : dto.getListTerms()) {
						TermsArticles po = new TermsArticles();
						po.setId(0);
						po.setArticleId(Integer.parseInt(dto.getId() + ""));
						po.setTermsId(Integer.parseInt(cate + ""));
						addTermSArticle(po);
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			// add product article
			try {
				deleteArticleProduct(dto.getId());
				if (dto.getProductId() > 0) {
					ProductArticle po = new ProductArticle();
					po.setId(0);
					po.setArticleId(Integer.parseInt(dto.getId() + ""));
					po.setProductID(Integer.parseInt(dto.getProductId() + ""));
					addProductArticle(po);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public void createArticleLinkUpload(ArticleLinkUpload dto) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (dto != null) {
				dto.setId(0);
				dto.setType(1);
				session.persist(dto);
				session.flush();

				// }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void addTermSArticle(TermsArticles caTerm) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (caTerm != null) {
				caTerm.setId(0);
				session.persist(caTerm);
				session.flush();

				// }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void addProductArticle(ProductArticle caTerm) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (caTerm != null) {
				caTerm.setId(0);
				session.persist(caTerm);
				session.flush();

				// }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public List<Articles> checkSlug(String slug, long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Articles.class);

		if (slug != null && !slug.equals("") && id <= 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) like '" + slug.toUpperCase().trim() + "%') "));
		} else if (slug != null && !slug.equals("") && id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) = '" + slug.toUpperCase().trim() + "%') "));
		}
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
		}
		List<Articles> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Articles> getAllVideosPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Articles.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		criteria.addOrder(Order.desc("id"));

		List<Articles> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllVideos(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Articles.class);
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Articles> list = criteria.list();
		return list.size();
	}

	public Articles update(Articles re) {
		Articles dto = new Articles();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}
		List<Articles> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size()) + ")");
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				session.update(dto);
			}
			if (dto.getId() > 0 && dto.getListArticleLinkUpload() != null
					&& dto.getListArticleLinkUpload().size() > 0) {
				for (int i = 0; i < dto.getListArticleLinkUpload().size(); i++) {
					ArticleLinkUpload d = new ArticleLinkUpload();
					d = dto.getListArticleLinkUpload().get(i);
					if (d.getStatus() > 0) {
						d.setType(1);
						d.setArticleId(dto.getId());
						createArticleLinkUpload(d);
					}
				}
			}
			try {
				if (dto.getListTerms() != null && dto.getListTerms().size() > 0) {
					if (deleteTermArticle(dto.getId())) {
						for (long cate : dto.getListTerms()) {
							TermsArticles po = new TermsArticles();
							po.setId(0);
							po.setArticleId(Integer.parseInt(dto.getId() + ""));
							po.setTermsId(Integer.parseInt(cate + ""));
							addTermSArticle(po);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return dto;
	}

	public boolean deleteTermArticle(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("DELETE FROM articles_terms where article_id = ?").setParameter(0, id)
					.executeUpdate();
			session.flush();
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteArticleProduct(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("DELETE FROM products_articles where article_id = ?").setParameter(0, id)
					.executeUpdate();
			session.flush();
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	// public void addVideosCatetory(CategoriesVideos caTerm, int type) {
	// Session session = this.sessionFactory.getCurrentSession();
	// try {
	// if (caTerm != null) {
	// caTerm.setId(0);
	// session.persist(caTerm);
	// session.flush();
	//
	// // }
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	// }
	public List<ArticleLinkUpload> getByIdArticleLinkUpload(long idArticle) {
		List<ArticleLinkUpload> listVideos = new ArrayList<ArticleLinkUpload>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery("select * from article_link_upload where type = 1 and article_id = ? ")
					.setParameter(0, idArticle);
			query.addEntity(ArticleLinkUpload.class);
			listVideos = query.list();
			if (listVideos != null && listVideos.size() > 0) {
				return listVideos;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	public List<ArticleLinkUpload> getByIdProductLinkUpload(long productID) {
		List<ArticleLinkUpload> listVideos = new ArrayList<ArticleLinkUpload>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery("select * from article_link_upload where type = 2 and article_id = ? ")
					.setParameter(0, productID);
			query.addEntity(ArticleLinkUpload.class);
			listVideos = query.list();
			if (listVideos != null && listVideos.size() > 0) {
				return listVideos;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public List<ArticleLinkUpload> getSearchArticleLinkUpload(SelectLinkFile dto) {
		List<ArticleLinkUpload> listVideos = new ArrayList<ArticleLinkUpload>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "select au.* " + " FROM admin_tranzi.article_link_upload au "
					+ " left join articles ar on au.article_id = ar.id and au.type = 1 "
					+ " left join products ps on au.article_id = ps.id and au.type = 2 " + " where 1 = 1  ";
			if (dto!= null && !"".equals(dto.getName())&& dto.getName() != null && dto.getName().trim()!=null) {
				sql = sql + " and (UPPER(ps.name)  like '%" + dto.getName().toUpperCase() + "%') ";
				sql = sql + " or (UPPER(ps.sku) like '%" + dto.getName().toUpperCase() + "%') ";
				sql = sql + " or (UPPER(ar.name) like '%" + dto.getName().toUpperCase() + "%') ";
			}

			if (dto!= null  && dto.getIdArticle() > 0) {
				sql = sql + " and (au.article_id = "+dto.getIdArticle()+") ";
			}
			sql = sql + " order by au.id desc limit 100";
			SQLQuery query = (SQLQuery) session.createSQLQuery(sql);
			query.addEntity(ArticleLinkUpload.class);
			listVideos = query.list();
			if (listVideos != null && listVideos.size() > 0) {
				return listVideos;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}
