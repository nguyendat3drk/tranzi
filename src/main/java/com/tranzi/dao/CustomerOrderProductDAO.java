package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.CustomerOrder;
import com.tranzi.model.CustomerOrderProduct;

@Repository
public class CustomerOrderProductDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<CustomerOrderProduct> getAllByCustomerOrderId(int id) {
		List<CustomerOrderProduct> list = new ArrayList<CustomerOrderProduct>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT customer_order_product.id,"
					+ "customer_order_product.product_id ,"
					+ "customer_order_product.customer_order_id ,"
					+ "customer_order_product.quantity ,"
					+ "customer_order_product.unit_price ,"
					+ "customer_order_product.sub_total ,"
					+ "customer_order_product.discount_total ,"
					+ "customer_order_product.discount_id ,"
					+ "customer_order_product.discount_code ,"
					+ "customer_order_product.total ,"
					+ "customer_order_product.created_at ,"
					+ "customer_order_product.updated_at ,"
					+ "products.orgin_sku ,"
					+ "products.sku ,"
					+ "products.name "
					+ " FROM customer_order_product"
					+ " left join products on customer_order_product.product_id = products.id  "
					+ " where customer_order_product.customer_order_id = "+id+" ";
			
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				CustomerOrderProduct dto;
				list = new ArrayList<CustomerOrderProduct>();
				for (Object[] row : rows) {
					dto = new CustomerOrderProduct();
					dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
					dto.setProductId(Integer.parseInt(row[1] != null ? row[1].toString() : "0"));
					dto.setCustomerOrderId(Integer.parseInt(row[2] != null ? row[2].toString() : "0"));
					dto.setQuantity(Integer.parseInt(row[3] != null ? row[3].toString() : "0"));
					dto.setUnitPrice(Integer.parseInt(row[4] != null ? row[4].toString() : "0"));
					dto.setSubTotal(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
					dto.setDiscountTotal(Integer.parseInt(row[6] != null ? row[6].toString() : "0"));
					dto.setDiscountId(Integer.parseInt(row[7] != null ? row[7].toString() : "0"));
					dto.setDiscountCode(Integer.parseInt(row[8] != null ? row[8].toString() : "0"));
					dto.setTotal(Integer.parseInt(row[9] != null ? row[9].toString() : "0"));
					dto.setCreatedAt(Integer.parseInt(row[10] != null ? row[10].toString() : "0"));
					dto.setUpdatedAt(Integer.parseInt(row[11] != null ? row[11].toString() : "0"));
					dto.setOrginSku(row[12] != null ? row[12].toString() : "");
					dto.setSku(row[13] != null ? row[13].toString() : "");
					dto.setProductName(row[14] != null ? row[14].toString() : "");
					list.add(dto);
				}
				if (list != null && list.size() > 0) {
					return list;
				}
			}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	public List<CustomerOrderProduct> getByCustomerOrderIDExport(long id) {
		List<CustomerOrderProduct> list = new ArrayList<CustomerOrderProduct>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT customer_order_product.id,"
					+ "customer_order_product.product_id ,"
					+ "customer_order_product.customer_order_id ,"
					+ "customer_order_product.quantity ,"
					+ "customer_order_product.unit_price ,"
					+ "customer_order_product.sub_total ,"
					+ "customer_order_product.discount_total ,"
					+ "customer_order_product.discount_id ,"
					+ "customer_order_product.discount_code ,"
					+ "customer_order_product.total ,"
					+ "customer_order_product.created_at ,"
					+ "customer_order_product.updated_at ,"
					+ "products.orgin_sku ,"
					+ "products.sku ,"
					+ "products.name, "
					+ "products.short_description, "
					+ "products.manufacture_id "
					+ " FROM customer_order_product"
					+ " left join products on customer_order_product.product_id = products.id  "
					+ " where customer_order_product.customer_order_id = "+id+" ";
			
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				CustomerOrderProduct dto;
				list = new ArrayList<CustomerOrderProduct>();
				for (Object[] row : rows) {
					dto = new CustomerOrderProduct();
					dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
					dto.setProductId(Integer.parseInt(row[1] != null ? row[1].toString() : "0"));
					dto.setCustomerOrderId(Integer.parseInt(row[2] != null ? row[2].toString() : "0"));
					dto.setQuantity(Integer.parseInt(row[3] != null ? row[3].toString() : "0"));
					dto.setUnitPrice(Integer.parseInt(row[4] != null ? row[4].toString() : "0"));
					dto.setSubTotal(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
					dto.setDiscountTotal(Integer.parseInt(row[6] != null ? row[6].toString() : "0"));
					dto.setDiscountId(Integer.parseInt(row[7] != null ? row[7].toString() : "0"));
					dto.setDiscountCode(Integer.parseInt(row[8] != null ? row[8].toString() : "0"));
					dto.setTotal(Integer.parseInt(row[9] != null ? row[9].toString() : "0"));
					dto.setCreatedAt(Integer.parseInt(row[10] != null ? row[10].toString() : "0"));
					dto.setUpdatedAt(Integer.parseInt(row[11] != null ? row[11].toString() : "0"));
					dto.setOrginSku(row[12] != null ? row[12].toString() : "");
					dto.setSku(row[13] != null ? row[13].toString() : "");
					dto.setProductName(row[14] != null ? row[14].toString() : "");
					dto.setProductDescription(row[15] != null ? row[15].toString() : "");
					dto.setManufactureId(Integer.parseInt(row[16] != null ? row[16].toString() : "0"));
					list.add(dto);
				}
				if (list != null && list.size() > 0) {
					return list;
				}
			}
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	

}
