package com.tranzi.dao;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AgentReport;
import com.tranzi.model.ContactReport;
import com.tranzi.model.QueueReport;

@Repository
public class ReportDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<ContactReport> getReport(String type, String fromDate, String toDate, String userName, String oder,
			float timeUTC) {
		List<ContactReport> list = new ArrayList<ContactReport>();
		if (type != null && type.equals("1")) {
			list = getContact(type, fromDate, toDate, userName, oder, timeUTC);
		}
		return list;
	}

	public List<ContactReport> gettFullStatic(List<ContactReport> list, String fromdate, String toDate) {
		List<ContactReport> listRestpon = new ArrayList<ContactReport>();
		ContactReport dto = new ContactReport();
		// QueueReport max = new QueueReport();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		if (list != null && list.size() > 0) {
			try {
				Date min = df.parse(fromdate);
				Date max = df.parse(toDate);
				String dmax = sdf.format(max);
				int leng = 1;
				String datestr = "";
				for (int i = 0; i < leng; i++) {
					dto = new ContactReport();
					datestr = sdf.format(min);
					dto.setDateTime(datestr);
					dto.setTotalCall(0);
					dto.setAbandonCall(0);
					dto.setAnsweredCall(0);
					dto.setAverageTime("0");
					for (ContactReport dt : list) {
						if (datestr.equals(dt.getDateTime().trim())) {
							dto = dt;
							continue;
						}
					}

					listRestpon.add(dto);
					if (min.equals(max)) {
						break;
					} else {
						leng++;
					}
					min.setDate(min.getDate() + 1);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		return listRestpon;
	}

	public List<ContactReport> gettFullStaticHours(List<ContactReport> list, String fromdate, String toDate) {
		List<ContactReport> listRestpon = new ArrayList<ContactReport>();
		ContactReport dto = new ContactReport();
		System.out.println("fromdateFOR" + fromdate);
		// QueueReport max = new QueueReport();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:00:00");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:00:00");
		if (list != null && list.size() > 0) {
			try {
				Date min = df.parse(fromdate);
				min.setHours(0);
				Date max = df.parse(fromdate);
				max.setHours(23);
				String dmax = sdf.format(max);
				int leng = 1;
				String datestr = "";
				for (int i = 0; i < leng; i++) {
					dto = new ContactReport();
					datestr = sdf.format(min);
					dto.setDateTime(datestr);
					dto.setTotalCall(0);
					dto.setAbandonCall(0);
					dto.setAnsweredCall(0);
					dto.setAverageTime("0");
					for (ContactReport dt : list) {
						if (datestr.equals(dt.getDateTime().trim())) {
							dto = dt;
							continue;
						}
					}

					listRestpon.add(dto);
					if (min.equals(max)) {
						break;
					} else {
						leng++;
					}
					min.setHours(min.getHours() + 1);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		return listRestpon;
	}

	public List<ContactReport> getContact(String type, String fromDate, String toDate, String userName, String oder,
			float timeUTC) {
		List<ContactReport> list = new ArrayList<ContactReport>();
		int typeRole = checkTypeRole(userName);
		String frDateconvert = "";
		String tDateconvert = "";
		String frDate = "";
		String tDate = "";
		String frDateCheckUTC = "";
		String tDateCheckUTC = "";
		String toMaxDate = "";
		String tominDate = "";
		String fromMinDate = "";
		DateFormat formatCheck = new SimpleDateFormat("MM/dd/yyyy");
		// formatCheck.setTimeZone(TimeZone.getTimeZone("UTC"));
		DateFormat toloadFomat = new SimpleDateFormat("MM/dd/yyyy 00:00:00");
		// toloadFomat.setTimeZone(TimeZone.getTimeZone("UTC"));
		float hourUTC = 0;
		try {
			hourUTC = timeUTC;
			hourUTC = hourUTC * (-1);

		} catch (Exception e) {
			// TODO: handle exception
		}
		String tDateUtC = "";
		String frDateUtc = "";

		Long f = 0L;
		Long t = 0L;
		Date datet = new Date();
		Date date = new Date();
		DateFormat formatTimeUTC = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		formatTimeUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {

			DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			format.setTimeZone(TimeZone.getTimeZone("UTC"));

			DateFormat formatConvert = new SimpleDateFormat("MM/dd/yyyy 00:00:00");
			formatConvert.setTimeZone(TimeZone.getTimeZone("UTC"));
			SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			try {
				f = Long.parseLong(fromDate);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			if (f != 0) {
				date = new Date(f);

				frDate = format.format(date);
				frDateconvert = formatConvert.format(date);
				Date dateFromUTC = new Date(f);
				System.out.println("fromdate" + formatTimeUTC.format(date));
				dateFromUTC.setHours((int) (date.getHours() + hourUTC));
				frDateUtc = formatTimeUTC.format(dateFromUTC);
				if (hourUTC > -9 && hourUTC < 9) {
					dateFromUTC.setHours((int) (date.getHours() - hourUTC * 2));
					frDateCheckUTC = formatCheck.format(dateFromUTC);
					tominDate = toloadFomat.format(dateFromUTC);
					fromMinDate = toloadFomat.format(dateFromUTC);
				} else {
					frDateCheckUTC = formatCheck.format(date);
					tominDate = toloadFomat.format(date);
					fromMinDate = toloadFomat.format(dateFromUTC);
				}

			} else {

				date = formatter.parse(fromDate);
				frDate = format.format(date);

			}

			try {
				t = Long.parseLong(toDate);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			if (t != 0) {
				datet = new Date(t);
				tDate = format.format(datet);
				tDateconvert = formatConvert.format(datet);
				Date dateFromUTC = new Date(t);
				System.out.println("todateff" + formatTimeUTC.format(datet));
				dateFromUTC.setHours((int) (datet.getHours() + hourUTC));
				tDateUtC = formatTimeUTC.format(dateFromUTC);
				tDateCheckUTC = formatCheck.format(dateFromUTC);
				toMaxDate = toloadFomat.format(dateFromUTC);
			} else {
				Date datettt = formatter.parse(toDate);
				tDate = format.format(datettt);

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		String convetDate = "%m/%d/%Y";
		String hourUTCr = "";
		hourUTCr = " + INTERVAL " + hourUTC + " HOUR";
		System.out.println("frDateCheckUTC=" + frDateCheckUTC);
		System.out.println("tDateCheckUTC=" + tDateCheckUTC);
		if (frDateCheckUTC.equals(tDateCheckUTC) && !oder.equals("")) {
			convetDate = "%m/%d/%Y %H:00:00";

		}
		try {
			int id = 0;
			Session session = this.sessionFactory.getCurrentSession();
			String sql = "select " + " DATE_FORMAT(COALESCE((cd.start_call_time  + INTERVAL " + hourUTC
					+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC + " HOUR)),'" + convetDate + "') as dateTime , "
					+ " concat('',SEC_TO_TIME(SUM(UNIX_TIMESTAMP((cd.end_call_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cd.start_call_time + INTERVAL " + hourUTC + " HOUR))))) as timecall, "
					+ " SUM((case  when (UPPER(cd.state_of_miss_call) != 'TIMEOUT' or cd.state_of_miss_call is null ) then 1 else 0 end)) as TotalCall, "
					+ " SUM((case  when UPPER(cd.state_of_miss_call) = 'ABANDON' then 1 else 0 end)) as AbandonCall, "
					+ " SUM((case  when (cd.start_call_time is not null and cd.end_call_time is not null) then 1 else 0 end)) as AnsweredCall, "
					+ " concat('',TIME(ROUND((SEC_TO_TIME(SUM(UNIX_TIMESTAMP((cd.end_call_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cd.start_call_time + INTERVAL " + hourUTC
					+ " HOUR)))))/(SUM((case  when (cd.start_call_time is not null and cd.end_call_time is not null) then 1 else 0 end))))))  as AverageTime, "
					+ "  ROUND((SEC_TO_TIME(SUM(UNIX_TIMESTAMP((cd.end_call_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cd.start_call_time + INTERVAL " + hourUTC
					+ " HOUR)))))/(SUM((case  when (cd.start_call_time is not null and cd.end_call_time is not null) then 1 else 0 end))))  as avgTime "
					+ " from call_in_queue cq, call_details cd where cq.call_id = cd.id  ";

			if (fromDate != null && toDate != null) {

				// sql = sql + " and
				// DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) ,
				// '%Y%m%d') >= '"
				// + frDate.replace("/", "").trim() + "' "
				// + " and
				// DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) ,
				// '%Y%m%d') <= '"
				// + tDate.replace("/", "").trim() + "' ";
				sql = sql
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) >= UNIX_TIMESTAMP(DATE_ADD('"
						+ frDateUtc + "', INTERVAL 0 MINUTE)) "
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) <= UNIX_TIMESTAMP(DATE_ADD('"
						+ tDateUtC + "', INTERVAL 0 MINUTE)) ";
				System.out.println("frDateUtc" + frDateUtc);
				System.out.println("tDateUtC" + tDateUtC);

			}
			if (typeRole == 2) {
				sql = sql
						+ " and cq.agent_id in (select ''+acc.account_id from account_in_queue acc where (acc.order_of_agent is not null and acc.order_of_agent != 0) and "
						+ " acc.queue_id in (select a.queue_id from account_in_queue a where a.account_id = "
						+ userName.trim() + " group by a.queue_id) group by acc.account_id ) "
						+ " and cq.queue_id in (select a.queue_id from account_in_queue a where a.account_id ="
						+ userName.trim() + " )";
			} else if (typeRole == 1) {
				sql = sql + " and cq.agent_id = " + userName.trim() + " ";
			}

			sql = sql + " group by  " + " DATE_FORMAT(COALESCE((cd.start_call_time  " + hourUTCr
					+ "),(cd.end_call_time  " + hourUTCr + ")),'" + convetDate
					+ "') order by DATE_FORMAT(COALESCE((cd.start_call_time " + hourUTCr + "),(cd.end_call_time  "
					+ hourUTCr + ")),'" + convetDate + "') " + oder + "";
			Query query = session.createSQLQuery(sql);
			System.out.println("sql=" + sql);
			List<Object[]> lstObj = query.list();
			ContactReport dto;
			if (lstObj != null && lstObj.size() > 0) {
				System.out.println(lstObj);
				for (Object[] od : lstObj) {
					dto = new ContactReport();
					dto.setDateTime(od[0].toString());
					dto.setTimeCall(od[1] != null ? od[1].toString() : null);
					dto.setTotalCall(Integer.parseInt(od[2].toString()));
					dto.setAbandonCall(Integer.parseInt(od[3].toString()));
					dto.setAnsweredCall(Integer.parseInt(od[4].toString()));
					dto.setAverageTime(od[5] != null ? od[5].toString() : "0");
					dto.setAvgTime(od[6] != null ? Integer.parseInt(od[6].toString()) : 0);
					list.add(dto);
				}
			}
			if (oder != null && !frDateCheckUTC.equals(tDateCheckUTC) && oder.equals("asc")) {
				List<ContactReport> listRe = new ArrayList<ContactReport>();
				listRe = gettFullStatic(list, tominDate, toMaxDate);
				if (oder != null && oder.equals("desc")) {
					List<ContactReport> listRe2 = new ArrayList<ContactReport>();
					for (int i = listRe.size() - 1; i >= 0; i--) {
						listRe2.add(listRe.get(i));
					}
					return listRe2;
				}
				return listRe;
			}

			if (oder != null && frDateCheckUTC.equals(tDateCheckUTC) && oder.equals("asc")) {
				List<ContactReport> listRe = new ArrayList<ContactReport>();
				System.out.println("fromdateFOR2" + fromMinDate);
				listRe = gettFullStaticHours(list, fromMinDate, tDateconvert);
				if (oder != null && oder.equals("desc")) {
					List<ContactReport> listRe2 = new ArrayList<ContactReport>();
					for (int i = listRe.size() - 1; i >= 0; i--) {
						listRe2.add(listRe.get(i));
					}
					return listRe2;
				}
				return listRe;
			}

			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public int checkTypeRole(String id) {
		int re = 0;
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "select role.type from user u left join role ON role.id = u.role where u.id = "
				+ Integer.parseInt(id.trim()) + "";
		try {
			Query query = session.createSQLQuery(sql);

			List<Integer> lstObj = query.list();
			if (lstObj != null && lstObj.size() > 0) {
				re = lstObj.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return re;
	}

	public void getQueueReport() {
		String sql = " ";
	}

	public List<QueueReport> gettFullQueue(List<QueueReport> list, String fromDate, String toDate) {
		List<QueueReport> listRestpon = new ArrayList<QueueReport>();
		QueueReport dto = new QueueReport();
		// QueueReport max = new QueueReport();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		if (list != null && list.size() > 0) {
			try {
				Date min = df.parse(fromDate);
				Date max = df.parse(toDate);
				String dmax = sdf.format(max);
				int leng = 1;
				String datestr = "";
				for (int i = 0; i < leng; i++) {
					dto = new QueueReport();
					datestr = sdf.format(min);
					dto.setDateTime(datestr);
					dto.setQueueName("");
					dto.setMaxTimeNumber(0);
					dto.setAvgTimeNumber(0);
					for (QueueReport dt : list) {
						if (datestr.equals(dt.getDateTime().trim())) {
							dto = dt;
							continue;
						}
					}

					listRestpon.add(dto);
					if (min.equals(max)) {
						break;
					} else {
						leng++;
					}
					min.setDate(min.getDate() + 1);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		}

		return listRestpon;
	}

	public ResponseObject getQueueReport(String type, String fromDate, String toDate, String userName, String oder,
			String queue, float timeUTC) {
		ResponseObject responseObject = new ResponseObject();
		List<QueueReport> list = new ArrayList<QueueReport>();
		int typeRole = checkTypeRole(userName);
		float hourUTC = 0;
		try {
			hourUTC = timeUTC;
			hourUTC = hourUTC * (-1);

		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			int id = 0;
			Session session = this.sessionFactory.getCurrentSession();
			String sql = "select " + " DATE_FORMAT(COALESCE((cd.start_call_time + INTERVAL " + hourUTC
					+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC + " HOUR)),'%m/%d/%Y') as dateTime ,  "
					+ " (select name from queue q where q.id =cq.queue_id and q.delete_state =0) as queuename, "
					+ " concat('',SEC_TO_TIME(MAX(UNIX_TIMESTAMP((cq.end_queue_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cq.in_queue_time + INTERVAL " + hourUTC + " HOUR))))) as maxwait, "
					+ " TIME_TO_SEC(SEC_TO_TIME(MAX(UNIX_TIMESTAMP((cq.end_queue_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cq.in_queue_time + INTERVAL " + hourUTC
					+ " HOUR))))) as maxwaitNumber, "
					+ " concat('',TIME(ROUND(AVG(UNIX_TIMESTAMP((cq.end_queue_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cq.in_queue_time + INTERVAL " + hourUTC + " HOUR)))))) as avgwait, "
					+ " TIME_TO_SEC(TIME(ROUND(AVG(UNIX_TIMESTAMP((cq.end_queue_time + INTERVAL " + hourUTC
					+ " HOUR)) - UNIX_TIMESTAMP((cq.in_queue_time + INTERVAL " + hourUTC
					+ " HOUR)))))) as avgwaitNumber, "
					+ " SUM((case  when UPPER(cd.state_of_miss_call) = 'ABANDON' then 1 else 0 end)) as AbandonCall, "
					+ "  COALESCE(TIME(ROUND((SEC_TO_TIME(  SUM((case  when UPPER(cd.state_of_miss_call) = 'ABANDON' then UNIX_TIMESTAMP((cd.end_call_time + INTERVAL "
					+ hourUTC + " HOUR)) - UNIX_TIMESTAMP((cd.receive_time + INTERVAL " + hourUTC
					+ " HOUR)) else 0 end))   )  )/( SUM((case  when UPPER(cd.state_of_miss_call) = 'ABANDON' then 1 else 0 end))  )  )),'0')  as avgBefore "
					+ " from call_in_queue cq, call_details cd where cq.call_id = cd.id and (select name from queue q where q.id =cq.queue_id) is not null ";
			String frDate = "";
			String tDate = "";
			String frDatecheck = "";
			String tDatecheck = "";
			if (fromDate != null && toDate != null) {
				Date date = new Date();
				Date datet = new Date();
				Long f = 0L;
				Long t = 0L;
				DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				format.setTimeZone(TimeZone.getTimeZone("UTC"));
				DateFormat formatCheck = new SimpleDateFormat("MM/dd/yyyy");
				formatCheck.setTimeZone(TimeZone.getTimeZone("UTC"));
				DateFormat formatTimeUTC = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				formatTimeUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
				SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
				formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
				try {
					f = Long.parseLong(fromDate);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (f != 0) {
					date = new Date(f);

					Date dateFromUTC = new Date(f);
					dateFromUTC.setHours((int) (date.getHours() + hourUTC));
					frDate = formatTimeUTC.format(dateFromUTC);
					frDatecheck = formatCheck.format(dateFromUTC);
				} else {

					date = formatter.parse(fromDate);
					frDate = format.format(date);

				}

				try {
					t = Long.parseLong(toDate);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (t != 0) {
					datet = new Date(t);
					// tDate = formatTimeUTC.format(datet);
					Date dateFromUTC = new Date(t);
					dateFromUTC.setHours((int) (datet.getHours() + hourUTC));
					tDate = formatTimeUTC.format(dateFromUTC);
					tDatecheck = formatCheck.format(dateFromUTC);
				} else {
					Date datettt = formatter.parse(toDate);
					tDate = format.format(datettt);
				}
				String tDateUtC = formatTimeUTC.format(datet);
				String frDateUtc = formatTimeUTC.format(date);
				// sql = sql + " and
				// DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) ,
				// '%Y%m%d') >= '"+ frDate.replace("/", "").trim() + "' "
				// + " and
				// DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) ,
				// '%Y%m%d') <= '"+ tDate.replace("/", "").trim() + "' ";
				sql = sql
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) >= UNIX_TIMESTAMP(DATE_ADD('"
						+ frDate + "', INTERVAL 0 MINUTE)) "
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) <= UNIX_TIMESTAMP(DATE_ADD('"
						+ tDate + "', INTERVAL 0 MINUTE)) ";
			}
			if (typeRole == 2) {
				sql = sql
						+ " and cq.agent_id in (select ''+acc.account_id from account_in_queue acc where (acc.order_of_agent is not null and acc.order_of_agent != 0) and "
						+ " acc.queue_id in (select a.queue_id from account_in_queue a where a.account_id = "
						+ userName.trim()
						+ " group by a.queue_id) group by acc.account_id )  and cq.queue_id in (select a.queue_id from account_in_queue a where a.account_id ="
						+ userName.trim() + " )";
			} else if (typeRole == 1) {
				sql = sql + " and cq.agent_id = " + userName.trim() + " ";
			}
			if (queue != null && !queue.equals("")) {
				sql = sql + " and cq.queue_id in (select que.id from queue que where que.name ='" + queue.trim()
						+ "') ";
			}
			sql = sql
					+ " and (select name from queue q where q.id =cq.queue_id and q.delete_state =0) is not null group by  "
					+ " DATE_FORMAT(COALESCE((cd.start_call_time + INTERVAL " + hourUTC
					+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC
					+ " HOUR)),'%d/%m/%Y'),  (select name from queue q where q.id =cq.queue_id) ";
			if (oder.equals("asc")) {
				sql = sql + " order by DATE_FORMAT(COALESCE((cd.start_call_time + INTERVAL " + hourUTC
						+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC + " HOUR)),'%Y/%m/%d') " + oder
						+ ",queuename desc";
			} else {
				sql = sql + " order by DATE_FORMAT(COALESCE((cd.start_call_time + INTERVAL " + hourUTC
						+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC + " HOUR)),'%Y/%m/%d') " + oder
						+ ",queuename asc";
			}

			Query query = session.createSQLQuery(sql);
			System.out.println(sql);
			List<Object[]> lstObj = query.list();
			QueueReport dto;
			if (oder != null && oder.equals("desc")) {
				if (lstObj != null && lstObj.size() > 0) {
					for (Object[] od : lstObj) {
						dto = new QueueReport();
						dto.setDateTime(od[0].toString());
						dto.setQueueName(od[1] != null ? od[1].toString() : " ");
						dto.setMaxTime(od[2] != null ? od[2].toString() : null);
						dto.setMaxTimeNumber(od[3] != null ? Integer.parseInt(od[3].toString()) : 0);
						dto.setAvgTime(od[4] != null ? od[4].toString() : "0");
						dto.setAvgTimeNumber(od[5] != null ? Integer.parseInt(od[5].toString()) : 0);
						dto.setAbandonCall(od[6] != null ? Integer.parseInt(od[6].toString()) : 0);
						dto.setAvgBeforeTime(od[7] != null ? od[7].toString() : null);
						list.add(dto);

					}
				}
			} else {
				String queueName = "";
				if (lstObj != null && lstObj.size() > 0) {
					if (lstObj.get(lstObj.size() - 1)[1] != null) {
						queueName = lstObj.get(lstObj.size() - 1)[1].toString();
					}
					System.out.println(lstObj);
					for (Object[] od : lstObj) {
						dto = new QueueReport();
						dto.setDateTime(od[0].toString());
						dto.setQueueName(od[1] != null ? od[1].toString() : null);
						dto.setMaxTime(od[2] != null ? od[2].toString() : null);
						dto.setMaxTimeNumber(od[3] != null ? Integer.parseInt(od[3].toString()) : 0);
						dto.setAvgTime(od[4] != null ? od[4].toString() : "0");
						dto.setAvgTimeNumber(od[5] != null ? Integer.parseInt(od[5].toString()) : 0);
						dto.setAbandonCall(od[6] != null ? Integer.parseInt(od[6].toString()) : 0);
						dto.setAvgBeforeTime(od[7] != null ? od[7].toString() : null);
						if (queueName.equals(dto.getQueueName()) && oder != null && oder.equals("asc")) {
							list.add(dto);
						}

					}
				}
			}
			if (oder != null && oder.equals("asc")) {
				List<QueueReport> listRe = new ArrayList<QueueReport>();
				listRe = gettFullQueue(list, frDatecheck, tDatecheck);
				System.out.println("frDatecheck=" + frDatecheck);
				System.out.println("tDatecheck=" + tDatecheck);
				responseObject.setResultCode(200);
				responseObject.setObject(listRe);
				return responseObject;
			}
			responseObject.setResultCode(200);
			responseObject.setObject(list);
			return responseObject;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public ResponseObject getAgentReport(String type, String fromDate, String toDate, String userName, String oder,
			String queue, float timeUTC) {
		ResponseObject responseObject = new ResponseObject();
		List<AgentReport> list = new ArrayList<AgentReport>();
		int typeRole = checkTypeRole(userName);
		float hourUTC = 0;
		try {
			hourUTC = timeUTC;
			hourUTC = hourUTC * (-1);

		} catch (Exception e) {
			// TODO: handle exception
		}
		Date date = new Date();
		Date datet = new Date();
		Format formatTimeUTC = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			int id = 0;
			Session session = this.sessionFactory.getCurrentSession();
			String frDate = "";
			String tDate = "";
			String frDatecheck = "";
			String tDatecheck = "";
			if (fromDate != null && toDate != null) {

				Long f = 0L;
				Long t = 0L;
				Format format = new SimpleDateFormat("yyyy/MM/dd");
				Format formatCheck = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a");
				try {
					f = Long.parseLong(fromDate);
					System.out.println("f" + f);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (f != 0) {
					date = new Date(f);
					frDate = format.format(date);
					frDatecheck = formatCheck.format(date);
				} else {

					date = formatter.parse(fromDate);
					frDate = format.format(date);

				}

				try {
					t = Long.parseLong(toDate);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				if (t != 0) {
					datet = new Date(t);
					tDate = format.format(datet);
					tDatecheck = formatCheck.format(datet);
				} else {
					Date datettt = formatter.parse(toDate);
					tDate = format.format(datettt);
				}

			}
			String tDateUtC = formatTimeUTC.format(datet);
			String frDateUtc = formatTimeUTC.format(date);

			System.out.println("datet " + datet.getTime());
			System.out.println("date " + date.getTime());
			System.out.println("tDateUtC " + tDateUtC);
			System.out.println("frDateUtc " + frDateUtc);

			if (oder != null && oder.equals("asc")) {
				List<AgentReport> listRe = new ArrayList<AgentReport>();
				String sql = "select  u.user_name, "
						+ " concat('',SEC_TO_TIME(SUM(UNIX_TIMESTAMP(COALESCE((acc.log_out_time),(utc_timestamp))) - UNIX_TIMESTAMP((acc.log_in_time))))),"
						+ "Round((TIME_TO_SEC(SEC_TO_TIME(SUM(UNIX_TIMESTAMP(COALESCE((acc.log_out_time),(utc_timestamp))) - UNIX_TIMESTAMP((acc.log_in_time )))))/3600),2) as workingTimeNUM  "
						+ " from user u  " + " inner join role r  on u.role = r.id "
						+ " left join account_logged_details acc on u.id = acc.account_id and UNIX_TIMESTAMP(acc.log_in_time ) >=  UNIX_TIMESTAMP(DATE_ADD('"
						+ frDateUtc + "', INTERVAL " + hourUTC + " HOUR))  "
						+ " and UNIX_TIMESTAMP(acc.log_in_time) <=UNIX_TIMESTAMP(DATE_ADD('" + tDateUtC + "', INTERVAL "
						+ hourUTC + " HOUR)) " + " where     r.type = 1  and u.delete_state =  0  ";
				if (typeRole == 2) {
					sql = sql
							+ " and u.id  in (select acc2.account_id from account_in_queue acc2 where (acc2.order_of_agent is not null and acc2.order_of_agent != 0) and "
							+ " acc2.queue_id in (select a.queue_id from account_in_queue a where a.account_id = "
							+ userName.trim() + " group by a.queue_id) group by acc2.account_id )";
				} else if (typeRole == 1) {
					sql = sql + " and  u.id  = " + userName.trim() + " ";
				}
				sql = sql + " group by u.user_name  " + " order by u.user_name asc ";
				Query query = session.createSQLQuery(sql);
				System.out.println("sql================" + sql);
				List<Object[]> lstObj = query.list();
				AgentReport dto;
				String queueName = "";
				if (lstObj != null && lstObj.size() > 0) {
					System.out.println(lstObj);
					for (Object[] od : lstObj) {
						dto = new AgentReport();
						dto.setAgentName(od[0] != null ? od[0].toString() : "");
						dto.setWorkingTime(od[1] != null ? od[1].toString() : "0");
						dto.setWorkingTimeNumber(od[2] != null ? od[2].toString() : "0");
						listRe.add(dto);
					}
				}
				responseObject.setResultCode(200);
				responseObject.setObject(listRe);
				responseObject.setErrorMessage(null);
				return responseObject;
			} else {
				String sql = " select   DATE_FORMAT((aa.time),'%m/%d/%Y') as dateTime,aa.agentname,concat('',dd.maxtalktime),concat('',dd.avgtalktime),dd.totalcalls,dd.AbandonCall,dd.Answeredcall,aa.logIn,aa.logIn2,aa.logout, "
						+ " concat('',aa.workingTime) " + " ,concat('',aa.workingTimeNUM) " + " from (   "
						+ " select  DATE_FORMAT(COALESCE((cd.start_call_time + INTERVAL " + hourUTC
						+ " HOUR),(cd.end_call_time + INTERVAL " + hourUTC + " HOUR)),'%Y/%m/%d') as dateTime ,       "
						+ " cq.agent_id as agentid,        "
						+ " (select user_name from user q where q.id =cq.agent_id and q.delete_state =0) as agentname,  "
						+ " concat('',SEC_TO_TIME(MAX(UNIX_TIMESTAMP((cd.end_call_time + INTERVAL " + hourUTC
						+ " HOUR)) - UNIX_TIMESTAMP((cd.start_call_time + INTERVAL " + hourUTC
						+ " HOUR))))) as maxtalktime, "
						+ " concat('',SEC_TO_TIME(TIME_TO_SEC(TIME(ROUND(SEC_TO_TIME(AVG(UNIX_TIMESTAMP((cd.end_call_time + INTERVAL "
						+ hourUTC + " HOUR)) - UNIX_TIMESTAMP((cd.start_call_time + INTERVAL " + hourUTC
						+ " HOUR))))))))) as avgtalktime, "
						+ " SUM(1) as totalcalls,   SUM((case  when UPPER(cd.state_of_miss_call) = 'ABANDON' then 1 else 0 end)) as AbandonCall, "
						+ " SUM((case  when (cd.start_call_time is not null and cd.end_call_time is not null) then 1 else 0 end)) as Answeredcall  "
						+ " from call_in_queue cq, call_details cd   "
						+ " where cq.call_id = cd.id     and  cq.agent_id in (select user.id from user where user.role in (select id from role where type = 1))   ";
				/*
				 * sql = sql +
				 * " and DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) , '%Y%m%d') >= '"
				 * + frDate.replace("/", "").trim() + "' " +
				 * " and DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time) , '%Y%m%d') <= '"
				 * + tDate.replace("/", "").trim() + "'  ";
				 */

				sql = sql
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) >=UNIX_TIMESTAMP(DATE_ADD('"
						+ frDateUtc + "', INTERVAL " + hourUTC + " HOUR)) "
						+ " and UNIX_TIMESTAMP(COALESCE(cd.start_call_time,cd.end_call_time)) <=UNIX_TIMESTAMP(DATE_ADD('"
						+ tDateUtC + "', INTERVAL " + hourUTC + " HOUR)) ";
				System.out.println("tDateUtC " + tDateUtC);
				System.out.println("frDateUtc " + frDateUtc);

				sql = sql + " group by   DATE_FORMAT(COALESCE(cd.start_call_time,cd.end_call_time),'%Y/%m/%d'),  "
						+ " cq.agent_id,  (select user_name from user q where q.id =cq.agent_id)  )dd  "
						+ " right join (select al.account_id as account,   "
						+ " DATE_FORMAT((al.log_in_time + INTERVAL " + hourUTC + " HOUR),'%Y/%m/%d') as time,   "
						+ " (select user_name from user q where q.id =al.account_id and q.delete_state =0) as agentname, "
						+ " TIME(MIN((al.log_in_time + INTERVAL " + hourUTC + " HOUR))) as logIn,  "
						+ " TIME(MAX((al.log_in_time + INTERVAL " + hourUTC + " HOUR))) as logIn2,   "
						+ " Case WHEN  TIME(MAX(COALESCE((al.log_out_time + INTERVAL " + hourUTC
						+ " HOUR),(NOW() + INTERVAL " + hourUTC + " HOUR)))) = TIME((NOW() + INTERVAL " + hourUTC
						+ " HOUR)) " + " THEN '-'  " + " ELSE TIME(MAX(COALESCE((al.log_out_time + INTERVAL " + hourUTC
						+ " HOUR),(NOW() + INTERVAL " + hourUTC + " HOUR)))) " + " END " + " AS  logout,  "
						+ " SEC_TO_TIME(SUM(UNIX_TIMESTAMP(COALESCE((al.log_out_time),(utc_timestamp))) - UNIX_TIMESTAMP((al.log_in_time )))) workingTime,  "
						+ " ROUND(TIME_TO_SEC(TIMEDIFF(COALESCE((al.log_out_time ),(utc_timestamp)),((al.log_in_time ))))/3600,4) as workingTimeNUM  "
						+ " from account_logged_details al  "
						+ " where UNIX_TIMESTAMP(DATE_ADD(al.log_in_time, INTERVAL 0 MINUTE)) >= UNIX_TIMESTAMP(DATE_ADD('"
						+ frDateUtc + "', INTERVAL " + hourUTC + " HOUR)) "
						+ " and UNIX_TIMESTAMP(DATE_ADD(al.log_in_time, INTERVAL 0 MINUTE)) <= UNIX_TIMESTAMP(DATE_ADD('"
						+ tDateUtC + "', INTERVAL " + hourUTC + " HOUR)) "

						+ " group by al.account_id,  DATE_FORMAT(al.log_in_time,'%Y/%m/%d') ,(select user_name from user q where q.id =al.account_id) "
						+ " order by al.log_in_time desc) aa ON  aa.time =dd.dateTime  and  aa.account = dd.agentid   where 1=1 and aa.agentname is not null "
						+ "  and aa.account in (select ur.id from user ur inner join role  rl ON ur.role = rl.id where rl.type =1) ";
				if (typeRole == 2) {
					sql = sql
							+ "  and aa.account  in (select ''+acc.account_id from account_in_queue acc where (acc.order_of_agent is not null and acc.order_of_agent != 0) and "
							+ " acc.queue_id in (select a.queue_id from account_in_queue a where a.account_id = "
							+ userName.trim() + " group by a.queue_id) group by acc.account_id ) ";
				} else if (typeRole == 1) {
					sql = sql + " and  aa.account  = " + userName.trim() + " ";
				}
				if (queue != null && !queue.equals("")) {
					sql = sql + " and  aa.account  in (select ddd.id from user ddd where ddd.user_name ='"
							+ queue.trim() + "') ";
				}
				/*
				 * sql = sql + " and DATE_FORMAT((aa.time), '%Y%m%d') >= '"+
				 * frDate.replace("/", "").trim() + "' " +
				 * " and DATE_FORMAT((aa.time) , '%Y%m%d') <= '"+
				 * tDate.replace("/", "").trim() + "'  ";
				 */

				if (oder.equals("asc")) {
					sql = sql + " order by  (aa.time + INTERVAL " + hourUTC + " HOUR) " + oder + ",aa.agentname  desc ";
				} else {
					sql = sql + " order by  (aa.time  + INTERVAL " + hourUTC + " HOUR) " + oder + ",aa.agentname  asc";
				}

				Query query = session.createSQLQuery(sql);
				System.out.println(sql);

				List<Object[]> lstObj = query.list();
				AgentReport dto;
				// if (oder != null && oder.equals("desc")) {
				String queueName = "";
				if (lstObj != null && lstObj.size() > 0) {
					System.out.println(lstObj);
					for (Object[] od : lstObj) {
						dto = new AgentReport();
						dto.setDateTime(od[0].toString());
						dto.setAgentName(od[1] != null ? od[1].toString() : "0");
						dto.setMaxTalkTime(od[2] != null ? od[2].toString() : "0");
						dto.setAvgTalkTime(od[3] != null ? od[3].toString() : "0");
						dto.setTotalCalls(od[4] != null ? Integer.parseInt(od[4].toString()) : 0);
						dto.setAbandonCalls(od[5] != null ? Integer.parseInt(od[5].toString()) : 0);
						dto.setAnsweredcall(od[6] != null ? Integer.parseInt(od[6].toString()) : 0);
						dto.setLoginTime(od[7] != null ? od[7].toString() : "0");
						dto.setLastLoginTime(od[8] != null ? od[8].toString() : "0");
						dto.setLogoutTime(od[9] != null ? od[9].toString() : "0");
						dto.setWorkingTime(od[10] != null ? od[10].toString() : "0");
						dto.setWorkingTimeNumber(od[11] != null ? od[11].toString() : "0");
						list.add(dto);

					}
				}
				// }
				// else {
				// String queueName = "";
				// if (lstObj != null && lstObj.size() > 0) {
				// if (lstObj.get(lstObj.size() - 1)[1] != null) {
				// queueName = lstObj.get(lstObj.size() - 1)[1].toString();
				// }
				// System.out.println(lstObj);
				// for (Object[] od : lstObj) {
				// dto = new AgentReport();
				// dto.setDateTime(od[0].toString());
				// dto.setAgentName(od[1] != null ? od[1].toString() : null);
				// dto.setMaxTalkTime(od[2] != null ? od[2].toString() : null);
				// dto.setAvgTalkTime(od[3] != null ? od[3].toString() : null);
				// dto.setTotalCalls(od[4] != null ?
				// Integer.parseInt(od[4].toString()) : 0);
				// dto.setAbandonCalls(od[5] != null ?
				// Integer.parseInt(od[5].toString()) : 0);
				// dto.setAnsweredcall(od[6] != null ?
				// Integer.parseInt(od[6].toString()) : 0);
				// dto.setLoginTime(od[7] != null ? od[7].toString() : null);
				// dto.setLogoutTime(od[8] != null ? od[8].toString() : null);
				// dto.setWorkingTime(od[9] != null ? od[9].toString() : null);
				// dto.setWorkingTimeNumber(od[9] != null ? od[9].toString() :
				// "0");
				// if (queueName.equals(dto.getAgentName()) && oder != null &&
				// oder.equals("asc")) {
				// list.add(dto);
				// }
				//
				// }
				// }
				// }
			}
			responseObject.setResultCode(200);
			responseObject.setObject(list);
			responseObject.setErrorMessage(null);
			return responseObject;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	// public List<AgentReport> gettFullAgent(List<AgentReport> list,String
	// fromDate,String toDate) {
	// List<AgentReport> listRestpon = new ArrayList<AgentReport>();
	// AgentReport dto = new AgentReport();
	// // QueueReport max = new QueueReport();
	// DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	// SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	// if (list != null && list.size() > 0) {
	// try {
	// Date min = df.parse(fromDate);
	// Date max = df.parse(toDate);
	// String dmax = sdf.format(max);
	// int leng = 1;
	// String datestr = "";
	// for (int i = 0; i < leng; i++) {
	// dto = new AgentReport();
	// datestr = sdf.format(min);
	// dto.setDateTime(datestr);
	// dto.setWorkingTimeNumber("0");
	// for (AgentReport dt : list) {
	// if (datestr.equals(dt.getDateTime().trim())) {
	// dto = dt;
	// continue;
	// }
	// }
	//
	// listRestpon.add(dto);
	// if (min.equals(max)) {
	// break;
	// } else {
	// leng++;
	// }
	// min.setDate(min.getDate() + 1);
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	//
	// }
	//
	// return listRestpon;
	// }

}
