package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.Inventory;
import com.tranzi.model.InventoryOrder;

@Repository
public class InventoryOrderDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private InventoryDAO inventoryDAO; 
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<InventoryOrder> getAlls() {
		List<InventoryOrder> listCategories = new ArrayList<InventoryOrder>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from inventoryorder  where 1 =1 ");
			query.addEntity(InventoryOrder.class);
			listCategories = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listCategories;
	}

	public InventoryOrder create(InventoryOrder re) {
		InventoryOrder dto = new InventoryOrder();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;

		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				dto.setCreatedTime(dt.getTime() / 1000);
				dto.setUpdatedAt(dt.getTime() / 1000);
				session.persist(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			if (dto.getId() > 0) {
				inventoryDAO.invoiceOrder(dto.getInventoryId(), dto.getQuantity(), dto.getType());
			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return dto;
	}

	@SuppressWarnings("unchecked")
	public List<InventoryOrder> getByInventoryId(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryOrder.class);
		criteria.addOrder(Order.desc("id"));
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (inventory_id = '" + id + ")"));
		}
		List<InventoryOrder> list = criteria.list();
		return list;
	}

	public InventoryOrder update(InventoryOrder re) {
		InventoryOrder dto = new InventoryOrder();
		dto = re;
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				Date dt = new Date();
				dto.setUpdatedAt(dt.getTime() / 1000);
				session.update(dto);
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}
	@SuppressWarnings("unchecked")
	public int getCountAllInventory(int page, int pageSize, String columnName, String typeOrder, String keySearch,int idInventory) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryOrder.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if(idInventory>0){
			criteria.add(Restrictions.sqlRestriction(" inventory_id = " + idInventory + "" ));
		}
		List<InventoryOrder> list = criteria.list();
		return list.size();
	}
	@SuppressWarnings("unchecked")
	public List<InventoryOrder> getAllInventoryPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch,int idInventory) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(InventoryOrder.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if(idInventory>0){
			criteria.add(Restrictions.sqlRestriction(" inventory_id = " + idInventory + "" ));
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		
		List<InventoryOrder> list = criteria.list();
		return list;
	}



}
