package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.CategoryAttribute;

@Repository
public class AttributesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Attributes getById(long id) {
		List<Attributes> list = new ArrayList<Attributes>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from attributes where id = ? ").setParameter(0,
					id);
			query.addEntity(Attributes.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public ResponseObject delete(long id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		if (!checkDeleteAttribute(id)) {
			re.setResultCode(205);
			re.setErrorMessage("Attribute đã có chi tiết, không thể xóa");
			;
			return re;
		}
		deleteAttributeCategory(id);
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from attributes where id = ? ").setParameter(0, id).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
		}
		return re;
	}

	public boolean checkDeleteAttribute(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<AttributeValues> list = new ArrayList<>();
		try {
			// session.createSQLQuery("select * from attribute_values where
			// attribute_id = ? ").setParameter(0, id);
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from attribute_values where attribute_id = ? ")
					.setParameter(0, id);
			query.addEntity(AttributeValues.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Attributes> getAlls() {
		List<Attributes> list = new ArrayList<Attributes>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from attributes  where 1 =1 ");
			query.addEntity(Attributes.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject create(Attributes re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Attributes dto = new Attributes();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		List<Attributes> list = new ArrayList<>();

		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				session.persist(dto);
				respon.setObject(dto);
				deleteAttributeCategory(dto.getId());
				if (dto.getSelectListCategory() != null && dto.getSelectListCategory().size() > 0) {
					for (int i = 0; i < dto.getSelectListCategory().size(); i++) {
						addAttributeCategory(dto.getSelectListCategory().get(i), dto.getId());
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}

	public void deleteAttributeCategory(long idCa) {
		long idCas = 0;
		idCas = idCa;
		if (idCas > 0) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.createSQLQuery("DELETE FROM categories_attributes WHERE attribute_id = ? ")
						.setParameter(0, idCas).executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}

	}

	public void addAttributeCategory(long idCa, long id) {
		long ids = 0;
		ids = id;
		long idCas = 0;
		idCas = idCa;

		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("INSERT INTO categories_attributes (category_id,attribute_id) VALUES (?,?) ")
					.setParameter(0, idCas).setParameter(1, ids).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public ResponseObject update(Attributes re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Attributes dto = new Attributes();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				respon.setObject(dto);
				deleteAttributeCategory(dto.getId());
				if (dto.getSelectListCategory() != null && dto.getSelectListCategory().size() > 0) {
					for (int i = 0; i < dto.getSelectListCategory().size(); i++) {
						addAttributeCategory(dto.getSelectListCategory().get(i), dto.getId());
					}
				}
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}

	// chua dung
	@SuppressWarnings("unchecked")
	public List<Attributes> getAllAttributesTest(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Attributes.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (name = '" + keySearch.replace("%", "^") + "' OR name LIKE '%"
					+ keySearch.replace("%", "^") + "%')"));
		}
		List<Attributes> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Attributes> getAllAttributes(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Attributes> list = new ArrayList<Attributes>();
			String sql = "SELECT au.id," + " au.name," + " au.description,"
					+ " (select GROUP_CONCAT(ca.name) from categories ca where ca.id in (select cc.category_id from categories_attributes cc where cc.attribute_id = au.id) ) as nameCategory,"
					+ "au.name_en, au.type " + " FROM attributes au  where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and ( UPPER(au.name) like '%" + keySearch.toUpperCase().trim() + "%' OR "
						+ " UPPER(au.name_en) like '%" + keySearch.toUpperCase().trim() + "%' )";
			}
			sql = sql + " order by au.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Attributes dto;
				list = new ArrayList<Attributes>();
				for (Object[] row : rows) {
					dto = new Attributes();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
					}
					if (row[1] != null && !row[1].equals("")) {
						dto.setName(row[1].toString());
					}

					if (row[2] != null && !row[2].equals("")) {
						dto.setDescription(row[2].toString());
					}
					if (row[3] != null && !row[3].equals("")) {
						dto.setListCategoryName(row[3].toString());
					}
					if (row[4] != null && !row[4].equals("")) {
						dto.setNameEn(row[4].toString());
					}
					if (row[5] != null && !row[5].equals("")) {
						dto.setType(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
					}

					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public int getCountAttributes(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Attributes.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc("id"));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc("id"));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) = '" + keySearch.replace("%", "^")
					+ "' OR UPPER(name_en) LIKE '%" + keySearch.replace("%", "^") + "%')"));
		}
		List<Attributes> list = criteria.list();
		return list.size();
	}

	public Attributes createImport(Attributes re) {
		Attributes dto = new Attributes();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		try {
			if (dto != null && !dto.equals("")) {
				if (dto.getId() > 0) {
					session.update(dto);
					System.out.println("update attribute");
				} else {
					System.out.println("add attribute");
					session.persist(dto);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
		}
		return dto;
	}

	public List<Long> getListCategoryByattribute(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Long> list = new ArrayList<>();
		try {
			String sqls = "select category_id from categories_attributes where attribute_id = ?  ";
			Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
			List<Integer> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				for (Integer in : idMax) {
					idm = in;
					list.add(Long.parseLong(idm + ""));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return list;
	}

	public List<Attributes> getAttributeByCategory(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Attributes> list = new ArrayList<Attributes>();
			String sql = "SELECT au.id," + " au.name," + " au.description," + "au.name_en, au.type, au.orderby "
					+ " FROM attributes au  where 1=1 ";
			if (id > 0) {
				sql = sql + " and  au.id in (SELECT distinct cat.attribute_id FROM categories_attributes cat )";
			} else {
				return null;
			}
			sql = sql + " order by au.orderby asc ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Attributes dto;
				list = new ArrayList<Attributes>();
				for (Object[] row : rows) {
					dto = new Attributes();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
					}
					if (row[1] != null && !row[1].equals("")) {
						dto.setName(row[1].toString());
					}

					if (row[2] != null && !row[2].equals("")) {
						dto.setDescription(row[2].toString());
					}

					if (row[3] != null && !row[3].equals("")) {
						dto.setNameEn(row[3].toString());
					}
					if (row[4] != null && !row[4].equals("")) {
						dto.setType(Integer.parseInt(row[4] != null ? row[4].toString() : "0"));
					}
					if (row[5] != null && !row[5].equals("")) {
						dto.setOrderby(Integer.parseInt(row[5] != null ? row[5].toString() : "0"));
					}

					list.add(dto);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	public Attributes updateOrder(List<Attributes> list) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (list != null && list.size() > 0) {
				for (Attributes pro : list) {
					updateAttributeOrder(pro);
				}
				System.out.println("ok");
				return list.get(0);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	public void updateAttributeOrder(Attributes dto) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Attributes.class);
			criteria.add(Restrictions.sqlRestriction(" id = " + dto.getId() + " "));
			List<Attributes> list = criteria.list();
			if (list != null && list.size() > 0) {
				Attributes re = new Attributes();
				re = list.get(0);
				re.setOrderby(dto.getOrderby());
				session.update(re);
				session.flush();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// FIX ATTRIBUTE PRODUCT
	public boolean fixAttribute() {
		// search category fix
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Categories.class);
			criteria.add(Restrictions.sqlRestriction(" parent_id>0 "));
			List<Categories> listCategoriesRes = criteria.list();
			session.flush();
			List<Categories> listCategories = new ArrayList<Categories>();
			listCategories = listCategoriesRes;
			if (listCategories != null && listCategories.size() > 0) {
				for(int i =0;i<listCategories.size();i++){
					Categories re = new Categories();
					re = listCategories.get(i);
					try {
						searchAttributeByCategory(re);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
	
				}
				
			}
		} catch (Exception ex) {
			// TODO: handle exception
			ex.printStackTrace();
		}
		return true;
	}

	public boolean searchAttributeByCategory(Categories item) {
		// search Attribute By Category
		List<CategoryAttribute> list = new ArrayList<CategoryAttribute>();
		if (item != null && item.getId() > 0) {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				try {
					SQLQuery query = (SQLQuery) session
							.createSQLQuery(
									"SELECT category_id,attribute_id FROM categories_attributes cate where category_id = ? ")
							.setParameter(0, item.getId());
					List<Object[]> listRes = new ArrayList<Object[]>();
					listRes = query.list();
					session.flush();
					List<Object[]> res = new ArrayList<Object[]>();
					res.addAll(listRes);
					if (res != null && res.size() > 0) {
						for (Object o[] : res) {
							if (o != null && o.length >= 2) {
								CategoryAttribute dto = new CategoryAttribute();
								dto.setAttributeId(Integer.parseInt(o[1] + ""));
								dto.setCategoryId(Integer.parseInt(o[0] + ""));
								list.add(dto);
							}

						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			if (list != null && list.size() > 0) {
				for (CategoryAttribute d : list) {
					searchAttributeAndProductValue(item, d);
				}

			}
		}

		return true;
	}

	public boolean searchAttributeAndProductValue(Categories item, CategoryAttribute cta) {
		// search Attribute By Category
		
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(AttributeValues.class);
			criteria.add(Restrictions.sqlRestriction(
					" category_id = " + item.getId() + " and attribute_id = " + cta.getAttributeId() + ""));
			List<AttributeValues> listRes = criteria.list();
			session.flush();
			List<AttributeValues> list = new ArrayList<AttributeValues>();
			list = listRes;
			if (list != null && list.size() > 0) {
				List<AttributeValues> listCheck = new ArrayList<AttributeValues>();
				for(int i =0;i<list.size();i++){
					AttributeValues dto = new AttributeValues ();
					dto = list.get(i);
					if (listCheck == null || listCheck.size() <= 0) {
						listCheck.add(dto);
					} else {
						String idDelete = "";
						boolean checkDelete = false;
						for(int j =0;j<listCheck.size();j++){
							AttributeValues dCheck = new AttributeValues ();
							dCheck = listCheck.get(j);
							if (!checkDelete && dto.getValue() != null && dCheck.getValue() != null
									&& dto.getValue().toLowerCase().equals(dCheck.getValue().toLowerCase())) {
								// Xoa dữ liệu thừa của thằng dto
								try {
									boolean check = fixUpdateAttributeValueById(dto.getId(), dCheck.getId(), dCheck.getAttributeId());
									if(check){
										if(idDelete ==null || "".equals(idDelete)){
											idDelete = dto.getId()+"";
										}else{
											idDelete = idDelete+","+dto.getId();
										}	
									}
									checkDelete = true;
								} catch (Exception ex) {
									ex.printStackTrace();
								}

							} 
						}
						if(!checkDelete) {
							listCheck.add(dto);
						}
						if(idDelete !=null && !"".equals(idDelete)){
							deleteAttributeFix(idDelete);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}
	public boolean fixUpdateAttributeValueById(long attributeValueOld,long attributeValueNew,long attributeID) {
		try {
			if (attributeValueNew > 0 && attributeValueOld >0 && attributeID >0) {
					String sql1 = "UPDATE products_attribute_values set attribute_value_id = "+attributeValueNew+
							" where attribute_value_id = "+attributeValueOld+" and attribute_id = "+attributeID+"";
					Session session = this.sessionFactory.getCurrentSession();
					session.createSQLQuery(sql1).executeUpdate();
					session.flush();
					return true;
				}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}
	public boolean deleteAttributeFix(String listId) {
		try {
			if (!"".equals(listId)) {
					String sql2 = "DELETE FROM  attribute_values where id in (" + listId + ")";
					Session session = this.sessionFactory.getCurrentSession();
					session.createSQLQuery(sql2).executeUpdate();
					session.flush();
					return true;
				}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}

}
