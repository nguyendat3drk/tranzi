package com.tranzi.dao;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.MenuItem;
import com.tranzi.entity.MenuPerItem;
import com.tranzi.model.Menu;

@Repository
public class MenuDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Menu> getMenuCheck(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			List<Menu> roleList = session.createQuery("from  menu where parent_code in ('REALTIME','PERIOD','GRAPHDAS')  and status=1").list();
//			List<Menu> roleList = session.createQuery("from  menu").list();
			List<Menu> list= new ArrayList<Menu>();
			String sql ="select m.id,m.menu_code,m.menu_name,m.status,m.parent_code from  menu m where m.parent_code in ('REALTIME','GRAPHDAS','PERIOD') and m.id in (select  menu_id from menu_permission  where menu_permission.role_id= "+id+" group by menu_id)  and m.status=1";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Menu dto;
				list = new ArrayList<Menu>();
				for (Object[] row : rows) {
					dto = new Menu();
					if(row[0]!=null && !row[0].equals("")){
						dto.setId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null && !row[1].equals("")){
						dto.setMenuCode( row[1].toString());
					}
					
					if(row[2]!=null && !row[2].equals("")){
						dto.setMenuName(row[2].toString());
					}
					if(row[3]!=null && !row[3].equals("")){
						dto.setStatus(Integer.parseInt(row[3].toString()));
					}
					if(row[4]!=null && !row[4].equals("")){
						dto.setParentCode( row[4].toString());
					}
					list.add(dto);
				}
			}
			return list;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}

	public List<Menu> getAllMenuDashboard() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
//			List<Menu> roleList = session.createQuery("from  menu where parent_code in ('REALTIME','PERIOD','GRAPHDAS')  and status=1").list();
//			List<Menu> roleList = session.createQuery("from  menu").list();
			List<Menu> list= new ArrayList<Menu>();
			String sql ="select m.id,m.menu_code,m.menu_name,m.status,m.parent_code from  menu m where m.parent_code in ('REALTIME','PERIOD','GRAPHDAS') and m.status=1";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Menu dto;
				list = new ArrayList<Menu>();
				for (Object[] row : rows) {
					dto = new Menu();
					if(row[0]!=null && !row[0].equals("")){
						dto.setId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null && !row[1].equals("")){
						dto.setMenuCode( row[1].toString());
					}
					
					if(row[2]!=null && !row[2].equals("")){
						dto.setMenuName(row[2].toString());
					}
					if(row[3]!=null && !row[3].equals("")){
						dto.setStatus(Integer.parseInt(row[3].toString()));
					}
					if(row[4]!=null && !row[4].equals("")){
						dto.setParentCode( row[4].toString());
					}
					list.add(dto);
				}
			}
			return list;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	public List<Menu> getAllMenu() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Menu> list= new ArrayList<Menu>();
			String sql ="select m.id,m.menu_code,m.menu_name,m.status,m.parent_code from  menu m where m.status=1";
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Menu dto;
				list = new ArrayList<Menu>();
				for (Object[] row : rows) {
					dto = new Menu();
					if(row[0]!=null && !row[0].equals("")){
						dto.setId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null && !row[1].equals("")){
						dto.setMenuCode( row[1].toString());
					}
					
					if(row[2]!=null && !row[2].equals("")){
						dto.setMenuName(row[2].toString());
					}
					if(row[3]!=null && !row[3].equals("")){
						dto.setStatus(Integer.parseInt(row[3].toString()));
					}
					if(row[4]!=null && !row[4].equals("")){
						dto.setParentCode( row[4].toString());
					}
					list.add(dto);
				}
			}
			return list;			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PageMenu> getPageMenuByRole(int roleId){
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<PageMenu> listResult= new ArrayList<PageMenu>();
			String sql ="select m.* from menu m, menu_permission mp, role r where m.id = mp.menu_id and r.id = mp.role_id and r.delete_state = 0 "
						+ "and m.status = 1 and (m.parent_code is null or m.parent_code in ('CONFIG', 'REPORT_MANAGEMENT')) and r.delete_state =0 AND mp.query_pri = 1 and r.id = ?;";
			SQLQuery query = session.createSQLQuery(sql);
			query.setParameter(0, roleId);
			query.addEntity(Menu.class);
			List<Menu> menues = query.list();
			if(menues.size() >0){
				for (Menu menu : menues) {
					MenuItem menuItem = new MenuItem();
					Data data = new Data();
					PageMenu pageMenu = new PageMenu();
					menuItem.setTitle(menu.getTitle());
					if(menu.getParentCode() != null){
						menuItem.setIsSubMenu(true);
						menuItem.setPathMatch("prefix");
					}else{
						menuItem.setIsSubMenu(false);
						menuItem.setPathMatch("nonprefix");
					}
					menuItem.setIcon(menu.getIcon());
					menuItem.setSelected(menu.getSelected()==1?true:false);
					menuItem.setExpanded(menu.getExpanded()==1?true:false);
					menuItem.setOrder(menu.getOrder());
					data.setMenu(menuItem);
					pageMenu.setPath(menu.getPath());
					pageMenu.setData(data);
					pageMenu.setParentCode(menu.getParentCode()!=null?menu.getParentCode():"");
					listResult.add(pageMenu);
				}
			}
			return listResult;			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//get menu permission item by role id
	@SuppressWarnings("unchecked")
	public List<MenuPerItem> getMenuItemsByRole(int roleId){
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<MenuPerItem> listResult= new ArrayList<MenuPerItem>();
			String sql ="select m.id, m.menu_code, m.menu_name, m.path, m.parent_code, mp.insert_pri, mp.update_pri, mp.delete_pri, mp.query_pri from menu m, menu_permission mp, role r "
					+ "where m.status = 1 and r.id = :roleId  "
					+ " and m.id = mp.menu_id and r.id = mp.role_id and r.delete_state =0";
			SQLQuery query = session.createSQLQuery(sql);
			query.setParameter("roleId", roleId);
			
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				for (Object[] row : rows) {
					MenuPerItem item = new MenuPerItem();
					if(row[0]!=null && !row[0].equals("")){
						item.setId(Integer.parseInt(row[0].toString()));
					}
					
					if(row[1]!=null && !row[1].equals("")){
						item.setMenuCode( row[1].toString());
					}
					
					if(row[2]!=null && !row[2].equals("")){
						item.setMenuName(row[2].toString());
					}
					
					if(row[3]!=null && !row[3].equals("")){
						item.setPath(row[3].toString());
					}
					
					if(row[4]!=null && !row[4].equals("")){
						item.setParentCode( row[4].toString());
					}
					
					if(row[5]!=null && !row[5].equals("")){
						item.setInsertPri( Integer.valueOf(row[5].toString()));
					}
					
					if(row[6]!=null && !row[6].equals("")){
						item.setUpdatePri(Integer.valueOf(row[6].toString()));
					}
					
					if(row[7]!=null && !row[7].equals("")){
						item.setDeletePri(Integer.valueOf(row[7].toString()));
					}
					
					if(row[8]!=null && !row[8].equals("")){
						item.setQueryPri(Integer.valueOf(row[8].toString()));
					}
					
					listResult.add(item);
				}
			}
			
			return listResult;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
	public List<Menu> getMenuParent() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Menu> list= new ArrayList<Menu>();
			String sql ="select m.id,m.menu_code,m.menu_name,m.status,m.parent_code from  menu m where m.status=1 and m.parent_code is null or  m.menu_code in ('REALTIME','PERIOD','GRAPHDAS')";
			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Menu dto;
				list = new ArrayList<Menu>();
				for (Object[] row : rows) {
					dto = new Menu();
					if(row[0]!=null && !row[0].equals("")){
						dto.setId(Integer.parseInt(row[0].toString()));
					}
					if(row[1]!=null && !row[1].equals("")){
						dto.setMenuCode( row[1].toString());
					}
					
					if(row[2]!=null && !row[2].equals("")){
						dto.setMenuName(row[2].toString());
					}
					if(row[3]!=null && !row[3].equals("")){
						dto.setStatus(Integer.parseInt(row[3].toString()));
					}
					if(row[4]!=null && !row[4].equals("")){
						dto.setParentCode( row[4].toString());
					}
					list.add(dto);
				}
			}
			return list;			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}
}
