package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.model.Manufactures;
import com.tranzi.model.ProductsCategories;

@Repository
public class ProductCategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public boolean updateProduct(long id,int type) {
		if(type == 1){
			return true;
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
				session.createSQLQuery("DELETE FROM   products_categories where product_id = ? ")
				.setParameter(0, id).executeUpdate();	
//				session.flush();
				return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	public void addProduct(ProductsCategories product, int type) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if(product!=null){
//				if(updateProduct(product.getProductId(),type)){
					if(product.getCategoryId() != null && product.getCategoryId()>0){
						session.createSQLQuery("INSERT INTO products_categories (product_id,category_id) VALUES (?,?) ")
						.setParameter(0, product.getProductId()).setParameter(1, product.getCategoryId()).executeUpdate();
//						session.flush();
					}
//				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
