package com.tranzi.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Setting;

@Repository
public class SettingDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Setting> getAlls() {
		List<Setting> list = new ArrayList<Setting>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from setting where 1=1 ");
			query.addEntity(Setting.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public Setting getById(int id) {
		List<Setting> list = new ArrayList<Setting>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from setting where id = ? ").setParameter(0,
					id);
			query.addEntity(Setting.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if(list!=null && list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Setting> getAllSettingPager(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			List<Setting> list = new ArrayList<Setting>();
			String sql = "SELECT st.id, st.key_item, st.type, st.value, st.url, st.short_description,st.description, "
					+ " st.updated_at, st.created_at, st.title FROM setting st  where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(st.key) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(st.type) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(st.value) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(st.title) like '%" + keySearch.toUpperCase().trim() + "%') ";
			}
			sql = sql + " order by st.id desc" + " limit " + ((page - 1) * pageSize) + "," + pageSize + " ";

			Query query = session.createSQLQuery(sql);
			List<Object[]> rows = query.list();
			if (rows != null && rows.size() > 0) {
				Setting dto;
				list = new ArrayList<Setting>();
				for (Object[] row : rows) {
					dto = new Setting();
					if (row[0] != null && !row[0].equals("")) {
						dto.setId(Integer.parseInt(row[0] != null ? row[0].toString() : "0"));
						dto.setKey(row[1] != null ? row[1].toString() : "");
						dto.setType(row[2] != null ? row[2].toString() : "");
						dto.setValue(row[3] != null ? row[3].toString() : "");
						dto.setUrl(row[4] != null ? row[4].toString() : "");
						dto.setShortDescription(row[5] != null ? row[5].toString() : "");
						dto.setDescription(row[6] != null ? row[6].toString() : "");
						dto.setUpdatedAt(Integer.parseInt(row[7] != null ? row[7].toString() : "0"));
						dto.setCreatedAt(Integer.parseInt(row[8] != null ? row[8].toString() : "0"));
						dto.setTitle(row[9] != null ? row[9].toString() : "");
					}

					list.add(dto);
				}
			}

			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public int getCountAllSetting(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Object[]> list = new ArrayList<>();

		try {
			String sql = "SELECT * FROM setting st  where 1=1 ";
			if (keySearch != null && !"".equals(keySearch.trim())) {
				sql = sql + " and (UPPER(st.key) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(st.type) like '%" + keySearch.toUpperCase().trim() + "%' "
						+ " or UPPER(st.value) like '%" + keySearch.toUpperCase().trim() + "%'"
						+ " or UPPER(st.title) like '%" + keySearch.toUpperCase().trim() + "%') ";
			}
			Query query = session.createSQLQuery(sql);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (list == null) {
			return 0;
		}
		return list.size();
	}

	public ResponseObject deleteByID(int id) {
		ResponseObject re = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		re.setResultCode(200);
		try {
			session.createSQLQuery("delete from setting where id = ? ").setParameter(0, id).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setErrorMessage(e.getMessage());
			re.setResultCode(201);
		}
		return re;
	}
	public Setting updateSetting(Setting dto) {
		Setting data = new Setting();
		data = dto;
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (data != null && !data.equals("")) {
				Date dt = new Date();
				data.setUpdatedAt(dt.getTime() / 1000);
				session.merge(data);
			}
			return data;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return data;
	}
	@SuppressWarnings("unchecked")
	public Setting createSetting(Setting dto) {
		Session session = this.sessionFactory.getCurrentSession();
		Setting data = new Setting();
		data = dto;
		try {
			if (data != null && !data.equals("")) {
				data.setId(0);
				Date dt = new Date();
				data.setCreatedAt(dt.getTime() / 1000);
				data.setUpdatedAt(dt.getTime() / 1000);
				session.persist(data);
			}
			return data;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return data;
	}

	public Integer getmaxID() {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			String sqls = "select max(id) from setting  where 1 = 1 ";
			Query query3 = session.createSQLQuery(sqls);
			List<Object> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				for (Object in : idMax) {
					if (Integer.parseInt(in + "") > 0) {
						return Integer.parseInt(in + "");
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return -1;
	}
	
	private Date getUTCDateTime() {
		Date mDate = null;
		try {
			String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
			final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			final String utcTime = sdf.format(new Date());
			mDate = dateFormat.parse(utcTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mDate;
	}
}
