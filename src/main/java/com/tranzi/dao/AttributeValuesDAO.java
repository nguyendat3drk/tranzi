package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.PackageTypes;
import com.tranzi.model.ProductsAtributeValues;

@Repository
public class AttributeValuesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public AttributeValues getById(long id) {
		List<AttributeValues> list = new ArrayList<AttributeValues>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from attribute_values where id = ? ").setParameter(0,
					id);
			query.addEntity(AttributeValues.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public ResponseObject delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			if(!checkDeleteAttributeValue(id)){
				re.setResultCode(205);
				re.setErrorMessage("AttributeValue đã liên kêt sản phẩm, không thể xóa");;
				return re;
			}
			session.createSQLQuery("delete from attribute_values where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xoa thành công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}
	public boolean checkDeleteAttributeValue(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			Query query = session.createSQLQuery("select * from products_attribute_values where attribute_value_id = ? ").setParameter(0, id);
			List<Object> list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<AttributeValues> getAlls() {
		List<AttributeValues> list = new ArrayList<AttributeValues>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from attribute_values  where 1 =1 ");
			query.addEntity(AttributeValues.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}


	public ResponseObject create(AttributeValues re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		AttributeValues dto = new AttributeValues();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		List<Attributes> list = new ArrayList<>();
		
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				session.persist(dto);
				respon.setObject(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}


	public ResponseObject update(AttributeValues re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		AttributeValues dto = new AttributeValues();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				respon.setObject(dto);
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}
	@SuppressWarnings("unchecked")
	public List<AttributeValues> getAllAttributes(int page, int pageSize, String columnName, String typeOrder,
			String keySearch ,long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AttributeValues.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (name = '" + keySearch.replace("%", "^") + "' OR name LIKE '%"
					+ keySearch.replace("%", "^") + "%')"));
		}
		if(id>0){
			criteria.add(Restrictions.sqlRestriction(" (attribute_id = " + id + ")"));
		}
		List<AttributeValues> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAttributes(int page, int pageSize, String columnName, String typeOrder, String keySearch, long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(AttributeValues.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (name = '" + keySearch.replace("%", "^") + "' OR name LIKE '%"
					+ keySearch.replace("%", "^") + "%')"));
		}
		if(id>0){
			criteria.add(Restrictions.sqlRestriction(" (attribute_id = " + id + ")"));
		}
		List<AttributeValues> list = criteria.list();
		return list.size();
	}


}
