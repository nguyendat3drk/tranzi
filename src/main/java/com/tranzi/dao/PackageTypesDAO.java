package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PackageTypes;

@Repository
public class PackageTypesDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public PackageTypes getById(int id) {
		List<PackageTypes> list = new ArrayList<PackageTypes>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from package_types where id = ? ").setParameter(0,
					id);
			query.addEntity(PackageTypes.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	public int delete(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from package_types where id = ? ").setParameter(0, id).executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public List<PackageTypes> getAlls() {
		List<PackageTypes> list = new ArrayList<PackageTypes>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from package_types  where 1 =1 ");
			query.addEntity(PackageTypes.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}


	public ResponseObject create(PackageTypes re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		PackageTypes dto = new PackageTypes();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		List<PackageTypes> list = new ArrayList<>();
		
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				session.persist(dto);
				respon.setObject(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}


	public ResponseObject update(PackageTypes re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		PackageTypes dto = new PackageTypes();
		dto = re;
		try {
			Session session = this.sessionFactory.getCurrentSession();
			if (re != null && !re.equals("")) {
				session.update(dto);
				respon.setObject(dto);
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}

}
