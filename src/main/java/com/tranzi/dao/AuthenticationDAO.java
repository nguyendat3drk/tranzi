package com.tranzi.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.encrypt.MD5EnCrypt;
import com.tranzi.entity.AuthenticationUser;
import com.tranzi.entity.LoginView;
import com.tranzi.entity.ResponseObject;
import com.tranzi.jwt.JWTokenUtility;
import com.tranzi.model.Account;
import com.tranzi.model.User;

@Repository
public class AuthenticationDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public ResponseObject checkLogin(LoginView accountLogin) {
		ResponseObject responseObj = new ResponseObject();
		// Session session = this.sessionFactory.getCurrentSession();

		Session session;

		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		try {

			System.out.println(
					"username: " + accountLogin.getAccountName() + ", password: " + accountLogin.getPassword());
			String txtEncryptPass = new MD5EnCrypt().getMD5(accountLogin.getPassword());
			SQLQuery query = session.createSQLQuery(
					"select * from users a where a.username = :accountName and a.password = :password ");
			query.setParameter("accountName", accountLogin.getAccountName()).setParameter("password", txtEncryptPass);
			// SQLQuery query = session.createSQLQuery(
			// "select * from users ");
			query.addEntity(Account.class);
			// System.out.println("pass Encrypt: " + txtEncryptPass);
			List<Account> result = query.list();
			if (result.size() > 0) {

				long nowMillis = System.currentTimeMillis();
				String tokenId = randomId();
				AuthenticationUser authUser = new AuthenticationUser();
				authUser.setId(result.get(0).getId());
				authUser.setUserName(accountLogin.getAccountName());
//				final String varFinal = accountLogin.getAccountName();
				authUser.setAuth(true);
				authUser.setPassword("");
				authUser.setRememberPassword(accountLogin.isRememberPassword());
				authUser.setExternalAccess(false);
				authUser.setUserRole(1);
				authUser.setFirstName(result.get(0).getFirstName());
				authUser.setLastName(result.get(0).getLastName());
				authUser.setEmail(result.get(0).getEmail());
				authUser.setCompany(result.get(0).getCompany());
				authUser.setUserRole(result.get(0).getRolesAdminId());
				authUser.setState("");
				authUser.setToken(new JWTokenUtility().buildJWT(tokenId, "callcenter api", 1,
						accountLogin.getAccountName(), nowMillis, 14400000));
//				setLoginDetail(result.get(0).getId(), authUser.getToken());
				try {
					SQLQuery query2 = session.createSQLQuery(
							"UPDATE users SET users.active = 1, users.currentSession =:currentSession WHERE users.id = :accountId ");
					query2.setParameter("accountId", result.get(0).getId());
					query2.setParameter("currentSession", authUser.getToken());
					query2.executeUpdate();
				} catch (HibernateException e) {
					e.printStackTrace();
				}
				// System.out.println("my token: " + authUser.getToken());
				responseObj.setObject(authUser);
				responseObj.setResultCode(200);
				responseObj.setErrorMessage(null);

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return responseObj;
	}

	private Date getUTCDateTime() {
		Date mDate = null;
		try {
			String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
			final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			final String utcTime = sdf.format(new Date());
			mDate = dateFormat.parse(utcTime);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mDate;
	}

	private void setLoginDetail(int accountId, String currentSession) {

		try {
			Session session = sessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(
					"UPDATE user SET users.active = 1, users.currentSession =:currentSession WHERE users.id = :accountId ");
			query.setParameter("accountId", accountId);
			query.setParameter("currentSession", currentSession);
			query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

	}

	private boolean isAnOtherAgentOnCall(int accountId, int extension) {
		Session session = this.sessionFactory.getCurrentSession();
		String strQuery = "SELECT cd.* FROM call_in_queue ciq, call_details cd WHERE ciq.call_id = cd.id AND cd.status =1 AND ciq.agent_id IN "
				+ "(SELECT u.id FROM user u WHERE u.status =1 AND u.delete_state = 0 AND u.role=268 AND u.id !=:accountId AND u.state != 'INVISIBLE' AND u.extension =:extension)";
		SQLQuery sqlQuery = session.createSQLQuery(strQuery);
		sqlQuery.setParameter("accountId", accountId);
		sqlQuery.setParameter("extension", extension);
		if (sqlQuery.list().size() > 0) {
			return true;
		}
		return false;
	}

	public ResponseObject updateLogoutDetail(String userLogin) {
		Session session = this.sessionFactory.openSession();
		ResponseObject responseObj = new ResponseObject();
		SQLQuery query = session.createSQLQuery(
				"UPDATE account_logged_details ald INNER JOIN user u ON u.id = ald.account_id SET ald.log_out_time = :logoutTime WHERE u.user_name = :userLogin AND ald.log_out_time IS NULL");
		query.setParameter("userLogin", userLogin).setParameter("logoutTime", getUTCDateTime());
		query.executeUpdate();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		responseObj.setObject(null);
		responseObj.setResultCode(200);
		responseObj.setErrorMessage(null);
		session.close();
		return responseObj;
	}

	private String randomId() {
		String SALTCHARS = "ABCDEFGHI";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	public boolean checkUserSession(String userName, String currentSession) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject responseObj = new ResponseObject();
		try {
			SQLQuery query = session.createSQLQuery(
					"select username from users where username =:userName and currentsession=:currentSession and active = 1");
			query.setParameter("userName", userName);
			query.setParameter("currentSession", currentSession);
			if (query.list().size() > 0) {
				return true;
			}	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
	}

	public int getRoleByUsername(String userName) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery("SELECT * FROM users where username =:userName AND active = 0");
		query.setParameter("userName", userName);
		query.addEntity(Account.class);
		List<Account> result = query.list();
		if (result.size() > 0) {
			return result.get(0).getId();
		}
		return -1;
	}
	public static void main(String[] args) {
		System.out.println(new MD5EnCrypt().getMD5("admin"));
	}
}
