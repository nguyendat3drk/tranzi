package com.tranzi.dao;

import com.tranzi.entity.MenuItem;

public class Data {
	private MenuItem menu;

	public MenuItem getMenu() {
		return menu;
	}

	public void setMenu(MenuItem menu) {
		this.menu = menu;
	}
}
