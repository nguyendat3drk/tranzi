package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.CustomerOrder;
import com.tranzi.model.CustomerOrderProduct;

@Repository
public class CustomerOrderDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public CustomerOrder getById(long id) {
		List<CustomerOrder> list = new ArrayList<CustomerOrder>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from customer_order where id = ? ")
					.setParameter(0, id);
			query.addEntity(CustomerOrder.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public ResponseObject delete(long id) {
		ResponseObject re = new ResponseObject();
		re.setResultCode(200);
		// TO-DO
		if (!checkDeleteCustomerOrder(id)) {
			re.setResultCode(205);
			re.setErrorMessage("Đơn hàng đã xử lý, không thể xóa");
			return re;
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from customer_order where id = ? ").setParameter(0, id).executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
		}
		return re;
	}

	public boolean checkDeleteCustomerOrder(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<CustomerOrder> list = new ArrayList<CustomerOrder>();
		try {
			// session.createSQLQuery("select * from Customer_values where
			// Customer_id = ? ").setParameter(0, id);
			SQLQuery query = (SQLQuery) session
					.createSQLQuery("select * from customer_order where id = ?  and (status = 1 or status = 2)")
					.setParameter(0, id);
			query.addEntity(CustomerOrder.class);
			list = query.list();
			if (list != null && list.size() > 0) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<CustomerOrder> getAlls() {
		List<CustomerOrder> list = new ArrayList<CustomerOrder>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from customer_order  where 1 =1 ");
			query.addEntity(CustomerOrder.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject update(CustomerOrder re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		CustomerOrder dto = new CustomerOrder();
		dto = re;
		// String pass = "";
		// try {
		// pass = checkUpdatePass(account);
		// } catch (Exception e) {
		// // TODO: handle exception
		// }
		if (isAccountOrDerIDCode(dto)) {
			respon.setResultCode(201);
			respon.setObject(dto);
			respon.setErrorMessage(
					"The Order code  " + dto.getOrderCode() + " existed. Please choose another Order Code");
		} else {

			try {
				if (dto != null && !dto.equals("")) {
					Session session = this.sessionFactory.getCurrentSession();
					Date dt = new Date();
					dto.setUpdatedAt(Integer.parseInt((dt.getTime() / 1000) + ""));
					session.update(dto);
					session.flush();
					respon.setObject(dto);
					respon.setResultCode(200);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				respon.setResultCode(204);
				respon.setObject(e.getMessage());
				return respon;
			}
		}
		// add product order customer
		try {
			deleteProductCustomerOrder(dto.getListProductOrderDelete()); 
			if (dto.getId() > 0 && dto.getListProductOrder() != null && dto.getListProductOrder().size() > 0) {
					addProductCustomerOrder(dto.getListProductOrder(), Integer.parseInt(dto.getId() + ""));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return respon;
	}

	public ResponseObject create(CustomerOrder re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		CustomerOrder dto = new CustomerOrder();

		dto = re;
		if (isAccountOrDerIDCode(dto)) {
			respon.setResultCode(201);
			respon.setObject(dto);
			respon.setErrorMessage(
					"The Order code  " + dto.getOrderCode() + " existed. Please choose another Order Code");
			return respon;
		} else {
			try {
				if (dto != null && !dto.equals("")) {
					Session session = this.sessionFactory.getCurrentSession();
					dto.setId(0);
					Date dt = new Date();
					dto.setCreatedAt(Integer.parseInt((dt.getTime() / 1000) + ""));
					session.persist(dto);
					session.flush();
					respon.setObject(dto);
					respon.setResultCode(200);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				e.printStackTrace();
				respon.setResultCode(204);
				respon.setErrorMessage("có loi xay ra");
				return respon;
			}
		}
		// update product order customer
		try {
			if (dto.getId() > 0 && dto.getListProductOrder() != null && dto.getListProductOrder().size() > 0) {

				addProductCustomerOrder(dto.getListProductOrder(), Integer.parseInt(dto.getId() + ""));
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return respon;
	}

	public boolean deleteProductCustomerOrder(List<CustomerOrderProduct> dto) {
		List<CustomerOrderProduct> list = new ArrayList<>();
		list = dto;
		String id = null;
		if(list!=null && list.size()>0){
			for(CustomerOrderProduct d:list){
				if(id==null){
					id = d.getId()+"";
				}else{
					id = id +","+ d.getId()+"";
				}
			}
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (id!=null && !"".equals(id) ) {
				session.createSQLQuery("delete from customer_order_product where id in (?) ")
						.setParameter(0, id).executeUpdate();
				session.flush();
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public void addProductCustomerOrder(List<CustomerOrderProduct> dto, Integer id) {
		List<CustomerOrderProduct> list = new ArrayList<CustomerOrderProduct>();
		list = dto;
		Session session = this.sessionFactory.getCurrentSession();
		CustomerOrderProduct dSave = new CustomerOrderProduct();
		Date date = new Date();
		for (CustomerOrderProduct d : list) {
			try {
				dSave = new CustomerOrderProduct();
				dSave = d;
				dSave.setCustomerOrderId(id);
				dSave.setUpdatedAt(Integer.parseInt((date.getTime() / 1000) + ""));
				if(dSave.getId()>0){
					dSave.setCreatedAt(Integer.parseInt((date.getTime() / 1000) + ""));
					session.update(dSave);	
				}else{
					session.persist(dSave);
				}
				
				session.flush();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	@SuppressWarnings("unchecked")
	public List<CustomerOrder> getAllCustomer(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerOrder.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (order_code LIKE '%" + keySearch.replace("%", "^")
					+ "' OR shipping_email LIKE '%" + keySearch.replace("%", "^") + "%')"));
		}
		List<CustomerOrder> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountCustomer(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerOrder.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (order_code LIKE '%" + keySearch.replace("%", "^")
					+ "' OR shipping_email LIKE '%" + keySearch.replace("%", "^") + "%')"));
		}
		List<CustomerOrder> list = criteria.list();
		return list.size();
	}

	private boolean isAccountOrDerIDCode(CustomerOrder customer) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (customer != null && customer.getId() > 0) {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(CustomerOrder.class);
				if (customer != null && customer.getOrderCode() != null) {
					criteria.add(Restrictions.sqlRestriction(
							" (UPPER(order_code) = '" + customer.getOrderCode().toUpperCase().trim() + "')"));
				}
				criteria.add(Restrictions.sqlRestriction(" (id != " + customer.getId() + ")"));
				List<CustomerOrder> list = criteria.list();
				// session.flush();
				if (list.size() > 0)
					return true;

			} else {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(CustomerOrder.class);
				if (customer != null && customer.getOrderCode() != null) {
					criteria.add(Restrictions.sqlRestriction(
							" (UPPER(order_code) = '" + customer.getOrderCode().toUpperCase().trim() + "')"));
				}
				List<CustomerOrder> list = criteria.list();
				// session.flush();
				if (list.size() > 0)
					return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;
	}

}
