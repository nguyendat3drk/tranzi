package com.tranzi.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Media;
import com.tranzi.model.ProductFile;
import com.tranzi.model.ProductMedia;

@Repository
public class MediaDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Media getById(int id) {
		List<Media> list = new ArrayList<Media>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from media where id = ? ").setParameter(0, id);
			query.addEntity(Media.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list.get(0);
	}

	// get list product-file theo product id
	public List<Media> getByProductId(int id) {
		List<Media> list = new ArrayList<Media>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery(
							"select * from media where id in (select media_id from  products_files where product_id = ? ) ")
					.setParameter(0, id);
			query.addEntity(Media.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	// get list product-media theo product id
	public List<Media> getMediaByProductIDMedia(int id) {
		List<Media> list = new ArrayList<Media>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session
					.createSQLQuery(
							"select * from media where id in (select media_id from  products_media where product_id = ? ) ")
					.setParameter(0, id);
			query.addEntity(Media.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public int delete(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from media where id = ? ").setParameter(0, id).executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public int deleteProductMedia(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from products_media where product_id = ? ").setParameter(0, id)
					.executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public int deleteProductMediaFile(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("delete from products_files where product_id = ? ").setParameter(0, id)
					.executeUpdate();
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	public List<Media> getAlls() {
		List<Media> list = new ArrayList<Media>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from media  where 1 =1 ");
			query.addEntity(Media.class);
			list = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public ResponseObject create(Media re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Media dto = new Media();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		
		try {
			if (dto != null && !dto.equals("")) {
				dto.setId(0);
				Date dt = new Date();
				dto.setCreatedTime(dt.getTime() / 1000);
				session.persist(dto);
				respon.setObject(dto);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
		return respon;
	}

	// LUU PRODUCT MEDIA
	public ResponseObject createListMediaAndelete(List<Media> list, long id) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Media dto = new Media();

		// xoa media theo meidia lien ket product
		// TO-DO
		int check = deleteProductMedia(id);
		// add lại media theo product id
		Session session = this.sessionFactory.getCurrentSession();
		if (check == 200 && list != null && list.size() > 0) {
			for (Media m : list) {
				dto = m;
				try {
					if (dto != null && !dto.equals("")) {
						dto.setId(0);
						Date dt = new Date();
						dto.setCreatedTime(dt.getTime() / 1000);
						session.persist(dto);
						if (id > 0 && dto.getId() > 0) {
							// add lien ket meida voi product
							addMeidaProduct(id, dto.getId());
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					e.printStackTrace();
					respon.setResultCode(204);
					respon.setErrorMessage("có loi xay ra");
				}
			}
		}

		return respon;
	}

	// LUU PRODUCT FILE
	public ResponseObject createListMediaAndeleteFile(List<Media> list, long id) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Media dto = new Media();

		// xoa media theo meidia lien ket product
		int check = deleteProductMediaFile(id);
		// TO-DO
		Session session = this.sessionFactory.getCurrentSession();
		// add lại media theo product id
		if (check == 200 && list != null && list.size() > 0) {
			for (Media m : list) {
				dto = m;
				try {
					if (dto != null && !dto.equals("")) {
						dto.setId(0);
						Date dt = new Date();
						dto.setCreatedTime(dt.getTime() / 1000);
						session.persist(dto);
						if (id > 0 && dto.getId() > 0) {
							addMeidaProductFile(id, dto);
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					e.printStackTrace();
					respon.setResultCode(204);
					respon.setErrorMessage("có loi xay ra");
				}
			}
		}

		return respon;
	}

	public void addMeidaProduct(long id, int mediaId) {
//		Session session = this.sessionFactory.getCurrentSession();
//		try {
//			session.createSQLQuery("INSERT INTO products_media (media_id,product_id) VALUES (?,?) ")
//					.setParameter(0, mediaId).setParameter(1, id).executeUpdate();
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
		threadEvent th = new threadEvent();
		th.run(id, mediaId);
	}

	class threadEvent extends Thread {
		public void run(long id, int mediaId) {
//			System.out.println(" ---Run Thread---");
			 try {
				Thread.sleep(2000);
				evenAddMediaProduct(id, mediaId);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			System.out.println("--- End Run Thread---");
		}

	}
	public void evenAddMediaProduct(long id, int mediaId){
		Session session = this.sessionFactory.getCurrentSession();
		try {
			ProductMedia pm = new ProductMedia();
			pm.setId(0);
			pm.setMediaId(mediaId);
			pm.setProductId(id);
			session.persist(pm);
//			session.createSQLQuery("INSERT INTO products_media (media_id,product_id) VALUES (?,?) ")
//					.setParameter(0, mediaId).setParameter(1, id).executeUpdate();fasdfasd
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	

	public void addMeidaProductFile(long id, Media media) {
		Session session = this.sessionFactory.getCurrentSession();
		if (media != null && media.getId() > 0 && id > 0) {
			String nameFile = "NotName";
			try {
				String[] list = media.getUrlPath().split("/");
				if (list != null && list.length > 0) {
					nameFile = list[list.length - 1];
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				ProductFile pr = new ProductFile();
				pr.setId(0);
				pr.setMediaId(media.getId());
				pr.setProductId(id);
				pr.setName(nameFile);
				session.persist(pr);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public ResponseObject update(Media re) {
		ResponseObject respon = new ResponseObject();
		respon.setResultCode(200);
		Media dto = new Media();
		dto = re;
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				session.update(dto);
				respon.setObject(dto);
			}
			return respon;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			respon.setResultCode(204);
			respon.setErrorMessage("có loi xay ra");
			return respon;
		}
	}

}
