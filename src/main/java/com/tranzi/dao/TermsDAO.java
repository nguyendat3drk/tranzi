package com.tranzi.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tranzi.entity.ResponseObject;
import com.tranzi.model.CategoriesTerms;
import com.tranzi.model.ProductsCategories;
import com.tranzi.model.Terms;

@Repository
public class TermsDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public Terms getById(int id) {
		List<Terms> listTerms = new ArrayList<Terms>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = (SQLQuery) session.createSQLQuery("select * from terms where id = ? ").setParameter(0, id);
			query.addEntity(Terms.class);
			listTerms = query.list();
			if (listTerms != null && listTerms.size() > 0) {
				listTerms.get(0).setListCategory(getListCategoryByTermId(id));
				return listTerms.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	public List<Long> getListCategoryByTermId(Integer id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Long> list = new ArrayList<>();
		try {
			String sqls = "select category_id from categories_terms where term_id = ? ";
			Query query3 = session.createSQLQuery(sqls).setParameter(0, id);
			List<Integer> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				Integer idm;
				for (Integer in : idMax) {
					idm = in;
					list.add(Long.parseLong(idm + ""));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return list;
	}

	public ResponseObject deleteTerm(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			 if (!checkDeleteCategori(id)) {
			 re.setResultCode(205);
			 re.setErrorMessage("Thể loại đã liên kết Sản phẩn. không thể xóa");
			 }else{
			session.createSQLQuery("delete from terms where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành công");
			 }
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public boolean checkDeleteCategori(long id) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (id > 0) {
				Criteria criteria = session.createCriteria(Terms.class);
				criteria.add(Restrictions.sqlRestriction(" parent_id = " + id + " or count_article > 0  "));
				List<Terms> list = criteria.list();
				if (list != null && list.size() > 0) {
					return false;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return true;
	}

	public List<Terms> getAlls() {
		List<Terms> listTerms = new ArrayList<Terms>();
		Session session = this.sessionFactory.getCurrentSession();
		try {
			SQLQuery query = session.createSQLQuery("select * from terms  where 1 =1 order by id Desc ");
			query.addEntity(Terms.class);
			listTerms = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listTerms;
	}

	public int getIdmax() {
		Session session = this.sessionFactory.getCurrentSession();

		int id = 1;
		try {
			String sqls = "select max(id)+1 from terms where 1=1";
			Query query3 = session.createSQLQuery(sqls);
			List<BigInteger> idMax = query3.list();
			if (idMax != null && idMax.size() > 0) {
				BigInteger idm;
				idm = idMax.get(0);
				if (idm == null || idm.equals("")) {
					id = 1;
				} else {
					id = Integer.parseInt(idm.toString());
				}

			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public Terms create(Terms re) {
		Terms dto = new Terms();
		Session session = this.sessionFactory.getCurrentSession();
		dto = re;
		dto.setCountArticle(0);
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}

		List<Terms> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), 0);
		} catch (Exception e) {
			// TODO: handle exception
		}

		Date dt = new Date();
		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug().replace("--", "-") + dt.getSeconds());
		}
		// int id = getIdmax();
		try {
			if (dto != null && !dto.equals("")) {
				session.persist(dto);
			}

			// add ProductsCategories
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					if (updateCateTerm(dto.getId(), 0)) {
						for (long cate : dto.getListCategory()) {
							CategoriesTerms po = new CategoriesTerms();
							po.setId(0);
							po.setTermsId(Integer.parseInt(dto.getId() + ""));
							po.setCategoryId(Integer.parseInt(cate + ""));
							addTermSCatetory(po, 1);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dto;
	}

	public List<Terms> checkSlug(String slug, long id) {

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Terms.class);

		if (slug != null && !slug.equals("") && id <= 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) like '" + slug.toUpperCase().trim() + "%') "));
		} else if (slug != null && !slug.equals("") && id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(slug) = '" + slug.toUpperCase().trim() + "%') "));
		}
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
		}
		List<Terms> list = criteria.list();
		return list;
	}
	public void updateCountTerm(long id,Integer parentIdNew, int count) {

		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Terms.class);
		if (id > 0) {
			criteria.add(Restrictions.sqlRestriction(" (id != '" + id + "') "));
			List<Terms> list = criteria.list();
			if(list!=null && list.size()>0){
				Terms te = new Terms();
				te = list.get(0);
				//
				if(te.getParentId()>0 && te.getCountArticle()>0){
					session.createSQLQuery("Update terms set count_article = (count_article - "+te.getCountArticle()+") where id = ? ").setParameter(0, te.getParentId());
					session.createSQLQuery("Update terms set count_article = (count_article + "+count+") where id = ? ").setParameter(0, parentIdNew);
				}
			}
			
		}
		
		
	}

	@SuppressWarnings("unchecked")
	public List<Terms> getAllTermsPager(int page, int pageSize, String columnName, String typeOrder, String keySearch, int level) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Terms.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
//		pageSize = 1000 la fix ra 1 page
		if (page >= 0 && pageSize >= 0 && pageSize < 1000) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (level == 0) {
			criteria.add(Restrictions.sqlRestriction(" (parent_id = NULL or parent_id is null or parent_id = 0) "));
		}else if(level == -1){
			criteria.add(Restrictions.sqlRestriction(" (parent_id >0) "));
		}
		else{
			criteria.add(Restrictions.sqlRestriction(" (parent_id = "+level+") "));
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		criteria.addOrder(Order.desc("id"));

		List<Terms> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllTerms(int page, int pageSize, String columnName, String typeOrder, String keySearch, int level) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Terms.class);
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + " UPPER(description) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' or " + "UPPER(slug) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		if (level == 0) {
			criteria.add(Restrictions.sqlRestriction(" (parent_id = NULL or parent_id is null  or parent_id = 0) "));
		}else if(level == -1){
			criteria.add(Restrictions.sqlRestriction(" (parent_id >0) "));
		}
		else{
			criteria.add(Restrictions.sqlRestriction(" (parent_id = "+level+") "));
		}
		List<Terms> list = criteria.list();
		return list.size();
	}

	public Terms update(Terms re) {
		Terms dto = new Terms();
		dto = re;
		if (re.getName() != null && re.getName().length() >= 254) {
			re.setName(re.getName().substring(0, 253));
		}
		List<Terms> list = new ArrayList<>();
		try {
			list = checkSlug(dto.getSlug(), dto.getId());
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (list != null && list.size() > 0) {
			dto.setSlug(dto.getSlug() + "(" + (list.size()) + ")");
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {

			if (re != null && !re.equals("")) {
				try {
					this.updateCountTerm(dto.getId(),dto.getParentId(),dto.getCountArticle());	
				} catch (Exception e) {
					// TODO: handle exception
				}
				session.update(dto);
			}
			try {
				if (dto.getListCategory() != null && dto.getListCategory().size() > 0) {
					if (updateCateTerm(dto.getId(), 0)) {
						for (long cate : dto.getListCategory()) {
							CategoriesTerms po = new CategoriesTerms();
							po.setId(0);
							po.setTermsId(Integer.parseInt(dto.getId() + ""));
							po.setCategoryId(Integer.parseInt(cate + ""));
							addTermSCatetory(po, 1);
						}
					}
				}

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		

		return dto;
	}

	public boolean updateCateTerm(long id, int type) {
		if (type == 1) {
			return true;
		}
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.createSQLQuery("DELETE FROM   categories_terms where term_id = ? ").setParameter(0, id)
					.executeUpdate();
			session.flush();
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	public void addTermSCatetory(CategoriesTerms caTerm, int type) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (caTerm != null) {
				caTerm.setId(0);
				session.persist(caTerm);
				session.flush();

				// }
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	public ResponseObject updateCountTerm(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			session.createSQLQuery("update terms set count_article = (SELECT count(*) FROM articles_terms WHERE term_id = ?) where id = ? ").setParameter(0, id).setParameter(1, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("update Thành công");
			// }
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}
}
