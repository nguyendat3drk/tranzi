package com.tranzi.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.NullPrecedence;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.tranzi.common.CommonConfig;
import com.tranzi.common.ResetMail;
import com.tranzi.encrypt.MD5EnCrypt;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Account;
import com.tranzi.model.Categories;
import com.tranzi.model.GroupACD;
import com.tranzi.model.Role;
import com.tranzi.model.User;

@Repository
public class AccountDAO {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AuthenticationDAO authenDao;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@SuppressWarnings("unchecked")
	public List<Account> getPageAccount(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Account.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (page >= 0 && pageSize >= 0) {
			criteria.setFirstResult((page - 1) * pageSize);
			criteria.setMaxResults(pageSize);
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(username) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "' OR UPPER(email) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%'  OR UPPER(first_name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%'  OR UPPER(phone) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' OR UPPER(last_name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Account> list = criteria.list();
		return list;
	}

	@SuppressWarnings("unchecked")
	public int getCountAllAccount(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Account.class);
		if (typeOrder != null && !typeOrder.equals("")) {
			if (typeOrder.equals("desc")) {
				criteria.addOrder(Order.desc(columnName));
			} else if (typeOrder.equals("asc")) {
				criteria.addOrder(Order.asc(columnName));
			}
		}
		if (keySearch != null && !keySearch.equals("")) {
			criteria.add(Restrictions.sqlRestriction(" (UPPER(username) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "' OR UPPER(email) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%'  OR UPPER(first_name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%'  OR UPPER(phone) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%' OR UPPER(last_name) LIKE '%"
					+ keySearch.replace("%", "^").toUpperCase().trim() + "%')"));
		}
		List<Account> list = criteria.list();
		return list.size();
	}

	private boolean isAccountExists(User account) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session
				.createSQLQuery("select (1) from users a where a.user_name = :accountName and a.id <> :accountId ")
				.setParameter("accountName", account.getAccountName()).setParameter("accountId", account.getId());
		List result = query.list();
		if (result.size() > 0)
			return true;
		return false;
	}

	private boolean isAccountEmailExists(Account account) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (account != null && account.getId() > 0) {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Account.class);
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(email) = '" + account.getEmail().toUpperCase() + "')"));
				criteria.add(Restrictions.sqlRestriction(" (id != " + account.getId() + ")"));
				List<Account> list = criteria.list();
				session.flush();
				session.clear();
				if (list.size() > 0)
					return true;

			} else {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Account.class);
				criteria.add(
						Restrictions.sqlRestriction(" (UPPER(email) = '" + account.getEmail().toUpperCase() + "')"));
				List<Account> list = criteria.list();
				session.flush();
				session.clear();
				if (list.size() > 0)
					return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;
	}

	private boolean isAccountNameExists(Account account) {
		// Session session = this.sessionFactory.getCurrentSession();
		try {
			if (account != null && account.getId() > 0) {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Account.class);
				criteria.add(Restrictions
						.sqlRestriction(" (UPPER(username) = '" + account.getUsername().toUpperCase() + "')"));
				criteria.add(Restrictions.sqlRestriction(" (id != " + account.getId() + ")"));
				List<Account> list = criteria.list();
				if (list.size() > 0)
					return true;
			} else {
				Session session = this.sessionFactory.getCurrentSession();
				Criteria criteria = session.createCriteria(Account.class);
				criteria.add(Restrictions
						.sqlRestriction(" (UPPER(username) = '" + account.getUsername().toUpperCase() + "')"));
				List<Account> list = criteria.list();
				if (list.size() > 0)
					return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return false;
	}

	public List<Account> getAllAccount() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Account> accountList = session.createSQLQuery("select * from Users where active = 1").list();
		return accountList;
	}

	public ResponseObject deleteId(int id) {

		ResponseObject re = new ResponseObject();
		deleteUserRolebyId(id);
		try {
			// if (!checkDeleteCategori(id)) {
			// re.setResultCode(205);
			// re.setErrorMessage("Thể loại đã liên kết Sản phẩn. không thể
			// xóa");
			// }
			Session session = this.sessionFactory.getCurrentSession();
			session.createSQLQuery("delete from users where id = ? ").setParameter(0, id).executeUpdate();
			re.setResultCode(200);
			re.setObject("Xóa Thành công");
			return re;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			re.setResultCode(204);
			re.setErrorMessage(e.getMessage());
			return re;
		}
	}

	public void deleteUserRolebyId(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject re = new ResponseObject();
		try {
			session.createSQLQuery("delete from users_roles where user_id = ? ").setParameter(0, id).executeUpdate();
			session.flush();
			session.clear();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public ResponseObject getAccountById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject responseObject = new ResponseObject();
		User account = (User) session.load(User.class, new Integer(id));
		if (account != null && account.getId() > 0) {
			responseObject.setResultCode(200);
			responseObject.setObject(account);
		} else {
			responseObject.setResultCode(203);
			responseObject.setObject(null);
		}
		return responseObject;
	}

	public ResponseObject addAccount(Account account) {
		if (account == null) {
			String pass = new MD5EnCrypt().getMD5("admin");
			return null;
		} else {
			Session session = this.sessionFactory.getCurrentSession();
			ResponseObject responseObject = new ResponseObject();
			try {
				if (isAccountEmailExists(account)) {
					responseObject.setResultCode(201);
					responseObject.setObject(account);
					responseObject.setErrorMessage(
							"The email " + account.getEmail() + " existed. Please choose another email");
				} else if (isAccountNameExists(account)) {
					responseObject.setResultCode(202);
					responseObject.setObject(account);
					responseObject.setErrorMessage(
							"The UserName " + account.getEmail() + " existed. Please choose another UserName");
				} else {
					String txtPass = account.getPassword();
					account.setPassword(new MD5EnCrypt().getMD5(account.getPassword()));
					Date dt = new Date();
					account.setCreatedOn(dt.getTime() / 1000);
					session.persist(account);
					responseObject.setResultCode(200);
					responseObject.setObject(account);
					responseObject.setErrorMessage("User " + account.getEmail() + " is created successfully");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				responseObject.setResultCode(204);
				responseObject.setObject(e.getMessage());
			}

			return responseObject;
		}

	}

	public String checkUpdatePass(Account acc) {

		String pass = "";
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Account.class);
			criteria.add(Restrictions.sqlRestriction(" (password = '" + acc.getPassword().replace("%", "^") + "')"));
			criteria.add(Restrictions.sqlRestriction(" (id = " + acc.getId() + ")"));
			List<Account> list = criteria.list();
			if (list != null && list.size() > 0) {
				pass = list.get(0).getPassword();
			} else {
				pass = new MD5EnCrypt().getMD5(acc.getPassword());
			}
			session.flush();
			session.clear();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return pass;
	}

	public ResponseObject updateAccount(Account accountRequest) {

		ResponseObject responseObject = new ResponseObject();
		// String isCheck = validateDelte(account.getId() + "");
		Account account = new Account();
		account = accountRequest;
		String pass = "";
		try {
			pass = checkUpdatePass(account);
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (isAccountEmailExists(account)) {
			responseObject.setResultCode(201);
			responseObject.setObject(account);
			responseObject.setErrorMessage("The email " + account.getEmail() + " existed. Please choose another email");
		} else if (isAccountNameExists(account)) {
			responseObject.setResultCode(202);
			responseObject.setObject(account);
			responseObject
					.setErrorMessage("The UserName " + account.getEmail() + " existed. Please choose another UserName");
		} else {
			try {
				Session session = this.sessionFactory.getCurrentSession();
				account.setPassword(pass);
				account.setCurrentSession("");
				session.update(account);
				responseObject.setObject(account);
				responseObject.setResultCode(200);
				// responseObject.setObject(putAccount(account));

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				responseObject.setResultCode(204);
				responseObject.setObject(e.getMessage());
			}
		}
		return responseObject;
	}

	public Account putAccount(Account acc) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			if (acc != null) {
				session.update(acc);
				return acc;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	private String randomMyPassword() {
		String SALTCHARS = "ABCDEFGHI012345";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;
	}

	public void resetMailActive(String email, String title, String userName2, String fullName2, String pNew) {

		ResetMail re = new ResetMail();
		String strContent = "";
		// User user = checkEmailExists(email);
		if (userName2 == null || userName2.equals("")) {
			return;
		} else {
			strContent = "Dear " + fullName2 + ", ";
			new CommonConfig().loadConfig();
			strContent = strContent
					+ "<br/>  <br/> Your account has been changed to active. Please check your account’s information in Contact Center system below:<br/> &nbsp&nbsp&nbsp&nbsp <b> Username:</b> "
					+ userName2 + " <br/> &nbsp&nbsp&nbsp&nbsp <b> Password:</b>  " + pNew + " <br/> " + "Please click "
					+ CommonConfig.linkResetPass
					+ " to login and Change Password. <br/>  <br/> Thanks and Best Regards!";

		}
		re.sendMail(email, title, strContent);
	}

	public List<User> getAllUserRole(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		SQLQuery query = session.createSQLQuery(
				"select * from user where status =1 and role in (select id from role where role.type = :id ) and delete_state =0");

		query.addEntity(User.class);
		query.setParameter("id", id);
		List<User> listUsser = new ArrayList<User>();
		listUsser = getAllUserRolename(query.list());
		return listUsser;
	}

	public List<User> getAllUserRolename(List<User> li) {
		List<User> list = new ArrayList<User>();
		list = li;
		Session session = this.sessionFactory.getCurrentSession();

		for (User dto : list) {
			List<Role> roleList = session.createQuery("from Role where delete_state =0 and  id = " + dto.getRole() + "")
					.list();
			Role dt = new Role();
			if (roleList != null && roleList.size() > 0) {
				dt = roleList.get(0);
			}
			if (dt != null) {
				dto.setRoleName(dt.getName());
			} else {
				dto.setRoleName("");
			}

		}
		return list;
	}

	public int resetMail(final String email, final String title) {
		final ResetMail re = new ResetMail();
		new CommonConfig().loadConfig();
		String strContent = "";
		final String mContent;
		User user = checkEmailExists(email);
		if (user == null || user.getAccountName().equals("")) {
			return 0;
		} else {
			strContent = "Dear " + user.getFullName() + ", ";
			String passnew = randomPass(user.getAccountName());
			strContent = strContent + "<br/><br/> Your password has been reset.<b> New Password </b>is " + passnew
					+ " <br/> " + "Please click " + CommonConfig.linkResetPass
					+ " to login and Change Password. <br/><br/> Thanks and Best Regards!";
			mContent = strContent;
			new Thread(new Runnable() {
				@Override
				public void run() {
					re.sendMail(email, title, mContent);
				}
			}).start();
		}
		return 1;
	}

	private User checkEmailExists(String email) {
		Session session = this.sessionFactory.openSession();
		SQLQuery query = session
				.createSQLQuery("select * from user a where delete_state =0 and a.email = :accountEmail");
		query.setParameter("accountEmail", email);
		query.addEntity(User.class);
		List<User> result = query.list();
		if (result.size() > 0 && result.size() == 1) {
			session.close();
			return result.get(0);
		}
		session.close();
		return null;
	}

	private String randomPass(String name) {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		String txtEncryptPass = new MD5EnCrypt().getMD5(saltStr);
		try {
			Session session = this.sessionFactory.openSession();
			System.out.println("da vao day!" + txtEncryptPass + ", " + name);
			String sql = "update user set password =:newPass, first_login = 1 where user_name =:userName and delete_state = 0";
			Query query = session.createSQLQuery(sql);
			query.setParameter("newPass", txtEncryptPass);
			query.setParameter("userName", name);
			query.executeUpdate();
			session.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return saltStr;
	}

	public String validateDelte(String ids) {
		String strList = null;
		String sql = "select user_name from users where  id in (select account_id from account_in_queue where account_id in ("
				+ ids + ")) and delete_state =0";
		try {
			Session session = this.sessionFactory.getCurrentSession();
			Query query = session.createSQLQuery(sql);
			List<String> rows = query.list();
			if (rows != null && rows.size() > 0) {
				for (int i = 0; i < rows.size(); i++) {
					String dto = rows.get(i);
					if (strList == null || strList.equals("")) {
						strList = dto;
					} else {
						strList = strList + ", " + dto;
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}

		return strList;
	}

	public ResponseObject changePassword(User account) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		ResponseObject responseObject = new ResponseObject();
		String newListPass = "";
		try {
			String sql = "update user set password = :newPass, first_login = 0, used_password = :usedPassword where user_name = :userName and delete_state = 0";
			// session.createSQLQuery(sql);
			if (!checkOldPass(account)) {
				responseObject.setResultCode(404);
				responseObject.setObject(null);
				responseObject.setErrorMessage("The old pass is wrong. Please try again.");
			} else {
				String usedPassString = (String) session
						.createSQLQuery(
								"SELECT used_password FROM user WHERE user_name = :userName AND delete_state = 0")
						.setParameter("userName", account.getAccountName()).uniqueResult();
				if (usedPassString != null) {
					System.out.println(usedPassString);
					List<String> passList = new ArrayList<String>(Arrays.asList(usedPassString.split(",")));
					if (passList.size() == 3) {
						passList = passList.subList(1, 3);
					}
					for (String each : passList) {
						if (newListPass.equals("")) {
							newListPass += each;
						} else {
							newListPass = newListPass + "," + each;
						}
					}
					newListPass += "," + new MD5EnCrypt().getMD5(account.getPassword());
				} else {
					newListPass += new MD5EnCrypt().getMD5(account.getPassword());
				}
				System.out.println(newListPass);
				account.setPassword(new MD5EnCrypt().getMD5(account.getPassword()));
				Query query = session.createSQLQuery(sql);
				query.setParameter("newPass", account.getPassword());
				query.setParameter("userName", account.getAccountName());
				query.setParameter("usedPassword", newListPass);
				query.executeUpdate();

				responseObject.setResultCode(200);
				responseObject.setObject(account);
				responseObject.setErrorMessage("change password success");
			}
			return responseObject;
		} catch (Exception e) {
			responseObject.setResultCode(500);
			responseObject.setObject(null);
			responseObject.setErrorMessage(e.getMessage());
			e.printStackTrace();
			return responseObject;
		}
	}

	private boolean checkOldPass(User account) {
		try {
			account.setOldPass(new MD5EnCrypt().getMD5(account.getOldPass()));
			String sql = "select (1) from user where delete_state =0 and user_name = :userName and password = :password";
			Session session = this.sessionFactory.getCurrentSession();
			// session.createSQLQuery(sql);
			Query query = session.createSQLQuery(sql);
			query.setParameter("userName", account.getAccountName());
			query.setParameter("password", account.getOldPass());
			// query.executeUpdate();
			List result = query.list();
			if (result.size() > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public int checkTypeRole(int id) {
		int re = 0;
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "select role.type from user u left join role ON role.id = u.role where u.delete_state =0 and role.delete_state = 0 and u.id = "
				+ id + "";
		try {
			Query query = session.createSQLQuery(sql);

			List<Integer> lstObj = query.list();
			if (lstObj != null && lstObj.size() > 0) {
				re = lstObj.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return re;
	}

	public ResponseObject checkUsedPassword(User account) {
		ResponseObject responseObject = new ResponseObject();
		Session session = this.sessionFactory.getCurrentSession();
		String[] passList = null;
		boolean isDifferent = true;
		try {
			String usedPassString = (String) session
					.createSQLQuery("SELECT used_password FROM user WHERE user_name = :userName AND delete_state = 0")
					.setParameter("userName", account.getAccountName()).uniqueResult();
			System.out.println(">>>>>>>" + usedPassString);
			if (usedPassString != null) {
				passList = usedPassString.split(",");
				System.out.println("/////////////////" + passList);
				for (String each : passList) {
					if (new MD5EnCrypt().getMD5(account.getPassword()).equals(each)) {
						isDifferent = false;
					}
				}
			}
			if (!isDifferent) {
				responseObject.setResultCode(404);
				responseObject.setObject(null);
				responseObject.setErrorMessage("This password is used");
			} else {
				responseObject.setResultCode(200);
				responseObject.setObject(null);
				responseObject.setErrorMessage("OK");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;

		}
		return responseObject;
	}

}
