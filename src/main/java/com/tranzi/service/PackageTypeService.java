package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.PackageTypesDAO;
import com.tranzi.dao.ProductDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.PackageTypes;
import com.tranzi.model.Product;

@Service("packageTypeService")
@Transactional
public class PackageTypeService {
	@Autowired
	PackageTypesDAO packageTypesDAO;

	@Transactional
	public List<PackageTypes> getAlls() {
		return packageTypesDAO.getAlls();
	}

	@Transactional
	public ResponseObject create(PackageTypes dto) {
		return packageTypesDAO.create(dto);
	}

	@Transactional
	public ResponseObject update(PackageTypes dto) {
		return packageTypesDAO.update(dto);
	}

	@Transactional
	public PackageTypes getId(int id) {
		return packageTypesDAO.getById(id);
	}

	@Transactional
	public int getDelete(int id) {
		return packageTypesDAO.delete(id);
	}
}
