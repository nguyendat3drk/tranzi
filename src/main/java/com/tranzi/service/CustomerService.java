package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.CustomerDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;

@Service("customerService")
public class CustomerService {
	
	@Autowired
	CustomerDAO customerDAO;

	@Transactional
	public List<Customer> getAlls() {
		return customerDAO.getAlls();
	}
	
	@Transactional
	public List<Customer> getPageCustomer(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return customerDAO.getAllCustomer(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return customerDAO.getCountCustomer(page, pageSize, columnName, typeOrder, keySearch);
	}
	
	@Transactional
	public ResponseObject create(Customer dto) {
		return customerDAO.create(dto);
	}

	@Transactional
	public ResponseObject update(Customer dto) {
		return customerDAO.update(dto);
	}

	@Transactional
	public Customer getId(int id) {
		return customerDAO.getById(id);
	}

	@Transactional
	public ResponseObject getDelete(int id) {
		return customerDAO.delete(id);
	}
	
	
}
