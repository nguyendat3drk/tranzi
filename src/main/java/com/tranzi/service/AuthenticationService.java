package com.tranzi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.AuthenticationDAO;
import com.tranzi.entity.LoginView;
import com.tranzi.entity.ResponseObject;

@Service("authenticationService")
@Transactional
public class AuthenticationService {
	
	@Autowired
	AuthenticationDAO authenticationDao;
	
	public ResponseObject checkLogin(LoginView accountLogin){
		return authenticationDao.checkLogin(accountLogin);
	}

	public ResponseObject updateLogoutDetail(String userLogin) {
		return authenticationDao.updateLogoutDetail(userLogin);
	}
	
	public boolean checkUserSession(String userName, String currentSession) {
		return authenticationDao.checkUserSession(userName, currentSession);
	}
	
	public int getRoleByUsername(String userName) {
		return authenticationDao.getRoleByUsername(userName);
	}
}
