package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.TagsDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Tags;

@Service("tagsService")
@Transactional
public class TagsService {
	@Autowired
	TagsDAO tagsDAO;

	public List<Tags> getTagsByProductId(int id) {
		return tagsDAO.getTagsByProductId(id);
	}

	public List<Tags> getTagsByArticleId(int id) {
		return tagsDAO.getTagsByArticleId(id);
	}

	public Tags getById(int id) {
		return tagsDAO.getById(id);
	}

	public int delete(int id) {
		return tagsDAO.delete(id);
	}

	public List<Tags> getAlls() {
		return tagsDAO.getAlls();
	}

	public ResponseObject create(Tags re) {
		return tagsDAO.create(re);
	}

	public ResponseObject update(Tags re) {
		return tagsDAO.update(re);
	}

	public int deleteTagsByIdProduct(long id) {
		return tagsDAO.deleteTagsByIdProduct(id);
	}
}
