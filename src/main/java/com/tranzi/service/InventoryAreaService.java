package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.InventoryAreaDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.InventoryArea;

@Service("inventoryAreaService")
@Transactional
public class InventoryAreaService {
	@Autowired
	InventoryAreaDAO inventoryAreaDAO;

	@Transactional
	public List<InventoryArea> getAlls() {
		return inventoryAreaDAO.getAlls();
	}

	@Transactional
	public List<InventoryArea> getPageInventoryArea(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		return inventoryAreaDAO.getAllpageding(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return inventoryAreaDAO.getCountPageAll(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public ResponseObject addupdate(InventoryArea dto) {
		return inventoryAreaDAO.addupdate(dto);
	}

	// @Transactional
	// public ResponseObject update(InventoryArea dto) {
	// return inventoryAreaDAO.update(dto);
	// }

	@Transactional
	public InventoryArea getId(int id) {
		return inventoryAreaDAO.getById(id);
	}

	@Transactional
	public ResponseObject delete(int id) {
		return inventoryAreaDAO.delete(id);
	}
}
