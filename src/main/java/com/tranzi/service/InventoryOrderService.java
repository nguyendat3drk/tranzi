package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.InventoryOrderDAO;
import com.tranzi.model.InventoryOrder;

@Service("inventoryOrderService")
public class InventoryOrderService {
	@Autowired
	InventoryOrderDAO inventoryOrderDAO;

	@Transactional
	public List<InventoryOrder> getAlls() {
		return inventoryOrderDAO.getAlls();
	}

	@Transactional
	public InventoryOrder create(InventoryOrder dto) {
		return inventoryOrderDAO.create(dto);
	}

	@Transactional
	public InventoryOrder update(InventoryOrder dto) {
		return inventoryOrderDAO.update(dto);
	}
	@Transactional
	public List<InventoryOrder> getPageInventory(int page, int pageSize, String columnName, String typeOrder,
			String keySearch,int idInventory) {
		return inventoryOrderDAO.getAllInventoryPager(page, pageSize, columnName, typeOrder, keySearch, idInventory);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch,int idInventory) {
		return inventoryOrderDAO.getCountAllInventory(page, pageSize, columnName, typeOrder, keySearch, idInventory);
	}

}
