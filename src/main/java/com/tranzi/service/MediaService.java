package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.MediaDAO;
import com.tranzi.model.Media;
@Service("mediaService")
public class MediaService {
	@Autowired
	MediaDAO mediaDAO;
	
	@Transactional
	public List<Media> getMediaByProductID(int id) {
		return mediaDAO.getByProductId(id);
	}
	@Transactional
	public List<Media> getMediaByProductIDMedia(int id) {
		return mediaDAO.getMediaByProductIDMedia(id);
	}
	
}
