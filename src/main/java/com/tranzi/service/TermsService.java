package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.TermsDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Terms;

@Service("termsService")
public class TermsService {
	@Autowired
	TermsDAO termsDAO;

	@Transactional
	public List<Terms> getAlls() {
		return termsDAO.getAlls();
	}

	@Transactional
	public Terms create(Terms dto) {
		return termsDAO.create(dto);
	}

	@Transactional
	public Terms update(Terms dto) {
		return termsDAO.update(dto);
	}

	@Transactional
	public Terms getId(int id) {
		return termsDAO.getById(id);
	}

	@Transactional
	public List<Terms> getPageTerms(int page, int pageSize, String columnName, String typeOrder, String keySearch, int level) {
		return termsDAO.getAllTermsPager(page, pageSize, columnName, typeOrder, keySearch, level);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch, int level) {
		return termsDAO.getCountAllTerms(page, pageSize, columnName, typeOrder, keySearch, level);
	}

	@Transactional
	public ResponseObject deleteTerms(int id) {
		return termsDAO.deleteTerm(id);
	}
}
