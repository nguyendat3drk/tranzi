package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.CustomerDAO;
import com.tranzi.dao.CustomerOrderDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Customer;
import com.tranzi.model.CustomerOrder;

@Service("customerOrderService")
public class CustomerOrderService {
	
	@Autowired
	CustomerOrderDAO customerOrderDAO;

	@Transactional
	public List<CustomerOrder> getAlls() {
		return customerOrderDAO.getAlls();
	}
	
	@Transactional
	public List<CustomerOrder> getPageCustomer(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return customerOrderDAO.getAllCustomer(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return customerOrderDAO.getCountCustomer(page, pageSize, columnName, typeOrder, keySearch);
	}
	
	@Transactional
	public ResponseObject create(CustomerOrder dto) {
		return customerOrderDAO.create(dto);
	}

	@Transactional
	public ResponseObject update(CustomerOrder dto) {
		return customerOrderDAO.update(dto);
	}

	@Transactional
	public CustomerOrder getId(int id) {
		return customerOrderDAO.getById(id);
	}

	@Transactional
	public ResponseObject getDelete(int id) {
		return customerOrderDAO.delete(id);
	}
	
	
}
