package com.tranzi.service;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tranzi.dao.MenuDAO;
import com.tranzi.dao.PageMenu;
import com.tranzi.entity.MenuPerItem;
import com.tranzi.model.Menu;

@Transactional
@Service("menuService")
public class MenuService {
	@Autowired
	MenuDAO menuDao;
	public List<Menu> getAllMenuDashboard(){
		return menuDao.getAllMenuDashboard();
	}
	
	public List<Menu> getAllMenu() {
		return menuDao.getAllMenu();
	}
	
	public List<Menu> getMenuCheck(int id) {
		return menuDao.getMenuCheck(id);
	}

	public List<MenuPerItem> getMenuItemsByRole(int roleId){
		return menuDao.getMenuItemsByRole(roleId);
	}
	
	public List<PageMenu> getPageMenuByRole(int roleId){
		return menuDao.getPageMenuByRole(roleId);
	}
	public List<Menu> getMenuParent() {
		return menuDao.getMenuParent();
	}
}
