package com.tranzi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.MenuPermissionDAO;
import com.tranzi.model.MenuPermission;
import com.tranzi.model.Role;

@Service("menupermissionService")
public class MenuPermissionService {
	@Autowired
	MenuPermissionDAO menuPermissionDao;
	

	@Transactional
	public ResponseEntity<?> addMenupermission(MenuPermission menupermission) {
		return menuPermissionDao.addMenuPermission(menupermission);
	}
	


	@Transactional
	public ResponseEntity<?> deleteMenupermission(Integer[] ids) {
		return menuPermissionDao.deleteMenuPermission(ids);
	}
	
	
	@Transactional
	public List<MenuPermission> getAllAgents() {
		// TODO Auto-generated method stub
		return menuPermissionDao.getAllMenuPermissionsList();
	}


	@Transactional
	public ResponseEntity<?> deletePermission(int ids) {
		// TODO Auto-generated method stub
		return menuPermissionDao.deletePermission(ids);
	}
	
	@Transactional
	public List<MenuPermission> getPerStatistic(int ids) {
		return menuPermissionDao.getPerStatistic(ids);	
	}
	@Transactional
	public List<MenuPermission> getPermissionRole(int ids) {
		return menuPermissionDao.getAllPermissionRole(ids);
	}
	@Transactional
	public MenuPermission updatePermission(List<MenuPermission> menupermission) {
		return menuPermissionDao.updatePermission(menupermission);

	}
	@Transactional
	public List<MenuPermission> getPermissionRealTime(int ids) {
		List<MenuPermission> list = new ArrayList<MenuPermission>();
		list = menuPermissionDao.getPermissionRealTime(ids);
		if(list!=null && list.size()>0){
			MenuPermission dto;
			for(int i = 0;i<list.size();i++){
				dto = new MenuPermission();
				dto = list.get(i);
				dto.setMenuName((i+1)+". "+dto.getMenuName());
			}
		}
		return list;
		
	}
	@Transactional
	public List<MenuPermission> getPerDashBoard(int ids) {
		return menuPermissionDao.getPerDashBoard(ids);
		
	}
	
	
}
