package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.ArticlesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ArticleLinkUpload;
import com.tranzi.model.Articles;
import com.tranzi.model.respontOrder.SelectLinkFile;

@Service("articlesService")
public class ArticlesService {
	@Autowired
	ArticlesDAO articlesDAO;

	@Transactional
	public List<Articles> getAlls() {
		return articlesDAO.getAlls();
	}

	@Transactional
	public Articles create(Articles dto) {
		return articlesDAO.create(dto);
	}

	@Transactional
	public Articles update(Articles dto) {
		return articlesDAO.update(dto);
	}

	@Transactional
	public Articles getId(int id) {
		return articlesDAO.getById(id);
	}
	
	
	@Transactional
	public Integer checkArticleProeduct(int id) {
		return articlesDAO.checkArticleProeduct(id);
	}

	@Transactional
	public List<Articles> getPageVideos(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return articlesDAO.getAllVideosPager(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return articlesDAO.getCountAllVideos(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public ResponseObject deleteVideos(int id) {
		return articlesDAO.deleteTerm(id);
	}
	@Transactional
	public List<ArticleLinkUpload> getListFileArticleUpload(int id) {
		return articlesDAO.getByIdArticleLinkUpload(id);
	}
	@Transactional
	public List<ArticleLinkUpload> getByIdProductLinkUpload(int id) {
		return articlesDAO.getByIdProductLinkUpload(id);
	}
	
	@Transactional
	public List<ArticleLinkUpload> getSearchArticleLinkUpload(SelectLinkFile dto) {
		return articlesDAO.getSearchArticleLinkUpload(dto);
	}
}
