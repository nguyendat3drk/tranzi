package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.InventoryDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Inventory;

@Service("inventoryService")
public class InventoryService {
	@Autowired
	InventoryDAO inventoryDAO;

	@Transactional
	public List<Inventory> getAlls() {
		return inventoryDAO.getAlls();
	}

	@Transactional
	public Inventory create(Inventory dto) {
		return inventoryDAO.create(dto);
	}

	@Transactional
	public Inventory update(Inventory dto) {
		return inventoryDAO.update(dto);
	}

	@Transactional
	public Inventory getId(int id) {
		return inventoryDAO.getById(id);
	}

	@Transactional
	public List<Inventory> getPageInventory(int page, int pageSize, String columnName, String typeOrder,
			String keySearch) {
		return inventoryDAO.getAllInventoryPager(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return inventoryDAO.getCountAllInventory(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public ResponseObject getDeleteInventory(int id) {
		return inventoryDAO.getDeleteInventory(id);
	}

}
