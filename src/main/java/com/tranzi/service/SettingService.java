package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.SettingDAO;
import com.tranzi.dao.SettingItemDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Categories;
import com.tranzi.model.Setting;
import com.tranzi.model.SettingItem;

@Service("settingService")
public class SettingService {
	
	@Autowired
	SettingDAO settingDao;
	
	@Transactional
	public List<Setting> getAlls() {
		return settingDao.getAlls();
	}
	@Transactional
	public List<Setting> getPageSetting(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return settingDao.getAllSettingPager(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return settingDao.getCountAllSetting(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public ResponseObject deleteByID(int id) {
		return settingDao.deleteByID(id);
	}
	@Transactional
	public Setting createSetting(Setting data) {
		return settingDao.createSetting(data);
	}
	@Transactional
	public Setting updateSetting(Setting data) {
		return settingDao.updateSetting(data);
	}
	@Transactional
	public Setting getId(int id) {
		return settingDao.getById(id);
	}
	
	
}