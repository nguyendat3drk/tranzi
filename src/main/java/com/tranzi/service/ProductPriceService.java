package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.ProductsPricesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ProductsPrices;

@Service("productpriceService")
@Transactional
public class ProductPriceService {
	@Autowired
	ProductsPricesDAO productsPricesDAO;

	@Transactional
	public List<ProductsPrices> getListByProductID(int id) {
		return productsPricesDAO.getALlByProductID(id);
	}

	@Transactional
	public ResponseObject create(ProductsPrices re) {
		return productsPricesDAO.create(re);
	}

	@Transactional
	public ResponseObject update(ProductsPrices re) {
		return productsPricesDAO.update(re);
	}

	@Transactional
	public ResponseObject createList(List<ProductsPrices> re) {
		return productsPricesDAO.createList(re);
	}

	@Transactional
	public ResponseObject deleteByProductID(int id) {
		return productsPricesDAO.deleteProductPriceByID(id);
	}
	@Transactional
	public ResponseObject deleteByProductSku(String  sku) {
		return productsPricesDAO.deleteByProductSku(sku);
	}


}
