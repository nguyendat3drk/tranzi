package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.AccountDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Account;
import com.tranzi.model.Categories;
import com.tranzi.model.User;

@Service("accountService")
@Transactional
public class AccountService {

	@Autowired
	AccountDAO accountDao;

	@Transactional
	public List<Account> getPageAccount(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return accountDao.getPageAccount(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return accountDao.getCountAllAccount(page, pageSize, columnName, typeOrder, keySearch);
	}

	public ResponseObject addAccount(Account account) {
		return accountDao.addAccount(account);
	}

	public ResponseObject updateAccount(Account account) {
		return accountDao.updateAccount(account);
	}

	public ResponseObject deleteId(int id) {
		return accountDao.deleteId(id);
	}

	public ResponseObject getAccountById(int id) {
		return accountDao.getAccountById(id);
	}

	public List<User> getAllUserRole(int id) {
		// TODO Auto-generated method stub
		return accountDao.getAllUserRole(id);
	}

	public int resetMail(String email, String title) {
		// TODO Auto-generated method stub
		return accountDao.resetMail(email, title);
	}

	public ResponseObject changePassword(User account) {
		// TODO Auto-generated method stub
		return accountDao.changePassword(account);
	}

	public ResponseObject checkUsedPassword(User account) {
		return accountDao.checkUsedPassword(account);
	}

}
