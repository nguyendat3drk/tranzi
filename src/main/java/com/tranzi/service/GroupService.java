package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.GroupDAO;
import com.tranzi.entity.KeyValueDTO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.GroupACD;
import com.tranzi.model.User;

@Service("groupService")
@Transactional
public class GroupService {
	@Autowired
	GroupDAO groupDao;
	
	
	public boolean isGroupExists(GroupACD group){
		return groupDao.isGroupExists(group);
	}
	
	public List<GroupACD> getAllsGroup(){
		return groupDao.getAllsGroup();
	}

	public List<GroupACD> getAllsGroupFull(){
		return groupDao.getAllsGroupFull();
	}
	
	public ResponseObject deleteListGroup(String groupIds){
		return groupDao.deleteListGroup(groupIds);
	}
	
	public ResponseObject getUserIdsByGroupId(int groupId){
		return groupDao.getUserIdsByGroupId(groupId);
	}
	
	public List<GroupACD> getAlls(int page, int pageSize,String columnName,String typeOrder) {
		return groupDao.getAlls(page, pageSize, columnName, typeOrder);
	}
	
	public int getRowCount() {
		// TODO Auto-generated method stub
		return groupDao.getRowCount();
	}

	public GroupACD getGroup(int id) {
		return groupDao.getGroup(id);
	}

	public ResponseObject addGroup(GroupACD group) {
		
		return groupDao.addGroup(group);
	}

	public ResponseObject updateGroup(GroupACD group) {
		return groupDao.updateGroup(group);

	}

	public ResponseObject deleteGroup(int id) {
		return groupDao.deleteGroup(id);
	}

	public List<GroupACD> searchGroup(String name) {
		// TODO Auto-generated method stub
		return groupDao.searchGroup(name);
	}
	
	public GroupACD changeGroupStatus(GroupACD group){
		return groupDao.changeGroupStatus(group);
	}
	
	public List<User> getUsersByGroupId(int groupId){
		return groupDao.getUsersByGroupId(groupId);
	}
	
	public ResponseObject getUsersByGroupIdPaging(int page, int pageSize, int id){
		return groupDao.getUsersByGroupIdPaging(page, pageSize, id);
	}
	
	public ResponseObject getRowCountUserByGroup(int groupId) {
		return groupDao.getRowCountUserByGroup(groupId);
	}
	
	public List<User> getUsersNotInGroup(int groupId){
		return groupDao.getUsersNotInGroup(groupId);
	}

	public List<KeyValueDTO> getGroupCombo() { 
		// TODO Auto-generated method stub
		return groupDao.getGroupCombo();
	}
	
}
