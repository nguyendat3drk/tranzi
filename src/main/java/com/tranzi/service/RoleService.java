package com.tranzi.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.RoleDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Role;

@Service("roleService")
public class RoleService {
	@Autowired
	RoleDAO roleDao;
	
	@Transactional
	public List<Role> getRolePaging(int page, int pageSize, String columnName, String typeOrder){
		return roleDao.getRolePagingList(page, pageSize, columnName, typeOrder);
	}
	
	@Transactional
	public List<Role> getAllCountries() {
		return roleDao.getAllRoles(1);
	}

	@Transactional
	public Role getRole(int id) {
		return roleDao.getRole(id);
	}

	@Transactional
	public int addRole(Role Role) {
		return roleDao.addRole(Role);
	}

	@Transactional
	public ResponseEntity<?> updateStatusRole(Role Role) {
		return roleDao.updateStatusRole(Role);

	}
	
	@Transactional
	public ResponseEntity<?> saveRole(Role Role) {
		return roleDao.saveRole(Role);

	}

//	@Transactional
//	public ResponseEntity<?> deleteRole(Integer[] ids) {
//		return roleDao.deleteRoleList(ids);
//	}

	@Transactional
	public List<Role> searchRoles(Integer role, Integer id, String name) {
		// TODO Auto-generated method stub
		return roleDao.searchRoles(role, id, name);
	}
	
	@Transactional
	public List<Role> getAllAgents(int id) {
		// TODO Auto-generated method stub
		return roleDao.getAllRoles(id);
	}
	
	@Transactional
	public List<Role> getAllSupervisors() {
		// TODO Auto-generated method stub
		return roleDao.getAllSupervisors();
	}
	
	@Transactional
	public int getRowCount() {
		// TODO Auto-generated method stub
		return roleDao.getRowCount();
	}
	@Transactional
	 public ResponseObject deleteRoleList(String roles) {
		return roleDao.deleteRoleList(roles);
	 }
	
	@Transactional
	public ResponseObject checkDelete(String roles) {
		return roleDao.checkDeleteList(roles);
	}
	
	@Transactional
	public int checkUnique(Role role){
		return roleDao.checkUnique(role);
	}
	@Transactional
	public int checkUser(Integer id){
		return roleDao.checkDelete(id);
	}
	
}
