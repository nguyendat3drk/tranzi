package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.tranzi.dao.ProductDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.ImportAttribute;
import com.tranzi.model.ListAttributeAndValues;
import com.tranzi.model.Product;
import com.tranzi.model.ProductSearch;

@Service("productService")
@Transactional
public class ProductService {
	@Autowired
	ProductDAO productDAO;

	@Transactional
	public List<Product> getPageProduct(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return productDAO.getAllProductPager(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public List<Product> getPageProductByInventoryArea(int page, int pageSize, String columnName, String typeOrder,
			String keySearch, int area) {
		return productDAO.getPageProductByInventoryArea(page, pageSize, columnName, typeOrder, keySearch, area);
	}

	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return productDAO.getCountAllProduct(page, pageSize, columnName, typeOrder, keySearch);
	}

	@Transactional
	public int getCountTotalPageByInventoryArea(int page, int pageSize, String columnName, String typeOrder,
			String keySearch, int area) {
		return productDAO.getCountAllProductByInventoryArea(page, pageSize, columnName, typeOrder, keySearch, area);
	}

	@Transactional
	public List<Product> getAlls() {
		return productDAO.getAlls();
	}

	@Transactional
	public List<Product> getAllsNotDetail() {
		return productDAO.getAllsNotDetail();
	}

	@Transactional
	public List<Product> getAllProduct() {
		return productDAO.getAllProduct();
	}

	@Transactional
	public ResponseObject create(Product dto) {
		return productDAO.create(dto);
	}

	@Transactional
	public ResponseObject updateProduct(Product dto) {
		return productDAO.updateProduct(dto);
	}

	@Transactional
	public Product updatePrice(Product dto) {
		return productDAO.updatePrice(dto);
	}

	@Transactional
	public Product getId(int id) {
		return productDAO.getById(id);
	}

	@Transactional
	public ResponseObject deleteProductID(int id) {
		return productDAO.deleteProduct(id);
		// return productDAO.deleteAllProduct();
	}

	@Transactional
	public int deleteAllProduct() {
		return productDAO.deleteAllProduct();
	}

	@Transactional
	public List<ListAttributeAndValues> getListIDAttributeValue(int id) {
		return productDAO.getListIDAttributeValue(id);
	}

	@Transactional
	public ResponseObject createImport(Product re, List<ImportAttribute> listAttributes) {
		evenAddProduct proService = new evenAddProduct();
		proService.run(re, listAttributes);
		// return productDAO.createImport(re, listAttributes);
		return null;
	}

	class evenAddProduct extends Thread {
		public void run(Product re, List<ImportAttribute> listAttributes) {
			System.out.println(" ---Run Thread---");
			productDAO.createImport(re, listAttributes);
			System.out.println("--- End Run Thread---");
		}

	}

	// update list product by mã nhà sản xuất
	@Transactional
	public ResponseObject updateProductByOginSku(List<Product> list) {
		return productDAO.updateProductByOginSku(list);
	}

	public List<Product> getPageProductMore(ProductSearch pro) {
		return productDAO.getAllProductPagerMore(pro);
	}

	@Transactional
	public int getCountTotalPageMore(ProductSearch pro) {
		return productDAO.getCountAllProductMore(pro);
	}

	@Transactional
	public ResponseObject deleteListID(@RequestBody List<Integer> list) {
		return productDAO.deleteListID(list);
	}

	@Transactional
	public void fixSlug() {
		productDAO.fixSlug();
	}

	@Transactional
	public Integer checkArticleProeduct(int id) {
		return productDAO.checkArticleProeduct(id);
	}

	@Transactional
	public Product getByLK(String sku, String startDate, String endDate) {
		return productDAO.getByLK(sku, startDate, endDate);
	}

	@Transactional
	public Product getByLKPrice(String sku, int price, int i) {
		return productDAO.getByLKPrice(sku, price, i);
	}

}
