package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.AttributeValuesDAO;
import com.tranzi.dao.AttributesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AttributeValues;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.PackageTypes;

@Service("attributeValuesService")
@Transactional
public class AttributeValuesService {
	@Autowired
	AttributeValuesDAO attributeValuesDAO;

	@Transactional
	public List<AttributeValues> getAlls() {
		return attributeValuesDAO.getAlls();
	}
	
	@Transactional
	public List<AttributeValues> getPageAttributes(int page, int pageSize, String columnName, String typeOrder, String keySearch, long id) {
		return attributeValuesDAO.getAllAttributes(page, pageSize, columnName, typeOrder, keySearch, id);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch, long id) {
		return attributeValuesDAO.getCountAttributes(page, pageSize, columnName, typeOrder, keySearch,id);
	}
	
	
	
	@Transactional
	public ResponseObject create(AttributeValues dto) {
		return attributeValuesDAO.create(dto);
	}

	@Transactional
	public ResponseObject update(AttributeValues dto) {
		return attributeValuesDAO.update(dto);
	}

	@Transactional
	public AttributeValues getId(int id) {
		return attributeValuesDAO.getById(id);
	}

	@Transactional
	public ResponseObject getDelete(int id) {
		return attributeValuesDAO.delete(id);
	}
}
