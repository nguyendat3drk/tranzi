package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.ReportDao;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AgentReport;
import com.tranzi.model.ContactReport;
import com.tranzi.model.QueueReport;
@Service("reportService")
public class ReportService {

	@Autowired
	ReportDao reportDao;

	@Transactional
	public List<ContactReport> getContact(String type,String fromDate,String toDate,String userName,String oder,float timeUTC) {
		return reportDao.getContact(type,fromDate,toDate,userName,oder,timeUTC);
	}
	@Transactional
	public ResponseObject getQueueReport(String type,String fromDate,String toDate,String userName,String oder,String queue, float timeUTC){
		return reportDao.getQueueReport(type,fromDate,toDate,userName,oder,queue,timeUTC);
	}
	@Transactional
	public ResponseObject getAgentReport(String type,String fromDate,String toDate,String userName,String oder,String agent, float timeUTC){
		return reportDao.getAgentReport(type,fromDate,toDate,userName,oder,agent,timeUTC);
	}
	
}
