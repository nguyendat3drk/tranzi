package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tranzi.dao.CategoriesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Categories;
import com.tranzi.model.Product;

@Service("categoriesService")
public class CategoriesService {
	@Autowired
	CategoriesDAO categoriesDAO;
	@Transactional
	public List<Categories> getAlls() {
		return categoriesDAO.getAlls();
	}
	@Transactional
	public Categories create(Categories dto) {
		return categoriesDAO.create(dto);
	}
	@Transactional
	public Categories update(Categories dto) {
		return categoriesDAO.update(dto);
	}
	@Transactional
	public Categories updateOrder(List<Categories> list) {
		return categoriesDAO.updateOrder(list);
	}
	
	@Transactional
	public Categories getId(int id) {
		return categoriesDAO.getById(id);
	}
	@Transactional
	public List<Categories> getPageCategories(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return categoriesDAO.getAllCategoryPager(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return categoriesDAO.getCountAllCategory(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public ResponseObject deleteCategori(int id) {
		return categoriesDAO.deleteCategori(id);
	}
	@Transactional
	public ResponseObject deleteAllCategori() {
		return categoriesDAO.deleteAllCategori();
	}
	@Transactional
	public List<Categories> getListParentCategories() {
		return categoriesDAO.getListParentCategories();
	}
	
	@Transactional
	public List<Categories> getParentTreeChildenById(int id) {
		return categoriesDAO.getParentTreeChildenById(id);
	}
	@Transactional
	public List<Categories> getListChildrenCategories(){
		return categoriesDAO.getListChildrenCategories();
	}
	@Transactional
	public long addCategoryImport(String name){
		long id = 0;
		if(name!=null && !"".equals(name) && !"null".equals(name)){
			String[] listName = name.split(">");
			if(listName!=null && listName.length>0){
				for(String n: listName){
					if(n!=null && !"".equals(n!=null?n.trim():"")){
						id = categoriesDAO.addCategoryImport((name!=null && !"".equals(name) && !"null".equals(name))?n.trim():"", id);
					}
				}
			}
		}
		return id;
	}
	@Transactional
	public long addCategoryImportNotCheck(String name){
		return categoriesDAO.addCategoryImportNotCheck(name);
	}
	@Transactional
	public Categories addCategoryName(Categories ca){
		return categoriesDAO.create(ca);
	}
}
