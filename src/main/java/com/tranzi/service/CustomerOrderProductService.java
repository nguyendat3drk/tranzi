package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.CustomerOrderProductDAO;
import com.tranzi.model.CustomerOrderProduct;

@Service("customerOrderProductService")
public class CustomerOrderProductService {
	
	@Autowired
	CustomerOrderProductDAO customerOrderProductDAO;

	@Transactional
	public List<CustomerOrderProduct> getByCustomerOrderID(int id) {
		return customerOrderProductDAO.getAllByCustomerOrderId(id);
	}
	@Transactional
	public List<CustomerOrderProduct> getByCustomerOrderIDExport(long id) {
		return customerOrderProductDAO.getByCustomerOrderIDExport(id);
	}
	
	
	
	
}
