package com.tranzi.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.tranzi.dao.DashboardDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.MenuPermission;
import com.tranzi.model.UserPreference;

@Service("dashboardService")
@Transactional
public class DashboardService {
	@Autowired
	DashboardDAO dashboardDAO;
	
	public List<ResponseObject> getTotalCall(List<String> dates) throws ParseException {
		return dashboardDAO.getTotalCall(dates);
	}
	
	public ResponseObject getDashboardState(int id) throws ParseException {
		return dashboardDAO.getDashboardState(id);
	}
	
	public ResponseObject saveDashboardState(UserPreference userPreference) {
		return dashboardDAO.saveDashboardState(userPreference);
	}
	
	public ResponseObject getDataDashBoard(int agentId, long fromDate, long toDate){
		return dashboardDAO.getDataDashBoard(agentId, fromDate, toDate);
	}
	
	public ResponseObject savePosition(UserPreference userPreference){
		return dashboardDAO.savePosition(userPreference);
	}
	
}
