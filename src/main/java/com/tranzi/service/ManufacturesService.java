package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.CategoriesDAO;
import com.tranzi.dao.ManufacturesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Categories;
import com.tranzi.model.Manufactures;
import com.tranzi.model.Product;

@Service("manufacturesService")
public class ManufacturesService {
	@Autowired
	ManufacturesDAO manufacturesDAO;
	@Transactional
	public List<Manufactures> getAlls() {
		return manufacturesDAO.getAlls();
	}
	@Transactional
	public Manufactures create(Manufactures dto) {
		return manufacturesDAO.create(dto);
	}
	@Transactional
	public Manufactures update(Manufactures dto) {
		return manufacturesDAO.update(dto);
	}
	@Transactional
	public Manufactures getId(int id) {
		return manufacturesDAO.getById(id);
	}
	@Transactional
	public List<Manufactures> getPageManufactures(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return manufacturesDAO.getAllManufacturesPager(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return manufacturesDAO.getCountAllManufactures(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public ResponseObject getDeleteManufactures(int id) {
		return manufacturesDAO.getDeleteManufactures(id);
	}

}
