package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.AttributesDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.Attributes;
import com.tranzi.model.Categories;
import com.tranzi.model.PackageTypes;

@Service("attributesService")
@Transactional
public class AttributesService {
	@Autowired
	AttributesDAO attributesDAO;

	@Transactional
	public List<Attributes> getAlls() {
		return attributesDAO.getAlls();
	}
	
	@Transactional
	public List<Attributes> getPageAttributes(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return attributesDAO.getAllAttributes(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return attributesDAO.getCountAttributes(page, pageSize, columnName, typeOrder, keySearch);
	}
	
	@Transactional
	public ResponseObject create(Attributes dto) {
		return attributesDAO.create(dto);
	}

	@Transactional
	public ResponseObject update(Attributes dto) {
		return attributesDAO.update(dto);
	}

	@Transactional
	public Attributes getId(int id) {
		return attributesDAO.getById(id);
	}

	@Transactional
	public ResponseObject delete(int id) {
		return attributesDAO.delete(id);
	}
	@Transactional
	public List<Long> getListCategoryByattribute(Integer id) {
		return attributesDAO.getListCategoryByattribute(id);
	}
	@Transactional
	public List<Attributes> getAttributeByCategory(Integer id) {
		return attributesDAO.getAttributeByCategory(id);
	}
	@Transactional
	public Attributes updateOrder(List<Attributes> list) {
		return attributesDAO.updateOrder(list);
	}
	@Transactional
	public boolean fixAttribute() {
		return attributesDAO.fixAttribute();
	}
	
	
}
