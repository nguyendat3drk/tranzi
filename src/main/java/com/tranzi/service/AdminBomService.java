package com.tranzi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tranzi.dao.AdminBomDAO;
import com.tranzi.entity.ResponseObject;
import com.tranzi.model.AdminBom;

@Service("adminbomService")
public class AdminBomService {
	@Autowired
	AdminBomDAO adminBomDAO;
	@Transactional
	public List<AdminBom> getAlls() {
		return adminBomDAO.getAlls();
	}
	@Transactional
	public AdminBom create(AdminBom dto) {
		return adminBomDAO.create(dto);
	}
	@Transactional
	public AdminBom update(AdminBom dto) {
		return adminBomDAO.update(dto);
	}
	@Transactional
	public AdminBom getId(int id) {
		return adminBomDAO.getById(id);
	}
	@Transactional
	public List<AdminBom> getPageAdminBom(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return adminBomDAO.getAllAdminBomPager(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public int getCountTotalPage(int page, int pageSize, String columnName, String typeOrder, String keySearch) {
		return adminBomDAO.getCountAllAdminBom(page, pageSize, columnName, typeOrder, keySearch);
	}
	@Transactional
	public ResponseObject deleteAdminBom(int id) {
		return adminBomDAO.deleteAdminBom(id);
	}

}
