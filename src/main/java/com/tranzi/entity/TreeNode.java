package com.tranzi.entity;

import java.util.List;

public class TreeNode {
	String label;
	Object data;
	Object icon;
	Object expandedIcon;
	Object collapsedIcon;
	List<TreeNode> children;
	boolean leaf;
	boolean expanded;
	String type;
	TreeNode parent;
	boolean partialSelected;
	String styleClass;
	boolean draggable;
	boolean droppable;
	boolean selectable;

	public TreeNode(String label, Object data, Object icon, Object expandedIcon, Object collapsedIcon,
			List<TreeNode> children, boolean leaf, boolean expanded, String type, TreeNode parent,
			boolean partialSelected, String styleClass, boolean draggable, boolean droppable, boolean selectable) {
		super();
		this.label = label;
		this.data = data;
		this.icon = icon;
		this.expandedIcon = expandedIcon;
		this.collapsedIcon = collapsedIcon;
		this.children = children;
		this.leaf = leaf;
		this.expanded = expanded;
		this.type = type;
		this.parent = parent;
		this.partialSelected = partialSelected;
		this.styleClass = styleClass;
		this.draggable = draggable;
		this.droppable = droppable;
		this.selectable = selectable;
	}

	public TreeNode() {
		super();
		
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getIcon() {
		return icon;
	}

	public void setIcon(Object icon) {
		this.icon = icon;
	}

	public Object getExpandedIcon() {
		return expandedIcon;
	}

	public void setExpandedIcon(Object expandedIcon) {
		this.expandedIcon = expandedIcon;
	}

	public Object getCollapsedIcon() {
		return collapsedIcon;
	}

	public void setCollapsedIcon(Object collapsedIcon) {
		this.collapsedIcon = collapsedIcon;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public TreeNode getParent() {
		return parent;
	}

	public void setParent(TreeNode parent) {
		this.parent = parent;
	}

	public boolean isPartialSelected() {
		return partialSelected;
	}

	public void setPartialSelected(boolean partialSelected) {
		this.partialSelected = partialSelected;
	}

	public String getStyleClass() {
		return styleClass;
	}

	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	public boolean isDraggable() {
		return draggable;
	}

	public void setDraggable(boolean draggable) {
		this.draggable = draggable;
	}

	public boolean isDroppable() {
		return droppable;
	}

	public void setDroppable(boolean droppable) {
		this.droppable = droppable;
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}

}
