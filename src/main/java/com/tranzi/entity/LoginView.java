package com.tranzi.entity;

public class LoginView {
	private String accountName;
	private String password;
	private boolean isRememberPassword;
	
	public LoginView(){
		
	}
	public LoginView(String accountName, String password, boolean isRememberPassword) {
		super();
		this.accountName = accountName;
		this.password = password;
		this.isRememberPassword = isRememberPassword;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isRememberPassword() {
		return isRememberPassword;
	}
	public void setRememberPassword(boolean isRememberPassword) {
		this.isRememberPassword = isRememberPassword;
	}
	
}
