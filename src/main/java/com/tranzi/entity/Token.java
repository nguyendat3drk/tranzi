package com.tranzi.entity;

public class Token {
	private String id;
	private String issuer;
	private String subject;
	private long expireMinute;
	private String scope;
	private long issueAt;
	
	public Token(String id, String issuer, String subject,long issueAt, long expireMinute, String scope) {
		super();
		this.id = id;
		this.issuer = issuer;
		this.subject = subject;
		this.expireMinute = expireMinute;
		this.scope = scope;
		this.issueAt = issueAt;
	}
	
	public long getIssueAt() {
		return issueAt;
	}

	public void setIssueAt(long issueAt) {
		this.issueAt = issueAt;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public long getExpireMinute() {
		return expireMinute;
	}
	public void setExpireMinute(long expireMinute) {
		this.expireMinute = expireMinute;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	
}
