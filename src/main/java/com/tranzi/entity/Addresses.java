package com.tranzi.entity;

import java.util.List;

public class Addresses {
	private List<Address> addresses;

	public Addresses() {
		super();
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
}
