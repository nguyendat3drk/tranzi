package com.tranzi.entity;

public class Address {
	private int id;
	private String displayName;
	private String number;
	private String state;
	private boolean registered;
	private boolean login;
	public Address() {
		super();
	}
	public Address(int id, String displayName, String number, String state) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.number = number;
		this.state = state;
	}
	
	public boolean getRegistered() {
		return registered;
	}
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}
	public boolean getLogin() {
		return login;
	}
	public void setLogin(boolean login) {
		this.login = login;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
