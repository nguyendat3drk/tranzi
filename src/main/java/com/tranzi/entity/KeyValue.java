package com.tranzi.entity;

public class KeyValue {
	Object value;
	String name;
	Object data;

	public KeyValue(Object value, String name, Object data) {
		super();
		this.value = value;
		this.name = name;
		this.data = data;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public KeyValue() {
		super();
	}

}
