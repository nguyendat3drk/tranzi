package com.tranzi.entity;

public class MenuItem {
	private String title;
	private boolean isSubMenu;
	private String icon;
	private String pathMatch;
	private boolean selected;
	private boolean expanded;
	private int order;
	public MenuItem() {
		super();
	}
	
	public boolean getIsSubMenu() {
		return isSubMenu;
	}

	public void setIsSubMenu(boolean isSubMenu) {
		this.isSubMenu = isSubMenu;
	}

	public String getPathMatch() {
		return pathMatch;
	}

	public void setPathMatch(String pathMatch) {
		this.pathMatch = pathMatch;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
}
