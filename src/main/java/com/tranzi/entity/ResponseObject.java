package com.tranzi.entity;

public class ResponseObject {
	private int resultCode;
	private Object object;
	private String errorMessage;
	
	private Object data;;
	
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	/*public AuthenticationUser getAuthUser() {
		return authUser;
	}
	public void setAuthUser(AuthenticationUser authUser) {
		this.authUser = authUser;
	}*/
	
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
