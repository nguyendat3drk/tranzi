package com.tranzi.entity;

public class KeyValueDTO {
	private int key;
	private int value;
	private String display;
	
	public KeyValueDTO(int key, int value, String display) {
		super();
		this.key = key;
		this.value = value;
		this.display = display;
	}
	
	public KeyValueDTO() {
		super();
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	
}
