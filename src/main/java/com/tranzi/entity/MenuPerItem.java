package com.tranzi.entity;

public class MenuPerItem {
	private int id;
	private String menuCode;
	private String menuName;
	private String path;
	private String parentCode;
	private int insertPri;
	private int updatePri;
	private int deletePri;
	private int queryPri;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	public int getInsertPri() {
		return insertPri;
	}
	public void setInsertPri(int insertPri) {
		this.insertPri = insertPri;
	}
	public int getUpdatePri() {
		return updatePri;
	}
	public void setUpdatePri(int updatePri) {
		this.updatePri = updatePri;
	}
	public int getDeletePri() {
		return deletePri;
	}
	public void setDeletePri(int deletePri) {
		this.deletePri = deletePri;
	}
	public int getQueryPri() {
		return queryPri;
	}
	public void setQueryPri(int queryPri) {
		this.queryPri = queryPri;
	}
}
