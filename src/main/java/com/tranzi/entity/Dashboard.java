package com.tranzi.entity;

public class Dashboard {
	private int agentsLoggedin;
	private int agentsOnCall;
	private int agentsAuxiliary;
	private int agentsLogout;
	private int waitingCall;
	private int agentAvailable;
	private int agentBreakingTime;
	private int queuesInService;

	private int totalCalls;
	private int answeredCalls;
	private int abandonCalls;
	private String maxAgentTalkTime;
	private String AVGAgentTalkTime;
	private String workingTime;
	private String maxWaitTime;
	private String AVGWaitTime;

	public int getQueuesInService() {
		return queuesInService;
	}

	public void setQueuesInService(int queuesInService) {
		this.queuesInService = queuesInService;
	}

	public int getAgentBreakingTime() {
		return agentBreakingTime;
	}

	public void setAgentBreakingTime(int agentBreakingTime) {
		this.agentBreakingTime = agentBreakingTime;
	}

	public int getAgentAvailable() {
		return agentAvailable;
	}

	public void setAgentAvailable(int agentAvailable) {
		this.agentAvailable = agentAvailable;
	}

	public int getAgentsLoggedin() {
		return agentsLoggedin;
	}

	public void setAgentsLoggedin(int agentsLoggedin) {
		this.agentsLoggedin = agentsLoggedin;
	}

	public int getAgentsOnCall() {
		return agentsOnCall;
	}

	public void setAgentsOnCall(int agentsOnCall) {
		this.agentsOnCall = agentsOnCall;
	}

	public int getAgentsAuxiliary() {
		return agentsAuxiliary;
	}

	public void setAgentsAuxiliary(int agentsAuxiliary) {
		this.agentsAuxiliary = agentsAuxiliary;
	}

	public int getAgentsLogout() {
		return agentsLogout;
	}

	public void setAgentsLogout(int agentsLogout) {
		this.agentsLogout = agentsLogout;
	}

	public int getWaitingCall() {
		return waitingCall;
	}

	public void setWaitingCall(int waitingCall) {
		this.waitingCall = waitingCall;
	}

	public int getTotalCalls() {
		return totalCalls;
	}

	public void setTotalCalls(int totalCalls) {
		this.totalCalls = totalCalls;
	}

	public int getAnsweredCalls() {
		return answeredCalls;
	}

	public void setAnsweredCalls(int answeredCalls) {
		this.answeredCalls = answeredCalls;
	}

	public String getMaxAgentTalkTime() {
		return maxAgentTalkTime;
	}

	public void setMaxAgentTalkTime(String maxAgentTalkTime) {
		this.maxAgentTalkTime = maxAgentTalkTime;
	}

	public String getAVGAgentTalkTime() {
		return AVGAgentTalkTime;
	}

	public void setAVGAgentTalkTime(String aVGAgentTalkTime) {
		AVGAgentTalkTime = aVGAgentTalkTime;
	}

	public int getAbandonCalls() {
		return abandonCalls;
	}

	public void setAbandonCalls(int abandonCalls) {
		this.abandonCalls = abandonCalls;
	}

	public String getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(String workingTime) {
		this.workingTime = workingTime;
	}

	public String getMaxWaitTime() {
		return maxWaitTime;
	}

	public void setMaxWaitTime(String maxWaitTime) {
		this.maxWaitTime = maxWaitTime;
	}

	public String getAVGWaitTime() {
		return AVGWaitTime;
	}

	public void setAVGWaitTime(String aVGWaitTime) {
		AVGWaitTime = aVGWaitTime;
	}
}
