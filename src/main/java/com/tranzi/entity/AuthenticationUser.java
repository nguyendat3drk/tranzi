package com.tranzi.entity;

public class AuthenticationUser {
	private int id;
	private String userName;
	private boolean isAuth;
	private String password;
	private boolean isRememberPassword;
	private boolean isExternalAccess;
	private int userRole;
	private String token;
	private int firstLogin;
	private String state;

	private String email;
	private String activationCode;
	private String rememberCode;
	private String firstName;
	private String lastName;

	private String company;

	private String phone;
	private Integer flags;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getFlags() {
		return flags;
	}

	public void setFlags(Integer flags) {
		this.flags = flags;
	}

	public AuthenticationUser() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public int getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(int firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isAuth() {
		return isAuth;
	}

	public void setAuth(boolean isAuth) {
		this.isAuth = isAuth;
	}

	public boolean isRememberPassword() {
		return isRememberPassword;
	}

	public void setRememberPassword(boolean isRememberPassword) {
		this.isRememberPassword = isRememberPassword;
	}

	public boolean isExternalAccess() {
		return isExternalAccess;
	}

	public void setExternalAccess(boolean isExternalAccess) {
		this.isExternalAccess = isExternalAccess;
	}

	public int getUserRole() {
		return userRole;
	}

	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getRememberCode() {
		return rememberCode;
	}

	public void setRememberCode(String rememberCode) {
		this.rememberCode = rememberCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
