package com.tranzi.jwt;

import java.io.Serializable;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import com.tranzi.entity.Token;

public class JWTokenUtility implements Serializable{
	private static final long serialVersionUID = 1L;

	public String buildJWT(String id, String subject, int scope, String issuer,long issueAt, long ttlMillis) {
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();

		JwtClaims claims = new JwtClaims();
		claims.setJwtId(id);
		claims.setSubject(subject); // the subject/principal is whom the token
		claims.setIssuer(issuer);
		claims.setStringClaim("issueAt", issueAt + "");
		claims.setStringClaim("scope", scope + "");
//		claims.setStringClaim("expire", ttlMillis + "");
		claims.setExpirationTimeMinutesInTheFuture(ttlMillis);

		JsonWebSignature jws = new JsonWebSignature();
		jws.setPayload(claims.toJson());
		jws.setKey(rsaJsonWebKey.getPrivateKey());
		jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

		String jwt = null;
		try {
			jwt = jws.getCompactSerialization();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		System.out.println("Claim:\n" + claims);
		System.out.println("JWS:\n" + jws);
		System.out.println("JWT:\n" + jwt);

		return jwt;
	}

	public Token validate(String jwt) {
		String subject = null;
		String jwtId = null;
		String issuer = null;
		String scope = null;
		long expire = 0;
		long issueAt = 0;
		Token token = null;
		RsaJsonWebKey rsaJsonWebKey = RsaKeyProducer.produce();

//		System.out.println("RSA hash code... " + rsaJsonWebKey.hashCode());

		JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireSubject()
				.setVerificationKey(rsaJsonWebKey.getKey())
				.build(); // create the JwtConsumer instance
		
		try {
			// Validate the JWT and process it to the Claims
			JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
			subject = (String) jwtClaims.getClaimValue("sub");
			jwtId = (String) jwtClaims.getClaimValue("jti");
			issuer = (String) jwtClaims.getClaimValue("iss");
			scope = (String) jwtClaims.getClaimValue("scope");
			expire = (long) jwtClaims.getClaimValue("exp");
			issueAt = Long.valueOf((String) jwtClaims.getClaimValue("issueAt"));
			token = new Token(jwtId, issuer, subject,issueAt, expire, scope); 
//			System.out.println("JWT validation succeeded! " + jwtClaims);
		} catch (Exception e) {
			e.printStackTrace(); // on purpose
		}
		return token;
	}
}
